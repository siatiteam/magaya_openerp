/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class InvitationFromPeer implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "InvitationFromPeer", 
/*  24 */       "ns1");
/*     */   
/*     */   protected int localId;
/*     */   
/*     */   protected String localUuid;
/*     */   
/*     */   protected String localEmployee;
/*     */   
/*     */   protected String localBody;
/*     */   
/*     */   protected String localCompany;
/*     */   
/*     */   protected String localCountry;
/*     */   
/*     */   protected String localCity;
/*     */   
/*     */   public int getId() {
/*  41 */     return this.localId;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setId(int param) {
/*  52 */     this.localId = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getUuid() {
/*  71 */     return this.localUuid;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setUuid(String param) {
/*  82 */     this.localUuid = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEmployee() {
/* 101 */     return this.localEmployee;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEmployee(String param) {
/* 112 */     this.localEmployee = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getBody() {
/* 131 */     return this.localBody;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setBody(String param) {
/* 142 */     this.localBody = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCompany() {
/* 161 */     return this.localCompany;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCompany(String param) {
/* 172 */     this.localCompany = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCountry() {
/* 191 */     return this.localCountry;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCountry(String param) {
/* 202 */     this.localCountry = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCity() {
/* 221 */     return this.localCity;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCity(String param) {
/* 232 */     this.localCity = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 252 */     ADBDataSource aDBDataSource = 
/* 253 */       new ADBDataSource(this, MY_QNAME);
/* 254 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 261 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 272 */     String prefix = null;
/* 273 */     String namespace = null;
/*     */ 
/*     */     
/* 276 */     prefix = parentQName.getPrefix();
/* 277 */     namespace = parentQName.getNamespaceURI();
/* 278 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 280 */     if (serializeType) {
/*     */ 
/*     */       
/* 283 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 284 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 285 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 286 */             String.valueOf(namespacePrefix) + ":InvitationFromPeer", 
/* 287 */             xmlWriter);
/*     */       } else {
/* 289 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 290 */             "InvitationFromPeer", 
/* 291 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 297 */     namespace = "";
/* 298 */     writeStartElement(null, namespace, "id", xmlWriter);
/*     */     
/* 300 */     if (this.localId == Integer.MIN_VALUE) {
/*     */       
/* 302 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 305 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localId));
/*     */     } 
/*     */     
/* 308 */     xmlWriter.writeEndElement();
/*     */     
/* 310 */     namespace = "";
/* 311 */     writeStartElement(null, namespace, "uuid", xmlWriter);
/*     */ 
/*     */     
/* 314 */     if (this.localUuid == null) {
/*     */ 
/*     */       
/* 317 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 322 */       xmlWriter.writeCharacters(this.localUuid);
/*     */     } 
/*     */ 
/*     */     
/* 326 */     xmlWriter.writeEndElement();
/*     */     
/* 328 */     namespace = "";
/* 329 */     writeStartElement(null, namespace, "employee", xmlWriter);
/*     */ 
/*     */     
/* 332 */     if (this.localEmployee == null) {
/*     */ 
/*     */       
/* 335 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 340 */       xmlWriter.writeCharacters(this.localEmployee);
/*     */     } 
/*     */ 
/*     */     
/* 344 */     xmlWriter.writeEndElement();
/*     */     
/* 346 */     namespace = "";
/* 347 */     writeStartElement(null, namespace, "body", xmlWriter);
/*     */ 
/*     */     
/* 350 */     if (this.localBody == null) {
/*     */ 
/*     */       
/* 353 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 358 */       xmlWriter.writeCharacters(this.localBody);
/*     */     } 
/*     */ 
/*     */     
/* 362 */     xmlWriter.writeEndElement();
/*     */     
/* 364 */     namespace = "";
/* 365 */     writeStartElement(null, namespace, "company", xmlWriter);
/*     */ 
/*     */     
/* 368 */     if (this.localCompany == null) {
/*     */ 
/*     */       
/* 371 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 376 */       xmlWriter.writeCharacters(this.localCompany);
/*     */     } 
/*     */ 
/*     */     
/* 380 */     xmlWriter.writeEndElement();
/*     */     
/* 382 */     namespace = "";
/* 383 */     writeStartElement(null, namespace, "country", xmlWriter);
/*     */ 
/*     */     
/* 386 */     if (this.localCountry == null) {
/*     */ 
/*     */       
/* 389 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 394 */       xmlWriter.writeCharacters(this.localCountry);
/*     */     } 
/*     */ 
/*     */     
/* 398 */     xmlWriter.writeEndElement();
/*     */     
/* 400 */     namespace = "";
/* 401 */     writeStartElement(null, namespace, "city", xmlWriter);
/*     */ 
/*     */     
/* 404 */     if (this.localCity == null) {
/*     */ 
/*     */       
/* 407 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 412 */       xmlWriter.writeCharacters(this.localCity);
/*     */     } 
/*     */ 
/*     */     
/* 416 */     xmlWriter.writeEndElement();
/*     */     
/* 418 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 424 */     if (namespace.equals("urn:CSSoapService")) {
/* 425 */       return "ns1";
/*     */     }
/* 427 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 435 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 436 */     if (writerPrefix != null) {
/* 437 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 439 */       if (namespace.length() == 0) {
/* 440 */         prefix = "";
/* 441 */       } else if (prefix == null) {
/* 442 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 445 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 446 */       xmlWriter.writeNamespace(prefix, namespace);
/* 447 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 456 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 457 */       xmlWriter.writeNamespace(prefix, namespace);
/* 458 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 460 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 468 */     if (namespace.equals("")) {
/* 469 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 471 */       registerPrefix(xmlWriter, namespace);
/* 472 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 483 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 484 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 485 */     if (attributePrefix == null) {
/* 486 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 489 */     if (attributePrefix.trim().length() > 0) {
/* 490 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 492 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 495 */     if (namespace.equals("")) {
/* 496 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 498 */       registerPrefix(xmlWriter, namespace);
/* 499 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 508 */     String namespaceURI = qname.getNamespaceURI();
/* 509 */     if (namespaceURI != null) {
/* 510 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 511 */       if (prefix == null) {
/* 512 */         prefix = generatePrefix(namespaceURI);
/* 513 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 514 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 517 */       if (prefix.trim().length() > 0) {
/* 518 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 521 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 525 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 532 */     if (qnames != null) {
/*     */ 
/*     */       
/* 535 */       StringBuffer stringToWrite = new StringBuffer();
/* 536 */       String namespaceURI = null;
/* 537 */       String prefix = null;
/*     */       
/* 539 */       for (int i = 0; i < qnames.length; i++) {
/* 540 */         if (i > 0) {
/* 541 */           stringToWrite.append(" ");
/*     */         }
/* 543 */         namespaceURI = qnames[i].getNamespaceURI();
/* 544 */         if (namespaceURI != null) {
/* 545 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 546 */           if (prefix == null || prefix.length() == 0) {
/* 547 */             prefix = generatePrefix(namespaceURI);
/* 548 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 549 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 552 */           if (prefix.trim().length() > 0) {
/* 553 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 555 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 558 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 561 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 571 */     String prefix = xmlWriter.getPrefix(namespace);
/* 572 */     if (prefix == null) {
/* 573 */       prefix = generatePrefix(namespace);
/* 574 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 576 */         String uri = nsContext.getNamespaceURI(prefix);
/* 577 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 580 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 582 */       xmlWriter.writeNamespace(prefix, namespace);
/* 583 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 585 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 599 */     ArrayList<QName> elementList = new ArrayList();
/* 600 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 603 */     elementList.add(new QName("", 
/* 604 */           "id"));
/*     */     
/* 606 */     elementList.add(
/* 607 */         new QName(ConverterUtil.convertToString(this.localId)));
/*     */     
/* 609 */     elementList.add(new QName("", 
/* 610 */           "uuid"));
/*     */     
/* 612 */     elementList.add((this.localUuid == null) ? null : 
/* 613 */         new QName(ConverterUtil.convertToString(this.localUuid)));
/*     */     
/* 615 */     elementList.add(new QName("", 
/* 616 */           "employee"));
/*     */     
/* 618 */     elementList.add((this.localEmployee == null) ? null : 
/* 619 */         new QName(ConverterUtil.convertToString(this.localEmployee)));
/*     */     
/* 621 */     elementList.add(new QName("", 
/* 622 */           "body"));
/*     */     
/* 624 */     elementList.add((this.localBody == null) ? null : 
/* 625 */         new QName(ConverterUtil.convertToString(this.localBody)));
/*     */     
/* 627 */     elementList.add(new QName("", 
/* 628 */           "company"));
/*     */     
/* 630 */     elementList.add((this.localCompany == null) ? null : 
/* 631 */         new QName(ConverterUtil.convertToString(this.localCompany)));
/*     */     
/* 633 */     elementList.add(new QName("", 
/* 634 */           "country"));
/*     */     
/* 636 */     elementList.add((this.localCountry == null) ? null : 
/* 637 */         new QName(ConverterUtil.convertToString(this.localCountry)));
/*     */     
/* 639 */     elementList.add(new QName("", 
/* 640 */           "city"));
/*     */     
/* 642 */     elementList.add((this.localCity == null) ? null : 
/* 643 */         new QName(ConverterUtil.convertToString(this.localCity)));
/*     */ 
/*     */     
/* 646 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static InvitationFromPeer parse(XMLStreamReader reader) throws Exception {
/* 670 */       InvitationFromPeer object = 
/* 671 */         new InvitationFromPeer();
/*     */ 
/*     */       
/* 674 */       String nillableValue = null;
/* 675 */       String prefix = "";
/* 676 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 679 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 680 */           reader.next();
/*     */         }
/*     */         
/* 683 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 684 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 685 */               "type");
/* 686 */           if (fullTypeName != null) {
/* 687 */             String nsPrefix = null;
/* 688 */             if (fullTypeName.indexOf(":") > -1) {
/* 689 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 691 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 693 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 695 */             if (!"InvitationFromPeer".equals(type)) {
/*     */               
/* 697 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 698 */               return (InvitationFromPeer)ExtensionMapper.getTypeObject(
/* 699 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 713 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 718 */         reader.next();
/*     */ 
/*     */         
/* 721 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 723 */         if (reader.isStartElement() && (new QName("", "id")).equals(reader.getName())) {
/*     */           
/* 725 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 726 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 729 */             String content = reader.getElementText();
/*     */             
/* 731 */             object.setId(
/* 732 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 737 */             object.setId(-2147483648);
/*     */             
/* 739 */             reader.getElementText();
/*     */           } 
/*     */           
/* 742 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 748 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 752 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 754 */         if (reader.isStartElement() && (new QName("", "uuid")).equals(reader.getName())) {
/*     */           
/* 756 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 757 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 760 */             String content = reader.getElementText();
/*     */             
/* 762 */             object.setUuid(
/* 763 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 768 */             reader.getElementText();
/*     */           } 
/*     */           
/* 771 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 777 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 781 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 783 */         if (reader.isStartElement() && (new QName("", "employee")).equals(reader.getName())) {
/*     */           
/* 785 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 786 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 789 */             String content = reader.getElementText();
/*     */             
/* 791 */             object.setEmployee(
/* 792 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 797 */             reader.getElementText();
/*     */           } 
/*     */           
/* 800 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 806 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 810 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 812 */         if (reader.isStartElement() && (new QName("", "body")).equals(reader.getName())) {
/*     */           
/* 814 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 815 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 818 */             String content = reader.getElementText();
/*     */             
/* 820 */             object.setBody(
/* 821 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 826 */             reader.getElementText();
/*     */           } 
/*     */           
/* 829 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 835 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 839 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 841 */         if (reader.isStartElement() && (new QName("", "company")).equals(reader.getName())) {
/*     */           
/* 843 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 844 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 847 */             String content = reader.getElementText();
/*     */             
/* 849 */             object.setCompany(
/* 850 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 855 */             reader.getElementText();
/*     */           } 
/*     */           
/* 858 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 864 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 868 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 870 */         if (reader.isStartElement() && (new QName("", "country")).equals(reader.getName())) {
/*     */           
/* 872 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 873 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 876 */             String content = reader.getElementText();
/*     */             
/* 878 */             object.setCountry(
/* 879 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 884 */             reader.getElementText();
/*     */           } 
/*     */           
/* 887 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 893 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 897 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 899 */         if (reader.isStartElement() && (new QName("", "city")).equals(reader.getName())) {
/*     */           
/* 901 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 902 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 905 */             String content = reader.getElementText();
/*     */             
/* 907 */             object.setCity(
/* 908 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 913 */             reader.getElementText();
/*     */           } 
/*     */           
/* 916 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 922 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 925 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 926 */           reader.next();
/*     */         }
/* 928 */         if (reader.isStartElement())
/*     */         {
/* 930 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 935 */       catch (XMLStreamException e) {
/* 936 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 939 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\InvitationFromPeer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */