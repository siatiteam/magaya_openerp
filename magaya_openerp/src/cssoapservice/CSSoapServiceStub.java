/*       */ package cssoapservice;
import com.siatigroup.utils.RuntimeConfig;
/*       */ import java.lang.reflect.Constructor;
/*       */ import java.lang.reflect.InvocationTargetException;
/*       */ import java.lang.reflect.Method;
/*       */ import java.rmi.RemoteException;
/*       */ import java.util.HashMap;
import java.util.Iterator;
/*       */ import java.util.Map;
/*       */ import javax.xml.namespace.QName;
/*       */ import org.apache.axiom.om.OMAbstractFactory;
/*       */ import org.apache.axiom.om.OMElement;
/*       */ import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
/*       */ import org.apache.axiom.om.OMNode;
/*       */ import org.apache.axiom.soap.SOAPEnvelope;
/*       */ import org.apache.axiom.soap.SOAPFactory;
/*       */ import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
/*       */ import org.apache.axis2.client.FaultMapKey;
/*       */ import org.apache.axis2.client.OperationClient;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.context.ConfigurationContext;
/*       */ import org.apache.axis2.context.MessageContext;
/*       */ import org.apache.axis2.databinding.ADBException;
/*       */ import org.apache.axis2.description.AxisOperation;
import org.apache.axis2.description.AxisService;
/*       */ import org.apache.axis2.description.OutInAxisOperation;
/*       */ 
/*       */ public class CSSoapServiceStub extends Stub implements CSSoapService {
/*    24 */   private HashMap faultExceptionNameMap = new HashMap<Object, Object>(); protected AxisOperation[] _operations;
/*    25 */   private HashMap faultExceptionClassNameMap = new HashMap<Object, Object>();
/*    26 */   private HashMap faultMessageMap = new HashMap<Object, Object>();
/*       */   
/*    28 */   private static int counter = 0;
/*       */   private QName[] opNameArray;
/*       */   
/*       */   private static synchronized String getUniqueSuffix() {
/*    32 */     if (counter > 99999) {
/*    33 */       counter = 0;
/*       */     }
/*    35 */     counter++;
/*    36 */     return String.valueOf(Long.toString(System.currentTimeMillis())) + "_" + counter;
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private void populateAxisService() throws AxisFault {
/*    43 */     this._service = new AxisService("CSSoapService" + getUniqueSuffix());
/*    44 */     addAnonymousOperations();
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*    49 */     this._operations = new AxisOperation[63];
/*       */     
/*    51 */     OutInAxisOperation outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*    54 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "existsTransaction"));
/*    55 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*    60 */     this._operations[0] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*    63 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*    66 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTrackingTransaction"));
/*    67 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*    72 */     this._operations[1] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*    75 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*    78 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getFirstTransbyDate"));
/*    79 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*    84 */     this._operations[2] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*    87 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*    90 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setRate"));
/*    91 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*    96 */     this._operations[3] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*    99 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   102 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "queryLog"));
/*   103 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   108 */     this._operations[4] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   111 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   114 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getMagayaDocument"));
/*   115 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   120 */     this._operations[5] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   123 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   126 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getWebDocument"));
/*   127 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   132 */     this._operations[6] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   135 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   138 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getFirstTransbyDateJS"));
/*   139 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   144 */     this._operations[7] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   147 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   150 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getPackageTypes"));
/*   151 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   156 */     this._operations[8] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   159 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   162 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getSecureTrackingTransaction"));
/*   163 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   168 */     this._operations[9] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   171 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   174 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getChargeDefinitions"));
/*   175 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   180 */     this._operations[10] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   183 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   186 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getAttachment"));
/*   187 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   192 */     this._operations[11] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   195 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   198 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "invitationFromPeer"));
/*   199 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   204 */     this._operations[12] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   207 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   210 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getInventoryItemsByItemDefinition"));
/*   211 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   216 */     this._operations[13] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   219 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   222 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setTransactionEvents"));
/*   223 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   228 */     this._operations[14] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   231 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   234 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getItemFromVIN"));
/*   235 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   240 */     this._operations[15] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   243 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   246 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "bookingRequest"));
/*   247 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   252 */     this._operations[16] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   255 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   258 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getWorkingPorts"));
/*   259 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   264 */     this._operations[17] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   267 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   270 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "endSession"));
/*   271 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   276 */     this._operations[18] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   279 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   282 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "updateOrder"));
/*   283 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   288 */     this._operations[19] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   291 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   294 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "startSession"));
/*   295 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   300 */     this._operations[20] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   303 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   306 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setShipmentStatus"));
/*   307 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   312 */     this._operations[21] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   315 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   318 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getNextTransbyDate"));
/*   319 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   324 */     this._operations[22] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   327 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   330 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getActiveCurrencies"));
/*   331 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   336 */     this._operations[23] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   339 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   342 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "startTracking"));
/*   343 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   348 */     this._operations[24] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   351 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   354 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getStandardRates"));
/*   355 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   360 */     this._operations[25] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   363 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   366 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTransactionStatus"));
/*   367 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   372 */     this._operations[26] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   375 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   378 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "submitSalesOrder"));
/*   379 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   384 */     this._operations[27] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   387 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   390 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "renameTransaction"));
/*   391 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   396 */     this._operations[28] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   399 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   402 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "queryLogJS"));
/*   403 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   408 */     this._operations[29] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   411 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   414 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getEntitiesOfType"));
/*   415 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   420 */     this._operations[30] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   423 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   426 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getItemDefinitionsByCustomer"));
/*   427 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   432 */     this._operations[31] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   435 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   438 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "startTracking2"));
/*   439 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   444 */     this._operations[32] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   447 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   450 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "deleteTransaction"));
/*   451 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   456 */     this._operations[33] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   459 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   462 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTransaction"));
/*   463 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   468 */     this._operations[34] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   471 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   474 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getEntityTransactions"));
/*   475 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   480 */     this._operations[35] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   483 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   486 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "submitShipment"));
/*   487 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   492 */     this._operations[36] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   495 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   498 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getEntityContacts"));
/*   499 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   504 */     this._operations[37] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   507 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   510 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "cancelBooking"));
/*   511 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   516 */     this._operations[38] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   519 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   522 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setTransaction"));
/*   523 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   528 */     this._operations[39] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   531 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   534 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setEntity"));
/*   535 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   540 */     this._operations[40] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   543 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   546 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getClientChargeDefinitions"));
/*   547 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   552 */     this._operations[41] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   555 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   558 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getAccountingTransactions"));
/*   559 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   564 */     this._operations[42] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   567 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   570 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTripSchedule"));
/*   571 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   576 */     this._operations[43] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   579 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   582 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getEventDefinitions"));
/*   583 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   588 */     this._operations[44] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   591 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   594 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getCarrierRates"));
/*   595 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   600 */     this._operations[45] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   603 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   606 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getPODData"));
/*   607 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   612 */     this._operations[46] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   615 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   618 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getCustomFieldDefinitions"));
/*   619 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   624 */     this._operations[47] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   627 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   630 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "submitCargoRelease"));
/*   631 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   636 */     this._operations[48] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   639 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   642 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getClientRates"));
/*   643 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   648 */     this._operations[49] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   651 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   654 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTransRangeByDateJS"));
/*   655 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   660 */     this._operations[50] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   663 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   666 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getAccountDefinitions"));
/*   667 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   672 */     this._operations[51] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   675 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   678 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setApprovalStatus"));
/*   679 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   684 */     this._operations[52] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   687 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   690 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getRelatedTransactions"));
/*   691 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   696 */     this._operations[53] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   699 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   702 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setCustomFieldValue"));
/*   703 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   708 */     this._operations[54] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   711 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   714 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getEntities"));
/*   715 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   720 */     this._operations[55] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   723 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   726 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "answerInvitation"));
/*   727 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   732 */     this._operations[56] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   735 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   738 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "testConnection"));
/*   739 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   744 */     this._operations[57] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   747 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   750 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTransRangeByDate"));
/*   751 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   756 */     this._operations[58] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   759 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   762 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getTransactionsByBillingClient"));
/*   763 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   768 */     this._operations[59] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   771 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   774 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "setTrackingUser"));
/*   775 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   780 */     this._operations[60] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   783 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   786 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "answerInvitation2"));
/*   787 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   792 */     this._operations[61] = (AxisOperation)outInAxisOperation;
/*       */ 
/*       */     
/*   795 */     outInAxisOperation = new OutInAxisOperation();
/*       */ 
/*       */     
/*   798 */     outInAxisOperation.setName(new QName("urn:CSSoapService", "getDocument"));
/*   799 */     this._service.addOperation((AxisOperation)outInAxisOperation);
/*       */ 
/*       */ 
/*       */ 
/*       */     
/*   804 */     this._operations[62] = (AxisOperation)outInAxisOperation;
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private void populateFaults() {}
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public CSSoapServiceStub(ConfigurationContext configurationContext, String targetEndpoint) throws AxisFault {
/*   823 */     this(configurationContext, targetEndpoint, false);
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public CSSoapServiceStub(ConfigurationContext configurationContext) throws AxisFault {
/*   853 */     this(configurationContext, "http://" + RuntimeConfig.getInstance().getMagayaHostOrIp() + ":" + RuntimeConfig.getInstance().getMagayaPort() + "/Invoke?Handler=CSSoapService");
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public CSSoapServiceStub() throws AxisFault {
/*   863 */     this("http://" + RuntimeConfig.getInstance().getMagayaHostOrIp() + ":" + RuntimeConfig.getInstance().getMagayaPort() + "/Invoke?Handler=CSSoapService");
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public CSSoapServiceStub(String targetEndpoint) throws AxisFault {
/*   871 */     this((ConfigurationContext)null, targetEndpoint);
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public ExistsTransactionResponse existsTransaction(ExistsTransaction existsTransaction0) throws RemoteException {
/*   895 */     MessageContext _messageContext = null;
/*       */     try {
/*   897 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[0].getName());
/*   898 */       _operationClient.getOptions().setAction("#ExistsTransaction");
/*   899 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*   903 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*   907 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*   912 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*   915 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*   916 */           existsTransaction0, 
/*   917 */           optimizeContent(new QName("urn:CSSoapService", 
/*   918 */               "existsTransaction")), new QName("urn:CSSoapService", 
/*   919 */             "existsTransaction"));
/*       */ 
/*       */       
/*   922 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*   924 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*   927 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*   930 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*   933 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*   934 */           "In");
/*   935 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*   938 */       Object object = fromOM(
/*   939 */           _returnEnv.getBody().getFirstElement(), 
/*   940 */           ExistsTransactionResponse.class, 
/*   941 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*   944 */       return (ExistsTransactionResponse)object;
/*       */     }
/*   946 */     catch (AxisFault f) {
/*       */       
/*   948 */       OMElement faultElt = f.getDetail();
/*   949 */       if (faultElt != null) {
/*   950 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "ExistsTransaction"))) {
/*       */           
/*       */           try {
/*   953 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "ExistsTransaction"));
/*   954 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*   955 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*   956 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*   958 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "ExistsTransaction"));
/*   959 */             Class<?> messageClass = Class.forName(messageClassName);
/*   960 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*   961 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*   962 */                 new Class[] { messageClass });
/*   963 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*   966 */             throw new RemoteException(ex.getMessage(), ex);
/*   967 */           } catch (ClassCastException e) {
/*       */             
/*   969 */             throw f;
/*   970 */           } catch (ClassNotFoundException e) {
/*       */             
/*   972 */             throw f;
/*   973 */           } catch (NoSuchMethodException e) {
/*       */             
/*   975 */             throw f;
/*   976 */           } catch (InvocationTargetException e) {
/*       */             
/*   978 */             throw f;
/*   979 */           } catch (IllegalAccessException e) {
/*       */             
/*   981 */             throw f;
/*   982 */           } catch (InstantiationException e) {
/*       */             
/*   984 */             throw f;
/*       */           } 
/*       */         }
/*   987 */         throw f;
/*       */       } 
/*       */       
/*   990 */       throw f;
/*       */     } finally {
/*       */       
/*   993 */       if (_messageContext.getTransportOut() != null) {
/*   994 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTrackingTransactionResponse getTrackingTransaction(GetTrackingTransaction getTrackingTransaction2) throws RemoteException {
/*  1017 */     MessageContext _messageContext = null;
/*       */     try {
/*  1019 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[1].getName());
/*  1020 */       _operationClient.getOptions().setAction("#GetTrackingTransaction");
/*  1021 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1025 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1029 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1034 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1037 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1038 */           getTrackingTransaction2, 
/*  1039 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1040 */               "getTrackingTransaction")), new QName("urn:CSSoapService", 
/*  1041 */             "getTrackingTransaction"));
/*       */ 
/*       */       
/*  1044 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1046 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1049 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1052 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1055 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1056 */           "In");
/*  1057 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1060 */       Object object = fromOM(
/*  1061 */           _returnEnv.getBody().getFirstElement(), 
/*  1062 */           GetTrackingTransactionResponse.class, 
/*  1063 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1066 */       return (GetTrackingTransactionResponse)object;
/*       */     }
/*  1068 */     catch (AxisFault f) {
/*       */       
/*  1070 */       OMElement faultElt = f.getDetail();
/*  1071 */       if (faultElt != null) {
/*  1072 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTrackingTransaction"))) {
/*       */           
/*       */           try {
/*  1075 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTrackingTransaction"));
/*  1076 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1077 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1078 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1080 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTrackingTransaction"));
/*  1081 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1082 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1083 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1084 */                 new Class[] { messageClass });
/*  1085 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1088 */             throw new RemoteException(ex.getMessage(), ex);
/*  1089 */           } catch (ClassCastException e) {
/*       */             
/*  1091 */             throw f;
/*  1092 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1094 */             throw f;
/*  1095 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1097 */             throw f;
/*  1098 */           } catch (InvocationTargetException e) {
/*       */             
/*  1100 */             throw f;
/*  1101 */           } catch (IllegalAccessException e) {
/*       */             
/*  1103 */             throw f;
/*  1104 */           } catch (InstantiationException e) {
/*       */             
/*  1106 */             throw f;
/*       */           } 
/*       */         }
/*  1109 */         throw f;
/*       */       } 
/*       */       
/*  1112 */       throw f;
/*       */     } finally {
/*       */       
/*  1115 */       if (_messageContext.getTransportOut() != null) {
/*  1116 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetFirstTransbyDateResponse getFirstTransbyDate(GetFirstTransbyDate getFirstTransbyDate4) throws RemoteException {
/*  1139 */     MessageContext _messageContext = null;
/*       */     try {
/*  1141 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[2].getName());
/*  1142 */       _operationClient.getOptions().setAction("#GetFirstTransbyDate");
/*  1143 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1147 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1151 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1156 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1159 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1160 */           getFirstTransbyDate4, 
/*  1161 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1162 */               "getFirstTransbyDate")), new QName("urn:CSSoapService", 
/*  1163 */             "getFirstTransbyDate"));
/*       */ 
/*       */       
/*  1166 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1168 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1171 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1174 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1177 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1178 */           "In");
/*  1179 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1182 */       Object object = fromOM(
/*  1183 */           _returnEnv.getBody().getFirstElement(), 
/*  1184 */           GetFirstTransbyDateResponse.class, 
/*  1185 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1188 */       return (GetFirstTransbyDateResponse)object;
/*       */     }
/*  1190 */     catch (AxisFault f) {
/*       */       
/*  1192 */       OMElement faultElt = f.getDetail();
/*  1193 */       if (faultElt != null) {
/*  1194 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetFirstTransbyDate"))) {
/*       */           
/*       */           try {
/*  1197 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetFirstTransbyDate"));
/*  1198 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1199 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1200 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1202 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetFirstTransbyDate"));
/*  1203 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1204 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1205 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1206 */                 new Class[] { messageClass });
/*  1207 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1210 */             throw new RemoteException(ex.getMessage(), ex);
/*  1211 */           } catch (ClassCastException e) {
/*       */             
/*  1213 */             throw f;
/*  1214 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1216 */             throw f;
/*  1217 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1219 */             throw f;
/*  1220 */           } catch (InvocationTargetException e) {
/*       */             
/*  1222 */             throw f;
/*  1223 */           } catch (IllegalAccessException e) {
/*       */             
/*  1225 */             throw f;
/*  1226 */           } catch (InstantiationException e) {
/*       */             
/*  1228 */             throw f;
/*       */           } 
/*       */         }
/*  1231 */         throw f;
/*       */       } 
/*       */       
/*  1234 */       throw f;
/*       */     } finally {
/*       */       
/*  1237 */       if (_messageContext.getTransportOut() != null) {
/*  1238 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetRateResponse setRate(SetRate setRate6) throws RemoteException {
/*  1261 */     MessageContext _messageContext = null;
/*       */     try {
/*  1263 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[3].getName());
/*  1264 */       _operationClient.getOptions().setAction("#SetRate");
/*  1265 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1269 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1273 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1278 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1281 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1282 */           setRate6, 
/*  1283 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1284 */               "setRate")), new QName("urn:CSSoapService", 
/*  1285 */             "setRate"));
/*       */ 
/*       */       
/*  1288 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1290 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1293 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1296 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1299 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1300 */           "In");
/*  1301 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1304 */       Object object = fromOM(
/*  1305 */           _returnEnv.getBody().getFirstElement(), 
/*  1306 */           SetRateResponse.class, 
/*  1307 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1310 */       return (SetRateResponse)object;
/*       */     }
/*  1312 */     catch (AxisFault f) {
/*       */       
/*  1314 */       OMElement faultElt = f.getDetail();
/*  1315 */       if (faultElt != null) {
/*  1316 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetRate"))) {
/*       */           
/*       */           try {
/*  1319 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetRate"));
/*  1320 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1321 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1322 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1324 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetRate"));
/*  1325 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1326 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1327 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1328 */                 new Class[] { messageClass });
/*  1329 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1332 */             throw new RemoteException(ex.getMessage(), ex);
/*  1333 */           } catch (ClassCastException e) {
/*       */             
/*  1335 */             throw f;
/*  1336 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1338 */             throw f;
/*  1339 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1341 */             throw f;
/*  1342 */           } catch (InvocationTargetException e) {
/*       */             
/*  1344 */             throw f;
/*  1345 */           } catch (IllegalAccessException e) {
/*       */             
/*  1347 */             throw f;
/*  1348 */           } catch (InstantiationException e) {
/*       */             
/*  1350 */             throw f;
/*       */           } 
/*       */         }
/*  1353 */         throw f;
/*       */       } 
/*       */       
/*  1356 */       throw f;
/*       */     } finally {
/*       */       
/*  1359 */       if (_messageContext.getTransportOut() != null) {
/*  1360 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public QueryLogResponse queryLog(QueryLog queryLog8) throws RemoteException {
/*  1383 */     MessageContext _messageContext = null;
/*       */     try {
/*  1385 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[4].getName());
/*  1386 */       _operationClient.getOptions().setAction("#QueryLog");
/*  1387 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1391 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1395 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1400 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1403 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1404 */           queryLog8, 
/*  1405 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1406 */               "queryLog")), new QName("urn:CSSoapService", 
/*  1407 */             "queryLog"));
/*       */ 
/*       */       
/*  1410 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1412 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1415 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1418 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1421 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1422 */           "In");
/*  1423 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1426 */       Object object = fromOM(
/*  1427 */           _returnEnv.getBody().getFirstElement(), 
/*  1428 */           QueryLogResponse.class, 
/*  1429 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1432 */       return (QueryLogResponse)object;
/*       */     }
/*  1434 */     catch (AxisFault f) {
/*       */       
/*  1436 */       OMElement faultElt = f.getDetail();
/*  1437 */       if (faultElt != null) {
/*  1438 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "QueryLog"))) {
/*       */           
/*       */           try {
/*  1441 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "QueryLog"));
/*  1442 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1443 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1444 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1446 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "QueryLog"));
/*  1447 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1448 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1449 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1450 */                 new Class[] { messageClass });
/*  1451 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1454 */             throw new RemoteException(ex.getMessage(), ex);
/*  1455 */           } catch (ClassCastException e) {
/*       */             
/*  1457 */             throw f;
/*  1458 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1460 */             throw f;
/*  1461 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1463 */             throw f;
/*  1464 */           } catch (InvocationTargetException e) {
/*       */             
/*  1466 */             throw f;
/*  1467 */           } catch (IllegalAccessException e) {
/*       */             
/*  1469 */             throw f;
/*  1470 */           } catch (InstantiationException e) {
/*       */             
/*  1472 */             throw f;
/*       */           } 
/*       */         }
/*  1475 */         throw f;
/*       */       } 
/*       */       
/*  1478 */       throw f;
/*       */     } finally {
/*       */       
/*  1481 */       if (_messageContext.getTransportOut() != null) {
/*  1482 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetMagayaDocumentResponse getMagayaDocument(GetMagayaDocument getMagayaDocument10) throws RemoteException {
/*  1505 */     MessageContext _messageContext = null;
/*       */     try {
/*  1507 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[5].getName());
/*  1508 */       _operationClient.getOptions().setAction("#GetMagayaDocument");
/*  1509 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1513 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1517 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1522 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1525 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1526 */           getMagayaDocument10, 
/*  1527 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1528 */               "getMagayaDocument")), new QName("urn:CSSoapService", 
/*  1529 */             "getMagayaDocument"));
/*       */ 
/*       */       
/*  1532 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1534 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1537 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1540 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1543 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1544 */           "In");
/*  1545 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1548 */       Object object = fromOM(
/*  1549 */           _returnEnv.getBody().getFirstElement(), 
/*  1550 */           GetMagayaDocumentResponse.class, 
/*  1551 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1554 */       return (GetMagayaDocumentResponse)object;
/*       */     }
/*  1556 */     catch (AxisFault f) {
/*       */       
/*  1558 */       OMElement faultElt = f.getDetail();
/*  1559 */       if (faultElt != null) {
/*  1560 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetMagayaDocument"))) {
/*       */           
/*       */           try {
/*  1563 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetMagayaDocument"));
/*  1564 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1565 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1566 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1568 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetMagayaDocument"));
/*  1569 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1570 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1571 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1572 */                 new Class[] { messageClass });
/*  1573 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1576 */             throw new RemoteException(ex.getMessage(), ex);
/*  1577 */           } catch (ClassCastException e) {
/*       */             
/*  1579 */             throw f;
/*  1580 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1582 */             throw f;
/*  1583 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1585 */             throw f;
/*  1586 */           } catch (InvocationTargetException e) {
/*       */             
/*  1588 */             throw f;
/*  1589 */           } catch (IllegalAccessException e) {
/*       */             
/*  1591 */             throw f;
/*  1592 */           } catch (InstantiationException e) {
/*       */             
/*  1594 */             throw f;
/*       */           } 
/*       */         }
/*  1597 */         throw f;
/*       */       } 
/*       */       
/*  1600 */       throw f;
/*       */     } finally {
/*       */       
/*  1603 */       if (_messageContext.getTransportOut() != null) {
/*  1604 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetWebDocumentResponse getWebDocument(GetWebDocument getWebDocument12) throws RemoteException {
/*  1627 */     MessageContext _messageContext = null;
/*       */     try {
/*  1629 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[6].getName());
/*  1630 */       _operationClient.getOptions().setAction("#GetWebDocument");
/*  1631 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1635 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1639 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1644 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1647 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1648 */           getWebDocument12, 
/*  1649 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1650 */               "getWebDocument")), new QName("urn:CSSoapService", 
/*  1651 */             "getWebDocument"));
/*       */ 
/*       */       
/*  1654 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1656 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1659 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1662 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1665 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1666 */           "In");
/*  1667 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1670 */       Object object = fromOM(
/*  1671 */           _returnEnv.getBody().getFirstElement(), 
/*  1672 */           GetWebDocumentResponse.class, 
/*  1673 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1676 */       return (GetWebDocumentResponse)object;
/*       */     }
/*  1678 */     catch (AxisFault f) {
/*       */       
/*  1680 */       OMElement faultElt = f.getDetail();
/*  1681 */       if (faultElt != null) {
/*  1682 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetWebDocument"))) {
/*       */           
/*       */           try {
/*  1685 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetWebDocument"));
/*  1686 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1687 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1688 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1690 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetWebDocument"));
/*  1691 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1692 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1693 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1694 */                 new Class[] { messageClass });
/*  1695 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1698 */             throw new RemoteException(ex.getMessage(), ex);
/*  1699 */           } catch (ClassCastException e) {
/*       */             
/*  1701 */             throw f;
/*  1702 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1704 */             throw f;
/*  1705 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1707 */             throw f;
/*  1708 */           } catch (InvocationTargetException e) {
/*       */             
/*  1710 */             throw f;
/*  1711 */           } catch (IllegalAccessException e) {
/*       */             
/*  1713 */             throw f;
/*  1714 */           } catch (InstantiationException e) {
/*       */             
/*  1716 */             throw f;
/*       */           } 
/*       */         }
/*  1719 */         throw f;
/*       */       } 
/*       */       
/*  1722 */       throw f;
/*       */     } finally {
/*       */       
/*  1725 */       if (_messageContext.getTransportOut() != null) {
/*  1726 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetFirstTransbyDateJSResponse getFirstTransbyDateJS(GetFirstTransbyDateJS getFirstTransbyDateJS14) throws RemoteException {
/*  1749 */     MessageContext _messageContext = null;
/*       */     try {
/*  1751 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[7].getName());
/*  1752 */       _operationClient.getOptions().setAction("#GetFirstTransbyDateJS");
/*  1753 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1757 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1761 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1766 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1769 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1770 */           getFirstTransbyDateJS14, 
/*  1771 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1772 */               "getFirstTransbyDateJS")), new QName("urn:CSSoapService", 
/*  1773 */             "getFirstTransbyDateJS"));
/*       */ 
/*       */       
/*  1776 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1778 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1781 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1784 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1787 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1788 */           "In");
/*  1789 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1792 */       Object object = fromOM(
/*  1793 */           _returnEnv.getBody().getFirstElement(), 
/*  1794 */           GetFirstTransbyDateJSResponse.class, 
/*  1795 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1798 */       return (GetFirstTransbyDateJSResponse)object;
/*       */     }
/*  1800 */     catch (AxisFault f) {
/*       */       
/*  1802 */       OMElement faultElt = f.getDetail();
/*  1803 */       if (faultElt != null) {
/*  1804 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetFirstTransbyDateJS"))) {
/*       */           
/*       */           try {
/*  1807 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetFirstTransbyDateJS"));
/*  1808 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1809 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1810 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1812 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetFirstTransbyDateJS"));
/*  1813 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1814 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1815 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1816 */                 new Class[] { messageClass });
/*  1817 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1820 */             throw new RemoteException(ex.getMessage(), ex);
/*  1821 */           } catch (ClassCastException e) {
/*       */             
/*  1823 */             throw f;
/*  1824 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1826 */             throw f;
/*  1827 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1829 */             throw f;
/*  1830 */           } catch (InvocationTargetException e) {
/*       */             
/*  1832 */             throw f;
/*  1833 */           } catch (IllegalAccessException e) {
/*       */             
/*  1835 */             throw f;
/*  1836 */           } catch (InstantiationException e) {
/*       */             
/*  1838 */             throw f;
/*       */           } 
/*       */         }
/*  1841 */         throw f;
/*       */       } 
/*       */       
/*  1844 */       throw f;
/*       */     } finally {
/*       */       
/*  1847 */       if (_messageContext.getTransportOut() != null) {
/*  1848 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetPackageTypesResponse getPackageTypes(GetPackageTypes getPackageTypes16) throws RemoteException {
/*  1871 */     MessageContext _messageContext = null;
/*       */     try {
/*  1873 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[8].getName());
/*  1874 */       _operationClient.getOptions().setAction("#GetPackageTypes");
/*  1875 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  1879 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  1883 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  1888 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  1891 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  1892 */           getPackageTypes16, 
/*  1893 */           optimizeContent(new QName("urn:CSSoapService", 
/*  1894 */               "getPackageTypes")), new QName("urn:CSSoapService", 
/*  1895 */             "getPackageTypes"));
/*       */ 
/*       */       
/*  1898 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  1900 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  1903 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  1906 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  1909 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  1910 */           "In");
/*  1911 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  1914 */       Object object = fromOM(
/*  1915 */           _returnEnv.getBody().getFirstElement(), 
/*  1916 */           GetPackageTypesResponse.class, 
/*  1917 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  1920 */       return (GetPackageTypesResponse)object;
/*       */     }
/*  1922 */     catch (AxisFault f) {
/*       */       
/*  1924 */       OMElement faultElt = f.getDetail();
/*  1925 */       if (faultElt != null) {
/*  1926 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetPackageTypes"))) {
/*       */           
/*       */           try {
/*  1929 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetPackageTypes"));
/*  1930 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  1931 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  1932 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  1934 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetPackageTypes"));
/*  1935 */             Class<?> messageClass = Class.forName(messageClassName);
/*  1936 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  1937 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  1938 */                 new Class[] { messageClass });
/*  1939 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  1942 */             throw new RemoteException(ex.getMessage(), ex);
/*  1943 */           } catch (ClassCastException e) {
/*       */             
/*  1945 */             throw f;
/*  1946 */           } catch (ClassNotFoundException e) {
/*       */             
/*  1948 */             throw f;
/*  1949 */           } catch (NoSuchMethodException e) {
/*       */             
/*  1951 */             throw f;
/*  1952 */           } catch (InvocationTargetException e) {
/*       */             
/*  1954 */             throw f;
/*  1955 */           } catch (IllegalAccessException e) {
/*       */             
/*  1957 */             throw f;
/*  1958 */           } catch (InstantiationException e) {
/*       */             
/*  1960 */             throw f;
/*       */           } 
/*       */         }
/*  1963 */         throw f;
/*       */       } 
/*       */       
/*  1966 */       throw f;
/*       */     } finally {
/*       */       
/*  1969 */       if (_messageContext.getTransportOut() != null) {
/*  1970 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetSecureTrackingTransactionResponse getSecureTrackingTransaction(GetSecureTrackingTransaction getSecureTrackingTransaction18) throws RemoteException {
/*  1993 */     MessageContext _messageContext = null;
/*       */     try {
/*  1995 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[9].getName());
/*  1996 */       _operationClient.getOptions().setAction("#GetSecureTrackingTransaction");
/*  1997 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2001 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2005 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2010 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2013 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2014 */           getSecureTrackingTransaction18, 
/*  2015 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2016 */               "getSecureTrackingTransaction")), new QName("urn:CSSoapService", 
/*  2017 */             "getSecureTrackingTransaction"));
/*       */ 
/*       */       
/*  2020 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2022 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2025 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2028 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2031 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2032 */           "In");
/*  2033 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2036 */       Object object = fromOM(
/*  2037 */           _returnEnv.getBody().getFirstElement(), 
/*  2038 */           GetSecureTrackingTransactionResponse.class, 
/*  2039 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2042 */       return (GetSecureTrackingTransactionResponse)object;
/*       */     }
/*  2044 */     catch (AxisFault f) {
/*       */       
/*  2046 */       OMElement faultElt = f.getDetail();
/*  2047 */       if (faultElt != null) {
/*  2048 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetSecureTrackingTransaction"))) {
/*       */           
/*       */           try {
/*  2051 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetSecureTrackingTransaction"));
/*  2052 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2053 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2054 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2056 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetSecureTrackingTransaction"));
/*  2057 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2058 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2059 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2060 */                 new Class[] { messageClass });
/*  2061 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2064 */             throw new RemoteException(ex.getMessage(), ex);
/*  2065 */           } catch (ClassCastException e) {
/*       */             
/*  2067 */             throw f;
/*  2068 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2070 */             throw f;
/*  2071 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2073 */             throw f;
/*  2074 */           } catch (InvocationTargetException e) {
/*       */             
/*  2076 */             throw f;
/*  2077 */           } catch (IllegalAccessException e) {
/*       */             
/*  2079 */             throw f;
/*  2080 */           } catch (InstantiationException e) {
/*       */             
/*  2082 */             throw f;
/*       */           } 
/*       */         }
/*  2085 */         throw f;
/*       */       } 
/*       */       
/*  2088 */       throw f;
/*       */     } finally {
/*       */       
/*  2091 */       if (_messageContext.getTransportOut() != null) {
/*  2092 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetChargeDefinitionsResponse getChargeDefinitions(GetChargeDefinitions getChargeDefinitions20) throws RemoteException {
/*  2115 */     MessageContext _messageContext = null;
/*       */     try {
/*  2117 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[10].getName());
/*  2118 */       _operationClient.getOptions().setAction("#GetChargeDefinitions");
/*  2119 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2123 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2127 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2132 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2135 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2136 */           getChargeDefinitions20, 
/*  2137 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2138 */               "getChargeDefinitions")), new QName("urn:CSSoapService", 
/*  2139 */             "getChargeDefinitions"));
/*       */ 
/*       */       
/*  2142 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2144 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2147 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2150 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2153 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2154 */           "In");
/*  2155 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2158 */       Object object = fromOM(
/*  2159 */           _returnEnv.getBody().getFirstElement(), 
/*  2160 */           GetChargeDefinitionsResponse.class, 
/*  2161 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2164 */       return (GetChargeDefinitionsResponse)object;
/*       */     }
/*  2166 */     catch (AxisFault f) {
/*       */       
/*  2168 */       OMElement faultElt = f.getDetail();
/*  2169 */       if (faultElt != null) {
/*  2170 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetChargeDefinitions"))) {
/*       */           
/*       */           try {
/*  2173 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetChargeDefinitions"));
/*  2174 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2175 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2176 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2178 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetChargeDefinitions"));
/*  2179 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2180 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2181 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2182 */                 new Class[] { messageClass });
/*  2183 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2186 */             throw new RemoteException(ex.getMessage(), ex);
/*  2187 */           } catch (ClassCastException e) {
/*       */             
/*  2189 */             throw f;
/*  2190 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2192 */             throw f;
/*  2193 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2195 */             throw f;
/*  2196 */           } catch (InvocationTargetException e) {
/*       */             
/*  2198 */             throw f;
/*  2199 */           } catch (IllegalAccessException e) {
/*       */             
/*  2201 */             throw f;
/*  2202 */           } catch (InstantiationException e) {
/*       */             
/*  2204 */             throw f;
/*       */           } 
/*       */         }
/*  2207 */         throw f;
/*       */       } 
/*       */       
/*  2210 */       throw f;
/*       */     } finally {
/*       */       
/*  2213 */       if (_messageContext.getTransportOut() != null) {
/*  2214 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetAttachmentResponse getAttachment(GetAttachment getAttachment22) throws RemoteException {
/*  2237 */     MessageContext _messageContext = null;
/*       */     try {
/*  2239 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[11].getName());
/*  2240 */       _operationClient.getOptions().setAction("#GetAttachment");
/*  2241 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2245 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2249 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2254 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2257 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2258 */           getAttachment22, 
/*  2259 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2260 */               "getAttachment")), new QName("urn:CSSoapService", 
/*  2261 */             "getAttachment"));
/*       */ 
/*       */       
/*  2264 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2266 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2269 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2272 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2275 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2276 */           "In");
/*  2277 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2280 */       Object object = fromOM(
/*  2281 */           _returnEnv.getBody().getFirstElement(), 
/*  2282 */           GetAttachmentResponse.class, 
/*  2283 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2286 */       return (GetAttachmentResponse)object;
/*       */     }
/*  2288 */     catch (AxisFault f) {
/*       */       
/*  2290 */       OMElement faultElt = f.getDetail();
/*  2291 */       if (faultElt != null) {
/*  2292 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetAttachment"))) {
/*       */           
/*       */           try {
/*  2295 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetAttachment"));
/*  2296 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2297 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2298 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2300 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetAttachment"));
/*  2301 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2302 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2303 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2304 */                 new Class[] { messageClass });
/*  2305 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2308 */             throw new RemoteException(ex.getMessage(), ex);
/*  2309 */           } catch (ClassCastException e) {
/*       */             
/*  2311 */             throw f;
/*  2312 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2314 */             throw f;
/*  2315 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2317 */             throw f;
/*  2318 */           } catch (InvocationTargetException e) {
/*       */             
/*  2320 */             throw f;
/*  2321 */           } catch (IllegalAccessException e) {
/*       */             
/*  2323 */             throw f;
/*  2324 */           } catch (InstantiationException e) {
/*       */             
/*  2326 */             throw f;
/*       */           } 
/*       */         }
/*  2329 */         throw f;
/*       */       } 
/*       */       
/*  2332 */       throw f;
/*       */     } finally {
/*       */       
/*  2335 */       if (_messageContext.getTransportOut() != null) {
/*  2336 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public InvitationFromPeerResponse invitationFromPeer(InvitationFromPeer invitationFromPeer24) throws RemoteException {
/*  2359 */     MessageContext _messageContext = null;
/*       */     try {
/*  2361 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[12].getName());
/*  2362 */       _operationClient.getOptions().setAction("#InvitationFromPeer");
/*  2363 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2367 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2371 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2376 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2379 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2380 */           invitationFromPeer24, 
/*  2381 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2382 */               "invitationFromPeer")), new QName("urn:CSSoapService", 
/*  2383 */             "invitationFromPeer"));
/*       */ 
/*       */       
/*  2386 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2388 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2391 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2394 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2397 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2398 */           "In");
/*  2399 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2402 */       Object object = fromOM(
/*  2403 */           _returnEnv.getBody().getFirstElement(), 
/*  2404 */           InvitationFromPeerResponse.class, 
/*  2405 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2408 */       return (InvitationFromPeerResponse)object;
/*       */     }
/*  2410 */     catch (AxisFault f) {
/*       */       
/*  2412 */       OMElement faultElt = f.getDetail();
/*  2413 */       if (faultElt != null) {
/*  2414 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "InvitationFromPeer"))) {
/*       */           
/*       */           try {
/*  2417 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "InvitationFromPeer"));
/*  2418 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2419 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2420 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2422 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "InvitationFromPeer"));
/*  2423 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2424 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2425 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2426 */                 new Class[] { messageClass });
/*  2427 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2430 */             throw new RemoteException(ex.getMessage(), ex);
/*  2431 */           } catch (ClassCastException e) {
/*       */             
/*  2433 */             throw f;
/*  2434 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2436 */             throw f;
/*  2437 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2439 */             throw f;
/*  2440 */           } catch (InvocationTargetException e) {
/*       */             
/*  2442 */             throw f;
/*  2443 */           } catch (IllegalAccessException e) {
/*       */             
/*  2445 */             throw f;
/*  2446 */           } catch (InstantiationException e) {
/*       */             
/*  2448 */             throw f;
/*       */           } 
/*       */         }
/*  2451 */         throw f;
/*       */       } 
/*       */       
/*  2454 */       throw f;
/*       */     } finally {
/*       */       
/*  2457 */       if (_messageContext.getTransportOut() != null) {
/*  2458 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetInventoryItemsByItemDefinitionResponse getInventoryItemsByItemDefinition(GetInventoryItemsByItemDefinition getInventoryItemsByItemDefinition26) throws RemoteException {
/*  2481 */     MessageContext _messageContext = null;
/*       */     try {
/*  2483 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[13].getName());
/*  2484 */       _operationClient.getOptions().setAction("#GetInventoryItemsByItemDefinition");
/*  2485 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2489 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2493 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2498 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2501 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2502 */           getInventoryItemsByItemDefinition26, 
/*  2503 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2504 */               "getInventoryItemsByItemDefinition")), new QName("urn:CSSoapService", 
/*  2505 */             "getInventoryItemsByItemDefinition"));
/*       */ 
/*       */       
/*  2508 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2510 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2513 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2516 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2519 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2520 */           "In");
/*  2521 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2524 */       Object object = fromOM(
/*  2525 */           _returnEnv.getBody().getFirstElement(), 
/*  2526 */           GetInventoryItemsByItemDefinitionResponse.class, 
/*  2527 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2530 */       return (GetInventoryItemsByItemDefinitionResponse)object;
/*       */     }
/*  2532 */     catch (AxisFault f) {
/*       */       
/*  2534 */       OMElement faultElt = f.getDetail();
/*  2535 */       if (faultElt != null) {
/*  2536 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetInventoryItemsByItemDefinition"))) {
/*       */           
/*       */           try {
/*  2539 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetInventoryItemsByItemDefinition"));
/*  2540 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2541 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2542 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2544 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetInventoryItemsByItemDefinition"));
/*  2545 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2546 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2547 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2548 */                 new Class[] { messageClass });
/*  2549 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2552 */             throw new RemoteException(ex.getMessage(), ex);
/*  2553 */           } catch (ClassCastException e) {
/*       */             
/*  2555 */             throw f;
/*  2556 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2558 */             throw f;
/*  2559 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2561 */             throw f;
/*  2562 */           } catch (InvocationTargetException e) {
/*       */             
/*  2564 */             throw f;
/*  2565 */           } catch (IllegalAccessException e) {
/*       */             
/*  2567 */             throw f;
/*  2568 */           } catch (InstantiationException e) {
/*       */             
/*  2570 */             throw f;
/*       */           } 
/*       */         }
/*  2573 */         throw f;
/*       */       } 
/*       */       
/*  2576 */       throw f;
/*       */     } finally {
/*       */       
/*  2579 */       if (_messageContext.getTransportOut() != null) {
/*  2580 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetTransactionEventsResponse setTransactionEvents(SetTransactionEvents setTransactionEvents28) throws RemoteException {
/*  2603 */     MessageContext _messageContext = null;
/*       */     try {
/*  2605 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[14].getName());
/*  2606 */       _operationClient.getOptions().setAction("#SetTransactionEvents");
/*  2607 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2611 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2615 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2620 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2623 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2624 */           setTransactionEvents28, 
/*  2625 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2626 */               "setTransactionEvents")), new QName("urn:CSSoapService", 
/*  2627 */             "setTransactionEvents"));
/*       */ 
/*       */       
/*  2630 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2632 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2635 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2638 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2641 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2642 */           "In");
/*  2643 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2646 */       Object object = fromOM(
/*  2647 */           _returnEnv.getBody().getFirstElement(), 
/*  2648 */           SetTransactionEventsResponse.class, 
/*  2649 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2652 */       return (SetTransactionEventsResponse)object;
/*       */     }
/*  2654 */     catch (AxisFault f) {
/*       */       
/*  2656 */       OMElement faultElt = f.getDetail();
/*  2657 */       if (faultElt != null) {
/*  2658 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetTransactionEvents"))) {
/*       */           
/*       */           try {
/*  2661 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetTransactionEvents"));
/*  2662 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2663 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2664 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2666 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetTransactionEvents"));
/*  2667 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2668 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2669 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2670 */                 new Class[] { messageClass });
/*  2671 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2674 */             throw new RemoteException(ex.getMessage(), ex);
/*  2675 */           } catch (ClassCastException e) {
/*       */             
/*  2677 */             throw f;
/*  2678 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2680 */             throw f;
/*  2681 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2683 */             throw f;
/*  2684 */           } catch (InvocationTargetException e) {
/*       */             
/*  2686 */             throw f;
/*  2687 */           } catch (IllegalAccessException e) {
/*       */             
/*  2689 */             throw f;
/*  2690 */           } catch (InstantiationException e) {
/*       */             
/*  2692 */             throw f;
/*       */           } 
/*       */         }
/*  2695 */         throw f;
/*       */       } 
/*       */       
/*  2698 */       throw f;
/*       */     } finally {
/*       */       
/*  2701 */       if (_messageContext.getTransportOut() != null) {
/*  2702 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetItemFromVINResponse getItemFromVIN(GetItemFromVIN getItemFromVIN30) throws RemoteException {
/*  2725 */     MessageContext _messageContext = null;
/*       */     try {
/*  2727 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[15].getName());
/*  2728 */       _operationClient.getOptions().setAction("#GetItemFromVIN");
/*  2729 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2733 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2737 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2742 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2745 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2746 */           getItemFromVIN30, 
/*  2747 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2748 */               "getItemFromVIN")), new QName("urn:CSSoapService", 
/*  2749 */             "getItemFromVIN"));
/*       */ 
/*       */       
/*  2752 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2754 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2757 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2760 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2763 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2764 */           "In");
/*  2765 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2768 */       Object object = fromOM(
/*  2769 */           _returnEnv.getBody().getFirstElement(), 
/*  2770 */           GetItemFromVINResponse.class, 
/*  2771 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2774 */       return (GetItemFromVINResponse)object;
/*       */     }
/*  2776 */     catch (AxisFault f) {
/*       */       
/*  2778 */       OMElement faultElt = f.getDetail();
/*  2779 */       if (faultElt != null) {
/*  2780 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetItemFromVIN"))) {
/*       */           
/*       */           try {
/*  2783 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetItemFromVIN"));
/*  2784 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2785 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2786 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2788 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetItemFromVIN"));
/*  2789 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2790 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2791 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2792 */                 new Class[] { messageClass });
/*  2793 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2796 */             throw new RemoteException(ex.getMessage(), ex);
/*  2797 */           } catch (ClassCastException e) {
/*       */             
/*  2799 */             throw f;
/*  2800 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2802 */             throw f;
/*  2803 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2805 */             throw f;
/*  2806 */           } catch (InvocationTargetException e) {
/*       */             
/*  2808 */             throw f;
/*  2809 */           } catch (IllegalAccessException e) {
/*       */             
/*  2811 */             throw f;
/*  2812 */           } catch (InstantiationException e) {
/*       */             
/*  2814 */             throw f;
/*       */           } 
/*       */         }
/*  2817 */         throw f;
/*       */       } 
/*       */       
/*  2820 */       throw f;
/*       */     } finally {
/*       */       
/*  2823 */       if (_messageContext.getTransportOut() != null) {
/*  2824 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public BookingRequestResponse bookingRequest(BookingRequest bookingRequest32) throws RemoteException {
/*  2847 */     MessageContext _messageContext = null;
/*       */     try {
/*  2849 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[16].getName());
/*  2850 */       _operationClient.getOptions().setAction("#BookingRequest");
/*  2851 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2855 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2859 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2864 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2867 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2868 */           bookingRequest32, 
/*  2869 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2870 */               "bookingRequest")), new QName("urn:CSSoapService", 
/*  2871 */             "bookingRequest"));
/*       */ 
/*       */       
/*  2874 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2876 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  2879 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  2882 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  2885 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  2886 */           "In");
/*  2887 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  2890 */       Object object = fromOM(
/*  2891 */           _returnEnv.getBody().getFirstElement(), 
/*  2892 */           BookingRequestResponse.class, 
/*  2893 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  2896 */       return (BookingRequestResponse)object;
/*       */     }
/*  2898 */     catch (AxisFault f) {
/*       */       
/*  2900 */       OMElement faultElt = f.getDetail();
/*  2901 */       if (faultElt != null) {
/*  2902 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "BookingRequest"))) {
/*       */           
/*       */           try {
/*  2905 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "BookingRequest"));
/*  2906 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  2907 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  2908 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  2910 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "BookingRequest"));
/*  2911 */             Class<?> messageClass = Class.forName(messageClassName);
/*  2912 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  2913 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  2914 */                 new Class[] { messageClass });
/*  2915 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  2918 */             throw new RemoteException(ex.getMessage(), ex);
/*  2919 */           } catch (ClassCastException e) {
/*       */             
/*  2921 */             throw f;
/*  2922 */           } catch (ClassNotFoundException e) {
/*       */             
/*  2924 */             throw f;
/*  2925 */           } catch (NoSuchMethodException e) {
/*       */             
/*  2927 */             throw f;
/*  2928 */           } catch (InvocationTargetException e) {
/*       */             
/*  2930 */             throw f;
/*  2931 */           } catch (IllegalAccessException e) {
/*       */             
/*  2933 */             throw f;
/*  2934 */           } catch (InstantiationException e) {
/*       */             
/*  2936 */             throw f;
/*       */           } 
/*       */         }
/*  2939 */         throw f;
/*       */       } 
/*       */       
/*  2942 */       throw f;
/*       */     } finally {
/*       */       
/*  2945 */       if (_messageContext.getTransportOut() != null) {
/*  2946 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetWorkingPortsResponse getWorkingPorts(GetWorkingPorts getWorkingPorts34) throws RemoteException {
/*  2969 */     MessageContext _messageContext = null;
/*       */     try {
/*  2971 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[17].getName());
/*  2972 */       _operationClient.getOptions().setAction("#GetWorkingPorts");
/*  2973 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  2977 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  2981 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  2986 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  2989 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  2990 */           getWorkingPorts34, 
/*  2991 */           optimizeContent(new QName("urn:CSSoapService", 
/*  2992 */               "getWorkingPorts")), new QName("urn:CSSoapService", 
/*  2993 */             "getWorkingPorts"));
/*       */ 
/*       */       
/*  2996 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  2998 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3001 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3004 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3007 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3008 */           "In");
/*  3009 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3012 */       Object object = fromOM(
/*  3013 */           _returnEnv.getBody().getFirstElement(), 
/*  3014 */           GetWorkingPortsResponse.class, 
/*  3015 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3018 */       return (GetWorkingPortsResponse)object;
/*       */     }
/*  3020 */     catch (AxisFault f) {
/*       */       
/*  3022 */       OMElement faultElt = f.getDetail();
/*  3023 */       if (faultElt != null) {
/*  3024 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetWorkingPorts"))) {
/*       */           
/*       */           try {
/*  3027 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetWorkingPorts"));
/*  3028 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3029 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3030 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3032 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetWorkingPorts"));
/*  3033 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3034 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3035 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3036 */                 new Class[] { messageClass });
/*  3037 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3040 */             throw new RemoteException(ex.getMessage(), ex);
/*  3041 */           } catch (ClassCastException e) {
/*       */             
/*  3043 */             throw f;
/*  3044 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3046 */             throw f;
/*  3047 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3049 */             throw f;
/*  3050 */           } catch (InvocationTargetException e) {
/*       */             
/*  3052 */             throw f;
/*  3053 */           } catch (IllegalAccessException e) {
/*       */             
/*  3055 */             throw f;
/*  3056 */           } catch (InstantiationException e) {
/*       */             
/*  3058 */             throw f;
/*       */           } 
/*       */         }
/*  3061 */         throw f;
/*       */       } 
/*       */       
/*  3064 */       throw f;
/*       */     } finally {
/*       */       
/*  3067 */       if (_messageContext.getTransportOut() != null) {
/*  3068 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public EndSessionResponse endSession(EndSession endSession36) throws RemoteException {
/*  3091 */     MessageContext _messageContext = null;
/*       */     try {
/*  3093 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[18].getName());
/*  3094 */       _operationClient.getOptions().setAction("#EndSession");
/*  3095 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3099 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3103 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3108 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3111 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3112 */           endSession36, 
/*  3113 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3114 */               "endSession")), new QName("urn:CSSoapService", 
/*  3115 */             "endSession"));
/*       */ 
/*       */       
/*  3118 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3120 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3123 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3126 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3129 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3130 */           "In");
/*  3131 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3134 */       Object object = fromOM(
/*  3135 */           _returnEnv.getBody().getFirstElement(), 
/*  3136 */           EndSessionResponse.class, 
/*  3137 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3140 */       return (EndSessionResponse)object;
/*       */     }
/*  3142 */     catch (AxisFault f) {
/*       */       
/*  3144 */       OMElement faultElt = f.getDetail();
/*  3145 */       if (faultElt != null) {
/*  3146 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "EndSession"))) {
/*       */           
/*       */           try {
/*  3149 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "EndSession"));
/*  3150 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3151 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3152 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3154 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "EndSession"));
/*  3155 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3156 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3157 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3158 */                 new Class[] { messageClass });
/*  3159 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3162 */             throw new RemoteException(ex.getMessage(), ex);
/*  3163 */           } catch (ClassCastException e) {
/*       */             
/*  3165 */             throw f;
/*  3166 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3168 */             throw f;
/*  3169 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3171 */             throw f;
/*  3172 */           } catch (InvocationTargetException e) {
/*       */             
/*  3174 */             throw f;
/*  3175 */           } catch (IllegalAccessException e) {
/*       */             
/*  3177 */             throw f;
/*  3178 */           } catch (InstantiationException e) {
/*       */             
/*  3180 */             throw f;
/*       */           } 
/*       */         }
/*  3183 */         throw f;
/*       */       } 
/*       */       
/*  3186 */       throw f;
/*       */     } finally {
/*       */       
/*  3189 */       if (_messageContext.getTransportOut() != null) {
/*  3190 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public UpdateOrderResponse updateOrder(UpdateOrder updateOrder38) throws RemoteException {
/*  3213 */     MessageContext _messageContext = null;
/*       */     try {
/*  3215 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[19].getName());
/*  3216 */       _operationClient.getOptions().setAction("#UpdateOrder");
/*  3217 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3221 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3225 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3230 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3233 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3234 */           updateOrder38, 
/*  3235 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3236 */               "updateOrder")), new QName("urn:CSSoapService", 
/*  3237 */             "updateOrder"));
/*       */ 
/*       */       
/*  3240 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3242 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3245 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3248 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3251 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3252 */           "In");
/*  3253 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3256 */       Object object = fromOM(
/*  3257 */           _returnEnv.getBody().getFirstElement(), 
/*  3258 */           UpdateOrderResponse.class, 
/*  3259 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3262 */       return (UpdateOrderResponse)object;
/*       */     }
/*  3264 */     catch (AxisFault f) {
/*       */       
/*  3266 */       OMElement faultElt = f.getDetail();
/*  3267 */       if (faultElt != null) {
/*  3268 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "UpdateOrder"))) {
/*       */           
/*       */           try {
/*  3271 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "UpdateOrder"));
/*  3272 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3273 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3274 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3276 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "UpdateOrder"));
/*  3277 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3278 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3279 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3280 */                 new Class[] { messageClass });
/*  3281 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3284 */             throw new RemoteException(ex.getMessage(), ex);
/*  3285 */           } catch (ClassCastException e) {
/*       */             
/*  3287 */             throw f;
/*  3288 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3290 */             throw f;
/*  3291 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3293 */             throw f;
/*  3294 */           } catch (InvocationTargetException e) {
/*       */             
/*  3296 */             throw f;
/*  3297 */           } catch (IllegalAccessException e) {
/*       */             
/*  3299 */             throw f;
/*  3300 */           } catch (InstantiationException e) {
/*       */             
/*  3302 */             throw f;
/*       */           } 
/*       */         }
/*  3305 */         throw f;
/*       */       } 
/*       */       
/*  3308 */       throw f;
/*       */     } finally {
/*       */       
/*  3311 */       if (_messageContext.getTransportOut() != null) {
/*  3312 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public StartSessionResponse startSession(StartSession startSession40) throws RemoteException {
/*  3335 */     MessageContext _messageContext = null;
/*       */     try {
/*  3337 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[20].getName());
/*  3338 */       _operationClient.getOptions().setAction("#StartSession");
/*  3339 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3343 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3347 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3352 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3355 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3356 */           startSession40, 
/*  3357 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3358 */               "startSession")), new QName("urn:CSSoapService", 
/*  3359 */             "startSession"));
/*       */ 
/*       */       
/*  3362 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3364 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3367 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3370 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3373 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3374 */           "In");
/*  3375 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3378 */       Object object = fromOM(
/*  3379 */           _returnEnv.getBody().getFirstElement(), 
/*  3380 */           StartSessionResponse.class, 
/*  3381 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3384 */       return (StartSessionResponse)object;
/*       */     }
/*  3386 */     catch (AxisFault f) {
/*       */       
/*  3388 */       OMElement faultElt = f.getDetail();
/*  3389 */       if (faultElt != null) {
/*  3390 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "StartSession"))) {
/*       */           
/*       */           try {
/*  3393 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "StartSession"));
/*  3394 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3395 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3396 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3398 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "StartSession"));
/*  3399 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3400 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3401 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3402 */                 new Class[] { messageClass });
/*  3403 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3406 */             throw new RemoteException(ex.getMessage(), ex);
/*  3407 */           } catch (ClassCastException e) {
/*       */             
/*  3409 */             throw f;
/*  3410 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3412 */             throw f;
/*  3413 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3415 */             throw f;
/*  3416 */           } catch (InvocationTargetException e) {
/*       */             
/*  3418 */             throw f;
/*  3419 */           } catch (IllegalAccessException e) {
/*       */             
/*  3421 */             throw f;
/*  3422 */           } catch (InstantiationException e) {
/*       */             
/*  3424 */             throw f;
/*       */           } 
/*       */         }
/*  3427 */         throw f;
/*       */       } 
/*       */       
/*  3430 */       throw f;
/*       */     } finally {
/*       */       
/*  3433 */       if (_messageContext.getTransportOut() != null) {
/*  3434 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetShipmentStatusResponse setShipmentStatus(SetShipmentStatus setShipmentStatus42) throws RemoteException {
/*  3457 */     MessageContext _messageContext = null;
/*       */     try {
/*  3459 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[21].getName());
/*  3460 */       _operationClient.getOptions().setAction("#SetShipmentStatus");
/*  3461 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3465 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3469 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3474 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3477 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3478 */           setShipmentStatus42, 
/*  3479 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3480 */               "setShipmentStatus")), new QName("urn:CSSoapService", 
/*  3481 */             "setShipmentStatus"));
/*       */ 
/*       */       
/*  3484 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3486 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3489 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3492 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3495 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3496 */           "In");
/*  3497 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3500 */       Object object = fromOM(
/*  3501 */           _returnEnv.getBody().getFirstElement(), 
/*  3502 */           SetShipmentStatusResponse.class, 
/*  3503 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3506 */       return (SetShipmentStatusResponse)object;
/*       */     }
/*  3508 */     catch (AxisFault f) {
/*       */       
/*  3510 */       OMElement faultElt = f.getDetail();
/*  3511 */       if (faultElt != null) {
/*  3512 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetShipmentStatus"))) {
/*       */           
/*       */           try {
/*  3515 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetShipmentStatus"));
/*  3516 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3517 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3518 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3520 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetShipmentStatus"));
/*  3521 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3522 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3523 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3524 */                 new Class[] { messageClass });
/*  3525 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3528 */             throw new RemoteException(ex.getMessage(), ex);
/*  3529 */           } catch (ClassCastException e) {
/*       */             
/*  3531 */             throw f;
/*  3532 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3534 */             throw f;
/*  3535 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3537 */             throw f;
/*  3538 */           } catch (InvocationTargetException e) {
/*       */             
/*  3540 */             throw f;
/*  3541 */           } catch (IllegalAccessException e) {
/*       */             
/*  3543 */             throw f;
/*  3544 */           } catch (InstantiationException e) {
/*       */             
/*  3546 */             throw f;
/*       */           } 
/*       */         }
/*  3549 */         throw f;
/*       */       } 
/*       */       
/*  3552 */       throw f;
/*       */     } finally {
/*       */       
/*  3555 */       if (_messageContext.getTransportOut() != null) {
/*  3556 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetNextTransbyDateResponse getNextTransbyDate(GetNextTransbyDate getNextTransbyDate44) throws RemoteException {
/*  3579 */     MessageContext _messageContext = null;
/*       */     try {
/*  3581 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[22].getName());
/*  3582 */       _operationClient.getOptions().setAction("#GetNextTransbyDate");
/*  3583 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3587 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3591 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3596 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3599 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3600 */           getNextTransbyDate44, 
/*  3601 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3602 */               "getNextTransbyDate")), new QName("urn:CSSoapService", 
/*  3603 */             "getNextTransbyDate"));
/*       */ 
/*       */       
/*  3606 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3608 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3611 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3614 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3617 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3618 */           "In");
/*  3619 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3622 */       Object object = fromOM(
/*  3623 */           _returnEnv.getBody().getFirstElement(), 
/*  3624 */           GetNextTransbyDateResponse.class, 
/*  3625 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3628 */       return (GetNextTransbyDateResponse)object;
/*       */     }
/*  3630 */     catch (AxisFault f) {
/*       */       
/*  3632 */       OMElement faultElt = f.getDetail();
/*  3633 */       if (faultElt != null) {
/*  3634 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetNextTransbyDate"))) {
/*       */           
/*       */           try {
/*  3637 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetNextTransbyDate"));
/*  3638 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3639 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3640 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3642 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetNextTransbyDate"));
/*  3643 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3644 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3645 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3646 */                 new Class[] { messageClass });
/*  3647 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3650 */             throw new RemoteException(ex.getMessage(), ex);
/*  3651 */           } catch (ClassCastException e) {
/*       */             
/*  3653 */             throw f;
/*  3654 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3656 */             throw f;
/*  3657 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3659 */             throw f;
/*  3660 */           } catch (InvocationTargetException e) {
/*       */             
/*  3662 */             throw f;
/*  3663 */           } catch (IllegalAccessException e) {
/*       */             
/*  3665 */             throw f;
/*  3666 */           } catch (InstantiationException e) {
/*       */             
/*  3668 */             throw f;
/*       */           } 
/*       */         }
/*  3671 */         throw f;
/*       */       } 
/*       */       
/*  3674 */       throw f;
/*       */     } finally {
/*       */       
/*  3677 */       if (_messageContext.getTransportOut() != null) {
/*  3678 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetActiveCurrenciesResponse getActiveCurrencies(GetActiveCurrencies getActiveCurrencies46) throws RemoteException {
/*  3701 */     MessageContext _messageContext = null;
/*       */     try {
/*  3703 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[23].getName());
/*  3704 */       _operationClient.getOptions().setAction("#GetActiveCurrencies");
/*  3705 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3709 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3713 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3718 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3721 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3722 */           getActiveCurrencies46, 
/*  3723 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3724 */               "getActiveCurrencies")), new QName("urn:CSSoapService", 
/*  3725 */             "getActiveCurrencies"));
/*       */ 
/*       */       
/*  3728 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3730 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3733 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3736 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3739 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3740 */           "In");
/*  3741 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3744 */       Object object = fromOM(
/*  3745 */           _returnEnv.getBody().getFirstElement(), 
/*  3746 */           GetActiveCurrenciesResponse.class, 
/*  3747 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3750 */       return (GetActiveCurrenciesResponse)object;
/*       */     }
/*  3752 */     catch (AxisFault f) {
/*       */       
/*  3754 */       OMElement faultElt = f.getDetail();
/*  3755 */       if (faultElt != null) {
/*  3756 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetActiveCurrencies"))) {
/*       */           
/*       */           try {
/*  3759 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetActiveCurrencies"));
/*  3760 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3761 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3762 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3764 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetActiveCurrencies"));
/*  3765 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3766 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3767 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3768 */                 new Class[] { messageClass });
/*  3769 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3772 */             throw new RemoteException(ex.getMessage(), ex);
/*  3773 */           } catch (ClassCastException e) {
/*       */             
/*  3775 */             throw f;
/*  3776 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3778 */             throw f;
/*  3779 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3781 */             throw f;
/*  3782 */           } catch (InvocationTargetException e) {
/*       */             
/*  3784 */             throw f;
/*  3785 */           } catch (IllegalAccessException e) {
/*       */             
/*  3787 */             throw f;
/*  3788 */           } catch (InstantiationException e) {
/*       */             
/*  3790 */             throw f;
/*       */           } 
/*       */         }
/*  3793 */         throw f;
/*       */       } 
/*       */       
/*  3796 */       throw f;
/*       */     } finally {
/*       */       
/*  3799 */       if (_messageContext.getTransportOut() != null) {
/*  3800 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public StartTrackingResponse startTracking(StartTracking startTracking48) throws RemoteException {
/*  3823 */     MessageContext _messageContext = null;
/*       */     try {
/*  3825 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[24].getName());
/*  3826 */       _operationClient.getOptions().setAction("#StartTracking");
/*  3827 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3831 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3835 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3840 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3843 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3844 */           startTracking48, 
/*  3845 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3846 */               "startTracking")), new QName("urn:CSSoapService", 
/*  3847 */             "startTracking"));
/*       */ 
/*       */       
/*  3850 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3852 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3855 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3858 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3861 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3862 */           "In");
/*  3863 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3866 */       Object object = fromOM(
/*  3867 */           _returnEnv.getBody().getFirstElement(), 
/*  3868 */           StartTrackingResponse.class, 
/*  3869 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3872 */       return (StartTrackingResponse)object;
/*       */     }
/*  3874 */     catch (AxisFault f) {
/*       */       
/*  3876 */       OMElement faultElt = f.getDetail();
/*  3877 */       if (faultElt != null) {
/*  3878 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "StartTracking"))) {
/*       */           
/*       */           try {
/*  3881 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "StartTracking"));
/*  3882 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  3883 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  3884 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  3886 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "StartTracking"));
/*  3887 */             Class<?> messageClass = Class.forName(messageClassName);
/*  3888 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  3889 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  3890 */                 new Class[] { messageClass });
/*  3891 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  3894 */             throw new RemoteException(ex.getMessage(), ex);
/*  3895 */           } catch (ClassCastException e) {
/*       */             
/*  3897 */             throw f;
/*  3898 */           } catch (ClassNotFoundException e) {
/*       */             
/*  3900 */             throw f;
/*  3901 */           } catch (NoSuchMethodException e) {
/*       */             
/*  3903 */             throw f;
/*  3904 */           } catch (InvocationTargetException e) {
/*       */             
/*  3906 */             throw f;
/*  3907 */           } catch (IllegalAccessException e) {
/*       */             
/*  3909 */             throw f;
/*  3910 */           } catch (InstantiationException e) {
/*       */             
/*  3912 */             throw f;
/*       */           } 
/*       */         }
/*  3915 */         throw f;
/*       */       } 
/*       */       
/*  3918 */       throw f;
/*       */     } finally {
/*       */       
/*  3921 */       if (_messageContext.getTransportOut() != null) {
/*  3922 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetStandardRatesResponse getStandardRates(GetStandardRates getStandardRates50) throws RemoteException {
/*  3945 */     MessageContext _messageContext = null;
/*       */     try {
/*  3947 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[25].getName());
/*  3948 */       _operationClient.getOptions().setAction("#GetStandardRates");
/*  3949 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  3953 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  3957 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  3962 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  3965 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  3966 */           getStandardRates50, 
/*  3967 */           optimizeContent(new QName("urn:CSSoapService", 
/*  3968 */               "getStandardRates")), new QName("urn:CSSoapService", 
/*  3969 */             "getStandardRates"));
/*       */ 
/*       */       
/*  3972 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  3974 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  3977 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  3980 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  3983 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  3984 */           "In");
/*  3985 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  3988 */       Object object = fromOM(
/*  3989 */           _returnEnv.getBody().getFirstElement(), 
/*  3990 */           GetStandardRatesResponse.class, 
/*  3991 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  3994 */       return (GetStandardRatesResponse)object;
/*       */     }
/*  3996 */     catch (AxisFault f) {
/*       */       
/*  3998 */       OMElement faultElt = f.getDetail();
/*  3999 */       if (faultElt != null) {
/*  4000 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetStandardRates"))) {
/*       */           
/*       */           try {
/*  4003 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetStandardRates"));
/*  4004 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4005 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4006 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4008 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetStandardRates"));
/*  4009 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4010 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4011 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4012 */                 new Class[] { messageClass });
/*  4013 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4016 */             throw new RemoteException(ex.getMessage(), ex);
/*  4017 */           } catch (ClassCastException e) {
/*       */             
/*  4019 */             throw f;
/*  4020 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4022 */             throw f;
/*  4023 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4025 */             throw f;
/*  4026 */           } catch (InvocationTargetException e) {
/*       */             
/*  4028 */             throw f;
/*  4029 */           } catch (IllegalAccessException e) {
/*       */             
/*  4031 */             throw f;
/*  4032 */           } catch (InstantiationException e) {
/*       */             
/*  4034 */             throw f;
/*       */           } 
/*       */         }
/*  4037 */         throw f;
/*       */       } 
/*       */       
/*  4040 */       throw f;
/*       */     } finally {
/*       */       
/*  4043 */       if (_messageContext.getTransportOut() != null) {
/*  4044 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTransactionStatusResponse getTransactionStatus(GetTransactionStatus getTransactionStatus52) throws RemoteException {
/*  4067 */     MessageContext _messageContext = null;
/*       */     try {
/*  4069 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[26].getName());
/*  4070 */       _operationClient.getOptions().setAction("#GetTransactionStatus");
/*  4071 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4075 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4079 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4084 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4087 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4088 */           getTransactionStatus52, 
/*  4089 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4090 */               "getTransactionStatus")), new QName("urn:CSSoapService", 
/*  4091 */             "getTransactionStatus"));
/*       */ 
/*       */       
/*  4094 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4096 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4099 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4102 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4105 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4106 */           "In");
/*  4107 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4110 */       Object object = fromOM(
/*  4111 */           _returnEnv.getBody().getFirstElement(), 
/*  4112 */           GetTransactionStatusResponse.class, 
/*  4113 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4116 */       return (GetTransactionStatusResponse)object;
/*       */     }
/*  4118 */     catch (AxisFault f) {
/*       */       
/*  4120 */       OMElement faultElt = f.getDetail();
/*  4121 */       if (faultElt != null) {
/*  4122 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTransactionStatus"))) {
/*       */           
/*       */           try {
/*  4125 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTransactionStatus"));
/*  4126 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4127 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4128 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4130 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTransactionStatus"));
/*  4131 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4132 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4133 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4134 */                 new Class[] { messageClass });
/*  4135 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4138 */             throw new RemoteException(ex.getMessage(), ex);
/*  4139 */           } catch (ClassCastException e) {
/*       */             
/*  4141 */             throw f;
/*  4142 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4144 */             throw f;
/*  4145 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4147 */             throw f;
/*  4148 */           } catch (InvocationTargetException e) {
/*       */             
/*  4150 */             throw f;
/*  4151 */           } catch (IllegalAccessException e) {
/*       */             
/*  4153 */             throw f;
/*  4154 */           } catch (InstantiationException e) {
/*       */             
/*  4156 */             throw f;
/*       */           } 
/*       */         }
/*  4159 */         throw f;
/*       */       } 
/*       */       
/*  4162 */       throw f;
/*       */     } finally {
/*       */       
/*  4165 */       if (_messageContext.getTransportOut() != null) {
/*  4166 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SubmitSalesOrderResponse submitSalesOrder(SubmitSalesOrder submitSalesOrder54) throws RemoteException {
/*  4189 */     MessageContext _messageContext = null;
/*       */     try {
/*  4191 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[27].getName());
/*  4192 */       _operationClient.getOptions().setAction("#SubmitSalesOrder");
/*  4193 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4197 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4201 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4206 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4209 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4210 */           submitSalesOrder54, 
/*  4211 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4212 */               "submitSalesOrder")), new QName("urn:CSSoapService", 
/*  4213 */             "submitSalesOrder"));
/*       */ 
/*       */       
/*  4216 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4218 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4221 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4224 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4227 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4228 */           "In");
/*  4229 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4232 */       Object object = fromOM(
/*  4233 */           _returnEnv.getBody().getFirstElement(), 
/*  4234 */           SubmitSalesOrderResponse.class, 
/*  4235 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4238 */       return (SubmitSalesOrderResponse)object;
/*       */     }
/*  4240 */     catch (AxisFault f) {
/*       */       
/*  4242 */       OMElement faultElt = f.getDetail();
/*  4243 */       if (faultElt != null) {
/*  4244 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SubmitSalesOrder"))) {
/*       */           
/*       */           try {
/*  4247 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SubmitSalesOrder"));
/*  4248 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4249 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4250 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4252 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SubmitSalesOrder"));
/*  4253 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4254 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4255 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4256 */                 new Class[] { messageClass });
/*  4257 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4260 */             throw new RemoteException(ex.getMessage(), ex);
/*  4261 */           } catch (ClassCastException e) {
/*       */             
/*  4263 */             throw f;
/*  4264 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4266 */             throw f;
/*  4267 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4269 */             throw f;
/*  4270 */           } catch (InvocationTargetException e) {
/*       */             
/*  4272 */             throw f;
/*  4273 */           } catch (IllegalAccessException e) {
/*       */             
/*  4275 */             throw f;
/*  4276 */           } catch (InstantiationException e) {
/*       */             
/*  4278 */             throw f;
/*       */           } 
/*       */         }
/*  4281 */         throw f;
/*       */       } 
/*       */       
/*  4284 */       throw f;
/*       */     } finally {
/*       */       
/*  4287 */       if (_messageContext.getTransportOut() != null) {
/*  4288 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public RenameTransactionResponse renameTransaction(RenameTransaction renameTransaction56) throws RemoteException {
/*  4311 */     MessageContext _messageContext = null;
/*       */     try {
/*  4313 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[28].getName());
/*  4314 */       _operationClient.getOptions().setAction("#RenameTransaction");
/*  4315 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4319 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4323 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4328 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4331 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4332 */           renameTransaction56, 
/*  4333 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4334 */               "renameTransaction")), new QName("urn:CSSoapService", 
/*  4335 */             "renameTransaction"));
/*       */ 
/*       */       
/*  4338 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4340 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4343 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4346 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4349 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4350 */           "In");
/*  4351 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4354 */       Object object = fromOM(
/*  4355 */           _returnEnv.getBody().getFirstElement(), 
/*  4356 */           RenameTransactionResponse.class, 
/*  4357 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4360 */       return (RenameTransactionResponse)object;
/*       */     }
/*  4362 */     catch (AxisFault f) {
/*       */       
/*  4364 */       OMElement faultElt = f.getDetail();
/*  4365 */       if (faultElt != null) {
/*  4366 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "RenameTransaction"))) {
/*       */           
/*       */           try {
/*  4369 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "RenameTransaction"));
/*  4370 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4371 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4372 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4374 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "RenameTransaction"));
/*  4375 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4376 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4377 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4378 */                 new Class[] { messageClass });
/*  4379 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4382 */             throw new RemoteException(ex.getMessage(), ex);
/*  4383 */           } catch (ClassCastException e) {
/*       */             
/*  4385 */             throw f;
/*  4386 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4388 */             throw f;
/*  4389 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4391 */             throw f;
/*  4392 */           } catch (InvocationTargetException e) {
/*       */             
/*  4394 */             throw f;
/*  4395 */           } catch (IllegalAccessException e) {
/*       */             
/*  4397 */             throw f;
/*  4398 */           } catch (InstantiationException e) {
/*       */             
/*  4400 */             throw f;
/*       */           } 
/*       */         }
/*  4403 */         throw f;
/*       */       } 
/*       */       
/*  4406 */       throw f;
/*       */     } finally {
/*       */       
/*  4409 */       if (_messageContext.getTransportOut() != null) {
/*  4410 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public QueryLogJSResponse queryLogJS(QueryLogJS queryLogJS58) throws RemoteException {
/*  4433 */     MessageContext _messageContext = null;
/*       */     try {
/*  4435 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[29].getName());
/*  4436 */       _operationClient.getOptions().setAction("#QueryLogJS");
/*  4437 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4441 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4445 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4450 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4453 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4454 */           queryLogJS58, 
/*  4455 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4456 */               "queryLogJS")), new QName("urn:CSSoapService", 
/*  4457 */             "queryLogJS"));
/*       */ 
/*       */       
/*  4460 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4462 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4465 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4468 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4471 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4472 */           "In");
/*  4473 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4476 */       Object object = fromOM(
/*  4477 */           _returnEnv.getBody().getFirstElement(), 
/*  4478 */           QueryLogJSResponse.class, 
/*  4479 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4482 */       return (QueryLogJSResponse)object;
/*       */     }
/*  4484 */     catch (AxisFault f) {
/*       */       
/*  4486 */       OMElement faultElt = f.getDetail();
/*  4487 */       if (faultElt != null) {
/*  4488 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "QueryLogJS"))) {
/*       */           
/*       */           try {
/*  4491 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "QueryLogJS"));
/*  4492 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4493 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4494 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4496 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "QueryLogJS"));
/*  4497 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4498 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4499 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4500 */                 new Class[] { messageClass });
/*  4501 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4504 */             throw new RemoteException(ex.getMessage(), ex);
/*  4505 */           } catch (ClassCastException e) {
/*       */             
/*  4507 */             throw f;
/*  4508 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4510 */             throw f;
/*  4511 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4513 */             throw f;
/*  4514 */           } catch (InvocationTargetException e) {
/*       */             
/*  4516 */             throw f;
/*  4517 */           } catch (IllegalAccessException e) {
/*       */             
/*  4519 */             throw f;
/*  4520 */           } catch (InstantiationException e) {
/*       */             
/*  4522 */             throw f;
/*       */           } 
/*       */         }
/*  4525 */         throw f;
/*       */       } 
/*       */       
/*  4528 */       throw f;
/*       */     } finally {
/*       */       
/*  4531 */       if (_messageContext.getTransportOut() != null) {
/*  4532 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetEntitiesOfTypeResponse getEntitiesOfType(GetEntitiesOfType getEntitiesOfType60) throws RemoteException {
/*  4555 */     MessageContext _messageContext = null;
/*       */     try {
/*  4557 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[30].getName());
/*  4558 */       _operationClient.getOptions().setAction("#GetEntitiesOfType");
/*  4559 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4563 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4567 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4572 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4575 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4576 */           getEntitiesOfType60, 
/*  4577 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4578 */               "getEntitiesOfType")), new QName("urn:CSSoapService", 
/*  4579 */             "getEntitiesOfType"));
/*       */ 
/*       */       
/*  4582 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4584 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4587 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4590 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4593 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4594 */           "In");
/*  4595 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4598 */       Object object = fromOM(
/*  4599 */           _returnEnv.getBody().getFirstElement(), 
/*  4600 */           GetEntitiesOfTypeResponse.class, 
/*  4601 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4604 */       return (GetEntitiesOfTypeResponse)object;
/*       */     }
/*  4606 */     catch (AxisFault f) {
/*       */       
/*  4608 */       OMElement faultElt = f.getDetail();
/*  4609 */       if (faultElt != null) {
/*  4610 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetEntitiesOfType"))) {
/*       */           
/*       */           try {
/*  4613 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetEntitiesOfType"));
/*  4614 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4615 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4616 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4618 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetEntitiesOfType"));
/*  4619 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4620 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4621 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4622 */                 new Class[] { messageClass });
/*  4623 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4626 */             throw new RemoteException(ex.getMessage(), ex);
/*  4627 */           } catch (ClassCastException e) {
/*       */             
/*  4629 */             throw f;
/*  4630 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4632 */             throw f;
/*  4633 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4635 */             throw f;
/*  4636 */           } catch (InvocationTargetException e) {
/*       */             
/*  4638 */             throw f;
/*  4639 */           } catch (IllegalAccessException e) {
/*       */             
/*  4641 */             throw f;
/*  4642 */           } catch (InstantiationException e) {
/*       */             
/*  4644 */             throw f;
/*       */           } 
/*       */         }
/*  4647 */         throw f;
/*       */       } 
/*       */       
/*  4650 */       throw f;
/*       */     } finally {
/*       */       
/*  4653 */       if (_messageContext.getTransportOut() != null) {
/*  4654 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetItemDefinitionsByCustomerResponse getItemDefinitionsByCustomer(GetItemDefinitionsByCustomer getItemDefinitionsByCustomer62) throws RemoteException {
/*  4677 */     MessageContext _messageContext = null;
/*       */     try {
/*  4679 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[31].getName());
/*  4680 */       _operationClient.getOptions().setAction("#GetItemDefinitionsByCustomer");
/*  4681 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4685 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4689 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4694 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4697 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4698 */           getItemDefinitionsByCustomer62, 
/*  4699 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4700 */               "getItemDefinitionsByCustomer")), new QName("urn:CSSoapService", 
/*  4701 */             "getItemDefinitionsByCustomer"));
/*       */ 
/*       */       
/*  4704 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4706 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4709 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4712 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4715 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4716 */           "In");
/*  4717 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4720 */       Object object = fromOM(
/*  4721 */           _returnEnv.getBody().getFirstElement(), 
/*  4722 */           GetItemDefinitionsByCustomerResponse.class, 
/*  4723 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4726 */       return (GetItemDefinitionsByCustomerResponse)object;
/*       */     }
/*  4728 */     catch (AxisFault f) {
/*       */       
/*  4730 */       OMElement faultElt = f.getDetail();
/*  4731 */       if (faultElt != null) {
/*  4732 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetItemDefinitionsByCustomer"))) {
/*       */           
/*       */           try {
/*  4735 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetItemDefinitionsByCustomer"));
/*  4736 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4737 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4738 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4740 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetItemDefinitionsByCustomer"));
/*  4741 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4742 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4743 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4744 */                 new Class[] { messageClass });
/*  4745 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4748 */             throw new RemoteException(ex.getMessage(), ex);
/*  4749 */           } catch (ClassCastException e) {
/*       */             
/*  4751 */             throw f;
/*  4752 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4754 */             throw f;
/*  4755 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4757 */             throw f;
/*  4758 */           } catch (InvocationTargetException e) {
/*       */             
/*  4760 */             throw f;
/*  4761 */           } catch (IllegalAccessException e) {
/*       */             
/*  4763 */             throw f;
/*  4764 */           } catch (InstantiationException e) {
/*       */             
/*  4766 */             throw f;
/*       */           } 
/*       */         }
/*  4769 */         throw f;
/*       */       } 
/*       */       
/*  4772 */       throw f;
/*       */     } finally {
/*       */       
/*  4775 */       if (_messageContext.getTransportOut() != null) {
/*  4776 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public StartTracking2Response startTracking2(StartTracking2 startTracking264) throws RemoteException {
/*  4799 */     MessageContext _messageContext = null;
/*       */     try {
/*  4801 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[32].getName());
/*  4802 */       _operationClient.getOptions().setAction("#StartTracking2");
/*  4803 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4807 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4811 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4816 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4819 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4820 */           startTracking264, 
/*  4821 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4822 */               "startTracking2")), new QName("urn:CSSoapService", 
/*  4823 */             "startTracking2"));
/*       */ 
/*       */       
/*  4826 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4828 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4831 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4834 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4837 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4838 */           "In");
/*  4839 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4842 */       Object object = fromOM(
/*  4843 */           _returnEnv.getBody().getFirstElement(), 
/*  4844 */           StartTracking2Response.class, 
/*  4845 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4848 */       return (StartTracking2Response)object;
/*       */     }
/*  4850 */     catch (AxisFault f) {
/*       */       
/*  4852 */       OMElement faultElt = f.getDetail();
/*  4853 */       if (faultElt != null) {
/*  4854 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "StartTracking2"))) {
/*       */           
/*       */           try {
/*  4857 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "StartTracking2"));
/*  4858 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4859 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4860 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4862 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "StartTracking2"));
/*  4863 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4864 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4865 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4866 */                 new Class[] { messageClass });
/*  4867 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4870 */             throw new RemoteException(ex.getMessage(), ex);
/*  4871 */           } catch (ClassCastException e) {
/*       */             
/*  4873 */             throw f;
/*  4874 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4876 */             throw f;
/*  4877 */           } catch (NoSuchMethodException e) {
/*       */             
/*  4879 */             throw f;
/*  4880 */           } catch (InvocationTargetException e) {
/*       */             
/*  4882 */             throw f;
/*  4883 */           } catch (IllegalAccessException e) {
/*       */             
/*  4885 */             throw f;
/*  4886 */           } catch (InstantiationException e) {
/*       */             
/*  4888 */             throw f;
/*       */           } 
/*       */         }
/*  4891 */         throw f;
/*       */       } 
/*       */       
/*  4894 */       throw f;
/*       */     } finally {
/*       */       
/*  4897 */       if (_messageContext.getTransportOut() != null) {
/*  4898 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public DeleteTransactionResponse deleteTransaction(DeleteTransaction deleteTransaction66) throws RemoteException {
/*  4921 */     MessageContext _messageContext = null;
/*       */     try {
/*  4923 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[33].getName());
/*  4924 */       _operationClient.getOptions().setAction("#DeleteTransaction");
/*  4925 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  4929 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  4933 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  4938 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  4941 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  4942 */           deleteTransaction66, 
/*  4943 */           optimizeContent(new QName("urn:CSSoapService", 
/*  4944 */               "deleteTransaction")), new QName("urn:CSSoapService", 
/*  4945 */             "deleteTransaction"));
/*       */ 
/*       */       
/*  4948 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  4950 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  4953 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  4956 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  4959 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  4960 */           "In");
/*  4961 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  4964 */       Object object = fromOM(
/*  4965 */           _returnEnv.getBody().getFirstElement(), 
/*  4966 */           DeleteTransactionResponse.class, 
/*  4967 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  4970 */       return (DeleteTransactionResponse)object;
/*       */     }
/*  4972 */     catch (AxisFault f) {
/*       */       
/*  4974 */       OMElement faultElt = f.getDetail();
/*  4975 */       if (faultElt != null) {
/*  4976 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "DeleteTransaction"))) {
/*       */           
/*       */           try {
/*  4979 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "DeleteTransaction"));
/*  4980 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  4981 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  4982 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  4984 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "DeleteTransaction"));
/*  4985 */             Class<?> messageClass = Class.forName(messageClassName);
/*  4986 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  4987 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  4988 */                 new Class[] { messageClass });
/*  4989 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  4992 */             throw new RemoteException(ex.getMessage(), ex);
/*  4993 */           } catch (ClassCastException e) {
/*       */             
/*  4995 */             throw f;
/*  4996 */           } catch (ClassNotFoundException e) {
/*       */             
/*  4998 */             throw f;
/*  4999 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5001 */             throw f;
/*  5002 */           } catch (InvocationTargetException e) {
/*       */             
/*  5004 */             throw f;
/*  5005 */           } catch (IllegalAccessException e) {
/*       */             
/*  5007 */             throw f;
/*  5008 */           } catch (InstantiationException e) {
/*       */             
/*  5010 */             throw f;
/*       */           } 
/*       */         }
/*  5013 */         throw f;
/*       */       } 
/*       */       
/*  5016 */       throw f;
/*       */     } finally {
/*       */       
/*  5019 */       if (_messageContext.getTransportOut() != null) {
/*  5020 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTransactionResponse getTransaction(GetTransaction getTransaction68) throws RemoteException {
/*  5043 */     MessageContext _messageContext = null;
/*       */     try {
/*  5045 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[34].getName());
/*  5046 */       _operationClient.getOptions().setAction("#GetTransaction");
/*  5047 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5051 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5055 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5060 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5063 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5064 */           getTransaction68, 
/*  5065 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5066 */               "getTransaction")), new QName("urn:CSSoapService", 
/*  5067 */             "getTransaction"));
/*       */ 
/*       */       
/*  5070 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5072 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5075 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5078 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5081 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5082 */           "In");
/*  5083 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5086 */       Object object = fromOM(
/*  5087 */           _returnEnv.getBody().getFirstElement(), 
/*  5088 */           GetTransactionResponse.class, 
/*  5089 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5092 */       return (GetTransactionResponse)object;
/*       */     }
/*  5094 */     catch (AxisFault f) {
/*       */       
/*  5096 */       OMElement faultElt = f.getDetail();
/*  5097 */       if (faultElt != null) {
/*  5098 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTransaction"))) {
/*       */           
/*       */           try {
/*  5101 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTransaction"));
/*  5102 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5103 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5104 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5106 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTransaction"));
/*  5107 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5108 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5109 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5110 */                 new Class[] { messageClass });
/*  5111 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5114 */             throw new RemoteException(ex.getMessage(), ex);
/*  5115 */           } catch (ClassCastException e) {
/*       */             
/*  5117 */             throw f;
/*  5118 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5120 */             throw f;
/*  5121 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5123 */             throw f;
/*  5124 */           } catch (InvocationTargetException e) {
/*       */             
/*  5126 */             throw f;
/*  5127 */           } catch (IllegalAccessException e) {
/*       */             
/*  5129 */             throw f;
/*  5130 */           } catch (InstantiationException e) {
/*       */             
/*  5132 */             throw f;
/*       */           } 
/*       */         }
/*  5135 */         throw f;
/*       */       } 
/*       */       
/*  5138 */       throw f;
/*       */     } finally {
/*       */       
/*  5141 */       if (_messageContext.getTransportOut() != null) {
/*  5142 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetEntityTransactionsResponse getEntityTransactions(GetEntityTransactions getEntityTransactions70) throws RemoteException {
/*  5165 */     MessageContext _messageContext = null;
/*       */     try {
/*  5167 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[35].getName());
/*  5168 */       _operationClient.getOptions().setAction("#GetEntityTransactions");
/*  5169 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5173 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5177 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5182 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5185 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5186 */           getEntityTransactions70, 
/*  5187 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5188 */               "getEntityTransactions")), new QName("urn:CSSoapService", 
/*  5189 */             "getEntityTransactions"));
/*       */ 
/*       */       
/*  5192 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5194 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5197 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5200 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5203 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5204 */           "In");
/*  5205 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5208 */       Object object = fromOM(
/*  5209 */           _returnEnv.getBody().getFirstElement(), 
/*  5210 */           GetEntityTransactionsResponse.class, 
/*  5211 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5214 */       return (GetEntityTransactionsResponse)object;
/*       */     }
/*  5216 */     catch (AxisFault f) {
/*       */       
/*  5218 */       OMElement faultElt = f.getDetail();
/*  5219 */       if (faultElt != null) {
/*  5220 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetEntityTransactions"))) {
/*       */           
/*       */           try {
/*  5223 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetEntityTransactions"));
/*  5224 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5225 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5226 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5228 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetEntityTransactions"));
/*  5229 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5230 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5231 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5232 */                 new Class[] { messageClass });
/*  5233 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5236 */             throw new RemoteException(ex.getMessage(), ex);
/*  5237 */           } catch (ClassCastException e) {
/*       */             
/*  5239 */             throw f;
/*  5240 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5242 */             throw f;
/*  5243 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5245 */             throw f;
/*  5246 */           } catch (InvocationTargetException e) {
/*       */             
/*  5248 */             throw f;
/*  5249 */           } catch (IllegalAccessException e) {
/*       */             
/*  5251 */             throw f;
/*  5252 */           } catch (InstantiationException e) {
/*       */             
/*  5254 */             throw f;
/*       */           } 
/*       */         }
/*  5257 */         throw f;
/*       */       } 
/*       */       
/*  5260 */       throw f;
/*       */     } finally {
/*       */       
/*  5263 */       if (_messageContext.getTransportOut() != null) {
/*  5264 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SubmitShipmentResponse submitShipment(SubmitShipment submitShipment72) throws RemoteException {
/*  5287 */     MessageContext _messageContext = null;
/*       */     try {
/*  5289 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[36].getName());
/*  5290 */       _operationClient.getOptions().setAction("#SubmitShipment");
/*  5291 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5295 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5299 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5304 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5307 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5308 */           submitShipment72, 
/*  5309 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5310 */               "submitShipment")), new QName("urn:CSSoapService", 
/*  5311 */             "submitShipment"));
/*       */ 
/*       */       
/*  5314 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5316 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5319 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5322 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5325 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5326 */           "In");
/*  5327 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5330 */       Object object = fromOM(
/*  5331 */           _returnEnv.getBody().getFirstElement(), 
/*  5332 */           SubmitShipmentResponse.class, 
/*  5333 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5336 */       return (SubmitShipmentResponse)object;
/*       */     }
/*  5338 */     catch (AxisFault f) {
/*       */       
/*  5340 */       OMElement faultElt = f.getDetail();
/*  5341 */       if (faultElt != null) {
/*  5342 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SubmitShipment"))) {
/*       */           
/*       */           try {
/*  5345 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SubmitShipment"));
/*  5346 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5347 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5348 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5350 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SubmitShipment"));
/*  5351 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5352 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5353 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5354 */                 new Class[] { messageClass });
/*  5355 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5358 */             throw new RemoteException(ex.getMessage(), ex);
/*  5359 */           } catch (ClassCastException e) {
/*       */             
/*  5361 */             throw f;
/*  5362 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5364 */             throw f;
/*  5365 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5367 */             throw f;
/*  5368 */           } catch (InvocationTargetException e) {
/*       */             
/*  5370 */             throw f;
/*  5371 */           } catch (IllegalAccessException e) {
/*       */             
/*  5373 */             throw f;
/*  5374 */           } catch (InstantiationException e) {
/*       */             
/*  5376 */             throw f;
/*       */           } 
/*       */         }
/*  5379 */         throw f;
/*       */       } 
/*       */       
/*  5382 */       throw f;
/*       */     } finally {
/*       */       
/*  5385 */       if (_messageContext.getTransportOut() != null) {
/*  5386 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetEntityContactsResponse getEntityContacts(GetEntityContacts getEntityContacts74) throws RemoteException {
/*  5409 */     MessageContext _messageContext = null;
/*       */     try {
/*  5411 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[37].getName());
/*  5412 */       _operationClient.getOptions().setAction("#GetEntityContacts");
/*  5413 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5417 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5421 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5426 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5429 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5430 */           getEntityContacts74, 
/*  5431 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5432 */               "getEntityContacts")), new QName("urn:CSSoapService", 
/*  5433 */             "getEntityContacts"));
/*       */ 
/*       */       
/*  5436 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5438 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5441 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5444 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5447 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5448 */           "In");
/*  5449 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5452 */       Object object = fromOM(
/*  5453 */           _returnEnv.getBody().getFirstElement(), 
/*  5454 */           GetEntityContactsResponse.class, 
/*  5455 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5458 */       return (GetEntityContactsResponse)object;
/*       */     }
/*  5460 */     catch (AxisFault f) {
/*       */       
/*  5462 */       OMElement faultElt = f.getDetail();
/*  5463 */       if (faultElt != null) {
/*  5464 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetEntityContacts"))) {
/*       */           
/*       */           try {
/*  5467 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetEntityContacts"));
/*  5468 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5469 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5470 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5472 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetEntityContacts"));
/*  5473 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5474 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5475 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5476 */                 new Class[] { messageClass });
/*  5477 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5480 */             throw new RemoteException(ex.getMessage(), ex);
/*  5481 */           } catch (ClassCastException e) {
/*       */             
/*  5483 */             throw f;
/*  5484 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5486 */             throw f;
/*  5487 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5489 */             throw f;
/*  5490 */           } catch (InvocationTargetException e) {
/*       */             
/*  5492 */             throw f;
/*  5493 */           } catch (IllegalAccessException e) {
/*       */             
/*  5495 */             throw f;
/*  5496 */           } catch (InstantiationException e) {
/*       */             
/*  5498 */             throw f;
/*       */           } 
/*       */         }
/*  5501 */         throw f;
/*       */       } 
/*       */       
/*  5504 */       throw f;
/*       */     } finally {
/*       */       
/*  5507 */       if (_messageContext.getTransportOut() != null) {
/*  5508 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public CancelBookingResponse cancelBooking(CancelBooking cancelBooking76) throws RemoteException {
/*  5531 */     MessageContext _messageContext = null;
/*       */     try {
/*  5533 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[38].getName());
/*  5534 */       _operationClient.getOptions().setAction("#CancelBooking");
/*  5535 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5539 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5543 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5548 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5551 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5552 */           cancelBooking76, 
/*  5553 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5554 */               "cancelBooking")), new QName("urn:CSSoapService", 
/*  5555 */             "cancelBooking"));
/*       */ 
/*       */       
/*  5558 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5560 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5563 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5566 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5569 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5570 */           "In");
/*  5571 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5574 */       Object object = fromOM(
/*  5575 */           _returnEnv.getBody().getFirstElement(), 
/*  5576 */           CancelBookingResponse.class, 
/*  5577 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5580 */       return (CancelBookingResponse)object;
/*       */     }
/*  5582 */     catch (AxisFault f) {
/*       */       
/*  5584 */       OMElement faultElt = f.getDetail();
/*  5585 */       if (faultElt != null) {
/*  5586 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "CancelBooking"))) {
/*       */           
/*       */           try {
/*  5589 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "CancelBooking"));
/*  5590 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5591 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5592 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5594 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "CancelBooking"));
/*  5595 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5596 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5597 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5598 */                 new Class[] { messageClass });
/*  5599 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5602 */             throw new RemoteException(ex.getMessage(), ex);
/*  5603 */           } catch (ClassCastException e) {
/*       */             
/*  5605 */             throw f;
/*  5606 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5608 */             throw f;
/*  5609 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5611 */             throw f;
/*  5612 */           } catch (InvocationTargetException e) {
/*       */             
/*  5614 */             throw f;
/*  5615 */           } catch (IllegalAccessException e) {
/*       */             
/*  5617 */             throw f;
/*  5618 */           } catch (InstantiationException e) {
/*       */             
/*  5620 */             throw f;
/*       */           } 
/*       */         }
/*  5623 */         throw f;
/*       */       } 
/*       */       
/*  5626 */       throw f;
/*       */     } finally {
/*       */       
/*  5629 */       if (_messageContext.getTransportOut() != null) {
/*  5630 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetTransactionResponse setTransaction(SetTransaction setTransaction78) throws RemoteException {
/*  5653 */     MessageContext _messageContext = null;
/*       */     try {
/*  5655 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[39].getName());
/*  5656 */       _operationClient.getOptions().setAction("#SetTransaction");
/*  5657 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5661 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5665 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5670 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5673 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5674 */           setTransaction78, 
/*  5675 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5676 */               "setTransaction")), new QName("urn:CSSoapService", 
/*  5677 */             "setTransaction"));
/*       */ 
/*       */       
/*  5680 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5682 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5685 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5688 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5691 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5692 */           "In");
/*  5693 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5696 */       Object object = fromOM(
/*  5697 */           _returnEnv.getBody().getFirstElement(), 
/*  5698 */           SetTransactionResponse.class, 
/*  5699 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5702 */       return (SetTransactionResponse)object;
/*       */     }
/*  5704 */     catch (AxisFault f) {
/*       */       
/*  5706 */       OMElement faultElt = f.getDetail();
/*  5707 */       if (faultElt != null) {
/*  5708 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetTransaction"))) {
/*       */           
/*       */           try {
/*  5711 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetTransaction"));
/*  5712 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5713 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5714 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5716 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetTransaction"));
/*  5717 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5718 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5719 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5720 */                 new Class[] { messageClass });
/*  5721 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5724 */             throw new RemoteException(ex.getMessage(), ex);
/*  5725 */           } catch (ClassCastException e) {
/*       */             
/*  5727 */             throw f;
/*  5728 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5730 */             throw f;
/*  5731 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5733 */             throw f;
/*  5734 */           } catch (InvocationTargetException e) {
/*       */             
/*  5736 */             throw f;
/*  5737 */           } catch (IllegalAccessException e) {
/*       */             
/*  5739 */             throw f;
/*  5740 */           } catch (InstantiationException e) {
/*       */             
/*  5742 */             throw f;
/*       */           } 
/*       */         }
/*  5745 */         throw f;
/*       */       } 
/*       */       
/*  5748 */       throw f;
/*       */     } finally {
/*       */       
/*  5751 */       if (_messageContext.getTransportOut() != null) {
/*  5752 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetEntityResponse setEntity(SetEntity setEntity80) throws RemoteException {
/*  5775 */     MessageContext _messageContext = null;
/*       */     try {
/*  5777 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[40].getName());
/*  5778 */       _operationClient.getOptions().setAction("#SetEntity");
/*  5779 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5783 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5787 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5792 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5795 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5796 */           setEntity80, 
/*  5797 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5798 */               "setEntity")), new QName("urn:CSSoapService", 
/*  5799 */             "setEntity"));
/*       */ 
/*       */       
/*  5802 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5804 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5807 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5810 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5813 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5814 */           "In");
/*  5815 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5818 */       Object object = fromOM(
/*  5819 */           _returnEnv.getBody().getFirstElement(), 
/*  5820 */           SetEntityResponse.class, 
/*  5821 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5824 */       return (SetEntityResponse)object;
/*       */     }
/*  5826 */     catch (AxisFault f) {
/*       */       
/*  5828 */       OMElement faultElt = f.getDetail();
/*  5829 */       if (faultElt != null) {
/*  5830 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetEntity"))) {
/*       */           
/*       */           try {
/*  5833 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetEntity"));
/*  5834 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5835 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5836 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5838 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetEntity"));
/*  5839 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5840 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5841 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5842 */                 new Class[] { messageClass });
/*  5843 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5846 */             throw new RemoteException(ex.getMessage(), ex);
/*  5847 */           } catch (ClassCastException e) {
/*       */             
/*  5849 */             throw f;
/*  5850 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5852 */             throw f;
/*  5853 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5855 */             throw f;
/*  5856 */           } catch (InvocationTargetException e) {
/*       */             
/*  5858 */             throw f;
/*  5859 */           } catch (IllegalAccessException e) {
/*       */             
/*  5861 */             throw f;
/*  5862 */           } catch (InstantiationException e) {
/*       */             
/*  5864 */             throw f;
/*       */           } 
/*       */         }
/*  5867 */         throw f;
/*       */       } 
/*       */       
/*  5870 */       throw f;
/*       */     } finally {
/*       */       
/*  5873 */       if (_messageContext.getTransportOut() != null) {
/*  5874 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetClientChargeDefinitionsResponse getClientChargeDefinitions(GetClientChargeDefinitions getClientChargeDefinitions82) throws RemoteException {
/*  5897 */     MessageContext _messageContext = null;
/*       */     try {
/*  5899 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[41].getName());
/*  5900 */       _operationClient.getOptions().setAction("#GetClientChargeDefinitions");
/*  5901 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  5905 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  5909 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  5914 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  5917 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  5918 */           getClientChargeDefinitions82, 
/*  5919 */           optimizeContent(new QName("urn:CSSoapService", 
/*  5920 */               "getClientChargeDefinitions")), new QName("urn:CSSoapService", 
/*  5921 */             "getClientChargeDefinitions"));
/*       */ 
/*       */       
/*  5924 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  5926 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  5929 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  5932 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  5935 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  5936 */           "In");
/*  5937 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  5940 */       Object object = fromOM(
/*  5941 */           _returnEnv.getBody().getFirstElement(), 
/*  5942 */           GetClientChargeDefinitionsResponse.class, 
/*  5943 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  5946 */       return (GetClientChargeDefinitionsResponse)object;
/*       */     }
/*  5948 */     catch (AxisFault f) {
/*       */       
/*  5950 */       OMElement faultElt = f.getDetail();
/*  5951 */       if (faultElt != null) {
/*  5952 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetClientChargeDefinitions"))) {
/*       */           
/*       */           try {
/*  5955 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetClientChargeDefinitions"));
/*  5956 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  5957 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  5958 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  5960 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetClientChargeDefinitions"));
/*  5961 */             Class<?> messageClass = Class.forName(messageClassName);
/*  5962 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  5963 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  5964 */                 new Class[] { messageClass });
/*  5965 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  5968 */             throw new RemoteException(ex.getMessage(), ex);
/*  5969 */           } catch (ClassCastException e) {
/*       */             
/*  5971 */             throw f;
/*  5972 */           } catch (ClassNotFoundException e) {
/*       */             
/*  5974 */             throw f;
/*  5975 */           } catch (NoSuchMethodException e) {
/*       */             
/*  5977 */             throw f;
/*  5978 */           } catch (InvocationTargetException e) {
/*       */             
/*  5980 */             throw f;
/*  5981 */           } catch (IllegalAccessException e) {
/*       */             
/*  5983 */             throw f;
/*  5984 */           } catch (InstantiationException e) {
/*       */             
/*  5986 */             throw f;
/*       */           } 
/*       */         }
/*  5989 */         throw f;
/*       */       } 
/*       */       
/*  5992 */       throw f;
/*       */     } finally {
/*       */       
/*  5995 */       if (_messageContext.getTransportOut() != null) {
/*  5996 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetAccountingTransactionsResponse getAccountingTransactions(GetAccountingTransactions getAccountingTransactions84) throws RemoteException {
/*  6019 */     MessageContext _messageContext = null;
/*       */     try {
/*  6021 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[42].getName());
/*  6022 */       _operationClient.getOptions().setAction("#GetAccountingTransactions");
/*  6023 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6027 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6031 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6036 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6039 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6040 */           getAccountingTransactions84, 
/*  6041 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6042 */               "getAccountingTransactions")), new QName("urn:CSSoapService", 
/*  6043 */             "getAccountingTransactions"));
/*       */ 
/*       */       
/*  6046 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6048 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6051 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6054 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6057 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6058 */           "In");
/*  6059 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6062 */       Object object = fromOM(
/*  6063 */           _returnEnv.getBody().getFirstElement(), 
/*  6064 */           GetAccountingTransactionsResponse.class, 
/*  6065 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6068 */       return (GetAccountingTransactionsResponse)object;
/*       */     }
/*  6070 */     catch (AxisFault f) {
/*       */       
/*  6072 */       OMElement faultElt = f.getDetail();
/*  6073 */       if (faultElt != null) {
/*  6074 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetAccountingTransactions"))) {
/*       */           
/*       */           try {
/*  6077 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetAccountingTransactions"));
/*  6078 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6079 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6080 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6082 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetAccountingTransactions"));
/*  6083 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6084 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6085 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6086 */                 new Class[] { messageClass });
/*  6087 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6090 */             throw new RemoteException(ex.getMessage(), ex);
/*  6091 */           } catch (ClassCastException e) {
/*       */             
/*  6093 */             throw f;
/*  6094 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6096 */             throw f;
/*  6097 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6099 */             throw f;
/*  6100 */           } catch (InvocationTargetException e) {
/*       */             
/*  6102 */             throw f;
/*  6103 */           } catch (IllegalAccessException e) {
/*       */             
/*  6105 */             throw f;
/*  6106 */           } catch (InstantiationException e) {
/*       */             
/*  6108 */             throw f;
/*       */           } 
/*       */         }
/*  6111 */         throw f;
/*       */       } 
/*       */       
/*  6114 */       throw f;
/*       */     } finally {
/*       */       
/*  6117 */       if (_messageContext.getTransportOut() != null) {
/*  6118 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTripScheduleResponse getTripSchedule(GetTripSchedule getTripSchedule86) throws RemoteException {
/*  6141 */     MessageContext _messageContext = null;
/*       */     try {
/*  6143 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[43].getName());
/*  6144 */       _operationClient.getOptions().setAction("#GetTripSchedule");
/*  6145 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6149 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6153 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6158 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6161 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6162 */           getTripSchedule86, 
/*  6163 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6164 */               "getTripSchedule")), new QName("urn:CSSoapService", 
/*  6165 */             "getTripSchedule"));
/*       */ 
/*       */       
/*  6168 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6170 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6173 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6176 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6179 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6180 */           "In");
/*  6181 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6184 */       Object object = fromOM(
/*  6185 */           _returnEnv.getBody().getFirstElement(), 
/*  6186 */           GetTripScheduleResponse.class, 
/*  6187 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6190 */       return (GetTripScheduleResponse)object;
/*       */     }
/*  6192 */     catch (AxisFault f) {
/*       */       
/*  6194 */       OMElement faultElt = f.getDetail();
/*  6195 */       if (faultElt != null) {
/*  6196 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTripSchedule"))) {
/*       */           
/*       */           try {
/*  6199 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTripSchedule"));
/*  6200 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6201 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6202 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6204 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTripSchedule"));
/*  6205 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6206 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6207 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6208 */                 new Class[] { messageClass });
/*  6209 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6212 */             throw new RemoteException(ex.getMessage(), ex);
/*  6213 */           } catch (ClassCastException e) {
/*       */             
/*  6215 */             throw f;
/*  6216 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6218 */             throw f;
/*  6219 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6221 */             throw f;
/*  6222 */           } catch (InvocationTargetException e) {
/*       */             
/*  6224 */             throw f;
/*  6225 */           } catch (IllegalAccessException e) {
/*       */             
/*  6227 */             throw f;
/*  6228 */           } catch (InstantiationException e) {
/*       */             
/*  6230 */             throw f;
/*       */           } 
/*       */         }
/*  6233 */         throw f;
/*       */       } 
/*       */       
/*  6236 */       throw f;
/*       */     } finally {
/*       */       
/*  6239 */       if (_messageContext.getTransportOut() != null) {
/*  6240 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetEventDefinitionsResponse getEventDefinitions(GetEventDefinitions getEventDefinitions88) throws RemoteException {
/*  6263 */     MessageContext _messageContext = null;
/*       */     try {
/*  6265 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[44].getName());
/*  6266 */       _operationClient.getOptions().setAction("#GetEventDefinitions");
/*  6267 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6271 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6275 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6280 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6283 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6284 */           getEventDefinitions88, 
/*  6285 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6286 */               "getEventDefinitions")), new QName("urn:CSSoapService", 
/*  6287 */             "getEventDefinitions"));
/*       */ 
/*       */       
/*  6290 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6292 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6295 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6298 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6301 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6302 */           "In");
/*  6303 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6306 */       Object object = fromOM(
/*  6307 */           _returnEnv.getBody().getFirstElement(), 
/*  6308 */           GetEventDefinitionsResponse.class, 
/*  6309 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6312 */       return (GetEventDefinitionsResponse)object;
/*       */     }
/*  6314 */     catch (AxisFault f) {
/*       */       
/*  6316 */       OMElement faultElt = f.getDetail();
/*  6317 */       if (faultElt != null) {
/*  6318 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetEventDefinitions"))) {
/*       */           
/*       */           try {
/*  6321 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetEventDefinitions"));
/*  6322 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6323 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6324 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6326 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetEventDefinitions"));
/*  6327 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6328 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6329 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6330 */                 new Class[] { messageClass });
/*  6331 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6334 */             throw new RemoteException(ex.getMessage(), ex);
/*  6335 */           } catch (ClassCastException e) {
/*       */             
/*  6337 */             throw f;
/*  6338 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6340 */             throw f;
/*  6341 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6343 */             throw f;
/*  6344 */           } catch (InvocationTargetException e) {
/*       */             
/*  6346 */             throw f;
/*  6347 */           } catch (IllegalAccessException e) {
/*       */             
/*  6349 */             throw f;
/*  6350 */           } catch (InstantiationException e) {
/*       */             
/*  6352 */             throw f;
/*       */           } 
/*       */         }
/*  6355 */         throw f;
/*       */       } 
/*       */       
/*  6358 */       throw f;
/*       */     } finally {
/*       */       
/*  6361 */       if (_messageContext.getTransportOut() != null) {
/*  6362 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetCarrierRatesResponse getCarrierRates(GetCarrierRates getCarrierRates90) throws RemoteException {
/*  6385 */     MessageContext _messageContext = null;
/*       */     try {
/*  6387 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[45].getName());
/*  6388 */       _operationClient.getOptions().setAction("#GetCarrierRates");
/*  6389 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6393 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6397 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6402 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6405 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6406 */           getCarrierRates90, 
/*  6407 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6408 */               "getCarrierRates")), new QName("urn:CSSoapService", 
/*  6409 */             "getCarrierRates"));
/*       */ 
/*       */       
/*  6412 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6414 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6417 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6420 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6423 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6424 */           "In");
/*  6425 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6428 */       Object object = fromOM(
/*  6429 */           _returnEnv.getBody().getFirstElement(), 
/*  6430 */           GetCarrierRatesResponse.class, 
/*  6431 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6434 */       return (GetCarrierRatesResponse)object;
/*       */     }
/*  6436 */     catch (AxisFault f) {
/*       */       
/*  6438 */       OMElement faultElt = f.getDetail();
/*  6439 */       if (faultElt != null) {
/*  6440 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetCarrierRates"))) {
/*       */           
/*       */           try {
/*  6443 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetCarrierRates"));
/*  6444 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6445 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6446 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6448 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetCarrierRates"));
/*  6449 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6450 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6451 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6452 */                 new Class[] { messageClass });
/*  6453 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6456 */             throw new RemoteException(ex.getMessage(), ex);
/*  6457 */           } catch (ClassCastException e) {
/*       */             
/*  6459 */             throw f;
/*  6460 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6462 */             throw f;
/*  6463 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6465 */             throw f;
/*  6466 */           } catch (InvocationTargetException e) {
/*       */             
/*  6468 */             throw f;
/*  6469 */           } catch (IllegalAccessException e) {
/*       */             
/*  6471 */             throw f;
/*  6472 */           } catch (InstantiationException e) {
/*       */             
/*  6474 */             throw f;
/*       */           } 
/*       */         }
/*  6477 */         throw f;
/*       */       } 
/*       */       
/*  6480 */       throw f;
/*       */     } finally {
/*       */       
/*  6483 */       if (_messageContext.getTransportOut() != null) {
/*  6484 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetPODDataResponse getPODData(GetPODData getPODData92) throws RemoteException {
/*  6507 */     MessageContext _messageContext = null;
/*       */     try {
/*  6509 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[46].getName());
/*  6510 */       _operationClient.getOptions().setAction("#GetPODData");
/*  6511 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6515 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6519 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6524 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6527 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6528 */           getPODData92, 
/*  6529 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6530 */               "getPODData")), new QName("urn:CSSoapService", 
/*  6531 */             "getPODData"));
/*       */ 
/*       */       
/*  6534 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6536 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6539 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6542 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6545 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6546 */           "In");
/*  6547 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6550 */       Object object = fromOM(
/*  6551 */           _returnEnv.getBody().getFirstElement(), 
/*  6552 */           GetPODDataResponse.class, 
/*  6553 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6556 */       return (GetPODDataResponse)object;
/*       */     }
/*  6558 */     catch (AxisFault f) {
/*       */       
/*  6560 */       OMElement faultElt = f.getDetail();
/*  6561 */       if (faultElt != null) {
/*  6562 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetPODData"))) {
/*       */           
/*       */           try {
/*  6565 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetPODData"));
/*  6566 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6567 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6568 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6570 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetPODData"));
/*  6571 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6572 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6573 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6574 */                 new Class[] { messageClass });
/*  6575 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6578 */             throw new RemoteException(ex.getMessage(), ex);
/*  6579 */           } catch (ClassCastException e) {
/*       */             
/*  6581 */             throw f;
/*  6582 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6584 */             throw f;
/*  6585 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6587 */             throw f;
/*  6588 */           } catch (InvocationTargetException e) {
/*       */             
/*  6590 */             throw f;
/*  6591 */           } catch (IllegalAccessException e) {
/*       */             
/*  6593 */             throw f;
/*  6594 */           } catch (InstantiationException e) {
/*       */             
/*  6596 */             throw f;
/*       */           } 
/*       */         }
/*  6599 */         throw f;
/*       */       } 
/*       */       
/*  6602 */       throw f;
/*       */     } finally {
/*       */       
/*  6605 */       if (_messageContext.getTransportOut() != null) {
/*  6606 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetCustomFieldDefinitionsResponse getCustomFieldDefinitions(GetCustomFieldDefinitions getCustomFieldDefinitions94) throws RemoteException {
/*  6629 */     MessageContext _messageContext = null;
/*       */     try {
/*  6631 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[47].getName());
/*  6632 */       _operationClient.getOptions().setAction("#GetCustomFieldDefinitions");
/*  6633 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6637 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6641 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6646 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6649 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6650 */           getCustomFieldDefinitions94, 
/*  6651 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6652 */               "getCustomFieldDefinitions")), new QName("urn:CSSoapService", 
/*  6653 */             "getCustomFieldDefinitions"));
/*       */ 
/*       */       
/*  6656 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6658 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6661 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6664 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6667 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6668 */           "In");
/*  6669 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6672 */       Object object = fromOM(
/*  6673 */           _returnEnv.getBody().getFirstElement(), 
/*  6674 */           GetCustomFieldDefinitionsResponse.class, 
/*  6675 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6678 */       return (GetCustomFieldDefinitionsResponse)object;
/*       */     }
/*  6680 */     catch (AxisFault f) {
/*       */       
/*  6682 */       OMElement faultElt = f.getDetail();
/*  6683 */       if (faultElt != null) {
/*  6684 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetCustomFieldDefinitions"))) {
/*       */           
/*       */           try {
/*  6687 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetCustomFieldDefinitions"));
/*  6688 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6689 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6690 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6692 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetCustomFieldDefinitions"));
/*  6693 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6694 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6695 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6696 */                 new Class[] { messageClass });
/*  6697 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6700 */             throw new RemoteException(ex.getMessage(), ex);
/*  6701 */           } catch (ClassCastException e) {
/*       */             
/*  6703 */             throw f;
/*  6704 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6706 */             throw f;
/*  6707 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6709 */             throw f;
/*  6710 */           } catch (InvocationTargetException e) {
/*       */             
/*  6712 */             throw f;
/*  6713 */           } catch (IllegalAccessException e) {
/*       */             
/*  6715 */             throw f;
/*  6716 */           } catch (InstantiationException e) {
/*       */             
/*  6718 */             throw f;
/*       */           } 
/*       */         }
/*  6721 */         throw f;
/*       */       } 
/*       */       
/*  6724 */       throw f;
/*       */     } finally {
/*       */       
/*  6727 */       if (_messageContext.getTransportOut() != null) {
/*  6728 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SubmitCargoReleaseResponse submitCargoRelease(SubmitCargoRelease submitCargoRelease96) throws RemoteException {
/*  6751 */     MessageContext _messageContext = null;
/*       */     try {
/*  6753 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[48].getName());
/*  6754 */       _operationClient.getOptions().setAction("#SubmitCargoRelease");
/*  6755 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6759 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6763 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6768 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6771 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6772 */           submitCargoRelease96, 
/*  6773 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6774 */               "submitCargoRelease")), new QName("urn:CSSoapService", 
/*  6775 */             "submitCargoRelease"));
/*       */ 
/*       */       
/*  6778 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6780 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6783 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6786 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6789 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6790 */           "In");
/*  6791 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6794 */       Object object = fromOM(
/*  6795 */           _returnEnv.getBody().getFirstElement(), 
/*  6796 */           SubmitCargoReleaseResponse.class, 
/*  6797 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6800 */       return (SubmitCargoReleaseResponse)object;
/*       */     }
/*  6802 */     catch (AxisFault f) {
/*       */       
/*  6804 */       OMElement faultElt = f.getDetail();
/*  6805 */       if (faultElt != null) {
/*  6806 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SubmitCargoRelease"))) {
/*       */           
/*       */           try {
/*  6809 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SubmitCargoRelease"));
/*  6810 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6811 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6812 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6814 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SubmitCargoRelease"));
/*  6815 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6816 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6817 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6818 */                 new Class[] { messageClass });
/*  6819 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6822 */             throw new RemoteException(ex.getMessage(), ex);
/*  6823 */           } catch (ClassCastException e) {
/*       */             
/*  6825 */             throw f;
/*  6826 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6828 */             throw f;
/*  6829 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6831 */             throw f;
/*  6832 */           } catch (InvocationTargetException e) {
/*       */             
/*  6834 */             throw f;
/*  6835 */           } catch (IllegalAccessException e) {
/*       */             
/*  6837 */             throw f;
/*  6838 */           } catch (InstantiationException e) {
/*       */             
/*  6840 */             throw f;
/*       */           } 
/*       */         }
/*  6843 */         throw f;
/*       */       } 
/*       */       
/*  6846 */       throw f;
/*       */     } finally {
/*       */       
/*  6849 */       if (_messageContext.getTransportOut() != null) {
/*  6850 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetClientRatesResponse getClientRates(GetClientRates getClientRates98) throws RemoteException {
/*  6873 */     MessageContext _messageContext = null;
/*       */     try {
/*  6875 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[49].getName());
/*  6876 */       _operationClient.getOptions().setAction("#GetClientRates");
/*  6877 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  6881 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  6885 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  6890 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  6893 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  6894 */           getClientRates98, 
/*  6895 */           optimizeContent(new QName("urn:CSSoapService", 
/*  6896 */               "getClientRates")), new QName("urn:CSSoapService", 
/*  6897 */             "getClientRates"));
/*       */ 
/*       */       
/*  6900 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  6902 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  6905 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  6908 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  6911 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  6912 */           "In");
/*  6913 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  6916 */       Object object = fromOM(
/*  6917 */           _returnEnv.getBody().getFirstElement(), 
/*  6918 */           GetClientRatesResponse.class, 
/*  6919 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  6922 */       return (GetClientRatesResponse)object;
/*       */     }
/*  6924 */     catch (AxisFault f) {
/*       */       
/*  6926 */       OMElement faultElt = f.getDetail();
/*  6927 */       if (faultElt != null) {
/*  6928 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetClientRates"))) {
/*       */           
/*       */           try {
/*  6931 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetClientRates"));
/*  6932 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  6933 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  6934 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  6936 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetClientRates"));
/*  6937 */             Class<?> messageClass = Class.forName(messageClassName);
/*  6938 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  6939 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  6940 */                 new Class[] { messageClass });
/*  6941 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  6944 */             throw new RemoteException(ex.getMessage(), ex);
/*  6945 */           } catch (ClassCastException e) {
/*       */             
/*  6947 */             throw f;
/*  6948 */           } catch (ClassNotFoundException e) {
/*       */             
/*  6950 */             throw f;
/*  6951 */           } catch (NoSuchMethodException e) {
/*       */             
/*  6953 */             throw f;
/*  6954 */           } catch (InvocationTargetException e) {
/*       */             
/*  6956 */             throw f;
/*  6957 */           } catch (IllegalAccessException e) {
/*       */             
/*  6959 */             throw f;
/*  6960 */           } catch (InstantiationException e) {
/*       */             
/*  6962 */             throw f;
/*       */           } 
/*       */         }
/*  6965 */         throw f;
/*       */       } 
/*       */       
/*  6968 */       throw f;
/*       */     } finally {
/*       */       
/*  6971 */       if (_messageContext.getTransportOut() != null) {
/*  6972 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTransRangeByDateJSResponse getTransRangeByDateJS(GetTransRangeByDateJS getTransRangeByDateJS100) throws RemoteException {
/*  6995 */     MessageContext _messageContext = null;
/*       */     try {
/*  6997 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[50].getName());
/*  6998 */       _operationClient.getOptions().setAction("#GetTransRangeByDateJS");
/*  6999 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7003 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7007 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7012 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7015 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7016 */           getTransRangeByDateJS100, 
/*  7017 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7018 */               "getTransRangeByDateJS")), new QName("urn:CSSoapService", 
/*  7019 */             "getTransRangeByDateJS"));
/*       */ 
/*       */       
/*  7022 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7024 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7027 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7030 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7033 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7034 */           "In");
/*  7035 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7038 */       Object object = fromOM(
/*  7039 */           _returnEnv.getBody().getFirstElement(), 
/*  7040 */           GetTransRangeByDateJSResponse.class, 
/*  7041 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7044 */       return (GetTransRangeByDateJSResponse)object;
/*       */     }
/*  7046 */     catch (AxisFault f) {
/*       */       
/*  7048 */       OMElement faultElt = f.getDetail();
/*  7049 */       if (faultElt != null) {
/*  7050 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTransRangeByDateJS"))) {
/*       */           
/*       */           try {
/*  7053 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTransRangeByDateJS"));
/*  7054 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7055 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7056 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7058 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTransRangeByDateJS"));
/*  7059 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7060 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7061 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7062 */                 new Class[] { messageClass });
/*  7063 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7066 */             throw new RemoteException(ex.getMessage(), ex);
/*  7067 */           } catch (ClassCastException e) {
/*       */             
/*  7069 */             throw f;
/*  7070 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7072 */             throw f;
/*  7073 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7075 */             throw f;
/*  7076 */           } catch (InvocationTargetException e) {
/*       */             
/*  7078 */             throw f;
/*  7079 */           } catch (IllegalAccessException e) {
/*       */             
/*  7081 */             throw f;
/*  7082 */           } catch (InstantiationException e) {
/*       */             
/*  7084 */             throw f;
/*       */           } 
/*       */         }
/*  7087 */         throw f;
/*       */       } 
/*       */       
/*  7090 */       throw f;
/*       */     } finally {
/*       */       
/*  7093 */       if (_messageContext.getTransportOut() != null) {
/*  7094 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetAccountDefinitionsResponse getAccountDefinitions(GetAccountDefinitions getAccountDefinitions102) throws RemoteException {
/*  7117 */     MessageContext _messageContext = null;
/*       */     try {
/*  7119 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[51].getName());
/*  7120 */       _operationClient.getOptions().setAction("#GetAccountDefinitions");
/*  7121 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7125 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7129 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7134 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7137 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7138 */           getAccountDefinitions102, 
/*  7139 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7140 */               "getAccountDefinitions")), new QName("urn:CSSoapService", 
/*  7141 */             "getAccountDefinitions"));
/*       */ 
/*       */       
/*  7144 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7146 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7149 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7152 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7155 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7156 */           "In");
/*  7157 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7160 */       Object object = fromOM(
/*  7161 */           _returnEnv.getBody().getFirstElement(), 
/*  7162 */           GetAccountDefinitionsResponse.class, 
/*  7163 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7166 */       return (GetAccountDefinitionsResponse)object;
/*       */     }
/*  7168 */     catch (AxisFault f) {
/*       */       
/*  7170 */       OMElement faultElt = f.getDetail();
/*  7171 */       if (faultElt != null) {
/*  7172 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetAccountDefinitions"))) {
/*       */           
/*       */           try {
/*  7175 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetAccountDefinitions"));
/*  7176 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7177 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7178 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7180 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetAccountDefinitions"));
/*  7181 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7182 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7183 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7184 */                 new Class[] { messageClass });
/*  7185 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7188 */             throw new RemoteException(ex.getMessage(), ex);
/*  7189 */           } catch (ClassCastException e) {
/*       */             
/*  7191 */             throw f;
/*  7192 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7194 */             throw f;
/*  7195 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7197 */             throw f;
/*  7198 */           } catch (InvocationTargetException e) {
/*       */             
/*  7200 */             throw f;
/*  7201 */           } catch (IllegalAccessException e) {
/*       */             
/*  7203 */             throw f;
/*  7204 */           } catch (InstantiationException e) {
/*       */             
/*  7206 */             throw f;
/*       */           } 
/*       */         }
/*  7209 */         throw f;
/*       */       } 
/*       */       
/*  7212 */       throw f;
/*       */     } finally {
/*       */       
/*  7215 */       if (_messageContext.getTransportOut() != null) {
/*  7216 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetApprovalStatusResponse setApprovalStatus(SetApprovalStatus setApprovalStatus104) throws RemoteException {
/*  7239 */     MessageContext _messageContext = null;
/*       */     try {
/*  7241 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[52].getName());
/*  7242 */       _operationClient.getOptions().setAction("#SetApprovalStatus");
/*  7243 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7247 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7251 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7256 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7259 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7260 */           setApprovalStatus104, 
/*  7261 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7262 */               "setApprovalStatus")), new QName("urn:CSSoapService", 
/*  7263 */             "setApprovalStatus"));
/*       */ 
/*       */       
/*  7266 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7268 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7271 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7274 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7277 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7278 */           "In");
/*  7279 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7282 */       Object object = fromOM(
/*  7283 */           _returnEnv.getBody().getFirstElement(), 
/*  7284 */           SetApprovalStatusResponse.class, 
/*  7285 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7288 */       return (SetApprovalStatusResponse)object;
/*       */     }
/*  7290 */     catch (AxisFault f) {
/*       */       
/*  7292 */       OMElement faultElt = f.getDetail();
/*  7293 */       if (faultElt != null) {
/*  7294 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetApprovalStatus"))) {
/*       */           
/*       */           try {
/*  7297 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetApprovalStatus"));
/*  7298 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7299 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7300 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7302 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetApprovalStatus"));
/*  7303 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7304 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7305 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7306 */                 new Class[] { messageClass });
/*  7307 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7310 */             throw new RemoteException(ex.getMessage(), ex);
/*  7311 */           } catch (ClassCastException e) {
/*       */             
/*  7313 */             throw f;
/*  7314 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7316 */             throw f;
/*  7317 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7319 */             throw f;
/*  7320 */           } catch (InvocationTargetException e) {
/*       */             
/*  7322 */             throw f;
/*  7323 */           } catch (IllegalAccessException e) {
/*       */             
/*  7325 */             throw f;
/*  7326 */           } catch (InstantiationException e) {
/*       */             
/*  7328 */             throw f;
/*       */           } 
/*       */         }
/*  7331 */         throw f;
/*       */       } 
/*       */       
/*  7334 */       throw f;
/*       */     } finally {
/*       */       
/*  7337 */       if (_messageContext.getTransportOut() != null) {
/*  7338 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetRelatedTransactionsResponse getRelatedTransactions(GetRelatedTransactions getRelatedTransactions106) throws RemoteException {
/*  7361 */     MessageContext _messageContext = null;
/*       */     try {
/*  7363 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[53].getName());
/*  7364 */       _operationClient.getOptions().setAction("#GetRelatedTransactions");
/*  7365 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7369 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7373 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7378 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7381 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7382 */           getRelatedTransactions106, 
/*  7383 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7384 */               "getRelatedTransactions")), new QName("urn:CSSoapService", 
/*  7385 */             "getRelatedTransactions"));
/*       */ 
/*       */       
/*  7388 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7390 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7393 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7396 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7399 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7400 */           "In");
/*  7401 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7404 */       Object object = fromOM(
/*  7405 */           _returnEnv.getBody().getFirstElement(), 
/*  7406 */           GetRelatedTransactionsResponse.class, 
/*  7407 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7410 */       return (GetRelatedTransactionsResponse)object;
/*       */     }
/*  7412 */     catch (AxisFault f) {
/*       */       
/*  7414 */       OMElement faultElt = f.getDetail();
/*  7415 */       if (faultElt != null) {
/*  7416 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetRelatedTransactions"))) {
/*       */           
/*       */           try {
/*  7419 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetRelatedTransactions"));
/*  7420 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7421 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7422 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7424 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetRelatedTransactions"));
/*  7425 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7426 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7427 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7428 */                 new Class[] { messageClass });
/*  7429 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7432 */             throw new RemoteException(ex.getMessage(), ex);
/*  7433 */           } catch (ClassCastException e) {
/*       */             
/*  7435 */             throw f;
/*  7436 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7438 */             throw f;
/*  7439 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7441 */             throw f;
/*  7442 */           } catch (InvocationTargetException e) {
/*       */             
/*  7444 */             throw f;
/*  7445 */           } catch (IllegalAccessException e) {
/*       */             
/*  7447 */             throw f;
/*  7448 */           } catch (InstantiationException e) {
/*       */             
/*  7450 */             throw f;
/*       */           } 
/*       */         }
/*  7453 */         throw f;
/*       */       } 
/*       */       
/*  7456 */       throw f;
/*       */     } finally {
/*       */       
/*  7459 */       if (_messageContext.getTransportOut() != null) {
/*  7460 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetCustomFieldValueResponse setCustomFieldValue(SetCustomFieldValue setCustomFieldValue108) throws RemoteException {
/*  7483 */     MessageContext _messageContext = null;
/*       */     try {
/*  7485 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[54].getName());
/*  7486 */       _operationClient.getOptions().setAction("#SetCustomFieldValue");
/*  7487 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7491 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7495 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7500 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7503 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7504 */           setCustomFieldValue108, 
/*  7505 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7506 */               "setCustomFieldValue")), new QName("urn:CSSoapService", 
/*  7507 */             "setCustomFieldValue"));
/*       */ 
/*       */       
/*  7510 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7512 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7515 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7518 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7521 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7522 */           "In");
/*  7523 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7526 */       Object object = fromOM(
/*  7527 */           _returnEnv.getBody().getFirstElement(), 
/*  7528 */           SetCustomFieldValueResponse.class, 
/*  7529 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7532 */       return (SetCustomFieldValueResponse)object;
/*       */     }
/*  7534 */     catch (AxisFault f) {
/*       */       
/*  7536 */       OMElement faultElt = f.getDetail();
/*  7537 */       if (faultElt != null) {
/*  7538 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetCustomFieldValue"))) {
/*       */           
/*       */           try {
/*  7541 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetCustomFieldValue"));
/*  7542 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7543 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7544 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7546 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetCustomFieldValue"));
/*  7547 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7548 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7549 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7550 */                 new Class[] { messageClass });
/*  7551 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7554 */             throw new RemoteException(ex.getMessage(), ex);
/*  7555 */           } catch (ClassCastException e) {
/*       */             
/*  7557 */             throw f;
/*  7558 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7560 */             throw f;
/*  7561 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7563 */             throw f;
/*  7564 */           } catch (InvocationTargetException e) {
/*       */             
/*  7566 */             throw f;
/*  7567 */           } catch (IllegalAccessException e) {
/*       */             
/*  7569 */             throw f;
/*  7570 */           } catch (InstantiationException e) {
/*       */             
/*  7572 */             throw f;
/*       */           } 
/*       */         }
/*  7575 */         throw f;
/*       */       } 
/*       */       
/*  7578 */       throw f;
/*       */     } finally {
/*       */       
/*  7581 */       if (_messageContext.getTransportOut() != null) {
/*  7582 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetEntitiesResponse getEntities(GetEntities getEntities110) throws RemoteException {
/*  7605 */     MessageContext _messageContext = null;
/*       */     try {
/*  7607 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[55].getName());
/*  7608 */       _operationClient.getOptions().setAction("#GetEntities");
/*  7609 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7613 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7617 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7622 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7625 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7626 */           getEntities110, 
/*  7627 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7628 */               "getEntities")), new QName("urn:CSSoapService", 
/*  7629 */             "getEntities"));
/*       */ 
/*       */       
/*  7632 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7634 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7637 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7640 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7643 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7644 */           "In");
/*  7645 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7648 */       Object object = fromOM(
/*  7649 */           _returnEnv.getBody().getFirstElement(), 
/*  7650 */           GetEntitiesResponse.class, 
/*  7651 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7654 */       return (GetEntitiesResponse)object;
/*       */     }
/*  7656 */     catch (AxisFault f) {
/*       */       
/*  7658 */       OMElement faultElt = f.getDetail();
/*  7659 */       if (faultElt != null) {
/*  7660 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetEntities"))) {
/*       */           
/*       */           try {
/*  7663 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetEntities"));
/*  7664 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7665 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7666 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7668 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetEntities"));
/*  7669 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7670 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7671 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7672 */                 new Class[] { messageClass });
/*  7673 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7676 */             throw new RemoteException(ex.getMessage(), ex);
/*  7677 */           } catch (ClassCastException e) {
/*       */             
/*  7679 */             throw f;
/*  7680 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7682 */             throw f;
/*  7683 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7685 */             throw f;
/*  7686 */           } catch (InvocationTargetException e) {
/*       */             
/*  7688 */             throw f;
/*  7689 */           } catch (IllegalAccessException e) {
/*       */             
/*  7691 */             throw f;
/*  7692 */           } catch (InstantiationException e) {
/*       */             
/*  7694 */             throw f;
/*       */           } 
/*       */         }
/*  7697 */         throw f;
/*       */       } 
/*       */       
/*  7700 */       throw f;
/*       */     } finally {
/*       */       
/*  7703 */       if (_messageContext.getTransportOut() != null) {
/*  7704 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public AnswerInvitationResponse answerInvitation(AnswerInvitation answerInvitation112) throws RemoteException {
/*  7727 */     MessageContext _messageContext = null;
/*       */     try {
/*  7729 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[56].getName());
/*  7730 */       _operationClient.getOptions().setAction("#AnswerInvitation");
/*  7731 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7735 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7739 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7744 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7747 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7748 */           answerInvitation112, 
/*  7749 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7750 */               "answerInvitation")), new QName("urn:CSSoapService", 
/*  7751 */             "answerInvitation"));
/*       */ 
/*       */       
/*  7754 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7756 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7759 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7762 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7765 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7766 */           "In");
/*  7767 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7770 */       Object object = fromOM(
/*  7771 */           _returnEnv.getBody().getFirstElement(), 
/*  7772 */           AnswerInvitationResponse.class, 
/*  7773 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7776 */       return (AnswerInvitationResponse)object;
/*       */     }
/*  7778 */     catch (AxisFault f) {
/*       */       
/*  7780 */       OMElement faultElt = f.getDetail();
/*  7781 */       if (faultElt != null) {
/*  7782 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "AnswerInvitation"))) {
/*       */           
/*       */           try {
/*  7785 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "AnswerInvitation"));
/*  7786 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7787 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7788 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7790 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "AnswerInvitation"));
/*  7791 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7792 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7793 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7794 */                 new Class[] { messageClass });
/*  7795 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7798 */             throw new RemoteException(ex.getMessage(), ex);
/*  7799 */           } catch (ClassCastException e) {
/*       */             
/*  7801 */             throw f;
/*  7802 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7804 */             throw f;
/*  7805 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7807 */             throw f;
/*  7808 */           } catch (InvocationTargetException e) {
/*       */             
/*  7810 */             throw f;
/*  7811 */           } catch (IllegalAccessException e) {
/*       */             
/*  7813 */             throw f;
/*  7814 */           } catch (InstantiationException e) {
/*       */             
/*  7816 */             throw f;
/*       */           } 
/*       */         }
/*  7819 */         throw f;
/*       */       } 
/*       */       
/*  7822 */       throw f;
/*       */     } finally {
/*       */       
/*  7825 */       if (_messageContext.getTransportOut() != null) {
/*  7826 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public TestConnectionResponse testConnection(TestConnection testConnection114) throws RemoteException {
/*  7849 */     MessageContext _messageContext = null;
/*       */     try {
/*  7851 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[57].getName());
/*  7852 */       _operationClient.getOptions().setAction("#TestConnection");
/*  7853 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7857 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7861 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7866 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7869 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7870 */           testConnection114, 
/*  7871 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7872 */               "testConnection")), new QName("urn:CSSoapService", 
/*  7873 */             "testConnection"));
/*       */ 
/*       */       
/*  7876 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  7878 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  7881 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  7884 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  7887 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  7888 */           "In");
/*  7889 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  7892 */       Object object = fromOM(
/*  7893 */           _returnEnv.getBody().getFirstElement(), 
/*  7894 */           TestConnectionResponse.class, 
/*  7895 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  7898 */       return (TestConnectionResponse)object;
/*       */     }
/*  7900 */     catch (AxisFault f) {
/*       */       
/*  7902 */       OMElement faultElt = f.getDetail();
/*  7903 */       if (faultElt != null) {
/*  7904 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "TestConnection"))) {
/*       */           
/*       */           try {
/*  7907 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "TestConnection"));
/*  7908 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  7909 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  7910 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  7912 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "TestConnection"));
/*  7913 */             Class<?> messageClass = Class.forName(messageClassName);
/*  7914 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  7915 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  7916 */                 new Class[] { messageClass });
/*  7917 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  7920 */             throw new RemoteException(ex.getMessage(), ex);
/*  7921 */           } catch (ClassCastException e) {
/*       */             
/*  7923 */             throw f;
/*  7924 */           } catch (ClassNotFoundException e) {
/*       */             
/*  7926 */             throw f;
/*  7927 */           } catch (NoSuchMethodException e) {
/*       */             
/*  7929 */             throw f;
/*  7930 */           } catch (InvocationTargetException e) {
/*       */             
/*  7932 */             throw f;
/*  7933 */           } catch (IllegalAccessException e) {
/*       */             
/*  7935 */             throw f;
/*  7936 */           } catch (InstantiationException e) {
/*       */             
/*  7938 */             throw f;
/*       */           } 
/*       */         }
/*  7941 */         throw f;
/*       */       } 
/*       */       
/*  7944 */       throw f;
/*       */     } finally {
/*       */       
/*  7947 */       if (_messageContext.getTransportOut() != null) {
/*  7948 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTransRangeByDateResponse getTransRangeByDate(GetTransRangeByDate getTransRangeByDate116) throws RemoteException {
/*  7971 */     MessageContext _messageContext = null;
/*       */     try {
/*  7973 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[58].getName());
/*  7974 */       _operationClient.getOptions().setAction("#GetTransRangeByDate");
/*  7975 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  7979 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  7983 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  7988 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  7991 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  7992 */           getTransRangeByDate116, 
/*  7993 */           optimizeContent(new QName("urn:CSSoapService", 
/*  7994 */               "getTransRangeByDate")), new QName("urn:CSSoapService", 
/*  7995 */             "getTransRangeByDate"));
/*       */ 
/*       */       
/*  7998 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  8000 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  8003 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  8006 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  8009 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  8010 */           "In");
/*  8011 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  8014 */       Object object = fromOM(
/*  8015 */           _returnEnv.getBody().getFirstElement(), 
/*  8016 */           GetTransRangeByDateResponse.class, 
/*  8017 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  8020 */       return (GetTransRangeByDateResponse)object;
/*       */     }
/*  8022 */     catch (AxisFault f) {
/*       */       
/*  8024 */       OMElement faultElt = f.getDetail();
/*  8025 */       if (faultElt != null) {
/*  8026 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTransRangeByDate"))) {
/*       */           
/*       */           try {
/*  8029 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTransRangeByDate"));
/*  8030 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  8031 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  8032 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  8034 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTransRangeByDate"));
/*  8035 */             Class<?> messageClass = Class.forName(messageClassName);
/*  8036 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  8037 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  8038 */                 new Class[] { messageClass });
/*  8039 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  8042 */             throw new RemoteException(ex.getMessage(), ex);
/*  8043 */           } catch (ClassCastException e) {
/*       */             
/*  8045 */             throw f;
/*  8046 */           } catch (ClassNotFoundException e) {
/*       */             
/*  8048 */             throw f;
/*  8049 */           } catch (NoSuchMethodException e) {
/*       */             
/*  8051 */             throw f;
/*  8052 */           } catch (InvocationTargetException e) {
/*       */             
/*  8054 */             throw f;
/*  8055 */           } catch (IllegalAccessException e) {
/*       */             
/*  8057 */             throw f;
/*  8058 */           } catch (InstantiationException e) {
/*       */             
/*  8060 */             throw f;
/*       */           } 
/*       */         }
/*  8063 */         throw f;
/*       */       } 
/*       */       
/*  8066 */       throw f;
/*       */     } finally {
/*       */       
/*  8069 */       if (_messageContext.getTransportOut() != null) {
/*  8070 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetTransactionsByBillingClientResponse getTransactionsByBillingClient(GetTransactionsByBillingClient getTransactionsByBillingClient118) throws RemoteException {
/*  8093 */     MessageContext _messageContext = null;
/*       */     try {
/*  8095 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[59].getName());
/*  8096 */       _operationClient.getOptions().setAction("#GetTransactionsByBillingClient");
/*  8097 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  8101 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  8105 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  8110 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  8113 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  8114 */           getTransactionsByBillingClient118, 
/*  8115 */           optimizeContent(new QName("urn:CSSoapService", 
/*  8116 */               "getTransactionsByBillingClient")), new QName("urn:CSSoapService", 
/*  8117 */             "getTransactionsByBillingClient"));
/*       */ 
/*       */       
/*  8120 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  8122 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  8125 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  8128 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  8131 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  8132 */           "In");
/*  8133 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  8136 */       Object object = fromOM(
/*  8137 */           _returnEnv.getBody().getFirstElement(), 
/*  8138 */           GetTransactionsByBillingClientResponse.class, 
/*  8139 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  8142 */       return (GetTransactionsByBillingClientResponse)object;
/*       */     }
/*  8144 */     catch (AxisFault f) {
/*       */       
/*  8146 */       OMElement faultElt = f.getDetail();
/*  8147 */       if (faultElt != null) {
/*  8148 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetTransactionsByBillingClient"))) {
/*       */           
/*       */           try {
/*  8151 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetTransactionsByBillingClient"));
/*  8152 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  8153 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  8154 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  8156 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetTransactionsByBillingClient"));
/*  8157 */             Class<?> messageClass = Class.forName(messageClassName);
/*  8158 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  8159 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  8160 */                 new Class[] { messageClass });
/*  8161 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  8164 */             throw new RemoteException(ex.getMessage(), ex);
/*  8165 */           } catch (ClassCastException e) {
/*       */             
/*  8167 */             throw f;
/*  8168 */           } catch (ClassNotFoundException e) {
/*       */             
/*  8170 */             throw f;
/*  8171 */           } catch (NoSuchMethodException e) {
/*       */             
/*  8173 */             throw f;
/*  8174 */           } catch (InvocationTargetException e) {
/*       */             
/*  8176 */             throw f;
/*  8177 */           } catch (IllegalAccessException e) {
/*       */             
/*  8179 */             throw f;
/*  8180 */           } catch (InstantiationException e) {
/*       */             
/*  8182 */             throw f;
/*       */           } 
/*       */         }
/*  8185 */         throw f;
/*       */       } 
/*       */       
/*  8188 */       throw f;
/*       */     } finally {
/*       */       
/*  8191 */       if (_messageContext.getTransportOut() != null) {
/*  8192 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public SetTrackingUserResponse setTrackingUser(SetTrackingUser setTrackingUser120) throws RemoteException {
/*  8215 */     MessageContext _messageContext = null;
/*       */     try {
/*  8217 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[60].getName());
/*  8218 */       _operationClient.getOptions().setAction("#SetTrackingUser");
/*  8219 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  8223 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  8227 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  8232 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  8235 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  8236 */           setTrackingUser120, 
/*  8237 */           optimizeContent(new QName("urn:CSSoapService", 
/*  8238 */               "setTrackingUser")), new QName("urn:CSSoapService", 
/*  8239 */             "setTrackingUser"));
/*       */ 
/*       */       
/*  8242 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  8244 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  8247 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  8250 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  8253 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  8254 */           "In");
/*  8255 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  8258 */       Object object = fromOM(
/*  8259 */           _returnEnv.getBody().getFirstElement(), 
/*  8260 */           SetTrackingUserResponse.class, 
/*  8261 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  8264 */       return (SetTrackingUserResponse)object;
/*       */     }
/*  8266 */     catch (AxisFault f) {
/*       */       
/*  8268 */       OMElement faultElt = f.getDetail();
/*  8269 */       if (faultElt != null) {
/*  8270 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "SetTrackingUser"))) {
/*       */           
/*       */           try {
/*  8273 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "SetTrackingUser"));
/*  8274 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  8275 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  8276 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  8278 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "SetTrackingUser"));
/*  8279 */             Class<?> messageClass = Class.forName(messageClassName);
/*  8280 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  8281 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  8282 */                 new Class[] { messageClass });
/*  8283 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  8286 */             throw new RemoteException(ex.getMessage(), ex);
/*  8287 */           } catch (ClassCastException e) {
/*       */             
/*  8289 */             throw f;
/*  8290 */           } catch (ClassNotFoundException e) {
/*       */             
/*  8292 */             throw f;
/*  8293 */           } catch (NoSuchMethodException e) {
/*       */             
/*  8295 */             throw f;
/*  8296 */           } catch (InvocationTargetException e) {
/*       */             
/*  8298 */             throw f;
/*  8299 */           } catch (IllegalAccessException e) {
/*       */             
/*  8301 */             throw f;
/*  8302 */           } catch (InstantiationException e) {
/*       */             
/*  8304 */             throw f;
/*       */           } 
/*       */         }
/*  8307 */         throw f;
/*       */       } 
/*       */       
/*  8310 */       throw f;
/*       */     } finally {
/*       */       
/*  8313 */       if (_messageContext.getTransportOut() != null) {
/*  8314 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public AnswerInvitation2Response answerInvitation2(AnswerInvitation2 answerInvitation2122) throws RemoteException {
/*  8337 */     MessageContext _messageContext = null;
/*       */     try {
/*  8339 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[61].getName());
/*  8340 */       _operationClient.getOptions().setAction("#AnswerInvitation2");
/*  8341 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  8345 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  8349 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  8354 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  8357 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  8358 */           answerInvitation2122, 
/*  8359 */           optimizeContent(new QName("urn:CSSoapService", 
/*  8360 */               "answerInvitation2")), new QName("urn:CSSoapService", 
/*  8361 */             "answerInvitation2"));
/*       */ 
/*       */       
/*  8364 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  8366 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  8369 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  8372 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  8375 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  8376 */           "In");
/*  8377 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  8380 */       Object object = fromOM(
/*  8381 */           _returnEnv.getBody().getFirstElement(), 
/*  8382 */           AnswerInvitation2Response.class, 
/*  8383 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  8386 */       return (AnswerInvitation2Response)object;
/*       */     }
/*  8388 */     catch (AxisFault f) {
/*       */       
/*  8390 */       OMElement faultElt = f.getDetail();
/*  8391 */       if (faultElt != null) {
/*  8392 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "AnswerInvitation2"))) {
/*       */           
/*       */           try {
/*  8395 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "AnswerInvitation2"));
/*  8396 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  8397 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  8398 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  8400 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "AnswerInvitation2"));
/*  8401 */             Class<?> messageClass = Class.forName(messageClassName);
/*  8402 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  8403 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  8404 */                 new Class[] { messageClass });
/*  8405 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  8408 */             throw new RemoteException(ex.getMessage(), ex);
/*  8409 */           } catch (ClassCastException e) {
/*       */             
/*  8411 */             throw f;
/*  8412 */           } catch (ClassNotFoundException e) {
/*       */             
/*  8414 */             throw f;
/*  8415 */           } catch (NoSuchMethodException e) {
/*       */             
/*  8417 */             throw f;
/*  8418 */           } catch (InvocationTargetException e) {
/*       */             
/*  8420 */             throw f;
/*  8421 */           } catch (IllegalAccessException e) {
/*       */             
/*  8423 */             throw f;
/*  8424 */           } catch (InstantiationException e) {
/*       */             
/*  8426 */             throw f;
/*       */           } 
/*       */         }
/*  8429 */         throw f;
/*       */       } 
/*       */       
/*  8432 */       throw f;
/*       */     } finally {
/*       */       
/*  8435 */       if (_messageContext.getTransportOut() != null) {
/*  8436 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   public GetDocumentResponse getDocument(GetDocument getDocument124) throws RemoteException {
/*  8459 */     MessageContext _messageContext = null;
/*       */     try {
/*  8461 */       OperationClient _operationClient = this._serviceClient.createClient(this._operations[62].getName());
/*  8462 */       _operationClient.getOptions().setAction("#GetDocument");
/*  8463 */       _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
/*       */ 
/*       */ 
/*       */       
/*  8467 */       addPropertyToOperationClient(_operationClient, "whttp:queryParameterSeparator", "&");
/*       */ 
/*       */ 
/*       */       
/*  8471 */       _messageContext = new MessageContext();
/*       */ 
/*       */ 
/*       */ 
/*       */       
/*  8476 */       SOAPEnvelope env = null;
/*       */ 
/*       */       
/*  8479 */       env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), 
/*  8480 */           getDocument124, 
/*  8481 */           optimizeContent(new QName("urn:CSSoapService", 
/*  8482 */               "getDocument")), new QName("urn:CSSoapService", 
/*  8483 */             "getDocument"));
/*       */ 
/*       */       
/*  8486 */       this._serviceClient.addHeadersToEnvelope(env);
/*       */       
/*  8488 */       _messageContext.setEnvelope(env);
/*       */ 
/*       */       
/*  8491 */       _operationClient.addMessageContext(_messageContext);
/*       */ 
/*       */       
/*  8494 */       _operationClient.execute(true);
/*       */ 
/*       */       
/*  8497 */       MessageContext _returnMessageContext = _operationClient.getMessageContext(
/*  8498 */           "In");
/*  8499 */       SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
/*       */ 
/*       */       
/*  8502 */       Object object = fromOM(
/*  8503 */           _returnEnv.getBody().getFirstElement(), 
/*  8504 */           GetDocumentResponse.class, 
/*  8505 */           getEnvelopeNamespaces(_returnEnv));
/*       */ 
/*       */       
/*  8508 */       return (GetDocumentResponse)object;
/*       */     }
/*  8510 */     catch (AxisFault f) {
/*       */       
/*  8512 */       OMElement faultElt = f.getDetail();
/*  8513 */       if (faultElt != null) {
/*  8514 */         if (this.faultExceptionNameMap.containsKey(new FaultMapKey(faultElt.getQName(), "GetDocument"))) {
/*       */           
/*       */           try {
/*  8517 */             String exceptionClassName = (String)this.faultExceptionClassNameMap.get(new FaultMapKey(faultElt.getQName(), "GetDocument"));
/*  8518 */             Class<?> exceptionClass = Class.forName(exceptionClassName);
/*  8519 */             Constructor<?> constructor = exceptionClass.getConstructor(new Class[] { String.class });
/*  8520 */             Exception ex = (Exception)constructor.newInstance(new Object[] { f.getMessage() });
/*       */             
/*  8522 */             String messageClassName = (String)this.faultMessageMap.get(new FaultMapKey(faultElt.getQName(), "GetDocument"));
/*  8523 */             Class<?> messageClass = Class.forName(messageClassName);
/*  8524 */             Object messageObject = fromOM(faultElt, messageClass, (Map)null);
/*  8525 */             Method m = exceptionClass.getMethod("setFaultMessage", 
/*  8526 */                 new Class[] { messageClass });
/*  8527 */             m.invoke(ex, new Object[] { messageObject });
/*       */ 
/*       */             
/*  8530 */             throw new RemoteException(ex.getMessage(), ex);
/*  8531 */           } catch (ClassCastException e) {
/*       */             
/*  8533 */             throw f;
/*  8534 */           } catch (ClassNotFoundException e) {
/*       */             
/*  8536 */             throw f;
/*  8537 */           } catch (NoSuchMethodException e) {
/*       */             
/*  8539 */             throw f;
/*  8540 */           } catch (InvocationTargetException e) {
/*       */             
/*  8542 */             throw f;
/*  8543 */           } catch (IllegalAccessException e) {
/*       */             
/*  8545 */             throw f;
/*  8546 */           } catch (InstantiationException e) {
/*       */             
/*  8548 */             throw f;
/*       */           } 
/*       */         }
/*  8551 */         throw f;
/*       */       } 
/*       */       
/*  8554 */       throw f;
/*       */     } finally {
/*       */       
/*  8557 */       if (_messageContext.getTransportOut() != null) {
/*  8558 */         _messageContext.getTransportOut().getSender().cleanup(_messageContext);
/*       */       }
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private Map getEnvelopeNamespaces(SOAPEnvelope env) {
/*  8569 */     Map<Object, Object> returnMap = new HashMap<Object, Object>();
/*  8570 */     Iterator<OMNamespace> namespaceIterator = env.getAllDeclaredNamespaces();
/*  8571 */     while (namespaceIterator.hasNext()) {
/*  8572 */       OMNamespace ns = namespaceIterator.next();
/*  8573 */       returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
/*       */     } 
/*  8575 */     return returnMap;
/*       */   }
/*       */   
/*       */   public CSSoapServiceStub(ConfigurationContext configurationContext, String targetEndpoint, boolean useSeparateListener) throws AxisFault
/*       */   {
/*  8580 */     this.opNameArray = null; populateAxisService();
/*       */     populateFaults();
/*       */     this._serviceClient = new ServiceClient(configurationContext, this._service);
/*       */     this._serviceClient.getOptions().setTo(new EndpointReference(targetEndpoint));
/*  8584 */     this._serviceClient.getOptions().setUseSeparateListener(useSeparateListener); } private boolean optimizeContent(QName opName) { if (this.opNameArray == null) {
/*  8585 */       return false;
/*       */     }
/*  8587 */     for (int i = 0; i < this.opNameArray.length; i++) {
/*  8588 */       if (opName.equals(this.opNameArray[i])) {
/*  8589 */         return true;
/*       */       }
/*       */     } 
/*  8592 */     return false; }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(ExistsTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8600 */       return param.getOMElement(ExistsTransaction.MY_QNAME, 
/*  8601 */           OMAbstractFactory.getOMFactory());
/*  8602 */     } catch (ADBException e) {
/*  8603 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(ExistsTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8614 */       return param.getOMElement(ExistsTransactionResponse.MY_QNAME, 
/*  8615 */           OMAbstractFactory.getOMFactory());
/*  8616 */     } catch (ADBException e) {
/*  8617 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTrackingTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8628 */       return param.getOMElement(GetTrackingTransaction.MY_QNAME, 
/*  8629 */           OMAbstractFactory.getOMFactory());
/*  8630 */     } catch (ADBException e) {
/*  8631 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTrackingTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8642 */       return param.getOMElement(GetTrackingTransactionResponse.MY_QNAME, 
/*  8643 */           OMAbstractFactory.getOMFactory());
/*  8644 */     } catch (ADBException e) {
/*  8645 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetFirstTransbyDate param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8656 */       return param.getOMElement(GetFirstTransbyDate.MY_QNAME, 
/*  8657 */           OMAbstractFactory.getOMFactory());
/*  8658 */     } catch (ADBException e) {
/*  8659 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetFirstTransbyDateResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8670 */       return param.getOMElement(GetFirstTransbyDateResponse.MY_QNAME, 
/*  8671 */           OMAbstractFactory.getOMFactory());
/*  8672 */     } catch (ADBException e) {
/*  8673 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetRate param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8684 */       return param.getOMElement(SetRate.MY_QNAME, 
/*  8685 */           OMAbstractFactory.getOMFactory());
/*  8686 */     } catch (ADBException e) {
/*  8687 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetRateResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8698 */       return param.getOMElement(SetRateResponse.MY_QNAME, 
/*  8699 */           OMAbstractFactory.getOMFactory());
/*  8700 */     } catch (ADBException e) {
/*  8701 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(QueryLog param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8712 */       return param.getOMElement(QueryLog.MY_QNAME, 
/*  8713 */           OMAbstractFactory.getOMFactory());
/*  8714 */     } catch (ADBException e) {
/*  8715 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(QueryLogResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8726 */       return param.getOMElement(QueryLogResponse.MY_QNAME, 
/*  8727 */           OMAbstractFactory.getOMFactory());
/*  8728 */     } catch (ADBException e) {
/*  8729 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetMagayaDocument param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8740 */       return param.getOMElement(GetMagayaDocument.MY_QNAME, 
/*  8741 */           OMAbstractFactory.getOMFactory());
/*  8742 */     } catch (ADBException e) {
/*  8743 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetMagayaDocumentResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8754 */       return param.getOMElement(GetMagayaDocumentResponse.MY_QNAME, 
/*  8755 */           OMAbstractFactory.getOMFactory());
/*  8756 */     } catch (ADBException e) {
/*  8757 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetWebDocument param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8768 */       return param.getOMElement(GetWebDocument.MY_QNAME, 
/*  8769 */           OMAbstractFactory.getOMFactory());
/*  8770 */     } catch (ADBException e) {
/*  8771 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetWebDocumentResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8782 */       return param.getOMElement(GetWebDocumentResponse.MY_QNAME, 
/*  8783 */           OMAbstractFactory.getOMFactory());
/*  8784 */     } catch (ADBException e) {
/*  8785 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetFirstTransbyDateJS param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8796 */       return param.getOMElement(GetFirstTransbyDateJS.MY_QNAME, 
/*  8797 */           OMAbstractFactory.getOMFactory());
/*  8798 */     } catch (ADBException e) {
/*  8799 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetFirstTransbyDateJSResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8810 */       return param.getOMElement(GetFirstTransbyDateJSResponse.MY_QNAME, 
/*  8811 */           OMAbstractFactory.getOMFactory());
/*  8812 */     } catch (ADBException e) {
/*  8813 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetPackageTypes param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8824 */       return param.getOMElement(GetPackageTypes.MY_QNAME, 
/*  8825 */           OMAbstractFactory.getOMFactory());
/*  8826 */     } catch (ADBException e) {
/*  8827 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetPackageTypesResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8838 */       return param.getOMElement(GetPackageTypesResponse.MY_QNAME, 
/*  8839 */           OMAbstractFactory.getOMFactory());
/*  8840 */     } catch (ADBException e) {
/*  8841 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetSecureTrackingTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8852 */       return param.getOMElement(GetSecureTrackingTransaction.MY_QNAME, 
/*  8853 */           OMAbstractFactory.getOMFactory());
/*  8854 */     } catch (ADBException e) {
/*  8855 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetSecureTrackingTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8866 */       return param.getOMElement(GetSecureTrackingTransactionResponse.MY_QNAME, 
/*  8867 */           OMAbstractFactory.getOMFactory());
/*  8868 */     } catch (ADBException e) {
/*  8869 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetChargeDefinitions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8880 */       return param.getOMElement(GetChargeDefinitions.MY_QNAME, 
/*  8881 */           OMAbstractFactory.getOMFactory());
/*  8882 */     } catch (ADBException e) {
/*  8883 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetChargeDefinitionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8894 */       return param.getOMElement(GetChargeDefinitionsResponse.MY_QNAME, 
/*  8895 */           OMAbstractFactory.getOMFactory());
/*  8896 */     } catch (ADBException e) {
/*  8897 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetAttachment param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8908 */       return param.getOMElement(GetAttachment.MY_QNAME, 
/*  8909 */           OMAbstractFactory.getOMFactory());
/*  8910 */     } catch (ADBException e) {
/*  8911 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetAttachmentResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8922 */       return param.getOMElement(GetAttachmentResponse.MY_QNAME, 
/*  8923 */           OMAbstractFactory.getOMFactory());
/*  8924 */     } catch (ADBException e) {
/*  8925 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(InvitationFromPeer param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8936 */       return param.getOMElement(InvitationFromPeer.MY_QNAME, 
/*  8937 */           OMAbstractFactory.getOMFactory());
/*  8938 */     } catch (ADBException e) {
/*  8939 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(InvitationFromPeerResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8950 */       return param.getOMElement(InvitationFromPeerResponse.MY_QNAME, 
/*  8951 */           OMAbstractFactory.getOMFactory());
/*  8952 */     } catch (ADBException e) {
/*  8953 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetInventoryItemsByItemDefinition param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8964 */       return param.getOMElement(GetInventoryItemsByItemDefinition.MY_QNAME, 
/*  8965 */           OMAbstractFactory.getOMFactory());
/*  8966 */     } catch (ADBException e) {
/*  8967 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetInventoryItemsByItemDefinitionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8978 */       return param.getOMElement(GetInventoryItemsByItemDefinitionResponse.MY_QNAME, 
/*  8979 */           OMAbstractFactory.getOMFactory());
/*  8980 */     } catch (ADBException e) {
/*  8981 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetTransactionEvents param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  8992 */       return param.getOMElement(SetTransactionEvents.MY_QNAME, 
/*  8993 */           OMAbstractFactory.getOMFactory());
/*  8994 */     } catch (ADBException e) {
/*  8995 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetTransactionEventsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9006 */       return param.getOMElement(SetTransactionEventsResponse.MY_QNAME, 
/*  9007 */           OMAbstractFactory.getOMFactory());
/*  9008 */     } catch (ADBException e) {
/*  9009 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetItemFromVIN param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9020 */       return param.getOMElement(GetItemFromVIN.MY_QNAME, 
/*  9021 */           OMAbstractFactory.getOMFactory());
/*  9022 */     } catch (ADBException e) {
/*  9023 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetItemFromVINResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9034 */       return param.getOMElement(GetItemFromVINResponse.MY_QNAME, 
/*  9035 */           OMAbstractFactory.getOMFactory());
/*  9036 */     } catch (ADBException e) {
/*  9037 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(BookingRequest param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9048 */       return param.getOMElement(BookingRequest.MY_QNAME, 
/*  9049 */           OMAbstractFactory.getOMFactory());
/*  9050 */     } catch (ADBException e) {
/*  9051 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(BookingRequestResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9062 */       return param.getOMElement(BookingRequestResponse.MY_QNAME, 
/*  9063 */           OMAbstractFactory.getOMFactory());
/*  9064 */     } catch (ADBException e) {
/*  9065 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetWorkingPorts param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9076 */       return param.getOMElement(GetWorkingPorts.MY_QNAME, 
/*  9077 */           OMAbstractFactory.getOMFactory());
/*  9078 */     } catch (ADBException e) {
/*  9079 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetWorkingPortsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9090 */       return param.getOMElement(GetWorkingPortsResponse.MY_QNAME, 
/*  9091 */           OMAbstractFactory.getOMFactory());
/*  9092 */     } catch (ADBException e) {
/*  9093 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(EndSession param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9104 */       return param.getOMElement(EndSession.MY_QNAME, 
/*  9105 */           OMAbstractFactory.getOMFactory());
/*  9106 */     } catch (ADBException e) {
/*  9107 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(EndSessionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9118 */       return param.getOMElement(EndSessionResponse.MY_QNAME, 
/*  9119 */           OMAbstractFactory.getOMFactory());
/*  9120 */     } catch (ADBException e) {
/*  9121 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(UpdateOrder param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9132 */       return param.getOMElement(UpdateOrder.MY_QNAME, 
/*  9133 */           OMAbstractFactory.getOMFactory());
/*  9134 */     } catch (ADBException e) {
/*  9135 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(UpdateOrderResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9146 */       return param.getOMElement(UpdateOrderResponse.MY_QNAME, 
/*  9147 */           OMAbstractFactory.getOMFactory());
/*  9148 */     } catch (ADBException e) {
/*  9149 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(StartSession param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9160 */       return param.getOMElement(StartSession.MY_QNAME, 
/*  9161 */           OMAbstractFactory.getOMFactory());
/*  9162 */     } catch (ADBException e) {
/*  9163 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(StartSessionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9174 */       return param.getOMElement(StartSessionResponse.MY_QNAME, 
/*  9175 */           OMAbstractFactory.getOMFactory());
/*  9176 */     } catch (ADBException e) {
/*  9177 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetShipmentStatus param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9188 */       return param.getOMElement(SetShipmentStatus.MY_QNAME, 
/*  9189 */           OMAbstractFactory.getOMFactory());
/*  9190 */     } catch (ADBException e) {
/*  9191 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetShipmentStatusResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9202 */       return param.getOMElement(SetShipmentStatusResponse.MY_QNAME, 
/*  9203 */           OMAbstractFactory.getOMFactory());
/*  9204 */     } catch (ADBException e) {
/*  9205 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetNextTransbyDate param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9216 */       return param.getOMElement(GetNextTransbyDate.MY_QNAME, 
/*  9217 */           OMAbstractFactory.getOMFactory());
/*  9218 */     } catch (ADBException e) {
/*  9219 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetNextTransbyDateResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9230 */       return param.getOMElement(GetNextTransbyDateResponse.MY_QNAME, 
/*  9231 */           OMAbstractFactory.getOMFactory());
/*  9232 */     } catch (ADBException e) {
/*  9233 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetActiveCurrencies param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9244 */       return param.getOMElement(GetActiveCurrencies.MY_QNAME, 
/*  9245 */           OMAbstractFactory.getOMFactory());
/*  9246 */     } catch (ADBException e) {
/*  9247 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetActiveCurrenciesResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9258 */       return param.getOMElement(GetActiveCurrenciesResponse.MY_QNAME, 
/*  9259 */           OMAbstractFactory.getOMFactory());
/*  9260 */     } catch (ADBException e) {
/*  9261 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(StartTracking param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9272 */       return param.getOMElement(StartTracking.MY_QNAME, 
/*  9273 */           OMAbstractFactory.getOMFactory());
/*  9274 */     } catch (ADBException e) {
/*  9275 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(StartTrackingResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9286 */       return param.getOMElement(StartTrackingResponse.MY_QNAME, 
/*  9287 */           OMAbstractFactory.getOMFactory());
/*  9288 */     } catch (ADBException e) {
/*  9289 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetStandardRates param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9300 */       return param.getOMElement(GetStandardRates.MY_QNAME, 
/*  9301 */           OMAbstractFactory.getOMFactory());
/*  9302 */     } catch (ADBException e) {
/*  9303 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetStandardRatesResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9314 */       return param.getOMElement(GetStandardRatesResponse.MY_QNAME, 
/*  9315 */           OMAbstractFactory.getOMFactory());
/*  9316 */     } catch (ADBException e) {
/*  9317 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransactionStatus param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9328 */       return param.getOMElement(GetTransactionStatus.MY_QNAME, 
/*  9329 */           OMAbstractFactory.getOMFactory());
/*  9330 */     } catch (ADBException e) {
/*  9331 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransactionStatusResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9342 */       return param.getOMElement(GetTransactionStatusResponse.MY_QNAME, 
/*  9343 */           OMAbstractFactory.getOMFactory());
/*  9344 */     } catch (ADBException e) {
/*  9345 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SubmitSalesOrder param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9356 */       return param.getOMElement(SubmitSalesOrder.MY_QNAME, 
/*  9357 */           OMAbstractFactory.getOMFactory());
/*  9358 */     } catch (ADBException e) {
/*  9359 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SubmitSalesOrderResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9370 */       return param.getOMElement(SubmitSalesOrderResponse.MY_QNAME, 
/*  9371 */           OMAbstractFactory.getOMFactory());
/*  9372 */     } catch (ADBException e) {
/*  9373 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(RenameTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9384 */       return param.getOMElement(RenameTransaction.MY_QNAME, 
/*  9385 */           OMAbstractFactory.getOMFactory());
/*  9386 */     } catch (ADBException e) {
/*  9387 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(RenameTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9398 */       return param.getOMElement(RenameTransactionResponse.MY_QNAME, 
/*  9399 */           OMAbstractFactory.getOMFactory());
/*  9400 */     } catch (ADBException e) {
/*  9401 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(QueryLogJS param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9412 */       return param.getOMElement(QueryLogJS.MY_QNAME, 
/*  9413 */           OMAbstractFactory.getOMFactory());
/*  9414 */     } catch (ADBException e) {
/*  9415 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(QueryLogJSResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9426 */       return param.getOMElement(QueryLogJSResponse.MY_QNAME, 
/*  9427 */           OMAbstractFactory.getOMFactory());
/*  9428 */     } catch (ADBException e) {
/*  9429 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntitiesOfType param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9440 */       return param.getOMElement(GetEntitiesOfType.MY_QNAME, 
/*  9441 */           OMAbstractFactory.getOMFactory());
/*  9442 */     } catch (ADBException e) {
/*  9443 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntitiesOfTypeResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9454 */       return param.getOMElement(GetEntitiesOfTypeResponse.MY_QNAME, 
/*  9455 */           OMAbstractFactory.getOMFactory());
/*  9456 */     } catch (ADBException e) {
/*  9457 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetItemDefinitionsByCustomer param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9468 */       return param.getOMElement(GetItemDefinitionsByCustomer.MY_QNAME, 
/*  9469 */           OMAbstractFactory.getOMFactory());
/*  9470 */     } catch (ADBException e) {
/*  9471 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetItemDefinitionsByCustomerResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9482 */       return param.getOMElement(GetItemDefinitionsByCustomerResponse.MY_QNAME, 
/*  9483 */           OMAbstractFactory.getOMFactory());
/*  9484 */     } catch (ADBException e) {
/*  9485 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(StartTracking2 param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9496 */       return param.getOMElement(StartTracking2.MY_QNAME, 
/*  9497 */           OMAbstractFactory.getOMFactory());
/*  9498 */     } catch (ADBException e) {
/*  9499 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(StartTracking2Response param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9510 */       return param.getOMElement(StartTracking2Response.MY_QNAME, 
/*  9511 */           OMAbstractFactory.getOMFactory());
/*  9512 */     } catch (ADBException e) {
/*  9513 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(DeleteTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9524 */       return param.getOMElement(DeleteTransaction.MY_QNAME, 
/*  9525 */           OMAbstractFactory.getOMFactory());
/*  9526 */     } catch (ADBException e) {
/*  9527 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(DeleteTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9538 */       return param.getOMElement(DeleteTransactionResponse.MY_QNAME, 
/*  9539 */           OMAbstractFactory.getOMFactory());
/*  9540 */     } catch (ADBException e) {
/*  9541 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9552 */       return param.getOMElement(GetTransaction.MY_QNAME, 
/*  9553 */           OMAbstractFactory.getOMFactory());
/*  9554 */     } catch (ADBException e) {
/*  9555 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9566 */       return param.getOMElement(GetTransactionResponse.MY_QNAME, 
/*  9567 */           OMAbstractFactory.getOMFactory());
/*  9568 */     } catch (ADBException e) {
/*  9569 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntityTransactions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9580 */       return param.getOMElement(GetEntityTransactions.MY_QNAME, 
/*  9581 */           OMAbstractFactory.getOMFactory());
/*  9582 */     } catch (ADBException e) {
/*  9583 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntityTransactionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9594 */       return param.getOMElement(GetEntityTransactionsResponse.MY_QNAME, 
/*  9595 */           OMAbstractFactory.getOMFactory());
/*  9596 */     } catch (ADBException e) {
/*  9597 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SubmitShipment param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9608 */       return param.getOMElement(SubmitShipment.MY_QNAME, 
/*  9609 */           OMAbstractFactory.getOMFactory());
/*  9610 */     } catch (ADBException e) {
/*  9611 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SubmitShipmentResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9622 */       return param.getOMElement(SubmitShipmentResponse.MY_QNAME, 
/*  9623 */           OMAbstractFactory.getOMFactory());
/*  9624 */     } catch (ADBException e) {
/*  9625 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntityContacts param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9636 */       return param.getOMElement(GetEntityContacts.MY_QNAME, 
/*  9637 */           OMAbstractFactory.getOMFactory());
/*  9638 */     } catch (ADBException e) {
/*  9639 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntityContactsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9650 */       return param.getOMElement(GetEntityContactsResponse.MY_QNAME, 
/*  9651 */           OMAbstractFactory.getOMFactory());
/*  9652 */     } catch (ADBException e) {
/*  9653 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(CancelBooking param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9664 */       return param.getOMElement(CancelBooking.MY_QNAME, 
/*  9665 */           OMAbstractFactory.getOMFactory());
/*  9666 */     } catch (ADBException e) {
/*  9667 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(CancelBookingResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9678 */       return param.getOMElement(CancelBookingResponse.MY_QNAME, 
/*  9679 */           OMAbstractFactory.getOMFactory());
/*  9680 */     } catch (ADBException e) {
/*  9681 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetTransaction param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9692 */       return param.getOMElement(SetTransaction.MY_QNAME, 
/*  9693 */           OMAbstractFactory.getOMFactory());
/*  9694 */     } catch (ADBException e) {
/*  9695 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetTransactionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9706 */       return param.getOMElement(SetTransactionResponse.MY_QNAME, 
/*  9707 */           OMAbstractFactory.getOMFactory());
/*  9708 */     } catch (ADBException e) {
/*  9709 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetEntity param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9720 */       return param.getOMElement(SetEntity.MY_QNAME, 
/*  9721 */           OMAbstractFactory.getOMFactory());
/*  9722 */     } catch (ADBException e) {
/*  9723 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetEntityResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9734 */       return param.getOMElement(SetEntityResponse.MY_QNAME, 
/*  9735 */           OMAbstractFactory.getOMFactory());
/*  9736 */     } catch (ADBException e) {
/*  9737 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetClientChargeDefinitions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9748 */       return param.getOMElement(GetClientChargeDefinitions.MY_QNAME, 
/*  9749 */           OMAbstractFactory.getOMFactory());
/*  9750 */     } catch (ADBException e) {
/*  9751 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetClientChargeDefinitionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9762 */       return param.getOMElement(GetClientChargeDefinitionsResponse.MY_QNAME, 
/*  9763 */           OMAbstractFactory.getOMFactory());
/*  9764 */     } catch (ADBException e) {
/*  9765 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetAccountingTransactions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9776 */       return param.getOMElement(GetAccountingTransactions.MY_QNAME, 
/*  9777 */           OMAbstractFactory.getOMFactory());
/*  9778 */     } catch (ADBException e) {
/*  9779 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetAccountingTransactionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9790 */       return param.getOMElement(GetAccountingTransactionsResponse.MY_QNAME, 
/*  9791 */           OMAbstractFactory.getOMFactory());
/*  9792 */     } catch (ADBException e) {
/*  9793 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTripSchedule param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9804 */       return param.getOMElement(GetTripSchedule.MY_QNAME, 
/*  9805 */           OMAbstractFactory.getOMFactory());
/*  9806 */     } catch (ADBException e) {
/*  9807 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTripScheduleResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9818 */       return param.getOMElement(GetTripScheduleResponse.MY_QNAME, 
/*  9819 */           OMAbstractFactory.getOMFactory());
/*  9820 */     } catch (ADBException e) {
/*  9821 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEventDefinitions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9832 */       return param.getOMElement(GetEventDefinitions.MY_QNAME, 
/*  9833 */           OMAbstractFactory.getOMFactory());
/*  9834 */     } catch (ADBException e) {
/*  9835 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEventDefinitionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9846 */       return param.getOMElement(GetEventDefinitionsResponse.MY_QNAME, 
/*  9847 */           OMAbstractFactory.getOMFactory());
/*  9848 */     } catch (ADBException e) {
/*  9849 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetCarrierRates param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9860 */       return param.getOMElement(GetCarrierRates.MY_QNAME, 
/*  9861 */           OMAbstractFactory.getOMFactory());
/*  9862 */     } catch (ADBException e) {
/*  9863 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetCarrierRatesResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9874 */       return param.getOMElement(GetCarrierRatesResponse.MY_QNAME, 
/*  9875 */           OMAbstractFactory.getOMFactory());
/*  9876 */     } catch (ADBException e) {
/*  9877 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetPODData param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9888 */       return param.getOMElement(GetPODData.MY_QNAME, 
/*  9889 */           OMAbstractFactory.getOMFactory());
/*  9890 */     } catch (ADBException e) {
/*  9891 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetPODDataResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9902 */       return param.getOMElement(GetPODDataResponse.MY_QNAME, 
/*  9903 */           OMAbstractFactory.getOMFactory());
/*  9904 */     } catch (ADBException e) {
/*  9905 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetCustomFieldDefinitions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9916 */       return param.getOMElement(GetCustomFieldDefinitions.MY_QNAME, 
/*  9917 */           OMAbstractFactory.getOMFactory());
/*  9918 */     } catch (ADBException e) {
/*  9919 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetCustomFieldDefinitionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9930 */       return param.getOMElement(GetCustomFieldDefinitionsResponse.MY_QNAME, 
/*  9931 */           OMAbstractFactory.getOMFactory());
/*  9932 */     } catch (ADBException e) {
/*  9933 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SubmitCargoRelease param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9944 */       return param.getOMElement(SubmitCargoRelease.MY_QNAME, 
/*  9945 */           OMAbstractFactory.getOMFactory());
/*  9946 */     } catch (ADBException e) {
/*  9947 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SubmitCargoReleaseResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9958 */       return param.getOMElement(SubmitCargoReleaseResponse.MY_QNAME, 
/*  9959 */           OMAbstractFactory.getOMFactory());
/*  9960 */     } catch (ADBException e) {
/*  9961 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetClientRates param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9972 */       return param.getOMElement(GetClientRates.MY_QNAME, 
/*  9973 */           OMAbstractFactory.getOMFactory());
/*  9974 */     } catch (ADBException e) {
/*  9975 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetClientRatesResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/*  9986 */       return param.getOMElement(GetClientRatesResponse.MY_QNAME, 
/*  9987 */           OMAbstractFactory.getOMFactory());
/*  9988 */     } catch (ADBException e) {
/*  9989 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransRangeByDateJS param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10000 */       return param.getOMElement(GetTransRangeByDateJS.MY_QNAME, 
/* 10001 */           OMAbstractFactory.getOMFactory());
/* 10002 */     } catch (ADBException e) {
/* 10003 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransRangeByDateJSResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10014 */       return param.getOMElement(GetTransRangeByDateJSResponse.MY_QNAME, 
/* 10015 */           OMAbstractFactory.getOMFactory());
/* 10016 */     } catch (ADBException e) {
/* 10017 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetAccountDefinitions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10028 */       return param.getOMElement(GetAccountDefinitions.MY_QNAME, 
/* 10029 */           OMAbstractFactory.getOMFactory());
/* 10030 */     } catch (ADBException e) {
/* 10031 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetAccountDefinitionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10042 */       return param.getOMElement(GetAccountDefinitionsResponse.MY_QNAME, 
/* 10043 */           OMAbstractFactory.getOMFactory());
/* 10044 */     } catch (ADBException e) {
/* 10045 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetApprovalStatus param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10056 */       return param.getOMElement(SetApprovalStatus.MY_QNAME, 
/* 10057 */           OMAbstractFactory.getOMFactory());
/* 10058 */     } catch (ADBException e) {
/* 10059 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetApprovalStatusResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10070 */       return param.getOMElement(SetApprovalStatusResponse.MY_QNAME, 
/* 10071 */           OMAbstractFactory.getOMFactory());
/* 10072 */     } catch (ADBException e) {
/* 10073 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetRelatedTransactions param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10084 */       return param.getOMElement(GetRelatedTransactions.MY_QNAME, 
/* 10085 */           OMAbstractFactory.getOMFactory());
/* 10086 */     } catch (ADBException e) {
/* 10087 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetRelatedTransactionsResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10098 */       return param.getOMElement(GetRelatedTransactionsResponse.MY_QNAME, 
/* 10099 */           OMAbstractFactory.getOMFactory());
/* 10100 */     } catch (ADBException e) {
/* 10101 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetCustomFieldValue param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10112 */       return param.getOMElement(SetCustomFieldValue.MY_QNAME, 
/* 10113 */           OMAbstractFactory.getOMFactory());
/* 10114 */     } catch (ADBException e) {
/* 10115 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetCustomFieldValueResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10126 */       return param.getOMElement(SetCustomFieldValueResponse.MY_QNAME, 
/* 10127 */           OMAbstractFactory.getOMFactory());
/* 10128 */     } catch (ADBException e) {
/* 10129 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntities param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10140 */       return param.getOMElement(GetEntities.MY_QNAME, 
/* 10141 */           OMAbstractFactory.getOMFactory());
/* 10142 */     } catch (ADBException e) {
/* 10143 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetEntitiesResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10154 */       return param.getOMElement(GetEntitiesResponse.MY_QNAME, 
/* 10155 */           OMAbstractFactory.getOMFactory());
/* 10156 */     } catch (ADBException e) {
/* 10157 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(AnswerInvitation param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10168 */       return param.getOMElement(AnswerInvitation.MY_QNAME, 
/* 10169 */           OMAbstractFactory.getOMFactory());
/* 10170 */     } catch (ADBException e) {
/* 10171 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(AnswerInvitationResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10182 */       return param.getOMElement(AnswerInvitationResponse.MY_QNAME, 
/* 10183 */           OMAbstractFactory.getOMFactory());
/* 10184 */     } catch (ADBException e) {
/* 10185 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(TestConnection param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10196 */       return param.getOMElement(TestConnection.MY_QNAME, 
/* 10197 */           OMAbstractFactory.getOMFactory());
/* 10198 */     } catch (ADBException e) {
/* 10199 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(TestConnectionResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10210 */       return param.getOMElement(TestConnectionResponse.MY_QNAME, 
/* 10211 */           OMAbstractFactory.getOMFactory());
/* 10212 */     } catch (ADBException e) {
/* 10213 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransRangeByDate param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10224 */       return param.getOMElement(GetTransRangeByDate.MY_QNAME, 
/* 10225 */           OMAbstractFactory.getOMFactory());
/* 10226 */     } catch (ADBException e) {
/* 10227 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransRangeByDateResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10238 */       return param.getOMElement(GetTransRangeByDateResponse.MY_QNAME, 
/* 10239 */           OMAbstractFactory.getOMFactory());
/* 10240 */     } catch (ADBException e) {
/* 10241 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransactionsByBillingClient param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10252 */       return param.getOMElement(GetTransactionsByBillingClient.MY_QNAME, 
/* 10253 */           OMAbstractFactory.getOMFactory());
/* 10254 */     } catch (ADBException e) {
/* 10255 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetTransactionsByBillingClientResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10266 */       return param.getOMElement(GetTransactionsByBillingClientResponse.MY_QNAME, 
/* 10267 */           OMAbstractFactory.getOMFactory());
/* 10268 */     } catch (ADBException e) {
/* 10269 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetTrackingUser param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10280 */       return param.getOMElement(SetTrackingUser.MY_QNAME, 
/* 10281 */           OMAbstractFactory.getOMFactory());
/* 10282 */     } catch (ADBException e) {
/* 10283 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(SetTrackingUserResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10294 */       return param.getOMElement(SetTrackingUserResponse.MY_QNAME, 
/* 10295 */           OMAbstractFactory.getOMFactory());
/* 10296 */     } catch (ADBException e) {
/* 10297 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(AnswerInvitation2 param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10308 */       return param.getOMElement(AnswerInvitation2.MY_QNAME, 
/* 10309 */           OMAbstractFactory.getOMFactory());
/* 10310 */     } catch (ADBException e) {
/* 10311 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(AnswerInvitation2Response param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10322 */       return param.getOMElement(AnswerInvitation2Response.MY_QNAME, 
/* 10323 */           OMAbstractFactory.getOMFactory());
/* 10324 */     } catch (ADBException e) {
/* 10325 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetDocument param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10336 */       return param.getOMElement(GetDocument.MY_QNAME, 
/* 10337 */           OMAbstractFactory.getOMFactory());
/* 10338 */     } catch (ADBException e) {
/* 10339 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private OMElement toOM(GetDocumentResponse param, boolean optimizeContent) throws AxisFault {
/*       */     try {
/* 10350 */       return param.getOMElement(GetDocumentResponse.MY_QNAME, 
/* 10351 */           OMAbstractFactory.getOMFactory());
/* 10352 */     } catch (ADBException e) {
/* 10353 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, ExistsTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10366 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10367 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(ExistsTransaction.MY_QNAME, (OMFactory)factory));
/* 10368 */       return emptyEnvelope;
/* 10369 */     } catch (ADBException e) {
/* 10370 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTrackingTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10387 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10388 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTrackingTransaction.MY_QNAME, (OMFactory)factory));
/* 10389 */       return emptyEnvelope;
/* 10390 */     } catch (ADBException e) {
/* 10391 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetFirstTransbyDate param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10408 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10409 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetFirstTransbyDate.MY_QNAME, (OMFactory)factory));
/* 10410 */       return emptyEnvelope;
/* 10411 */     } catch (ADBException e) {
/* 10412 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetRate param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10429 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10430 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetRate.MY_QNAME, (OMFactory)factory));
/* 10431 */       return emptyEnvelope;
/* 10432 */     } catch (ADBException e) {
/* 10433 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, QueryLog param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10450 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10451 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(QueryLog.MY_QNAME, (OMFactory)factory));
/* 10452 */       return emptyEnvelope;
/* 10453 */     } catch (ADBException e) {
/* 10454 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetMagayaDocument param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10471 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10472 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetMagayaDocument.MY_QNAME, (OMFactory)factory));
/* 10473 */       return emptyEnvelope;
/* 10474 */     } catch (ADBException e) {
/* 10475 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetWebDocument param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10492 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10493 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetWebDocument.MY_QNAME, (OMFactory)factory));
/* 10494 */       return emptyEnvelope;
/* 10495 */     } catch (ADBException e) {
/* 10496 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetFirstTransbyDateJS param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10513 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10514 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetFirstTransbyDateJS.MY_QNAME, (OMFactory)factory));
/* 10515 */       return emptyEnvelope;
/* 10516 */     } catch (ADBException e) {
/* 10517 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetPackageTypes param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10534 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10535 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetPackageTypes.MY_QNAME, (OMFactory)factory));
/* 10536 */       return emptyEnvelope;
/* 10537 */     } catch (ADBException e) {
/* 10538 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetSecureTrackingTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10555 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10556 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetSecureTrackingTransaction.MY_QNAME, (OMFactory)factory));
/* 10557 */       return emptyEnvelope;
/* 10558 */     } catch (ADBException e) {
/* 10559 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetChargeDefinitions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10576 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10577 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetChargeDefinitions.MY_QNAME, (OMFactory)factory));
/* 10578 */       return emptyEnvelope;
/* 10579 */     } catch (ADBException e) {
/* 10580 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetAttachment param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10597 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10598 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetAttachment.MY_QNAME, (OMFactory)factory));
/* 10599 */       return emptyEnvelope;
/* 10600 */     } catch (ADBException e) {
/* 10601 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, InvitationFromPeer param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10618 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10619 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(InvitationFromPeer.MY_QNAME, (OMFactory)factory));
/* 10620 */       return emptyEnvelope;
/* 10621 */     } catch (ADBException e) {
/* 10622 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetInventoryItemsByItemDefinition param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10639 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10640 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetInventoryItemsByItemDefinition.MY_QNAME, (OMFactory)factory));
/* 10641 */       return emptyEnvelope;
/* 10642 */     } catch (ADBException e) {
/* 10643 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetTransactionEvents param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10660 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10661 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetTransactionEvents.MY_QNAME, (OMFactory)factory));
/* 10662 */       return emptyEnvelope;
/* 10663 */     } catch (ADBException e) {
/* 10664 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetItemFromVIN param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10681 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10682 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetItemFromVIN.MY_QNAME, (OMFactory)factory));
/* 10683 */       return emptyEnvelope;
/* 10684 */     } catch (ADBException e) {
/* 10685 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, BookingRequest param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10702 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10703 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(BookingRequest.MY_QNAME, (OMFactory)factory));
/* 10704 */       return emptyEnvelope;
/* 10705 */     } catch (ADBException e) {
/* 10706 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetWorkingPorts param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10723 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10724 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetWorkingPorts.MY_QNAME, (OMFactory)factory));
/* 10725 */       return emptyEnvelope;
/* 10726 */     } catch (ADBException e) {
/* 10727 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, EndSession param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10744 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10745 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(EndSession.MY_QNAME, (OMFactory)factory));
/* 10746 */       return emptyEnvelope;
/* 10747 */     } catch (ADBException e) {
/* 10748 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, UpdateOrder param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10765 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10766 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(UpdateOrder.MY_QNAME, (OMFactory)factory));
/* 10767 */       return emptyEnvelope;
/* 10768 */     } catch (ADBException e) {
/* 10769 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, StartSession param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10786 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10787 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(StartSession.MY_QNAME, (OMFactory)factory));
/* 10788 */       return emptyEnvelope;
/* 10789 */     } catch (ADBException e) {
/* 10790 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetShipmentStatus param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10807 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10808 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetShipmentStatus.MY_QNAME, (OMFactory)factory));
/* 10809 */       return emptyEnvelope;
/* 10810 */     } catch (ADBException e) {
/* 10811 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetNextTransbyDate param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10828 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10829 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetNextTransbyDate.MY_QNAME, (OMFactory)factory));
/* 10830 */       return emptyEnvelope;
/* 10831 */     } catch (ADBException e) {
/* 10832 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetActiveCurrencies param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10849 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10850 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetActiveCurrencies.MY_QNAME, (OMFactory)factory));
/* 10851 */       return emptyEnvelope;
/* 10852 */     } catch (ADBException e) {
/* 10853 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, StartTracking param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10870 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10871 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(StartTracking.MY_QNAME, (OMFactory)factory));
/* 10872 */       return emptyEnvelope;
/* 10873 */     } catch (ADBException e) {
/* 10874 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetStandardRates param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10891 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10892 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetStandardRates.MY_QNAME, (OMFactory)factory));
/* 10893 */       return emptyEnvelope;
/* 10894 */     } catch (ADBException e) {
/* 10895 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTransactionStatus param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10912 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10913 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTransactionStatus.MY_QNAME, (OMFactory)factory));
/* 10914 */       return emptyEnvelope;
/* 10915 */     } catch (ADBException e) {
/* 10916 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SubmitSalesOrder param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10933 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10934 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SubmitSalesOrder.MY_QNAME, (OMFactory)factory));
/* 10935 */       return emptyEnvelope;
/* 10936 */     } catch (ADBException e) {
/* 10937 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, RenameTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10954 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10955 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(RenameTransaction.MY_QNAME, (OMFactory)factory));
/* 10956 */       return emptyEnvelope;
/* 10957 */     } catch (ADBException e) {
/* 10958 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, QueryLogJS param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10975 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10976 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(QueryLogJS.MY_QNAME, (OMFactory)factory));
/* 10977 */       return emptyEnvelope;
/* 10978 */     } catch (ADBException e) {
/* 10979 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetEntitiesOfType param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 10996 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 10997 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetEntitiesOfType.MY_QNAME, (OMFactory)factory));
/* 10998 */       return emptyEnvelope;
/* 10999 */     } catch (ADBException e) {
/* 11000 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetItemDefinitionsByCustomer param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11017 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11018 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetItemDefinitionsByCustomer.MY_QNAME, (OMFactory)factory));
/* 11019 */       return emptyEnvelope;
/* 11020 */     } catch (ADBException e) {
/* 11021 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, StartTracking2 param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11038 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11039 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(StartTracking2.MY_QNAME, (OMFactory)factory));
/* 11040 */       return emptyEnvelope;
/* 11041 */     } catch (ADBException e) {
/* 11042 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, DeleteTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11059 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11060 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(DeleteTransaction.MY_QNAME, (OMFactory)factory));
/* 11061 */       return emptyEnvelope;
/* 11062 */     } catch (ADBException e) {
/* 11063 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11080 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11081 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTransaction.MY_QNAME, (OMFactory)factory));
/* 11082 */       return emptyEnvelope;
/* 11083 */     } catch (ADBException e) {
/* 11084 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetEntityTransactions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11101 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11102 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetEntityTransactions.MY_QNAME, (OMFactory)factory));
/* 11103 */       return emptyEnvelope;
/* 11104 */     } catch (ADBException e) {
/* 11105 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SubmitShipment param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11122 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11123 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SubmitShipment.MY_QNAME, (OMFactory)factory));
/* 11124 */       return emptyEnvelope;
/* 11125 */     } catch (ADBException e) {
/* 11126 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetEntityContacts param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11143 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11144 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetEntityContacts.MY_QNAME, (OMFactory)factory));
/* 11145 */       return emptyEnvelope;
/* 11146 */     } catch (ADBException e) {
/* 11147 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, CancelBooking param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11164 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11165 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(CancelBooking.MY_QNAME, (OMFactory)factory));
/* 11166 */       return emptyEnvelope;
/* 11167 */     } catch (ADBException e) {
/* 11168 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetTransaction param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11185 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11186 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetTransaction.MY_QNAME, (OMFactory)factory));
/* 11187 */       return emptyEnvelope;
/* 11188 */     } catch (ADBException e) {
/* 11189 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetEntity param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11206 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11207 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetEntity.MY_QNAME, (OMFactory)factory));
/* 11208 */       return emptyEnvelope;
/* 11209 */     } catch (ADBException e) {
/* 11210 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetClientChargeDefinitions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11227 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11228 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetClientChargeDefinitions.MY_QNAME, (OMFactory)factory));
/* 11229 */       return emptyEnvelope;
/* 11230 */     } catch (ADBException e) {
/* 11231 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetAccountingTransactions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11248 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11249 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetAccountingTransactions.MY_QNAME, (OMFactory)factory));
/* 11250 */       return emptyEnvelope;
/* 11251 */     } catch (ADBException e) {
/* 11252 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTripSchedule param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11269 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11270 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTripSchedule.MY_QNAME, (OMFactory)factory));
/* 11271 */       return emptyEnvelope;
/* 11272 */     } catch (ADBException e) {
/* 11273 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetEventDefinitions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11290 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11291 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetEventDefinitions.MY_QNAME, (OMFactory)factory));
/* 11292 */       return emptyEnvelope;
/* 11293 */     } catch (ADBException e) {
/* 11294 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetCarrierRates param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11311 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11312 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetCarrierRates.MY_QNAME, (OMFactory)factory));
/* 11313 */       return emptyEnvelope;
/* 11314 */     } catch (ADBException e) {
/* 11315 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetPODData param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11332 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11333 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetPODData.MY_QNAME, (OMFactory)factory));
/* 11334 */       return emptyEnvelope;
/* 11335 */     } catch (ADBException e) {
/* 11336 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetCustomFieldDefinitions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11353 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11354 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetCustomFieldDefinitions.MY_QNAME, (OMFactory)factory));
/* 11355 */       return emptyEnvelope;
/* 11356 */     } catch (ADBException e) {
/* 11357 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SubmitCargoRelease param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11374 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11375 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SubmitCargoRelease.MY_QNAME, (OMFactory)factory));
/* 11376 */       return emptyEnvelope;
/* 11377 */     } catch (ADBException e) {
/* 11378 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetClientRates param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11395 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11396 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetClientRates.MY_QNAME, (OMFactory)factory));
/* 11397 */       return emptyEnvelope;
/* 11398 */     } catch (ADBException e) {
/* 11399 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTransRangeByDateJS param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11416 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11417 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTransRangeByDateJS.MY_QNAME, (OMFactory)factory));
/* 11418 */       return emptyEnvelope;
/* 11419 */     } catch (ADBException e) {
/* 11420 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetAccountDefinitions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11437 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11438 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetAccountDefinitions.MY_QNAME, (OMFactory)factory));
/* 11439 */       return emptyEnvelope;
/* 11440 */     } catch (ADBException e) {
/* 11441 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetApprovalStatus param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11458 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11459 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetApprovalStatus.MY_QNAME, (OMFactory)factory));
/* 11460 */       return emptyEnvelope;
/* 11461 */     } catch (ADBException e) {
/* 11462 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetRelatedTransactions param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11479 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11480 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetRelatedTransactions.MY_QNAME, (OMFactory)factory));
/* 11481 */       return emptyEnvelope;
/* 11482 */     } catch (ADBException e) {
/* 11483 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetCustomFieldValue param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11500 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11501 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetCustomFieldValue.MY_QNAME, (OMFactory)factory));
/* 11502 */       return emptyEnvelope;
/* 11503 */     } catch (ADBException e) {
/* 11504 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetEntities param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11521 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11522 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetEntities.MY_QNAME, (OMFactory)factory));
/* 11523 */       return emptyEnvelope;
/* 11524 */     } catch (ADBException e) {
/* 11525 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, AnswerInvitation param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11542 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11543 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(AnswerInvitation.MY_QNAME, (OMFactory)factory));
/* 11544 */       return emptyEnvelope;
/* 11545 */     } catch (ADBException e) {
/* 11546 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, TestConnection param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11563 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11564 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(TestConnection.MY_QNAME, (OMFactory)factory));
/* 11565 */       return emptyEnvelope;
/* 11566 */     } catch (ADBException e) {
/* 11567 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTransRangeByDate param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11584 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11585 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTransRangeByDate.MY_QNAME, (OMFactory)factory));
/* 11586 */       return emptyEnvelope;
/* 11587 */     } catch (ADBException e) {
/* 11588 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetTransactionsByBillingClient param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11605 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11606 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetTransactionsByBillingClient.MY_QNAME, (OMFactory)factory));
/* 11607 */       return emptyEnvelope;
/* 11608 */     } catch (ADBException e) {
/* 11609 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, SetTrackingUser param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11626 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11627 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(SetTrackingUser.MY_QNAME, (OMFactory)factory));
/* 11628 */       return emptyEnvelope;
/* 11629 */     } catch (ADBException e) {
/* 11630 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, AnswerInvitation2 param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11647 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11648 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(AnswerInvitation2.MY_QNAME, (OMFactory)factory));
/* 11649 */       return emptyEnvelope;
/* 11650 */     } catch (ADBException e) {
/* 11651 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory, GetDocument param, boolean optimizeContent, QName methodQName) throws AxisFault {
/*       */     try {
/* 11668 */       SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
/* 11669 */       emptyEnvelope.getBody().addChild((OMNode)param.getOMElement(GetDocument.MY_QNAME, (OMFactory)factory));
/* 11670 */       return emptyEnvelope;
/* 11671 */     } catch (ADBException e) {
/* 11672 */       throw AxisFault.makeFault(e);
/*       */     } 
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private SOAPEnvelope toEnvelope(SOAPFactory factory) {
/* 11688 */     return factory.getDefaultEnvelope();
/*       */   }
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */ 
/*       */   
/*       */   private Object fromOM(OMElement param, Class type, Map extraNamespaces) throws AxisFault {
/*       */     try {
/* 11699 */       if (ExistsTransaction.class.equals(type))
/*       */       {
/* 11701 */         return ExistsTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11706 */       if (ExistsTransactionResponse.class.equals(type))
/*       */       {
/* 11708 */         return ExistsTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11713 */       if (GetTrackingTransaction.class.equals(type))
/*       */       {
/* 11715 */         return GetTrackingTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11720 */       if (GetTrackingTransactionResponse.class.equals(type))
/*       */       {
/* 11722 */         return GetTrackingTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11727 */       if (GetFirstTransbyDate.class.equals(type))
/*       */       {
/* 11729 */         return GetFirstTransbyDate.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11734 */       if (GetFirstTransbyDateResponse.class.equals(type))
/*       */       {
/* 11736 */         return GetFirstTransbyDateResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11741 */       if (SetRate.class.equals(type))
/*       */       {
/* 11743 */         return SetRate.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11748 */       if (SetRateResponse.class.equals(type))
/*       */       {
/* 11750 */         return SetRateResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11755 */       if (QueryLog.class.equals(type))
/*       */       {
/* 11757 */         return QueryLog.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11762 */       if (QueryLogResponse.class.equals(type))
/*       */       {
/* 11764 */         return QueryLogResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11769 */       if (GetMagayaDocument.class.equals(type))
/*       */       {
/* 11771 */         return GetMagayaDocument.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11776 */       if (GetMagayaDocumentResponse.class.equals(type))
/*       */       {
/* 11778 */         return GetMagayaDocumentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11783 */       if (GetWebDocument.class.equals(type))
/*       */       {
/* 11785 */         return GetWebDocument.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11790 */       if (GetWebDocumentResponse.class.equals(type))
/*       */       {
/* 11792 */         return GetWebDocumentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11797 */       if (GetFirstTransbyDateJS.class.equals(type))
/*       */       {
/* 11799 */         return GetFirstTransbyDateJS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11804 */       if (GetFirstTransbyDateJSResponse.class.equals(type))
/*       */       {
/* 11806 */         return GetFirstTransbyDateJSResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11811 */       if (GetPackageTypes.class.equals(type))
/*       */       {
/* 11813 */         return GetPackageTypes.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11818 */       if (GetPackageTypesResponse.class.equals(type))
/*       */       {
/* 11820 */         return GetPackageTypesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11825 */       if (GetSecureTrackingTransaction.class.equals(type))
/*       */       {
/* 11827 */         return GetSecureTrackingTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11832 */       if (GetSecureTrackingTransactionResponse.class.equals(type))
/*       */       {
/* 11834 */         return GetSecureTrackingTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11839 */       if (GetChargeDefinitions.class.equals(type))
/*       */       {
/* 11841 */         return GetChargeDefinitions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11846 */       if (GetChargeDefinitionsResponse.class.equals(type))
/*       */       {
/* 11848 */         return GetChargeDefinitionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11853 */       if (GetAttachment.class.equals(type))
/*       */       {
/* 11855 */         return GetAttachment.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11860 */       if (GetAttachmentResponse.class.equals(type))
/*       */       {
/* 11862 */         return GetAttachmentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11867 */       if (InvitationFromPeer.class.equals(type))
/*       */       {
/* 11869 */         return InvitationFromPeer.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11874 */       if (InvitationFromPeerResponse.class.equals(type))
/*       */       {
/* 11876 */         return InvitationFromPeerResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11881 */       if (GetInventoryItemsByItemDefinition.class.equals(type))
/*       */       {
/* 11883 */         return GetInventoryItemsByItemDefinition.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11888 */       if (GetInventoryItemsByItemDefinitionResponse.class.equals(type))
/*       */       {
/* 11890 */         return GetInventoryItemsByItemDefinitionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11895 */       if (SetTransactionEvents.class.equals(type))
/*       */       {
/* 11897 */         return SetTransactionEvents.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11902 */       if (SetTransactionEventsResponse.class.equals(type))
/*       */       {
/* 11904 */         return SetTransactionEventsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11909 */       if (GetItemFromVIN.class.equals(type))
/*       */       {
/* 11911 */         return GetItemFromVIN.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11916 */       if (GetItemFromVINResponse.class.equals(type))
/*       */       {
/* 11918 */         return GetItemFromVINResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11923 */       if (BookingRequest.class.equals(type))
/*       */       {
/* 11925 */         return BookingRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11930 */       if (BookingRequestResponse.class.equals(type))
/*       */       {
/* 11932 */         return BookingRequestResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11937 */       if (GetWorkingPorts.class.equals(type))
/*       */       {
/* 11939 */         return GetWorkingPorts.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11944 */       if (GetWorkingPortsResponse.class.equals(type))
/*       */       {
/* 11946 */         return GetWorkingPortsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11951 */       if (EndSession.class.equals(type))
/*       */       {
/* 11953 */         return EndSession.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11958 */       if (EndSessionResponse.class.equals(type))
/*       */       {
/* 11960 */         return EndSessionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11965 */       if (UpdateOrder.class.equals(type))
/*       */       {
/* 11967 */         return UpdateOrder.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11972 */       if (UpdateOrderResponse.class.equals(type))
/*       */       {
/* 11974 */         return UpdateOrderResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11979 */       if (StartSession.class.equals(type))
/*       */       {
/* 11981 */         return StartSession.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11986 */       if (StartSessionResponse.class.equals(type))
/*       */       {
/* 11988 */         return StartSessionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 11993 */       if (SetShipmentStatus.class.equals(type))
/*       */       {
/* 11995 */         return SetShipmentStatus.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12000 */       if (SetShipmentStatusResponse.class.equals(type))
/*       */       {
/* 12002 */         return SetShipmentStatusResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12007 */       if (GetNextTransbyDate.class.equals(type))
/*       */       {
/* 12009 */         return GetNextTransbyDate.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12014 */       if (GetNextTransbyDateResponse.class.equals(type))
/*       */       {
/* 12016 */         return GetNextTransbyDateResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12021 */       if (GetActiveCurrencies.class.equals(type))
/*       */       {
/* 12023 */         return GetActiveCurrencies.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12028 */       if (GetActiveCurrenciesResponse.class.equals(type))
/*       */       {
/* 12030 */         return GetActiveCurrenciesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12035 */       if (StartTracking.class.equals(type))
/*       */       {
/* 12037 */         return StartTracking.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12042 */       if (StartTrackingResponse.class.equals(type))
/*       */       {
/* 12044 */         return StartTrackingResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12049 */       if (GetStandardRates.class.equals(type))
/*       */       {
/* 12051 */         return GetStandardRates.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12056 */       if (GetStandardRatesResponse.class.equals(type))
/*       */       {
/* 12058 */         return GetStandardRatesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12063 */       if (GetTransactionStatus.class.equals(type))
/*       */       {
/* 12065 */         return GetTransactionStatus.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12070 */       if (GetTransactionStatusResponse.class.equals(type))
/*       */       {
/* 12072 */         return GetTransactionStatusResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12077 */       if (SubmitSalesOrder.class.equals(type))
/*       */       {
/* 12079 */         return SubmitSalesOrder.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12084 */       if (SubmitSalesOrderResponse.class.equals(type))
/*       */       {
/* 12086 */         return SubmitSalesOrderResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12091 */       if (RenameTransaction.class.equals(type))
/*       */       {
/* 12093 */         return RenameTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12098 */       if (RenameTransactionResponse.class.equals(type))
/*       */       {
/* 12100 */         return RenameTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12105 */       if (QueryLogJS.class.equals(type))
/*       */       {
/* 12107 */         return QueryLogJS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12112 */       if (QueryLogJSResponse.class.equals(type))
/*       */       {
/* 12114 */         return QueryLogJSResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12119 */       if (GetEntitiesOfType.class.equals(type))
/*       */       {
/* 12121 */         return GetEntitiesOfType.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12126 */       if (GetEntitiesOfTypeResponse.class.equals(type))
/*       */       {
/* 12128 */         return GetEntitiesOfTypeResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12133 */       if (GetItemDefinitionsByCustomer.class.equals(type))
/*       */       {
/* 12135 */         return GetItemDefinitionsByCustomer.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12140 */       if (GetItemDefinitionsByCustomerResponse.class.equals(type))
/*       */       {
/* 12142 */         return GetItemDefinitionsByCustomerResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12147 */       if (StartTracking2.class.equals(type))
/*       */       {
/* 12149 */         return StartTracking2.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12154 */       if (StartTracking2Response.class.equals(type))
/*       */       {
/* 12156 */         return StartTracking2Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12161 */       if (DeleteTransaction.class.equals(type))
/*       */       {
/* 12163 */         return DeleteTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12168 */       if (DeleteTransactionResponse.class.equals(type))
/*       */       {
/* 12170 */         return DeleteTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12175 */       if (GetTransaction.class.equals(type))
/*       */       {
/* 12177 */         return GetTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12182 */       if (GetTransactionResponse.class.equals(type))
/*       */       {
/* 12184 */         return GetTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12189 */       if (GetEntityTransactions.class.equals(type))
/*       */       {
/* 12191 */         return GetEntityTransactions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12196 */       if (GetEntityTransactionsResponse.class.equals(type))
/*       */       {
/* 12198 */         return GetEntityTransactionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12203 */       if (SubmitShipment.class.equals(type))
/*       */       {
/* 12205 */         return SubmitShipment.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12210 */       if (SubmitShipmentResponse.class.equals(type))
/*       */       {
/* 12212 */         return SubmitShipmentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12217 */       if (GetEntityContacts.class.equals(type))
/*       */       {
/* 12219 */         return GetEntityContacts.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12224 */       if (GetEntityContactsResponse.class.equals(type))
/*       */       {
/* 12226 */         return GetEntityContactsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12231 */       if (CancelBooking.class.equals(type))
/*       */       {
/* 12233 */         return CancelBooking.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12238 */       if (CancelBookingResponse.class.equals(type))
/*       */       {
/* 12240 */         return CancelBookingResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12245 */       if (SetTransaction.class.equals(type))
/*       */       {
/* 12247 */         return SetTransaction.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12252 */       if (SetTransactionResponse.class.equals(type))
/*       */       {
/* 12254 */         return SetTransactionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12259 */       if (SetEntity.class.equals(type))
/*       */       {
/* 12261 */         return SetEntity.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12266 */       if (SetEntityResponse.class.equals(type))
/*       */       {
/* 12268 */         return SetEntityResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12273 */       if (GetClientChargeDefinitions.class.equals(type))
/*       */       {
/* 12275 */         return GetClientChargeDefinitions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12280 */       if (GetClientChargeDefinitionsResponse.class.equals(type))
/*       */       {
/* 12282 */         return GetClientChargeDefinitionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12287 */       if (GetAccountingTransactions.class.equals(type))
/*       */       {
/* 12289 */         return GetAccountingTransactions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12294 */       if (GetAccountingTransactionsResponse.class.equals(type))
/*       */       {
/* 12296 */         return GetAccountingTransactionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12301 */       if (GetTripSchedule.class.equals(type))
/*       */       {
/* 12303 */         return GetTripSchedule.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12308 */       if (GetTripScheduleResponse.class.equals(type))
/*       */       {
/* 12310 */         return GetTripScheduleResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12315 */       if (GetEventDefinitions.class.equals(type))
/*       */       {
/* 12317 */         return GetEventDefinitions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12322 */       if (GetEventDefinitionsResponse.class.equals(type))
/*       */       {
/* 12324 */         return GetEventDefinitionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12329 */       if (GetCarrierRates.class.equals(type))
/*       */       {
/* 12331 */         return GetCarrierRates.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12336 */       if (GetCarrierRatesResponse.class.equals(type))
/*       */       {
/* 12338 */         return GetCarrierRatesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12343 */       if (GetPODData.class.equals(type))
/*       */       {
/* 12345 */         return GetPODData.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12350 */       if (GetPODDataResponse.class.equals(type))
/*       */       {
/* 12352 */         return GetPODDataResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12357 */       if (GetCustomFieldDefinitions.class.equals(type))
/*       */       {
/* 12359 */         return GetCustomFieldDefinitions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12364 */       if (GetCustomFieldDefinitionsResponse.class.equals(type))
/*       */       {
/* 12366 */         return GetCustomFieldDefinitionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12371 */       if (SubmitCargoRelease.class.equals(type))
/*       */       {
/* 12373 */         return SubmitCargoRelease.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12378 */       if (SubmitCargoReleaseResponse.class.equals(type))
/*       */       {
/* 12380 */         return SubmitCargoReleaseResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12385 */       if (GetClientRates.class.equals(type))
/*       */       {
/* 12387 */         return GetClientRates.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12392 */       if (GetClientRatesResponse.class.equals(type))
/*       */       {
/* 12394 */         return GetClientRatesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12399 */       if (GetTransRangeByDateJS.class.equals(type))
/*       */       {
/* 12401 */         return GetTransRangeByDateJS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12406 */       if (GetTransRangeByDateJSResponse.class.equals(type))
/*       */       {
/* 12408 */         return GetTransRangeByDateJSResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12413 */       if (GetAccountDefinitions.class.equals(type))
/*       */       {
/* 12415 */         return GetAccountDefinitions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12420 */       if (GetAccountDefinitionsResponse.class.equals(type))
/*       */       {
/* 12422 */         return GetAccountDefinitionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12427 */       if (SetApprovalStatus.class.equals(type))
/*       */       {
/* 12429 */         return SetApprovalStatus.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12434 */       if (SetApprovalStatusResponse.class.equals(type))
/*       */       {
/* 12436 */         return SetApprovalStatusResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12441 */       if (GetRelatedTransactions.class.equals(type))
/*       */       {
/* 12443 */         return GetRelatedTransactions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12448 */       if (GetRelatedTransactionsResponse.class.equals(type))
/*       */       {
/* 12450 */         return GetRelatedTransactionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12455 */       if (SetCustomFieldValue.class.equals(type))
/*       */       {
/* 12457 */         return SetCustomFieldValue.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12462 */       if (SetCustomFieldValueResponse.class.equals(type))
/*       */       {
/* 12464 */         return SetCustomFieldValueResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12469 */       if (GetEntities.class.equals(type))
/*       */       {
/* 12471 */         return GetEntities.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12476 */       if (GetEntitiesResponse.class.equals(type))
/*       */       {
/* 12478 */         return GetEntitiesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12483 */       if (AnswerInvitation.class.equals(type))
/*       */       {
/* 12485 */         return AnswerInvitation.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12490 */       if (AnswerInvitationResponse.class.equals(type))
/*       */       {
/* 12492 */         return AnswerInvitationResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12497 */       if (TestConnection.class.equals(type))
/*       */       {
/* 12499 */         return TestConnection.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12504 */       if (TestConnectionResponse.class.equals(type))
/*       */       {
/* 12506 */         return TestConnectionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12511 */       if (GetTransRangeByDate.class.equals(type))
/*       */       {
/* 12513 */         return GetTransRangeByDate.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12518 */       if (GetTransRangeByDateResponse.class.equals(type))
/*       */       {
/* 12520 */         return GetTransRangeByDateResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12525 */       if (GetTransactionsByBillingClient.class.equals(type))
/*       */       {
/* 12527 */         return GetTransactionsByBillingClient.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12532 */       if (GetTransactionsByBillingClientResponse.class.equals(type))
/*       */       {
/* 12534 */         return GetTransactionsByBillingClientResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12539 */       if (SetTrackingUser.class.equals(type))
/*       */       {
/* 12541 */         return SetTrackingUser.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12546 */       if (SetTrackingUserResponse.class.equals(type))
/*       */       {
/* 12548 */         return SetTrackingUserResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12553 */       if (AnswerInvitation2.class.equals(type))
/*       */       {
/* 12555 */         return AnswerInvitation2.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12560 */       if (AnswerInvitation2Response.class.equals(type))
/*       */       {
/* 12562 */         return AnswerInvitation2Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12567 */       if (GetDocument.class.equals(type))
/*       */       {
/* 12569 */         return GetDocument.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       }
/*       */ 
/*       */ 
/*       */       
/* 12574 */       if (GetDocumentResponse.class.equals(type))
/*       */       {
/* 12576 */         return GetDocumentResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
/*       */       
/*       */       }
/*       */     
/*       */     }
/* 12581 */     catch (Exception e) {
/* 12582 */       throw AxisFault.makeFault(e);
/*       */     } 
/* 12584 */     return null;
/*       */   }
/*       */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\CSSoapServiceStub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */