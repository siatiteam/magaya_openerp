/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SetTrackingUser implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SetTrackingUser", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */   
/*     */   protected String localContact_uuid;
/*     */   
/*     */   protected String localLogin;
/*     */   
/*     */   protected String localPassword;
/*     */   
/*     */   protected String localUser_config_xml;
/*     */   
/*     */   protected int localXml_flags;
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getContact_uuid() {
/*  71 */     return this.localContact_uuid;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setContact_uuid(String param) {
/*  82 */     this.localContact_uuid = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getLogin() {
/* 101 */     return this.localLogin;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setLogin(String param) {
/* 112 */     this.localLogin = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getPassword() {
/* 131 */     return this.localPassword;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPassword(String param) {
/* 142 */     this.localPassword = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getUser_config_xml() {
/* 161 */     return this.localUser_config_xml;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setUser_config_xml(String param) {
/* 172 */     this.localUser_config_xml = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getXml_flags() {
/* 191 */     return this.localXml_flags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setXml_flags(int param) {
/* 202 */     this.localXml_flags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 222 */     ADBDataSource aDBDataSource = 
/* 223 */       new ADBDataSource(this, MY_QNAME);
/* 224 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 231 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 242 */     String prefix = null;
/* 243 */     String namespace = null;
/*     */ 
/*     */     
/* 246 */     prefix = parentQName.getPrefix();
/* 247 */     namespace = parentQName.getNamespaceURI();
/* 248 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 250 */     if (serializeType) {
/*     */ 
/*     */       
/* 253 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 254 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 255 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 256 */             String.valueOf(namespacePrefix) + ":SetTrackingUser", 
/* 257 */             xmlWriter);
/*     */       } else {
/* 259 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 260 */             "SetTrackingUser", 
/* 261 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 267 */     namespace = "";
/* 268 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 270 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 272 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 275 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 278 */     xmlWriter.writeEndElement();
/*     */     
/* 280 */     namespace = "";
/* 281 */     writeStartElement(null, namespace, "contact_uuid", xmlWriter);
/*     */ 
/*     */     
/* 284 */     if (this.localContact_uuid == null) {
/*     */ 
/*     */       
/* 287 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 292 */       xmlWriter.writeCharacters(this.localContact_uuid);
/*     */     } 
/*     */ 
/*     */     
/* 296 */     xmlWriter.writeEndElement();
/*     */     
/* 298 */     namespace = "";
/* 299 */     writeStartElement(null, namespace, "login", xmlWriter);
/*     */ 
/*     */     
/* 302 */     if (this.localLogin == null) {
/*     */ 
/*     */       
/* 305 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 310 */       xmlWriter.writeCharacters(this.localLogin);
/*     */     } 
/*     */ 
/*     */     
/* 314 */     xmlWriter.writeEndElement();
/*     */     
/* 316 */     namespace = "";
/* 317 */     writeStartElement(null, namespace, "password", xmlWriter);
/*     */ 
/*     */     
/* 320 */     if (this.localPassword == null) {
/*     */ 
/*     */       
/* 323 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 328 */       xmlWriter.writeCharacters(this.localPassword);
/*     */     } 
/*     */ 
/*     */     
/* 332 */     xmlWriter.writeEndElement();
/*     */     
/* 334 */     namespace = "";
/* 335 */     writeStartElement(null, namespace, "user_config_xml", xmlWriter);
/*     */ 
/*     */     
/* 338 */     if (this.localUser_config_xml == null) {
/*     */ 
/*     */       
/* 341 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 346 */       xmlWriter.writeCharacters(this.localUser_config_xml);
/*     */     } 
/*     */ 
/*     */     
/* 350 */     xmlWriter.writeEndElement();
/*     */     
/* 352 */     namespace = "";
/* 353 */     writeStartElement(null, namespace, "xml_flags", xmlWriter);
/*     */     
/* 355 */     if (this.localXml_flags == Integer.MIN_VALUE) {
/*     */       
/* 357 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 360 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localXml_flags));
/*     */     } 
/*     */     
/* 363 */     xmlWriter.writeEndElement();
/*     */     
/* 365 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 371 */     if (namespace.equals("urn:CSSoapService")) {
/* 372 */       return "ns1";
/*     */     }
/* 374 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 382 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 383 */     if (writerPrefix != null) {
/* 384 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 386 */       if (namespace.length() == 0) {
/* 387 */         prefix = "";
/* 388 */       } else if (prefix == null) {
/* 389 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 392 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 393 */       xmlWriter.writeNamespace(prefix, namespace);
/* 394 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 403 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 404 */       xmlWriter.writeNamespace(prefix, namespace);
/* 405 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 407 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 415 */     if (namespace.equals("")) {
/* 416 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 418 */       registerPrefix(xmlWriter, namespace);
/* 419 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 430 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 431 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 432 */     if (attributePrefix == null) {
/* 433 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 436 */     if (attributePrefix.trim().length() > 0) {
/* 437 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 439 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 442 */     if (namespace.equals("")) {
/* 443 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 445 */       registerPrefix(xmlWriter, namespace);
/* 446 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 455 */     String namespaceURI = qname.getNamespaceURI();
/* 456 */     if (namespaceURI != null) {
/* 457 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 458 */       if (prefix == null) {
/* 459 */         prefix = generatePrefix(namespaceURI);
/* 460 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 461 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 464 */       if (prefix.trim().length() > 0) {
/* 465 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 468 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 472 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 479 */     if (qnames != null) {
/*     */ 
/*     */       
/* 482 */       StringBuffer stringToWrite = new StringBuffer();
/* 483 */       String namespaceURI = null;
/* 484 */       String prefix = null;
/*     */       
/* 486 */       for (int i = 0; i < qnames.length; i++) {
/* 487 */         if (i > 0) {
/* 488 */           stringToWrite.append(" ");
/*     */         }
/* 490 */         namespaceURI = qnames[i].getNamespaceURI();
/* 491 */         if (namespaceURI != null) {
/* 492 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 493 */           if (prefix == null || prefix.length() == 0) {
/* 494 */             prefix = generatePrefix(namespaceURI);
/* 495 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 496 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 499 */           if (prefix.trim().length() > 0) {
/* 500 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 502 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 505 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 508 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 518 */     String prefix = xmlWriter.getPrefix(namespace);
/* 519 */     if (prefix == null) {
/* 520 */       prefix = generatePrefix(namespace);
/* 521 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 523 */         String uri = nsContext.getNamespaceURI(prefix);
/* 524 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 527 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 529 */       xmlWriter.writeNamespace(prefix, namespace);
/* 530 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 532 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 546 */     ArrayList<QName> elementList = new ArrayList();
/* 547 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 550 */     elementList.add(new QName("", 
/* 551 */           "access_key"));
/*     */     
/* 553 */     elementList.add(
/* 554 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*     */     
/* 556 */     elementList.add(new QName("", 
/* 557 */           "contact_uuid"));
/*     */     
/* 559 */     elementList.add((this.localContact_uuid == null) ? null : 
/* 560 */         new QName(ConverterUtil.convertToString(this.localContact_uuid)));
/*     */     
/* 562 */     elementList.add(new QName("", 
/* 563 */           "login"));
/*     */     
/* 565 */     elementList.add((this.localLogin == null) ? null : 
/* 566 */         new QName(ConverterUtil.convertToString(this.localLogin)));
/*     */     
/* 568 */     elementList.add(new QName("", 
/* 569 */           "password"));
/*     */     
/* 571 */     elementList.add((this.localPassword == null) ? null : 
/* 572 */         new QName(ConverterUtil.convertToString(this.localPassword)));
/*     */     
/* 574 */     elementList.add(new QName("", 
/* 575 */           "user_config_xml"));
/*     */     
/* 577 */     elementList.add((this.localUser_config_xml == null) ? null : 
/* 578 */         new QName(ConverterUtil.convertToString(this.localUser_config_xml)));
/*     */     
/* 580 */     elementList.add(new QName("", 
/* 581 */           "xml_flags"));
/*     */     
/* 583 */     elementList.add(
/* 584 */         new QName(ConverterUtil.convertToString(this.localXml_flags)));
/*     */ 
/*     */     
/* 587 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SetTrackingUser parse(XMLStreamReader reader) throws Exception {
/* 611 */       SetTrackingUser object = 
/* 612 */         new SetTrackingUser();
/*     */ 
/*     */       
/* 615 */       String nillableValue = null;
/* 616 */       String prefix = "";
/* 617 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 620 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 621 */           reader.next();
/*     */         }
/*     */         
/* 624 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 625 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 626 */               "type");
/* 627 */           if (fullTypeName != null) {
/* 628 */             String nsPrefix = null;
/* 629 */             if (fullTypeName.indexOf(":") > -1) {
/* 630 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 632 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 634 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 636 */             if (!"SetTrackingUser".equals(type)) {
/*     */               
/* 638 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 639 */               return (SetTrackingUser)ExtensionMapper.getTypeObject(
/* 640 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 654 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 659 */         reader.next();
/*     */ 
/*     */         
/* 662 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 664 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 666 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 667 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 670 */             String content = reader.getElementText();
/*     */             
/* 672 */             object.setAccess_key(
/* 673 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 678 */             object.setAccess_key(-2147483648);
/*     */             
/* 680 */             reader.getElementText();
/*     */           } 
/*     */           
/* 683 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 689 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 693 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 695 */         if (reader.isStartElement() && (new QName("", "contact_uuid")).equals(reader.getName())) {
/*     */           
/* 697 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 698 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 701 */             String content = reader.getElementText();
/*     */             
/* 703 */             object.setContact_uuid(
/* 704 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 709 */             reader.getElementText();
/*     */           } 
/*     */           
/* 712 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 718 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 722 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 724 */         if (reader.isStartElement() && (new QName("", "login")).equals(reader.getName())) {
/*     */           
/* 726 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 727 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 730 */             String content = reader.getElementText();
/*     */             
/* 732 */             object.setLogin(
/* 733 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 738 */             reader.getElementText();
/*     */           } 
/*     */           
/* 741 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 747 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 751 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 753 */         if (reader.isStartElement() && (new QName("", "password")).equals(reader.getName())) {
/*     */           
/* 755 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 756 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 759 */             String content = reader.getElementText();
/*     */             
/* 761 */             object.setPassword(
/* 762 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 767 */             reader.getElementText();
/*     */           } 
/*     */           
/* 770 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 776 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 780 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 782 */         if (reader.isStartElement() && (new QName("", "user_config_xml")).equals(reader.getName())) {
/*     */           
/* 784 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 785 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 788 */             String content = reader.getElementText();
/*     */             
/* 790 */             object.setUser_config_xml(
/* 791 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 796 */             reader.getElementText();
/*     */           } 
/*     */           
/* 799 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 805 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 809 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 811 */         if (reader.isStartElement() && (new QName("", "xml_flags")).equals(reader.getName())) {
/*     */           
/* 813 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 814 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 817 */             String content = reader.getElementText();
/*     */             
/* 819 */             object.setXml_flags(
/* 820 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 825 */             object.setXml_flags(-2147483648);
/*     */             
/* 827 */             reader.getElementText();
/*     */           } 
/*     */           
/* 830 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 836 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 839 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 840 */           reader.next();
/*     */         }
/* 842 */         if (reader.isStartElement())
/*     */         {
/* 844 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 849 */       catch (XMLStreamException e) {
/* 850 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 853 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SetTrackingUser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */