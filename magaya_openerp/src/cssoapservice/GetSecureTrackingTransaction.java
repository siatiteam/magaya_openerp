/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetSecureTrackingTransaction implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetSecureTrackingTransaction", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected String localUser;
/*     */ 
/*     */   
/*     */   protected String localPass;
/*     */ 
/*     */   
/*     */   protected String localApp;
/*     */ 
/*     */   
/*     */   protected String localNumber;
/*     */ 
/*     */ 
/*     */   
/*     */   public String getUser() {
/*  41 */     return this.localUser;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setUser(String param) {
/*  52 */     this.localUser = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getPass() {
/*  71 */     return this.localPass;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPass(String param) {
/*  82 */     this.localPass = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getApp() {
/* 101 */     return this.localApp;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setApp(String param) {
/* 112 */     this.localApp = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNumber() {
/* 131 */     return this.localNumber;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNumber(String param) {
/* 142 */     this.localNumber = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 162 */     ADBDataSource aDBDataSource = 
/* 163 */       new ADBDataSource(this, MY_QNAME);
/* 164 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 171 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 182 */     String prefix = null;
/* 183 */     String namespace = null;
/*     */ 
/*     */     
/* 186 */     prefix = parentQName.getPrefix();
/* 187 */     namespace = parentQName.getNamespaceURI();
/* 188 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 190 */     if (serializeType) {
/*     */ 
/*     */       
/* 193 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 194 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 195 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 196 */             String.valueOf(namespacePrefix) + ":GetSecureTrackingTransaction", 
/* 197 */             xmlWriter);
/*     */       } else {
/* 199 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 200 */             "GetSecureTrackingTransaction", 
/* 201 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 207 */     namespace = "";
/* 208 */     writeStartElement(null, namespace, "user", xmlWriter);
/*     */ 
/*     */     
/* 211 */     if (this.localUser == null) {
/*     */ 
/*     */       
/* 214 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 219 */       xmlWriter.writeCharacters(this.localUser);
/*     */     } 
/*     */ 
/*     */     
/* 223 */     xmlWriter.writeEndElement();
/*     */     
/* 225 */     namespace = "";
/* 226 */     writeStartElement(null, namespace, "pass", xmlWriter);
/*     */ 
/*     */     
/* 229 */     if (this.localPass == null) {
/*     */ 
/*     */       
/* 232 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 237 */       xmlWriter.writeCharacters(this.localPass);
/*     */     } 
/*     */ 
/*     */     
/* 241 */     xmlWriter.writeEndElement();
/*     */     
/* 243 */     namespace = "";
/* 244 */     writeStartElement(null, namespace, "app", xmlWriter);
/*     */ 
/*     */     
/* 247 */     if (this.localApp == null) {
/*     */ 
/*     */       
/* 250 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 255 */       xmlWriter.writeCharacters(this.localApp);
/*     */     } 
/*     */ 
/*     */     
/* 259 */     xmlWriter.writeEndElement();
/*     */     
/* 261 */     namespace = "";
/* 262 */     writeStartElement(null, namespace, "number", xmlWriter);
/*     */ 
/*     */     
/* 265 */     if (this.localNumber == null) {
/*     */ 
/*     */       
/* 268 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 273 */       xmlWriter.writeCharacters(this.localNumber);
/*     */     } 
/*     */ 
/*     */     
/* 277 */     xmlWriter.writeEndElement();
/*     */     
/* 279 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 285 */     if (namespace.equals("urn:CSSoapService")) {
/* 286 */       return "ns1";
/*     */     }
/* 288 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 296 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 297 */     if (writerPrefix != null) {
/* 298 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 300 */       if (namespace.length() == 0) {
/* 301 */         prefix = "";
/* 302 */       } else if (prefix == null) {
/* 303 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 306 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 307 */       xmlWriter.writeNamespace(prefix, namespace);
/* 308 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 317 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 318 */       xmlWriter.writeNamespace(prefix, namespace);
/* 319 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 321 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 329 */     if (namespace.equals("")) {
/* 330 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 332 */       registerPrefix(xmlWriter, namespace);
/* 333 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 344 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 345 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 346 */     if (attributePrefix == null) {
/* 347 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 350 */     if (attributePrefix.trim().length() > 0) {
/* 351 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 353 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 356 */     if (namespace.equals("")) {
/* 357 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 359 */       registerPrefix(xmlWriter, namespace);
/* 360 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 369 */     String namespaceURI = qname.getNamespaceURI();
/* 370 */     if (namespaceURI != null) {
/* 371 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 372 */       if (prefix == null) {
/* 373 */         prefix = generatePrefix(namespaceURI);
/* 374 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 375 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 378 */       if (prefix.trim().length() > 0) {
/* 379 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 382 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 386 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 393 */     if (qnames != null) {
/*     */ 
/*     */       
/* 396 */       StringBuffer stringToWrite = new StringBuffer();
/* 397 */       String namespaceURI = null;
/* 398 */       String prefix = null;
/*     */       
/* 400 */       for (int i = 0; i < qnames.length; i++) {
/* 401 */         if (i > 0) {
/* 402 */           stringToWrite.append(" ");
/*     */         }
/* 404 */         namespaceURI = qnames[i].getNamespaceURI();
/* 405 */         if (namespaceURI != null) {
/* 406 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 407 */           if (prefix == null || prefix.length() == 0) {
/* 408 */             prefix = generatePrefix(namespaceURI);
/* 409 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 410 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 413 */           if (prefix.trim().length() > 0) {
/* 414 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 416 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 419 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 422 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 432 */     String prefix = xmlWriter.getPrefix(namespace);
/* 433 */     if (prefix == null) {
/* 434 */       prefix = generatePrefix(namespace);
/* 435 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 437 */         String uri = nsContext.getNamespaceURI(prefix);
/* 438 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 441 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 443 */       xmlWriter.writeNamespace(prefix, namespace);
/* 444 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 446 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 460 */     ArrayList<QName> elementList = new ArrayList();
/* 461 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 464 */     elementList.add(new QName("", 
/* 465 */           "user"));
/*     */     
/* 467 */     elementList.add((this.localUser == null) ? null : 
/* 468 */         new QName(ConverterUtil.convertToString(this.localUser)));
/*     */     
/* 470 */     elementList.add(new QName("", 
/* 471 */           "pass"));
/*     */     
/* 473 */     elementList.add((this.localPass == null) ? null : 
/* 474 */         new QName(ConverterUtil.convertToString(this.localPass)));
/*     */     
/* 476 */     elementList.add(new QName("", 
/* 477 */           "app"));
/*     */     
/* 479 */     elementList.add((this.localApp == null) ? null : 
/* 480 */         new QName(ConverterUtil.convertToString(this.localApp)));
/*     */     
/* 482 */     elementList.add(new QName("", 
/* 483 */           "number"));
/*     */     
/* 485 */     elementList.add((this.localNumber == null) ? null : 
/* 486 */         new QName(ConverterUtil.convertToString(this.localNumber)));
/*     */ 
/*     */     
/* 489 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetSecureTrackingTransaction parse(XMLStreamReader reader) throws Exception {
/* 513 */       GetSecureTrackingTransaction object = 
/* 514 */         new GetSecureTrackingTransaction();
/*     */ 
/*     */       
/* 517 */       String nillableValue = null;
/* 518 */       String prefix = "";
/* 519 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 522 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 523 */           reader.next();
/*     */         }
/*     */         
/* 526 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 527 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 528 */               "type");
/* 529 */           if (fullTypeName != null) {
/* 530 */             String nsPrefix = null;
/* 531 */             if (fullTypeName.indexOf(":") > -1) {
/* 532 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 534 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 536 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 538 */             if (!"GetSecureTrackingTransaction".equals(type)) {
/*     */               
/* 540 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 541 */               return (GetSecureTrackingTransaction)ExtensionMapper.getTypeObject(
/* 542 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 556 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 561 */         reader.next();
/*     */ 
/*     */         
/* 564 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 566 */         if (reader.isStartElement() && (new QName("", "user")).equals(reader.getName())) {
/*     */           
/* 568 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 569 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 572 */             String content = reader.getElementText();
/*     */             
/* 574 */             object.setUser(
/* 575 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 580 */             reader.getElementText();
/*     */           } 
/*     */           
/* 583 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 589 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 593 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 595 */         if (reader.isStartElement() && (new QName("", "pass")).equals(reader.getName())) {
/*     */           
/* 597 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 598 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 601 */             String content = reader.getElementText();
/*     */             
/* 603 */             object.setPass(
/* 604 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 609 */             reader.getElementText();
/*     */           } 
/*     */           
/* 612 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 618 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 622 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 624 */         if (reader.isStartElement() && (new QName("", "app")).equals(reader.getName())) {
/*     */           
/* 626 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 627 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 630 */             String content = reader.getElementText();
/*     */             
/* 632 */             object.setApp(
/* 633 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 638 */             reader.getElementText();
/*     */           } 
/*     */           
/* 641 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 647 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 651 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 653 */         if (reader.isStartElement() && (new QName("", "number")).equals(reader.getName())) {
/*     */           
/* 655 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 656 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 659 */             String content = reader.getElementText();
/*     */             
/* 661 */             object.setNumber(
/* 662 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 667 */             reader.getElementText();
/*     */           } 
/*     */           
/* 670 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 676 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 679 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 680 */           reader.next();
/*     */         }
/* 682 */         if (reader.isStartElement())
/*     */         {
/* 684 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 689 */       catch (XMLStreamException e) {
/* 690 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 693 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetSecureTrackingTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */