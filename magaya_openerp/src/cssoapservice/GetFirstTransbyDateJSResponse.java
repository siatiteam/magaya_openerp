/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetFirstTransbyDateJSResponse implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetFirstTransbyDateJSResponse", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */   
/*     */   protected Api_session_error local_return;
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localCookie;
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localMore_results;
/*     */ 
/*     */ 
/*     */   
/*     */   public Api_session_error get_return() {
/*  41 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(Api_session_error param) {
/*  52 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCookie() {
/*  71 */     return this.localCookie;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCookie(String param) {
/*  82 */     this.localCookie = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getMore_results() {
/* 101 */     return this.localMore_results;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setMore_results(int param) {
/* 112 */     this.localMore_results = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 132 */     ADBDataSource aDBDataSource = 
/* 133 */       new ADBDataSource(this, MY_QNAME);
/* 134 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 141 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 152 */     String prefix = null;
/* 153 */     String namespace = null;
/*     */ 
/*     */     
/* 156 */     prefix = parentQName.getPrefix();
/* 157 */     namespace = parentQName.getNamespaceURI();
/* 158 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 160 */     if (serializeType) {
/*     */ 
/*     */       
/* 163 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 164 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 165 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 166 */             String.valueOf(namespacePrefix) + ":GetFirstTransbyDateJSResponse", 
/* 167 */             xmlWriter);
/*     */       } else {
/* 169 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 170 */             "GetFirstTransbyDateJSResponse", 
/* 171 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 177 */     if (this.local_return == null) {
/*     */       
/* 179 */       writeStartElement(null, "", "return", xmlWriter);
/*     */ 
/*     */       
/* 182 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/* 183 */       xmlWriter.writeEndElement();
/*     */     } else {
/* 185 */       this.local_return.serialize(new QName("", "return"), 
/* 186 */           xmlWriter);
/*     */     } 
/*     */     
/* 189 */     namespace = "";
/* 190 */     writeStartElement(null, namespace, "cookie", xmlWriter);
/*     */ 
/*     */     
/* 193 */     if (this.localCookie == null) {
/*     */ 
/*     */       
/* 196 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 201 */       xmlWriter.writeCharacters(this.localCookie);
/*     */     } 
/*     */ 
/*     */     
/* 205 */     xmlWriter.writeEndElement();
/*     */     
/* 207 */     namespace = "";
/* 208 */     writeStartElement(null, namespace, "more_results", xmlWriter);
/*     */     
/* 210 */     if (this.localMore_results == Integer.MIN_VALUE) {
/*     */       
/* 212 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 215 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localMore_results));
/*     */     } 
/*     */     
/* 218 */     xmlWriter.writeEndElement();
/*     */     
/* 220 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 226 */     if (namespace.equals("urn:CSSoapService")) {
/* 227 */       return "ns1";
/*     */     }
/* 229 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 237 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 238 */     if (writerPrefix != null) {
/* 239 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 241 */       if (namespace.length() == 0) {
/* 242 */         prefix = "";
/* 243 */       } else if (prefix == null) {
/* 244 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 247 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 248 */       xmlWriter.writeNamespace(prefix, namespace);
/* 249 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 258 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 259 */       xmlWriter.writeNamespace(prefix, namespace);
/* 260 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 262 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 270 */     if (namespace.equals("")) {
/* 271 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 273 */       registerPrefix(xmlWriter, namespace);
/* 274 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 285 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 286 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 287 */     if (attributePrefix == null) {
/* 288 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 291 */     if (attributePrefix.trim().length() > 0) {
/* 292 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 294 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 297 */     if (namespace.equals("")) {
/* 298 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 300 */       registerPrefix(xmlWriter, namespace);
/* 301 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 310 */     String namespaceURI = qname.getNamespaceURI();
/* 311 */     if (namespaceURI != null) {
/* 312 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 313 */       if (prefix == null) {
/* 314 */         prefix = generatePrefix(namespaceURI);
/* 315 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 316 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 319 */       if (prefix.trim().length() > 0) {
/* 320 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 323 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 327 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 334 */     if (qnames != null) {
/*     */ 
/*     */       
/* 337 */       StringBuffer stringToWrite = new StringBuffer();
/* 338 */       String namespaceURI = null;
/* 339 */       String prefix = null;
/*     */       
/* 341 */       for (int i = 0; i < qnames.length; i++) {
/* 342 */         if (i > 0) {
/* 343 */           stringToWrite.append(" ");
/*     */         }
/* 345 */         namespaceURI = qnames[i].getNamespaceURI();
/* 346 */         if (namespaceURI != null) {
/* 347 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 348 */           if (prefix == null || prefix.length() == 0) {
/* 349 */             prefix = generatePrefix(namespaceURI);
/* 350 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 351 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 354 */           if (prefix.trim().length() > 0) {
/* 355 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 357 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 360 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 363 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 373 */     String prefix = xmlWriter.getPrefix(namespace);
/* 374 */     if (prefix == null) {
/* 375 */       prefix = generatePrefix(namespace);
/* 376 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 378 */         String uri = nsContext.getNamespaceURI(prefix);
/* 379 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 382 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 384 */       xmlWriter.writeNamespace(prefix, namespace);
/* 385 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 387 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 401 */     ArrayList<QName> elementList = new ArrayList();
/* 402 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 405 */     elementList.add(new QName("", 
/* 406 */           "return"));
/*     */ 
/*     */     
/* 409 */     elementList.add((this.local_return == null) ? null : 
/* 410 */         new QName(ConverterUtil.convertToString(this.local_return)));
/*     */     
/* 412 */     elementList.add(new QName("", 
/* 413 */           "cookie"));
/*     */     
/* 415 */     elementList.add((this.localCookie == null) ? null : 
/* 416 */         new QName(ConverterUtil.convertToString(this.localCookie)));
/*     */     
/* 418 */     elementList.add(new QName("", 
/* 419 */           "more_results"));
/*     */     
/* 421 */     elementList.add(
/* 422 */         new QName(ConverterUtil.convertToString(this.localMore_results)));
/*     */ 
/*     */     
/* 425 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetFirstTransbyDateJSResponse parse(XMLStreamReader reader) throws Exception {
/* 449 */       GetFirstTransbyDateJSResponse object = 
/* 450 */         new GetFirstTransbyDateJSResponse();
/*     */ 
/*     */       
/* 453 */       String nillableValue = null;
/* 454 */       String prefix = "";
/* 455 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 458 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 459 */           reader.next();
/*     */         }
/*     */         
/* 462 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 463 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 464 */               "type");
/* 465 */           if (fullTypeName != null) {
/* 466 */             String nsPrefix = null;
/* 467 */             if (fullTypeName.indexOf(":") > -1) {
/* 468 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 470 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 472 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 474 */             if (!"GetFirstTransbyDateJSResponse".equals(type)) {
/*     */               
/* 476 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 477 */               return (GetFirstTransbyDateJSResponse)ExtensionMapper.getTypeObject(
/* 478 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 492 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 497 */         reader.next();
/*     */ 
/*     */         
/* 500 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 502 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
/*     */           
/* 504 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 505 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 506 */             object.set_return(null);
/* 507 */             reader.next();
/*     */             
/* 509 */             reader.next();
/*     */           }
/*     */           else {
/*     */             
/* 513 */             object.set_return(Api_session_error.Factory.parse(reader));
/*     */             
/* 515 */             reader.next();
/*     */           }
/*     */         
/*     */         }
/*     */         else {
/*     */           
/* 521 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 525 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 527 */         if (reader.isStartElement() && (new QName("", "cookie")).equals(reader.getName())) {
/*     */           
/* 529 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 530 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 533 */             String content = reader.getElementText();
/*     */             
/* 535 */             object.setCookie(
/* 536 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 541 */             reader.getElementText();
/*     */           } 
/*     */           
/* 544 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 550 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 554 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 556 */         if (reader.isStartElement() && (new QName("", "more_results")).equals(reader.getName())) {
/*     */           
/* 558 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 559 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 562 */             String content = reader.getElementText();
/*     */             
/* 564 */             object.setMore_results(
/* 565 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 570 */             object.setMore_results(-2147483648);
/*     */             
/* 572 */             reader.getElementText();
/*     */           } 
/*     */           
/* 575 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 581 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 584 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 585 */           reader.next();
/*     */         }
/* 587 */         if (reader.isStartElement())
/*     */         {
/* 589 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 594 */       catch (XMLStreamException e) {
/* 595 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 598 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetFirstTransbyDateJSResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */