/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetEntitiesResponse implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetEntitiesResponse", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected Api_session_error local_return;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localEntity_list_xml;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public Api_session_error get_return() {
/*  41 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(Api_session_error param) {
/*  52 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEntity_list_xml() {
/*  71 */     return this.localEntity_list_xml;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEntity_list_xml(String param) {
/*  82 */     this.localEntity_list_xml = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 102 */     ADBDataSource aDBDataSource = 
/* 103 */       new ADBDataSource(this, MY_QNAME);
/* 104 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 111 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 122 */     String prefix = null;
/* 123 */     String namespace = null;
/*     */ 
/*     */     
/* 126 */     prefix = parentQName.getPrefix();
/* 127 */     namespace = parentQName.getNamespaceURI();
/* 128 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 130 */     if (serializeType) {
/*     */ 
/*     */       
/* 133 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 134 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 135 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 136 */             String.valueOf(namespacePrefix) + ":GetEntitiesResponse", 
/* 137 */             xmlWriter);
/*     */       } else {
/* 139 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 140 */             "GetEntitiesResponse", 
/* 141 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 147 */     if (this.local_return == null) {
/*     */       
/* 149 */       writeStartElement(null, "", "return", xmlWriter);
/*     */ 
/*     */       
/* 152 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/* 153 */       xmlWriter.writeEndElement();
/*     */     } else {
/* 155 */       this.local_return.serialize(new QName("", "return"), 
/* 156 */           xmlWriter);
/*     */     } 
/*     */     
/* 159 */     namespace = "";
/* 160 */     writeStartElement(null, namespace, "entity_list_xml", xmlWriter);
/*     */ 
/*     */     
/* 163 */     if (this.localEntity_list_xml == null) {
/*     */ 
/*     */       
/* 166 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 171 */       xmlWriter.writeCharacters(this.localEntity_list_xml);
/*     */     } 
/*     */ 
/*     */     
/* 175 */     xmlWriter.writeEndElement();
/*     */     
/* 177 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 183 */     if (namespace.equals("urn:CSSoapService")) {
/* 184 */       return "ns1";
/*     */     }
/* 186 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 194 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 195 */     if (writerPrefix != null) {
/* 196 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 198 */       if (namespace.length() == 0) {
/* 199 */         prefix = "";
/* 200 */       } else if (prefix == null) {
/* 201 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 204 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 205 */       xmlWriter.writeNamespace(prefix, namespace);
/* 206 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 215 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 216 */       xmlWriter.writeNamespace(prefix, namespace);
/* 217 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 219 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 227 */     if (namespace.equals("")) {
/* 228 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 230 */       registerPrefix(xmlWriter, namespace);
/* 231 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 242 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 243 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 244 */     if (attributePrefix == null) {
/* 245 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 248 */     if (attributePrefix.trim().length() > 0) {
/* 249 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 251 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 254 */     if (namespace.equals("")) {
/* 255 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 257 */       registerPrefix(xmlWriter, namespace);
/* 258 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 267 */     String namespaceURI = qname.getNamespaceURI();
/* 268 */     if (namespaceURI != null) {
/* 269 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 270 */       if (prefix == null) {
/* 271 */         prefix = generatePrefix(namespaceURI);
/* 272 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 273 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 276 */       if (prefix.trim().length() > 0) {
/* 277 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 280 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 284 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 291 */     if (qnames != null) {
/*     */ 
/*     */       
/* 294 */       StringBuffer stringToWrite = new StringBuffer();
/* 295 */       String namespaceURI = null;
/* 296 */       String prefix = null;
/*     */       
/* 298 */       for (int i = 0; i < qnames.length; i++) {
/* 299 */         if (i > 0) {
/* 300 */           stringToWrite.append(" ");
/*     */         }
/* 302 */         namespaceURI = qnames[i].getNamespaceURI();
/* 303 */         if (namespaceURI != null) {
/* 304 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 305 */           if (prefix == null || prefix.length() == 0) {
/* 306 */             prefix = generatePrefix(namespaceURI);
/* 307 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 308 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 311 */           if (prefix.trim().length() > 0) {
/* 312 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 314 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 317 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 320 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 330 */     String prefix = xmlWriter.getPrefix(namespace);
/* 331 */     if (prefix == null) {
/* 332 */       prefix = generatePrefix(namespace);
/* 333 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 335 */         String uri = nsContext.getNamespaceURI(prefix);
/* 336 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 339 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 341 */       xmlWriter.writeNamespace(prefix, namespace);
/* 342 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 344 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 358 */     ArrayList<QName> elementList = new ArrayList();
/* 359 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 362 */     elementList.add(new QName("", 
/* 363 */           "return"));
/*     */ 
/*     */     
/* 366 */     elementList.add((this.local_return == null) ? null : 
/* 367 */         new QName(ConverterUtil.convertToString(this.local_return)));
/*     */     
/* 369 */     elementList.add(new QName("", 
/* 370 */           "entity_list_xml"));
/*     */     
/* 372 */     elementList.add((this.localEntity_list_xml == null) ? null : 
/* 373 */         new QName(ConverterUtil.convertToString(this.localEntity_list_xml)));
/*     */ 
/*     */     
/* 376 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetEntitiesResponse parse(XMLStreamReader reader) throws Exception {
/* 400 */       GetEntitiesResponse object = 
/* 401 */         new GetEntitiesResponse();
/*     */ 
/*     */       
/* 404 */       String nillableValue = null;
/* 405 */       String prefix = "";
/* 406 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 409 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 410 */           reader.next();
/*     */         }
/*     */         
/* 413 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 414 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 415 */               "type");
/* 416 */           if (fullTypeName != null) {
/* 417 */             String nsPrefix = null;
/* 418 */             if (fullTypeName.indexOf(":") > -1) {
/* 419 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 421 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 423 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 425 */             if (!"GetEntitiesResponse".equals(type)) {
/*     */               
/* 427 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 428 */               return (GetEntitiesResponse)ExtensionMapper.getTypeObject(
/* 429 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 443 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 448 */         reader.next();
/*     */ 
/*     */         
/* 451 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 453 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
/*     */           
/* 455 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 456 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 457 */             object.set_return(null);
/* 458 */             reader.next();
/*     */             
/* 460 */             reader.next();
/*     */           }
/*     */           else {
/*     */             
/* 464 */             object.set_return(Api_session_error.Factory.parse(reader));
/*     */             
/* 466 */             reader.next();
/*     */           }
/*     */         
/*     */         }
/*     */         else {
/*     */           
/* 472 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 476 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 478 */         if (reader.isStartElement() && (new QName("", "entity_list_xml")).equals(reader.getName())) {
/*     */           
/* 480 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 481 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 484 */             String content = reader.getElementText();
/*     */             
/* 486 */             object.setEntity_list_xml(
/* 487 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 492 */             reader.getElementText();
/*     */           } 
/*     */           
/* 495 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 501 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 504 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 505 */           reader.next();
/*     */         }
/* 507 */         if (reader.isStartElement())
/*     */         {
/* 509 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 514 */       catch (XMLStreamException e) {
/* 515 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 518 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetEntitiesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */