/*    */ package cssoapservice;
/*    */ 
/*    */ import javax.xml.stream.XMLStreamReader;
/*    */ import org.apache.axis2.databinding.ADBException;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ExtensionMapper
/*    */ {
/*    */   public static Object getTypeObject(String namespaceURI, String typeName, XMLStreamReader reader) throws Exception {
/* 25 */     if ("urn:CSSoapService".equals(namespaceURI) && 
/* 26 */       "contact_info2".equals(typeName))
/*    */     {
/* 28 */       return Contact_info2.Factory.parse(reader);
/*    */     }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 35 */     if ("urn:CSSoapService".equals(namespaceURI) && 
/* 36 */       "contact_info".equals(typeName))
/*    */     {
/* 38 */       return Contact_info.Factory.parse(reader);
/*    */     }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 45 */     if ("urn:CSSoapService".equals(namespaceURI) && 
/* 46 */       "api_session_error".equals(typeName))
/*    */     {
/* 48 */       return Api_session_error.Factory.parse(reader);
/*    */     }
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 54 */     throw new ADBException("Unsupported type " + namespaceURI + " " + typeName);
/*    */   }
/*    */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\ExtensionMapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */