/*      */ package cssoapservice;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.Vector;
/*      */ import javax.xml.namespace.NamespaceContext;
/*      */ import javax.xml.namespace.QName;
/*      */ import javax.xml.stream.XMLStreamException;
/*      */ import javax.xml.stream.XMLStreamReader;
/*      */ import javax.xml.stream.XMLStreamWriter;
/*      */ import org.apache.axiom.om.OMDataSource;
/*      */ import org.apache.axiom.om.OMElement;
/*      */ import org.apache.axiom.om.OMFactory;
/*      */ import org.apache.axis2.databinding.ADBBean;
/*      */ import org.apache.axis2.databinding.ADBDataSource;
/*      */ import org.apache.axis2.databinding.ADBException;
/*      */ import org.apache.axis2.databinding.utils.BeanUtil;
/*      */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*      */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*      */ 
/*      */ public class AnswerInvitation implements ADBBean {
/*   21 */   public static final QName MY_QNAME = new QName(
/*   22 */       "urn:CSSoapService", 
/*   23 */       "AnswerInvitation", 
/*   24 */       "ns1");
/*      */   
/*      */   protected int localBAccepted;
/*      */   
/*      */   protected String localUuid;
/*      */   
/*      */   protected String localEmployee;
/*      */   
/*      */   protected String localBody;
/*      */   
/*      */   protected String localCompany;
/*      */   
/*      */   protected String localCountry;
/*      */   protected String localCity;
/*      */   protected Contact_info localPInfo;
/*      */   
/*      */   public int getBAccepted() {
/*   41 */     return this.localBAccepted;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setBAccepted(int param) {
/*   52 */     this.localBAccepted = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getUuid() {
/*   71 */     return this.localUuid;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setUuid(String param) {
/*   82 */     this.localUuid = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getEmployee() {
/*  101 */     return this.localEmployee;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setEmployee(String param) {
/*  112 */     this.localEmployee = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getBody() {
/*  131 */     return this.localBody;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setBody(String param) {
/*  142 */     this.localBody = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getCompany() {
/*  161 */     return this.localCompany;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setCompany(String param) {
/*  172 */     this.localCompany = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getCountry() {
/*  191 */     return this.localCountry;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setCountry(String param) {
/*  202 */     this.localCountry = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getCity() {
/*  221 */     return this.localCity;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setCity(String param) {
/*  232 */     this.localCity = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public Contact_info getPInfo() {
/*  251 */     return this.localPInfo;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setPInfo(Contact_info param) {
/*  262 */     this.localPInfo = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  282 */     ADBDataSource aDBDataSource = 
/*  283 */       new ADBDataSource(this, MY_QNAME);
/*  284 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  291 */     serialize(parentQName, xmlWriter, false);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  302 */     String prefix = null;
/*  303 */     String namespace = null;
/*      */ 
/*      */     
/*  306 */     prefix = parentQName.getPrefix();
/*  307 */     namespace = parentQName.getNamespaceURI();
/*  308 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*      */     
/*  310 */     if (serializeType) {
/*      */ 
/*      */       
/*  313 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/*  314 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/*  315 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  316 */             String.valueOf(namespacePrefix) + ":AnswerInvitation", 
/*  317 */             xmlWriter);
/*      */       } else {
/*  319 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  320 */             "AnswerInvitation", 
/*  321 */             xmlWriter);
/*      */       } 
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*  327 */     namespace = "";
/*  328 */     writeStartElement(null, namespace, "bAccepted", xmlWriter);
/*      */     
/*  330 */     if (this.localBAccepted == Integer.MIN_VALUE) {
/*      */       
/*  332 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  335 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localBAccepted));
/*      */     } 
/*      */     
/*  338 */     xmlWriter.writeEndElement();
/*      */     
/*  340 */     namespace = "";
/*  341 */     writeStartElement(null, namespace, "uuid", xmlWriter);
/*      */ 
/*      */     
/*  344 */     if (this.localUuid == null) {
/*      */ 
/*      */       
/*  347 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  352 */       xmlWriter.writeCharacters(this.localUuid);
/*      */     } 
/*      */ 
/*      */     
/*  356 */     xmlWriter.writeEndElement();
/*      */     
/*  358 */     namespace = "";
/*  359 */     writeStartElement(null, namespace, "employee", xmlWriter);
/*      */ 
/*      */     
/*  362 */     if (this.localEmployee == null) {
/*      */ 
/*      */       
/*  365 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  370 */       xmlWriter.writeCharacters(this.localEmployee);
/*      */     } 
/*      */ 
/*      */     
/*  374 */     xmlWriter.writeEndElement();
/*      */     
/*  376 */     namespace = "";
/*  377 */     writeStartElement(null, namespace, "body", xmlWriter);
/*      */ 
/*      */     
/*  380 */     if (this.localBody == null) {
/*      */ 
/*      */       
/*  383 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  388 */       xmlWriter.writeCharacters(this.localBody);
/*      */     } 
/*      */ 
/*      */     
/*  392 */     xmlWriter.writeEndElement();
/*      */     
/*  394 */     namespace = "";
/*  395 */     writeStartElement(null, namespace, "company", xmlWriter);
/*      */ 
/*      */     
/*  398 */     if (this.localCompany == null) {
/*      */ 
/*      */       
/*  401 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  406 */       xmlWriter.writeCharacters(this.localCompany);
/*      */     } 
/*      */ 
/*      */     
/*  410 */     xmlWriter.writeEndElement();
/*      */     
/*  412 */     namespace = "";
/*  413 */     writeStartElement(null, namespace, "country", xmlWriter);
/*      */ 
/*      */     
/*  416 */     if (this.localCountry == null) {
/*      */ 
/*      */       
/*  419 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  424 */       xmlWriter.writeCharacters(this.localCountry);
/*      */     } 
/*      */ 
/*      */     
/*  428 */     xmlWriter.writeEndElement();
/*      */     
/*  430 */     namespace = "";
/*  431 */     writeStartElement(null, namespace, "city", xmlWriter);
/*      */ 
/*      */     
/*  434 */     if (this.localCity == null) {
/*      */ 
/*      */       
/*  437 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  442 */       xmlWriter.writeCharacters(this.localCity);
/*      */     } 
/*      */ 
/*      */     
/*  446 */     xmlWriter.writeEndElement();
/*      */     
/*  448 */     if (this.localPInfo == null) {
/*      */       
/*  450 */       writeStartElement(null, "", "pInfo", xmlWriter);
/*      */ 
/*      */       
/*  453 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*  454 */       xmlWriter.writeEndElement();
/*      */     } else {
/*  456 */       this.localPInfo.serialize(new QName("", "pInfo"), 
/*  457 */           xmlWriter);
/*      */     } 
/*      */     
/*  460 */     xmlWriter.writeEndElement();
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private static String generatePrefix(String namespace) {
/*  466 */     if (namespace.equals("urn:CSSoapService")) {
/*  467 */       return "ns1";
/*      */     }
/*  469 */     return BeanUtil.getUniquePrefix();
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  477 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/*  478 */     if (writerPrefix != null) {
/*  479 */       xmlWriter.writeStartElement(namespace, localPart);
/*      */     } else {
/*  481 */       if (namespace.length() == 0) {
/*  482 */         prefix = "";
/*  483 */       } else if (prefix == null) {
/*  484 */         prefix = generatePrefix(namespace);
/*      */       } 
/*      */       
/*  487 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/*  488 */       xmlWriter.writeNamespace(prefix, namespace);
/*  489 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  498 */     if (xmlWriter.getPrefix(namespace) == null) {
/*  499 */       xmlWriter.writeNamespace(prefix, namespace);
/*  500 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  502 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  510 */     if (namespace.equals("")) {
/*  511 */       xmlWriter.writeAttribute(attName, attValue);
/*      */     } else {
/*  513 */       registerPrefix(xmlWriter, namespace);
/*  514 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  525 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/*  526 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/*  527 */     if (attributePrefix == null) {
/*  528 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*      */     }
/*      */     
/*  531 */     if (attributePrefix.trim().length() > 0) {
/*  532 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*      */     } else {
/*  534 */       attributeValue = qname.getLocalPart();
/*      */     } 
/*      */     
/*  537 */     if (namespace.equals("")) {
/*  538 */       xmlWriter.writeAttribute(attName, attributeValue);
/*      */     } else {
/*  540 */       registerPrefix(xmlWriter, namespace);
/*  541 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  550 */     String namespaceURI = qname.getNamespaceURI();
/*  551 */     if (namespaceURI != null) {
/*  552 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/*  553 */       if (prefix == null) {
/*  554 */         prefix = generatePrefix(namespaceURI);
/*  555 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/*  556 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*      */       } 
/*      */       
/*  559 */       if (prefix.trim().length() > 0) {
/*  560 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*      */       } else {
/*      */         
/*  563 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */       } 
/*      */     } else {
/*      */       
/*  567 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  574 */     if (qnames != null) {
/*      */ 
/*      */       
/*  577 */       StringBuffer stringToWrite = new StringBuffer();
/*  578 */       String namespaceURI = null;
/*  579 */       String prefix = null;
/*      */       
/*  581 */       for (int i = 0; i < qnames.length; i++) {
/*  582 */         if (i > 0) {
/*  583 */           stringToWrite.append(" ");
/*      */         }
/*  585 */         namespaceURI = qnames[i].getNamespaceURI();
/*  586 */         if (namespaceURI != null) {
/*  587 */           prefix = xmlWriter.getPrefix(namespaceURI);
/*  588 */           if (prefix == null || prefix.length() == 0) {
/*  589 */             prefix = generatePrefix(namespaceURI);
/*  590 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/*  591 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*      */           } 
/*      */           
/*  594 */           if (prefix.trim().length() > 0) {
/*  595 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*      */           } else {
/*  597 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */           } 
/*      */         } else {
/*  600 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */         } 
/*      */       } 
/*  603 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/*  613 */     String prefix = xmlWriter.getPrefix(namespace);
/*  614 */     if (prefix == null) {
/*  615 */       prefix = generatePrefix(namespace);
/*  616 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*      */       while (true) {
/*  618 */         String uri = nsContext.getNamespaceURI(prefix);
/*  619 */         if (uri == null || uri.length() == 0) {
/*      */           break;
/*      */         }
/*  622 */         prefix = BeanUtil.getUniquePrefix();
/*      */       } 
/*  624 */       xmlWriter.writeNamespace(prefix, namespace);
/*  625 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  627 */     return prefix;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/*  641 */     ArrayList<QName> elementList = new ArrayList();
/*  642 */     ArrayList attribList = new ArrayList();
/*      */ 
/*      */     
/*  645 */     elementList.add(new QName("", 
/*  646 */           "bAccepted"));
/*      */     
/*  648 */     elementList.add(new QName(ConverterUtil.convertToString(this.localBAccepted)));
/*      */     
/*  651 */     elementList.add(new QName("", 
/*  652 */           "uuid"));
/*      */     
/*  654 */     elementList.add((this.localUuid == null) ? null : new QName(ConverterUtil.convertToString(this.localUuid)));
/*      */     
/*  657 */     elementList.add(new QName("", 
/*  658 */           "employee"));
/*      */     
/*  660 */     elementList.add((this.localEmployee == null) ? null : new QName(ConverterUtil.convertToString(this.localEmployee)));
/*      */     
/*  663 */     elementList.add(new QName("", 
/*  664 */           "body"));
/*      */     
/*  666 */     elementList.add((this.localBody == null) ? null : new QName(ConverterUtil.convertToString(this.localBody)));
/*      */     
/*  669 */     elementList.add(new QName("", 
/*  670 */           "company"));
/*      */     
/*  672 */     elementList.add((this.localCompany == null) ? null : new QName(ConverterUtil.convertToString(this.localCompany)));
/*      */     
/*  675 */     elementList.add(new QName("", 
/*  676 */           "country"));
/*      */     
/*  678 */     elementList.add((this.localCountry == null) ? null : new QName(ConverterUtil.convertToString(this.localCountry)));
/*      */     
/*  681 */     elementList.add(new QName("", 
/*  682 */           "city"));
/*      */     
/*  684 */     elementList.add((this.localCity == null) ? null : new QName(ConverterUtil.convertToString(this.localCity)));
/*      */     
/*  687 */     elementList.add(new QName("", 
/*  688 */           "pInfo"));
/*      */ 
/*      */     
/*  691 */     elementList.add((this.localPInfo == null) ? null : new QName(this.localPInfo.getName()));
/*      */ 
/*      */     
/*  695 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public static class Factory
/*      */   {
/*      */     public static AnswerInvitation parse(XMLStreamReader reader) throws Exception {
/*  719 */       AnswerInvitation object = 
/*  720 */         new AnswerInvitation();
/*      */ 
/*      */       
/*  723 */       String nillableValue = null;
/*  724 */       String prefix = "";
/*  725 */       String namespaceuri = "";
/*      */       
/*      */       try {
/*  728 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/*  729 */           reader.next();
/*      */         }
/*      */         
/*  732 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/*  733 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/*  734 */               "type");
/*  735 */           if (fullTypeName != null) {
/*  736 */             String nsPrefix = null;
/*  737 */             if (fullTypeName.indexOf(":") > -1) {
/*  738 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*      */             }
/*  740 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*      */             
/*  742 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*      */             
/*  744 */             if (!"AnswerInvitation".equals(type)) {
/*      */               
/*  746 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/*  747 */               return (AnswerInvitation)ExtensionMapper.getTypeObject(
/*  748 */                   nsUri, type, reader);
/*      */             } 
/*      */           } 
/*      */         } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  762 */         Vector handledAttributes = new Vector();
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  767 */         reader.next();
/*      */ 
/*      */         
/*  770 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  772 */         if (reader.isStartElement() && (new QName("", "bAccepted")).equals(reader.getName())) {
/*      */           
/*  774 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  775 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  778 */             String content = reader.getElementText();
/*      */             
/*  780 */             object.setBAccepted(
/*  781 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  786 */             object.setBAccepted(-2147483648);
/*      */             
/*  788 */             reader.getElementText();
/*      */           } 
/*      */           
/*  791 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  797 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  801 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  803 */         if (reader.isStartElement() && (new QName("", "uuid")).equals(reader.getName())) {
/*      */           
/*  805 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  806 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  809 */             String content = reader.getElementText();
/*      */             
/*  811 */             object.setUuid(
/*  812 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  817 */             reader.getElementText();
/*      */           } 
/*      */           
/*  820 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  826 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  830 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  832 */         if (reader.isStartElement() && (new QName("", "employee")).equals(reader.getName())) {
/*      */           
/*  834 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  835 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  838 */             String content = reader.getElementText();
/*      */             
/*  840 */             object.setEmployee(
/*  841 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  846 */             reader.getElementText();
/*      */           } 
/*      */           
/*  849 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  855 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  859 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  861 */         if (reader.isStartElement() && (new QName("", "body")).equals(reader.getName())) {
/*      */           
/*  863 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  864 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  867 */             String content = reader.getElementText();
/*      */             
/*  869 */             object.setBody(
/*  870 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  875 */             reader.getElementText();
/*      */           } 
/*      */           
/*  878 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  884 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  888 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  890 */         if (reader.isStartElement() && (new QName("", "company")).equals(reader.getName())) {
/*      */           
/*  892 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  893 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  896 */             String content = reader.getElementText();
/*      */             
/*  898 */             object.setCompany(
/*  899 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  904 */             reader.getElementText();
/*      */           } 
/*      */           
/*  907 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  913 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  917 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  919 */         if (reader.isStartElement() && (new QName("", "country")).equals(reader.getName())) {
/*      */           
/*  921 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  922 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  925 */             String content = reader.getElementText();
/*      */             
/*  927 */             object.setCountry(
/*  928 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  933 */             reader.getElementText();
/*      */           } 
/*      */           
/*  936 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  942 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  946 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  948 */         if (reader.isStartElement() && (new QName("", "city")).equals(reader.getName())) {
/*      */           
/*  950 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  951 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  954 */             String content = reader.getElementText();
/*      */             
/*  956 */             object.setCity(
/*  957 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  962 */             reader.getElementText();
/*      */           } 
/*      */           
/*  965 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  971 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  975 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  977 */         if (reader.isStartElement() && (new QName("", "pInfo")).equals(reader.getName())) {
/*      */           
/*  979 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  980 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/*  981 */             object.setPInfo(null);
/*  982 */             reader.next();
/*      */             
/*  984 */             reader.next();
/*      */           }
/*      */           else {
/*      */             
/*  988 */             object.setPInfo(Contact_info.Factory.parse(reader));
/*      */             
/*  990 */             reader.next();
/*      */           }
/*      */         
/*      */         }
/*      */         else {
/*      */           
/*  996 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */         
/*  999 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1000 */           reader.next();
/*      */         }
/* 1002 */         if (reader.isStartElement())
/*      */         {
/* 1004 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         
/*      */         }
/*      */       
/*      */       }
/* 1009 */       catch (XMLStreamException e) {
/* 1010 */         throw new Exception(e);
/*      */       } 
/*      */       
/* 1013 */       return object;
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\AnswerInvitation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */