/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SubmitCargoRelease implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SubmitCargoRelease", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localCargo_release_xml;
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localFlags;
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCargo_release_xml() {
/*  71 */     return this.localCargo_release_xml;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCargo_release_xml(String param) {
/*  82 */     this.localCargo_release_xml = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/* 101 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/* 112 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 132 */     ADBDataSource aDBDataSource = 
/* 133 */       new ADBDataSource(this, MY_QNAME);
/* 134 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 141 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 152 */     String prefix = null;
/* 153 */     String namespace = null;
/*     */ 
/*     */     
/* 156 */     prefix = parentQName.getPrefix();
/* 157 */     namespace = parentQName.getNamespaceURI();
/* 158 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 160 */     if (serializeType) {
/*     */ 
/*     */       
/* 163 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 164 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 165 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 166 */             String.valueOf(namespacePrefix) + ":SubmitCargoRelease", 
/* 167 */             xmlWriter);
/*     */       } else {
/* 169 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 170 */             "SubmitCargoRelease", 
/* 171 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 177 */     namespace = "";
/* 178 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 180 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 182 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 185 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 188 */     xmlWriter.writeEndElement();
/*     */     
/* 190 */     namespace = "";
/* 191 */     writeStartElement(null, namespace, "cargo_release_xml", xmlWriter);
/*     */ 
/*     */     
/* 194 */     if (this.localCargo_release_xml == null) {
/*     */ 
/*     */       
/* 197 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 202 */       xmlWriter.writeCharacters(this.localCargo_release_xml);
/*     */     } 
/*     */ 
/*     */     
/* 206 */     xmlWriter.writeEndElement();
/*     */     
/* 208 */     namespace = "";
/* 209 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 211 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 213 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 216 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 219 */     xmlWriter.writeEndElement();
/*     */     
/* 221 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 227 */     if (namespace.equals("urn:CSSoapService")) {
/* 228 */       return "ns1";
/*     */     }
/* 230 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 238 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 239 */     if (writerPrefix != null) {
/* 240 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 242 */       if (namespace.length() == 0) {
/* 243 */         prefix = "";
/* 244 */       } else if (prefix == null) {
/* 245 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 248 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 249 */       xmlWriter.writeNamespace(prefix, namespace);
/* 250 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 259 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 260 */       xmlWriter.writeNamespace(prefix, namespace);
/* 261 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 263 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 271 */     if (namespace.equals("")) {
/* 272 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 274 */       registerPrefix(xmlWriter, namespace);
/* 275 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 286 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 287 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 288 */     if (attributePrefix == null) {
/* 289 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 292 */     if (attributePrefix.trim().length() > 0) {
/* 293 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 295 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 298 */     if (namespace.equals("")) {
/* 299 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 301 */       registerPrefix(xmlWriter, namespace);
/* 302 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 311 */     String namespaceURI = qname.getNamespaceURI();
/* 312 */     if (namespaceURI != null) {
/* 313 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 314 */       if (prefix == null) {
/* 315 */         prefix = generatePrefix(namespaceURI);
/* 316 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 317 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 320 */       if (prefix.trim().length() > 0) {
/* 321 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 324 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 328 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 335 */     if (qnames != null) {
/*     */ 
/*     */       
/* 338 */       StringBuffer stringToWrite = new StringBuffer();
/* 339 */       String namespaceURI = null;
/* 340 */       String prefix = null;
/*     */       
/* 342 */       for (int i = 0; i < qnames.length; i++) {
/* 343 */         if (i > 0) {
/* 344 */           stringToWrite.append(" ");
/*     */         }
/* 346 */         namespaceURI = qnames[i].getNamespaceURI();
/* 347 */         if (namespaceURI != null) {
/* 348 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 349 */           if (prefix == null || prefix.length() == 0) {
/* 350 */             prefix = generatePrefix(namespaceURI);
/* 351 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 352 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 355 */           if (prefix.trim().length() > 0) {
/* 356 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 358 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 361 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 364 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 374 */     String prefix = xmlWriter.getPrefix(namespace);
/* 375 */     if (prefix == null) {
/* 376 */       prefix = generatePrefix(namespace);
/* 377 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 379 */         String uri = nsContext.getNamespaceURI(prefix);
/* 380 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 383 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 385 */       xmlWriter.writeNamespace(prefix, namespace);
/* 386 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 388 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 402 */     ArrayList<QName> elementList = new ArrayList();
/* 403 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 406 */     elementList.add(new QName("", 
/* 407 */           "access_key"));
/*     */     
/* 409 */     elementList.add(
/* 410 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*     */     
/* 412 */     elementList.add(new QName("", 
/* 413 */           "cargo_release_xml"));
/*     */     
/* 415 */     elementList.add((this.localCargo_release_xml == null) ? null : 
/* 416 */         new QName(ConverterUtil.convertToString(this.localCargo_release_xml)));
/*     */     
/* 418 */     elementList.add(new QName("", 
/* 419 */           "flags"));
/*     */     
/* 421 */     elementList.add(
/* 422 */         new QName(ConverterUtil.convertToString(this.localFlags)));
/*     */ 
/*     */     
/* 425 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SubmitCargoRelease parse(XMLStreamReader reader) throws Exception {
/* 449 */       SubmitCargoRelease object = 
/* 450 */         new SubmitCargoRelease();
/*     */ 
/*     */       
/* 453 */       String nillableValue = null;
/* 454 */       String prefix = "";
/* 455 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 458 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 459 */           reader.next();
/*     */         }
/*     */         
/* 462 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 463 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 464 */               "type");
/* 465 */           if (fullTypeName != null) {
/* 466 */             String nsPrefix = null;
/* 467 */             if (fullTypeName.indexOf(":") > -1) {
/* 468 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 470 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 472 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 474 */             if (!"SubmitCargoRelease".equals(type)) {
/*     */               
/* 476 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 477 */               return (SubmitCargoRelease)ExtensionMapper.getTypeObject(
/* 478 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 492 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 497 */         reader.next();
/*     */ 
/*     */         
/* 500 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 502 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 504 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 505 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 508 */             String content = reader.getElementText();
/*     */             
/* 510 */             object.setAccess_key(
/* 511 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 516 */             object.setAccess_key(-2147483648);
/*     */             
/* 518 */             reader.getElementText();
/*     */           } 
/*     */           
/* 521 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 527 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 531 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 533 */         if (reader.isStartElement() && (new QName("", "cargo_release_xml")).equals(reader.getName())) {
/*     */           
/* 535 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 536 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 539 */             String content = reader.getElementText();
/*     */             
/* 541 */             object.setCargo_release_xml(
/* 542 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 547 */             reader.getElementText();
/*     */           } 
/*     */           
/* 550 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 556 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 560 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 562 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 564 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 565 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 568 */             String content = reader.getElementText();
/*     */             
/* 570 */             object.setFlags(
/* 571 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 576 */             object.setFlags(-2147483648);
/*     */             
/* 578 */             reader.getElementText();
/*     */           } 
/*     */           
/* 581 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 587 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 590 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 591 */           reader.next();
/*     */         }
/* 593 */         if (reader.isStartElement())
/*     */         {
/* 595 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 600 */       catch (XMLStreamException e) {
/* 601 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 604 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SubmitCargoRelease.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */