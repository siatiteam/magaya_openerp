/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetTripSchedule implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetTripSchedule", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected String localArr_start_date;
/*     */   
/*     */   protected String localArr_end_date;
/*     */   
/*     */   protected String localDep_start_date;
/*     */   
/*     */   protected String localDep_end_date;
/*     */   
/*     */   protected String localOrg_port;
/*     */   
/*     */   protected String localDest_port;
/*     */ 
/*     */   
/*     */   public String getArr_start_date() {
/*  41 */     return this.localArr_start_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setArr_start_date(String param) {
/*  52 */     this.localArr_start_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getArr_end_date() {
/*  71 */     return this.localArr_end_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setArr_end_date(String param) {
/*  82 */     this.localArr_end_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getDep_start_date() {
/* 101 */     return this.localDep_start_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setDep_start_date(String param) {
/* 112 */     this.localDep_start_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getDep_end_date() {
/* 131 */     return this.localDep_end_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setDep_end_date(String param) {
/* 142 */     this.localDep_end_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getOrg_port() {
/* 161 */     return this.localOrg_port;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setOrg_port(String param) {
/* 172 */     this.localOrg_port = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getDest_port() {
/* 191 */     return this.localDest_port;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setDest_port(String param) {
/* 202 */     this.localDest_port = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 222 */     ADBDataSource aDBDataSource = 
/* 223 */       new ADBDataSource(this, MY_QNAME);
/* 224 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 231 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 242 */     String prefix = null;
/* 243 */     String namespace = null;
/*     */ 
/*     */     
/* 246 */     prefix = parentQName.getPrefix();
/* 247 */     namespace = parentQName.getNamespaceURI();
/* 248 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 250 */     if (serializeType) {
/*     */ 
/*     */       
/* 253 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 254 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 255 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 256 */             String.valueOf(namespacePrefix) + ":GetTripSchedule", 
/* 257 */             xmlWriter);
/*     */       } else {
/* 259 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 260 */             "GetTripSchedule", 
/* 261 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 267 */     namespace = "";
/* 268 */     writeStartElement(null, namespace, "arr_start_date", xmlWriter);
/*     */ 
/*     */     
/* 271 */     if (this.localArr_start_date == null) {
/*     */ 
/*     */       
/* 274 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 279 */       xmlWriter.writeCharacters(this.localArr_start_date);
/*     */     } 
/*     */ 
/*     */     
/* 283 */     xmlWriter.writeEndElement();
/*     */     
/* 285 */     namespace = "";
/* 286 */     writeStartElement(null, namespace, "arr_end_date", xmlWriter);
/*     */ 
/*     */     
/* 289 */     if (this.localArr_end_date == null) {
/*     */ 
/*     */       
/* 292 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 297 */       xmlWriter.writeCharacters(this.localArr_end_date);
/*     */     } 
/*     */ 
/*     */     
/* 301 */     xmlWriter.writeEndElement();
/*     */     
/* 303 */     namespace = "";
/* 304 */     writeStartElement(null, namespace, "dep_start_date", xmlWriter);
/*     */ 
/*     */     
/* 307 */     if (this.localDep_start_date == null) {
/*     */ 
/*     */       
/* 310 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 315 */       xmlWriter.writeCharacters(this.localDep_start_date);
/*     */     } 
/*     */ 
/*     */     
/* 319 */     xmlWriter.writeEndElement();
/*     */     
/* 321 */     namespace = "";
/* 322 */     writeStartElement(null, namespace, "dep_end_date", xmlWriter);
/*     */ 
/*     */     
/* 325 */     if (this.localDep_end_date == null) {
/*     */ 
/*     */       
/* 328 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 333 */       xmlWriter.writeCharacters(this.localDep_end_date);
/*     */     } 
/*     */ 
/*     */     
/* 337 */     xmlWriter.writeEndElement();
/*     */     
/* 339 */     namespace = "";
/* 340 */     writeStartElement(null, namespace, "org_port", xmlWriter);
/*     */ 
/*     */     
/* 343 */     if (this.localOrg_port == null) {
/*     */ 
/*     */       
/* 346 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 351 */       xmlWriter.writeCharacters(this.localOrg_port);
/*     */     } 
/*     */ 
/*     */     
/* 355 */     xmlWriter.writeEndElement();
/*     */     
/* 357 */     namespace = "";
/* 358 */     writeStartElement(null, namespace, "dest_port", xmlWriter);
/*     */ 
/*     */     
/* 361 */     if (this.localDest_port == null) {
/*     */ 
/*     */       
/* 364 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 369 */       xmlWriter.writeCharacters(this.localDest_port);
/*     */     } 
/*     */ 
/*     */     
/* 373 */     xmlWriter.writeEndElement();
/*     */     
/* 375 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 381 */     if (namespace.equals("urn:CSSoapService")) {
/* 382 */       return "ns1";
/*     */     }
/* 384 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 392 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 393 */     if (writerPrefix != null) {
/* 394 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 396 */       if (namespace.length() == 0) {
/* 397 */         prefix = "";
/* 398 */       } else if (prefix == null) {
/* 399 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 402 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 403 */       xmlWriter.writeNamespace(prefix, namespace);
/* 404 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 413 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 414 */       xmlWriter.writeNamespace(prefix, namespace);
/* 415 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 417 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 425 */     if (namespace.equals("")) {
/* 426 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 428 */       registerPrefix(xmlWriter, namespace);
/* 429 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 440 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 441 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 442 */     if (attributePrefix == null) {
/* 443 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 446 */     if (attributePrefix.trim().length() > 0) {
/* 447 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 449 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 452 */     if (namespace.equals("")) {
/* 453 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 455 */       registerPrefix(xmlWriter, namespace);
/* 456 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 465 */     String namespaceURI = qname.getNamespaceURI();
/* 466 */     if (namespaceURI != null) {
/* 467 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 468 */       if (prefix == null) {
/* 469 */         prefix = generatePrefix(namespaceURI);
/* 470 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 471 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 474 */       if (prefix.trim().length() > 0) {
/* 475 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 478 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 482 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 489 */     if (qnames != null) {
/*     */ 
/*     */       
/* 492 */       StringBuffer stringToWrite = new StringBuffer();
/* 493 */       String namespaceURI = null;
/* 494 */       String prefix = null;
/*     */       
/* 496 */       for (int i = 0; i < qnames.length; i++) {
/* 497 */         if (i > 0) {
/* 498 */           stringToWrite.append(" ");
/*     */         }
/* 500 */         namespaceURI = qnames[i].getNamespaceURI();
/* 501 */         if (namespaceURI != null) {
/* 502 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 503 */           if (prefix == null || prefix.length() == 0) {
/* 504 */             prefix = generatePrefix(namespaceURI);
/* 505 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 506 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 509 */           if (prefix.trim().length() > 0) {
/* 510 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 512 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 515 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 518 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 528 */     String prefix = xmlWriter.getPrefix(namespace);
/* 529 */     if (prefix == null) {
/* 530 */       prefix = generatePrefix(namespace);
/* 531 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 533 */         String uri = nsContext.getNamespaceURI(prefix);
/* 534 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 537 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 539 */       xmlWriter.writeNamespace(prefix, namespace);
/* 540 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 542 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 556 */     ArrayList<QName> elementList = new ArrayList();
/* 557 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 560 */     elementList.add(new QName("", 
/* 561 */           "arr_start_date"));
/*     */     
/* 563 */     elementList.add((this.localArr_start_date == null) ? null : 
/* 564 */         new QName(ConverterUtil.convertToString(this.localArr_start_date)));
/*     */     
/* 566 */     elementList.add(new QName("", 
/* 567 */           "arr_end_date"));
/*     */     
/* 569 */     elementList.add((this.localArr_end_date == null) ? null : 
/* 570 */         new QName(ConverterUtil.convertToString(this.localArr_end_date)));
/*     */     
/* 572 */     elementList.add(new QName("", 
/* 573 */           "dep_start_date"));
/*     */     
/* 575 */     elementList.add((this.localDep_start_date == null) ? null : 
/* 576 */         new QName(ConverterUtil.convertToString(this.localDep_start_date)));
/*     */     
/* 578 */     elementList.add(new QName("", 
/* 579 */           "dep_end_date"));
/*     */     
/* 581 */     elementList.add((this.localDep_end_date == null) ? null : 
/* 582 */         new QName(ConverterUtil.convertToString(this.localDep_end_date)));
/*     */     
/* 584 */     elementList.add(new QName("", 
/* 585 */           "org_port"));
/*     */     
/* 587 */     elementList.add((this.localOrg_port == null) ? null : 
/* 588 */         new QName(ConverterUtil.convertToString(this.localOrg_port)));
/*     */     
/* 590 */     elementList.add(new QName("", 
/* 591 */           "dest_port"));
/*     */     
/* 593 */     elementList.add((this.localDest_port == null) ? null : 
/* 594 */         new QName(ConverterUtil.convertToString(this.localDest_port)));
/*     */ 
/*     */     
/* 597 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetTripSchedule parse(XMLStreamReader reader) throws Exception {
/* 621 */       GetTripSchedule object = 
/* 622 */         new GetTripSchedule();
/*     */ 
/*     */       
/* 625 */       String nillableValue = null;
/* 626 */       String prefix = "";
/* 627 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 630 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 631 */           reader.next();
/*     */         }
/*     */         
/* 634 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 635 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 636 */               "type");
/* 637 */           if (fullTypeName != null) {
/* 638 */             String nsPrefix = null;
/* 639 */             if (fullTypeName.indexOf(":") > -1) {
/* 640 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 642 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 644 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 646 */             if (!"GetTripSchedule".equals(type)) {
/*     */               
/* 648 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 649 */               return (GetTripSchedule)ExtensionMapper.getTypeObject(
/* 650 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 664 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 669 */         reader.next();
/*     */ 
/*     */         
/* 672 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 674 */         if (reader.isStartElement() && (new QName("", "arr_start_date")).equals(reader.getName())) {
/*     */           
/* 676 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 677 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 680 */             String content = reader.getElementText();
/*     */             
/* 682 */             object.setArr_start_date(
/* 683 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 688 */             reader.getElementText();
/*     */           } 
/*     */           
/* 691 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 697 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 701 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 703 */         if (reader.isStartElement() && (new QName("", "arr_end_date")).equals(reader.getName())) {
/*     */           
/* 705 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 706 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 709 */             String content = reader.getElementText();
/*     */             
/* 711 */             object.setArr_end_date(
/* 712 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 717 */             reader.getElementText();
/*     */           } 
/*     */           
/* 720 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 726 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 730 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 732 */         if (reader.isStartElement() && (new QName("", "dep_start_date")).equals(reader.getName())) {
/*     */           
/* 734 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 735 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 738 */             String content = reader.getElementText();
/*     */             
/* 740 */             object.setDep_start_date(
/* 741 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 746 */             reader.getElementText();
/*     */           } 
/*     */           
/* 749 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 755 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 759 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 761 */         if (reader.isStartElement() && (new QName("", "dep_end_date")).equals(reader.getName())) {
/*     */           
/* 763 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 764 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 767 */             String content = reader.getElementText();
/*     */             
/* 769 */             object.setDep_end_date(
/* 770 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 775 */             reader.getElementText();
/*     */           } 
/*     */           
/* 778 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 784 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 788 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 790 */         if (reader.isStartElement() && (new QName("", "org_port")).equals(reader.getName())) {
/*     */           
/* 792 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 793 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 796 */             String content = reader.getElementText();
/*     */             
/* 798 */             object.setOrg_port(
/* 799 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 804 */             reader.getElementText();
/*     */           } 
/*     */           
/* 807 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 813 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 817 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 819 */         if (reader.isStartElement() && (new QName("", "dest_port")).equals(reader.getName())) {
/*     */           
/* 821 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 822 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 825 */             String content = reader.getElementText();
/*     */             
/* 827 */             object.setDest_port(
/* 828 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 833 */             reader.getElementText();
/*     */           } 
/*     */           
/* 836 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 842 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 845 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 846 */           reader.next();
/*     */         }
/* 848 */         if (reader.isStartElement())
/*     */         {
/* 850 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 855 */       catch (XMLStreamException e) {
/* 856 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 859 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetTripSchedule.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */