/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class StartTracking2Response implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "StartTracking2Response", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int local_return;
/*     */ 
/*     */   
/*     */   protected int localServices;
/*     */ 
/*     */   
/*     */   protected String localCompany_info;
/*     */   
/*     */   protected String localUser_info;
/*     */   
/*     */   protected int localCookie;
/*     */ 
/*     */   
/*     */   public int get_return() {
/*  41 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(int param) {
/*  52 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getServices() {
/*  71 */     return this.localServices;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setServices(int param) {
/*  82 */     this.localServices = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCompany_info() {
/* 101 */     return this.localCompany_info;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCompany_info(String param) {
/* 112 */     this.localCompany_info = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getUser_info() {
/* 131 */     return this.localUser_info;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setUser_info(String param) {
/* 142 */     this.localUser_info = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getCookie() {
/* 161 */     return this.localCookie;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCookie(int param) {
/* 172 */     this.localCookie = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 192 */     ADBDataSource aDBDataSource = 
/* 193 */       new ADBDataSource(this, MY_QNAME);
/* 194 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 201 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 212 */     String prefix = null;
/* 213 */     String namespace = null;
/*     */ 
/*     */     
/* 216 */     prefix = parentQName.getPrefix();
/* 217 */     namespace = parentQName.getNamespaceURI();
/* 218 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 220 */     if (serializeType) {
/*     */ 
/*     */       
/* 223 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 224 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 225 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 226 */             String.valueOf(namespacePrefix) + ":StartTracking2Response", 
/* 227 */             xmlWriter);
/*     */       } else {
/* 229 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 230 */             "StartTracking2Response", 
/* 231 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 237 */     namespace = "";
/* 238 */     writeStartElement(null, namespace, "return", xmlWriter);
/*     */     
/* 240 */     if (this.local_return == Integer.MIN_VALUE) {
/*     */       
/* 242 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 245 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.local_return));
/*     */     } 
/*     */     
/* 248 */     xmlWriter.writeEndElement();
/*     */     
/* 250 */     namespace = "";
/* 251 */     writeStartElement(null, namespace, "services", xmlWriter);
/*     */     
/* 253 */     if (this.localServices == Integer.MIN_VALUE) {
/*     */       
/* 255 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 258 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localServices));
/*     */     } 
/*     */     
/* 261 */     xmlWriter.writeEndElement();
/*     */     
/* 263 */     namespace = "";
/* 264 */     writeStartElement(null, namespace, "company_info", xmlWriter);
/*     */ 
/*     */     
/* 267 */     if (this.localCompany_info == null) {
/*     */ 
/*     */       
/* 270 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 275 */       xmlWriter.writeCharacters(this.localCompany_info);
/*     */     } 
/*     */ 
/*     */     
/* 279 */     xmlWriter.writeEndElement();
/*     */     
/* 281 */     namespace = "";
/* 282 */     writeStartElement(null, namespace, "user_info", xmlWriter);
/*     */ 
/*     */     
/* 285 */     if (this.localUser_info == null) {
/*     */ 
/*     */       
/* 288 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 293 */       xmlWriter.writeCharacters(this.localUser_info);
/*     */     } 
/*     */ 
/*     */     
/* 297 */     xmlWriter.writeEndElement();
/*     */     
/* 299 */     namespace = "";
/* 300 */     writeStartElement(null, namespace, "cookie", xmlWriter);
/*     */     
/* 302 */     if (this.localCookie == Integer.MIN_VALUE) {
/*     */       
/* 304 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 307 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localCookie));
/*     */     } 
/*     */     
/* 310 */     xmlWriter.writeEndElement();
/*     */     
/* 312 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 318 */     if (namespace.equals("urn:CSSoapService")) {
/* 319 */       return "ns1";
/*     */     }
/* 321 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 329 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 330 */     if (writerPrefix != null) {
/* 331 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 333 */       if (namespace.length() == 0) {
/* 334 */         prefix = "";
/* 335 */       } else if (prefix == null) {
/* 336 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 339 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 340 */       xmlWriter.writeNamespace(prefix, namespace);
/* 341 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 350 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 351 */       xmlWriter.writeNamespace(prefix, namespace);
/* 352 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 354 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 362 */     if (namespace.equals("")) {
/* 363 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 365 */       registerPrefix(xmlWriter, namespace);
/* 366 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 377 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 378 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 379 */     if (attributePrefix == null) {
/* 380 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 383 */     if (attributePrefix.trim().length() > 0) {
/* 384 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 386 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 389 */     if (namespace.equals("")) {
/* 390 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 392 */       registerPrefix(xmlWriter, namespace);
/* 393 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 402 */     String namespaceURI = qname.getNamespaceURI();
/* 403 */     if (namespaceURI != null) {
/* 404 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 405 */       if (prefix == null) {
/* 406 */         prefix = generatePrefix(namespaceURI);
/* 407 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 408 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 411 */       if (prefix.trim().length() > 0) {
/* 412 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 415 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 419 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 426 */     if (qnames != null) {
/*     */ 
/*     */       
/* 429 */       StringBuffer stringToWrite = new StringBuffer();
/* 430 */       String namespaceURI = null;
/* 431 */       String prefix = null;
/*     */       
/* 433 */       for (int i = 0; i < qnames.length; i++) {
/* 434 */         if (i > 0) {
/* 435 */           stringToWrite.append(" ");
/*     */         }
/* 437 */         namespaceURI = qnames[i].getNamespaceURI();
/* 438 */         if (namespaceURI != null) {
/* 439 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 440 */           if (prefix == null || prefix.length() == 0) {
/* 441 */             prefix = generatePrefix(namespaceURI);
/* 442 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 443 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 446 */           if (prefix.trim().length() > 0) {
/* 447 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 449 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 452 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 455 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 465 */     String prefix = xmlWriter.getPrefix(namespace);
/* 466 */     if (prefix == null) {
/* 467 */       prefix = generatePrefix(namespace);
/* 468 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 470 */         String uri = nsContext.getNamespaceURI(prefix);
/* 471 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 474 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 476 */       xmlWriter.writeNamespace(prefix, namespace);
/* 477 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 479 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 493 */     ArrayList<QName> elementList = new ArrayList();
/* 494 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 497 */     elementList.add(new QName("", 
/* 498 */           "return"));
/*     */     
/* 500 */     elementList.add(
/* 501 */         new QName(ConverterUtil.convertToString(this.local_return)));
/*     */     
/* 503 */     elementList.add(new QName("", 
/* 504 */           "services"));
/*     */     
/* 506 */     elementList.add(
/* 507 */         new QName(ConverterUtil.convertToString(this.localServices)));
/*     */     
/* 509 */     elementList.add(new QName("", 
/* 510 */           "company_info"));
/*     */     
/* 512 */     elementList.add((this.localCompany_info == null) ? null : 
/* 513 */         new QName(ConverterUtil.convertToString(this.localCompany_info)));
/*     */     
/* 515 */     elementList.add(new QName("", 
/* 516 */           "user_info"));
/*     */     
/* 518 */     elementList.add((this.localUser_info == null) ? null : 
/* 519 */         new QName(ConverterUtil.convertToString(this.localUser_info)));
/*     */     
/* 521 */     elementList.add(new QName("", 
/* 522 */           "cookie"));
/*     */     
/* 524 */     elementList.add(
/* 525 */         new QName(ConverterUtil.convertToString(this.localCookie)));
/*     */ 
/*     */     
/* 528 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static StartTracking2Response parse(XMLStreamReader reader) throws Exception {
/* 552 */       StartTracking2Response object = 
/* 553 */         new StartTracking2Response();
/*     */ 
/*     */       
/* 556 */       String nillableValue = null;
/* 557 */       String prefix = "";
/* 558 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 561 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 562 */           reader.next();
/*     */         }
/*     */         
/* 565 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 566 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 567 */               "type");
/* 568 */           if (fullTypeName != null) {
/* 569 */             String nsPrefix = null;
/* 570 */             if (fullTypeName.indexOf(":") > -1) {
/* 571 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 573 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 575 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 577 */             if (!"StartTracking2Response".equals(type)) {
/*     */               
/* 579 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 580 */               return (StartTracking2Response)ExtensionMapper.getTypeObject(
/* 581 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 595 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 600 */         reader.next();
/*     */ 
/*     */         
/* 603 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 605 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
/*     */           
/* 607 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 608 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 611 */             String content = reader.getElementText();
/*     */             
/* 613 */             object.set_return(
/* 614 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 619 */             object.set_return(-2147483648);
/*     */             
/* 621 */             reader.getElementText();
/*     */           } 
/*     */           
/* 624 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 630 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 634 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 636 */         if (reader.isStartElement() && (new QName("", "services")).equals(reader.getName())) {
/*     */           
/* 638 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 639 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 642 */             String content = reader.getElementText();
/*     */             
/* 644 */             object.setServices(
/* 645 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 650 */             object.setServices(-2147483648);
/*     */             
/* 652 */             reader.getElementText();
/*     */           } 
/*     */           
/* 655 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 661 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 665 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 667 */         if (reader.isStartElement() && (new QName("", "company_info")).equals(reader.getName())) {
/*     */           
/* 669 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 670 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 673 */             String content = reader.getElementText();
/*     */             
/* 675 */             object.setCompany_info(
/* 676 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 681 */             reader.getElementText();
/*     */           } 
/*     */           
/* 684 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 690 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 694 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 696 */         if (reader.isStartElement() && (new QName("", "user_info")).equals(reader.getName())) {
/*     */           
/* 698 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 699 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 702 */             String content = reader.getElementText();
/*     */             
/* 704 */             object.setUser_info(
/* 705 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 710 */             reader.getElementText();
/*     */           } 
/*     */           
/* 713 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 719 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 723 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 725 */         if (reader.isStartElement() && (new QName("", "cookie")).equals(reader.getName())) {
/*     */           
/* 727 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 728 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 731 */             String content = reader.getElementText();
/*     */             
/* 733 */             object.setCookie(
/* 734 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 739 */             object.setCookie(-2147483648);
/*     */             
/* 741 */             reader.getElementText();
/*     */           } 
/*     */           
/* 744 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 750 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 753 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 754 */           reader.next();
/*     */         }
/* 756 */         if (reader.isStartElement())
/*     */         {
/* 758 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 763 */       catch (XMLStreamException e) {
/* 764 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 767 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\StartTracking2Response.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */