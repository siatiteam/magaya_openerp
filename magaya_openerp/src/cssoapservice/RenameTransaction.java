/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class RenameTransaction implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "RenameTransaction", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */   
/*     */   protected String localType;
/*     */ 
/*     */   
/*     */   protected String localNumber;
/*     */ 
/*     */   
/*     */   protected String localNew_name;
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/*  71 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/*  82 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNumber() {
/* 101 */     return this.localNumber;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNumber(String param) {
/* 112 */     this.localNumber = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNew_name() {
/* 131 */     return this.localNew_name;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNew_name(String param) {
/* 142 */     this.localNew_name = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 162 */     ADBDataSource aDBDataSource = 
/* 163 */       new ADBDataSource(this, MY_QNAME);
/* 164 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 171 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 182 */     String prefix = null;
/* 183 */     String namespace = null;
/*     */ 
/*     */     
/* 186 */     prefix = parentQName.getPrefix();
/* 187 */     namespace = parentQName.getNamespaceURI();
/* 188 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 190 */     if (serializeType) {
/*     */ 
/*     */       
/* 193 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 194 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 195 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 196 */             String.valueOf(namespacePrefix) + ":RenameTransaction", 
/* 197 */             xmlWriter);
/*     */       } else {
/* 199 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 200 */             "RenameTransaction", 
/* 201 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 207 */     namespace = "";
/* 208 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 210 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 212 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 215 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 218 */     xmlWriter.writeEndElement();
/*     */     
/* 220 */     namespace = "";
/* 221 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 224 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 227 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 232 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 236 */     xmlWriter.writeEndElement();
/*     */     
/* 238 */     namespace = "";
/* 239 */     writeStartElement(null, namespace, "number", xmlWriter);
/*     */ 
/*     */     
/* 242 */     if (this.localNumber == null) {
/*     */ 
/*     */       
/* 245 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 250 */       xmlWriter.writeCharacters(this.localNumber);
/*     */     } 
/*     */ 
/*     */     
/* 254 */     xmlWriter.writeEndElement();
/*     */     
/* 256 */     namespace = "";
/* 257 */     writeStartElement(null, namespace, "new_name", xmlWriter);
/*     */ 
/*     */     
/* 260 */     if (this.localNew_name == null) {
/*     */ 
/*     */       
/* 263 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 268 */       xmlWriter.writeCharacters(this.localNew_name);
/*     */     } 
/*     */ 
/*     */     
/* 272 */     xmlWriter.writeEndElement();
/*     */     
/* 274 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 280 */     if (namespace.equals("urn:CSSoapService")) {
/* 281 */       return "ns1";
/*     */     }
/* 283 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 291 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 292 */     if (writerPrefix != null) {
/* 293 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 295 */       if (namespace.length() == 0) {
/* 296 */         prefix = "";
/* 297 */       } else if (prefix == null) {
/* 298 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 301 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 302 */       xmlWriter.writeNamespace(prefix, namespace);
/* 303 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 312 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 313 */       xmlWriter.writeNamespace(prefix, namespace);
/* 314 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 316 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 324 */     if (namespace.equals("")) {
/* 325 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 327 */       registerPrefix(xmlWriter, namespace);
/* 328 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 339 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 340 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 341 */     if (attributePrefix == null) {
/* 342 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 345 */     if (attributePrefix.trim().length() > 0) {
/* 346 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 348 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 351 */     if (namespace.equals("")) {
/* 352 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 354 */       registerPrefix(xmlWriter, namespace);
/* 355 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 364 */     String namespaceURI = qname.getNamespaceURI();
/* 365 */     if (namespaceURI != null) {
/* 366 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 367 */       if (prefix == null) {
/* 368 */         prefix = generatePrefix(namespaceURI);
/* 369 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 370 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 373 */       if (prefix.trim().length() > 0) {
/* 374 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 377 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 381 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 388 */     if (qnames != null) {
/*     */ 
/*     */       
/* 391 */       StringBuffer stringToWrite = new StringBuffer();
/* 392 */       String namespaceURI = null;
/* 393 */       String prefix = null;
/*     */       
/* 395 */       for (int i = 0; i < qnames.length; i++) {
/* 396 */         if (i > 0) {
/* 397 */           stringToWrite.append(" ");
/*     */         }
/* 399 */         namespaceURI = qnames[i].getNamespaceURI();
/* 400 */         if (namespaceURI != null) {
/* 401 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 402 */           if (prefix == null || prefix.length() == 0) {
/* 403 */             prefix = generatePrefix(namespaceURI);
/* 404 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 405 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 408 */           if (prefix.trim().length() > 0) {
/* 409 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 411 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 414 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 417 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 427 */     String prefix = xmlWriter.getPrefix(namespace);
/* 428 */     if (prefix == null) {
/* 429 */       prefix = generatePrefix(namespace);
/* 430 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 432 */         String uri = nsContext.getNamespaceURI(prefix);
/* 433 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 436 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 438 */       xmlWriter.writeNamespace(prefix, namespace);
/* 439 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 441 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 455 */     ArrayList<QName> elementList = new ArrayList();
/* 456 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 459 */     elementList.add(new QName("", 
/* 460 */           "access_key"));
/*     */     
/* 462 */     elementList.add(
/* 463 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*     */     
/* 465 */     elementList.add(new QName("", 
/* 466 */           "type"));
/*     */     
/* 468 */     elementList.add((this.localType == null) ? null : 
/* 469 */         new QName(ConverterUtil.convertToString(this.localType)));
/*     */     
/* 471 */     elementList.add(new QName("", 
/* 472 */           "number"));
/*     */     
/* 474 */     elementList.add((this.localNumber == null) ? null : 
/* 475 */         new QName(ConverterUtil.convertToString(this.localNumber)));
/*     */     
/* 477 */     elementList.add(new QName("", 
/* 478 */           "new_name"));
/*     */     
/* 480 */     elementList.add((this.localNew_name == null) ? null : 
/* 481 */         new QName(ConverterUtil.convertToString(this.localNew_name)));
/*     */ 
/*     */     
/* 484 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static RenameTransaction parse(XMLStreamReader reader) throws Exception {
/* 508 */       RenameTransaction object = 
/* 509 */         new RenameTransaction();
/*     */ 
/*     */       
/* 512 */       String nillableValue = null;
/* 513 */       String prefix = "";
/* 514 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 517 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 518 */           reader.next();
/*     */         }
/*     */         
/* 521 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 522 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 523 */               "type");
/* 524 */           if (fullTypeName != null) {
/* 525 */             String nsPrefix = null;
/* 526 */             if (fullTypeName.indexOf(":") > -1) {
/* 527 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 529 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 531 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 533 */             if (!"RenameTransaction".equals(type)) {
/*     */               
/* 535 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 536 */               return (RenameTransaction)ExtensionMapper.getTypeObject(
/* 537 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 551 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 556 */         reader.next();
/*     */ 
/*     */         
/* 559 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 561 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 563 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 564 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 567 */             String content = reader.getElementText();
/*     */             
/* 569 */             object.setAccess_key(
/* 570 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 575 */             object.setAccess_key(-2147483648);
/*     */             
/* 577 */             reader.getElementText();
/*     */           } 
/*     */           
/* 580 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 586 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 590 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 592 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 594 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 595 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 598 */             String content = reader.getElementText();
/*     */             
/* 600 */             object.setType(
/* 601 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 606 */             reader.getElementText();
/*     */           } 
/*     */           
/* 609 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 615 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 619 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 621 */         if (reader.isStartElement() && (new QName("", "number")).equals(reader.getName())) {
/*     */           
/* 623 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 624 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 627 */             String content = reader.getElementText();
/*     */             
/* 629 */             object.setNumber(
/* 630 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 635 */             reader.getElementText();
/*     */           } 
/*     */           
/* 638 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 644 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 648 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 650 */         if (reader.isStartElement() && (new QName("", "new_name")).equals(reader.getName())) {
/*     */           
/* 652 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 653 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 656 */             String content = reader.getElementText();
/*     */             
/* 658 */             object.setNew_name(
/* 659 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 664 */             reader.getElementText();
/*     */           } 
/*     */           
/* 667 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 673 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 676 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 677 */           reader.next();
/*     */         }
/* 679 */         if (reader.isStartElement())
/*     */         {
/* 681 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 686 */       catch (XMLStreamException e) {
/* 687 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 690 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\RenameTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */