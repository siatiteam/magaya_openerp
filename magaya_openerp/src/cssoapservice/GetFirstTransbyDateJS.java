/*      */ package cssoapservice;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.Vector;
/*      */ import javax.xml.namespace.NamespaceContext;
/*      */ import javax.xml.namespace.QName;
/*      */ import javax.xml.stream.XMLStreamException;
/*      */ import javax.xml.stream.XMLStreamReader;
/*      */ import javax.xml.stream.XMLStreamWriter;
/*      */ import org.apache.axiom.om.OMDataSource;
/*      */ import org.apache.axiom.om.OMElement;
/*      */ import org.apache.axiom.om.OMFactory;
/*      */ import org.apache.axis2.databinding.ADBBean;
/*      */ import org.apache.axis2.databinding.ADBDataSource;
/*      */ import org.apache.axis2.databinding.ADBException;
/*      */ import org.apache.axis2.databinding.utils.BeanUtil;
/*      */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*      */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*      */ 
/*      */ public class GetFirstTransbyDateJS implements ADBBean {
/*   21 */   public static final QName MY_QNAME = new QName(
/*   22 */       "urn:CSSoapService", 
/*   23 */       "GetFirstTransbyDateJS", 
/*   24 */       "ns1");
/*      */   
/*      */   protected int localAccess_key;
/*      */   
/*      */   protected String localType;
/*      */   
/*      */   protected String localStart_date;
/*      */   
/*      */   protected String localEnd_date;
/*      */   
/*      */   protected int localFlags;
/*      */   protected int localRecord_quantity;
/*      */   protected int localBackwards_order;
/*      */   protected String localFunction;
/*      */   protected String localXml_params;
/*      */   
/*      */   public int getAccess_key() {
/*   41 */     return this.localAccess_key;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setAccess_key(int param) {
/*   52 */     this.localAccess_key = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getType() {
/*   71 */     return this.localType;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setType(String param) {
/*   82 */     this.localType = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getStart_date() {
/*  101 */     return this.localStart_date;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setStart_date(String param) {
/*  112 */     this.localStart_date = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getEnd_date() {
/*  131 */     return this.localEnd_date;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setEnd_date(String param) {
/*  142 */     this.localEnd_date = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getFlags() {
/*  161 */     return this.localFlags;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setFlags(int param) {
/*  172 */     this.localFlags = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getRecord_quantity() {
/*  191 */     return this.localRecord_quantity;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setRecord_quantity(int param) {
/*  202 */     this.localRecord_quantity = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getBackwards_order() {
/*  221 */     return this.localBackwards_order;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setBackwards_order(int param) {
/*  232 */     this.localBackwards_order = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getFunction() {
/*  251 */     return this.localFunction;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setFunction(String param) {
/*  262 */     this.localFunction = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getXml_params() {
/*  281 */     return this.localXml_params;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setXml_params(String param) {
/*  292 */     this.localXml_params = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  312 */     ADBDataSource aDBDataSource = 
/*  313 */       new ADBDataSource(this, MY_QNAME);
/*  314 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  321 */     serialize(parentQName, xmlWriter, false);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  332 */     String prefix = null;
/*  333 */     String namespace = null;
/*      */ 
/*      */     
/*  336 */     prefix = parentQName.getPrefix();
/*  337 */     namespace = parentQName.getNamespaceURI();
/*  338 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*      */     
/*  340 */     if (serializeType) {
/*      */ 
/*      */       
/*  343 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/*  344 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/*  345 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  346 */             String.valueOf(namespacePrefix) + ":GetFirstTransbyDateJS", 
/*  347 */             xmlWriter);
/*      */       } else {
/*  349 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  350 */             "GetFirstTransbyDateJS", 
/*  351 */             xmlWriter);
/*      */       } 
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*  357 */     namespace = "";
/*  358 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*      */     
/*  360 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*      */       
/*  362 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  365 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*      */     } 
/*      */     
/*  368 */     xmlWriter.writeEndElement();
/*      */     
/*  370 */     namespace = "";
/*  371 */     writeStartElement(null, namespace, "type", xmlWriter);
/*      */ 
/*      */     
/*  374 */     if (this.localType == null) {
/*      */ 
/*      */       
/*  377 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  382 */       xmlWriter.writeCharacters(this.localType);
/*      */     } 
/*      */ 
/*      */     
/*  386 */     xmlWriter.writeEndElement();
/*      */     
/*  388 */     namespace = "";
/*  389 */     writeStartElement(null, namespace, "start_date", xmlWriter);
/*      */ 
/*      */     
/*  392 */     if (this.localStart_date == null) {
/*      */ 
/*      */       
/*  395 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  400 */       xmlWriter.writeCharacters(this.localStart_date);
/*      */     } 
/*      */ 
/*      */     
/*  404 */     xmlWriter.writeEndElement();
/*      */     
/*  406 */     namespace = "";
/*  407 */     writeStartElement(null, namespace, "end_date", xmlWriter);
/*      */ 
/*      */     
/*  410 */     if (this.localEnd_date == null) {
/*      */ 
/*      */       
/*  413 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  418 */       xmlWriter.writeCharacters(this.localEnd_date);
/*      */     } 
/*      */ 
/*      */     
/*  422 */     xmlWriter.writeEndElement();
/*      */     
/*  424 */     namespace = "";
/*  425 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*      */     
/*  427 */     if (this.localFlags == Integer.MIN_VALUE) {
/*      */       
/*  429 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  432 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*      */     } 
/*      */     
/*  435 */     xmlWriter.writeEndElement();
/*      */     
/*  437 */     namespace = "";
/*  438 */     writeStartElement(null, namespace, "record_quantity", xmlWriter);
/*      */     
/*  440 */     if (this.localRecord_quantity == Integer.MIN_VALUE) {
/*      */       
/*  442 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  445 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localRecord_quantity));
/*      */     } 
/*      */     
/*  448 */     xmlWriter.writeEndElement();
/*      */     
/*  450 */     namespace = "";
/*  451 */     writeStartElement(null, namespace, "backwards_order", xmlWriter);
/*      */     
/*  453 */     if (this.localBackwards_order == Integer.MIN_VALUE) {
/*      */       
/*  455 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  458 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localBackwards_order));
/*      */     } 
/*      */     
/*  461 */     xmlWriter.writeEndElement();
/*      */     
/*  463 */     namespace = "";
/*  464 */     writeStartElement(null, namespace, "function", xmlWriter);
/*      */ 
/*      */     
/*  467 */     if (this.localFunction == null) {
/*      */ 
/*      */       
/*  470 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  475 */       xmlWriter.writeCharacters(this.localFunction);
/*      */     } 
/*      */ 
/*      */     
/*  479 */     xmlWriter.writeEndElement();
/*      */     
/*  481 */     namespace = "";
/*  482 */     writeStartElement(null, namespace, "xml_params", xmlWriter);
/*      */ 
/*      */     
/*  485 */     if (this.localXml_params == null) {
/*      */ 
/*      */       
/*  488 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  493 */       xmlWriter.writeCharacters(this.localXml_params);
/*      */     } 
/*      */ 
/*      */     
/*  497 */     xmlWriter.writeEndElement();
/*      */     
/*  499 */     xmlWriter.writeEndElement();
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private static String generatePrefix(String namespace) {
/*  505 */     if (namespace.equals("urn:CSSoapService")) {
/*  506 */       return "ns1";
/*      */     }
/*  508 */     return BeanUtil.getUniquePrefix();
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  516 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/*  517 */     if (writerPrefix != null) {
/*  518 */       xmlWriter.writeStartElement(namespace, localPart);
/*      */     } else {
/*  520 */       if (namespace.length() == 0) {
/*  521 */         prefix = "";
/*  522 */       } else if (prefix == null) {
/*  523 */         prefix = generatePrefix(namespace);
/*      */       } 
/*      */       
/*  526 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/*  527 */       xmlWriter.writeNamespace(prefix, namespace);
/*  528 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  537 */     if (xmlWriter.getPrefix(namespace) == null) {
/*  538 */       xmlWriter.writeNamespace(prefix, namespace);
/*  539 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  541 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  549 */     if (namespace.equals("")) {
/*  550 */       xmlWriter.writeAttribute(attName, attValue);
/*      */     } else {
/*  552 */       registerPrefix(xmlWriter, namespace);
/*  553 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  564 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/*  565 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/*  566 */     if (attributePrefix == null) {
/*  567 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*      */     }
/*      */     
/*  570 */     if (attributePrefix.trim().length() > 0) {
/*  571 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*      */     } else {
/*  573 */       attributeValue = qname.getLocalPart();
/*      */     } 
/*      */     
/*  576 */     if (namespace.equals("")) {
/*  577 */       xmlWriter.writeAttribute(attName, attributeValue);
/*      */     } else {
/*  579 */       registerPrefix(xmlWriter, namespace);
/*  580 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  589 */     String namespaceURI = qname.getNamespaceURI();
/*  590 */     if (namespaceURI != null) {
/*  591 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/*  592 */       if (prefix == null) {
/*  593 */         prefix = generatePrefix(namespaceURI);
/*  594 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/*  595 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*      */       } 
/*      */       
/*  598 */       if (prefix.trim().length() > 0) {
/*  599 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*      */       } else {
/*      */         
/*  602 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */       } 
/*      */     } else {
/*      */       
/*  606 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  613 */     if (qnames != null) {
/*      */ 
/*      */       
/*  616 */       StringBuffer stringToWrite = new StringBuffer();
/*  617 */       String namespaceURI = null;
/*  618 */       String prefix = null;
/*      */       
/*  620 */       for (int i = 0; i < qnames.length; i++) {
/*  621 */         if (i > 0) {
/*  622 */           stringToWrite.append(" ");
/*      */         }
/*  624 */         namespaceURI = qnames[i].getNamespaceURI();
/*  625 */         if (namespaceURI != null) {
/*  626 */           prefix = xmlWriter.getPrefix(namespaceURI);
/*  627 */           if (prefix == null || prefix.length() == 0) {
/*  628 */             prefix = generatePrefix(namespaceURI);
/*  629 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/*  630 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*      */           } 
/*      */           
/*  633 */           if (prefix.trim().length() > 0) {
/*  634 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*      */           } else {
/*  636 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */           } 
/*      */         } else {
/*  639 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */         } 
/*      */       } 
/*  642 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/*  652 */     String prefix = xmlWriter.getPrefix(namespace);
/*  653 */     if (prefix == null) {
/*  654 */       prefix = generatePrefix(namespace);
/*  655 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*      */       while (true) {
/*  657 */         String uri = nsContext.getNamespaceURI(prefix);
/*  658 */         if (uri == null || uri.length() == 0) {
/*      */           break;
/*      */         }
/*  661 */         prefix = BeanUtil.getUniquePrefix();
/*      */       } 
/*  663 */       xmlWriter.writeNamespace(prefix, namespace);
/*  664 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  666 */     return prefix;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/*  680 */     ArrayList<QName> elementList = new ArrayList();
/*  681 */     ArrayList attribList = new ArrayList();
/*      */ 
/*      */     
/*  684 */     elementList.add(new QName("", 
/*  685 */           "access_key"));
/*      */     
/*  687 */     elementList.add(
/*  688 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*      */     
/*  690 */     elementList.add(new QName("", 
/*  691 */           "type"));
/*      */     
/*  693 */     elementList.add((this.localType == null) ? null : 
/*  694 */         new QName(ConverterUtil.convertToString(this.localType)));
/*      */     
/*  696 */     elementList.add(new QName("", 
/*  697 */           "start_date"));
/*      */     
/*  699 */     elementList.add((this.localStart_date == null) ? null : 
/*  700 */         new QName(ConverterUtil.convertToString(this.localStart_date)));
/*      */     
/*  702 */     elementList.add(new QName("", 
/*  703 */           "end_date"));
/*      */     
/*  705 */     elementList.add((this.localEnd_date == null) ? null : 
/*  706 */         new QName(ConverterUtil.convertToString(this.localEnd_date)));
/*      */     
/*  708 */     elementList.add(new QName("", 
/*  709 */           "flags"));
/*      */     
/*  711 */     elementList.add(
/*  712 */         new QName(ConverterUtil.convertToString(this.localFlags)));
/*      */     
/*  714 */     elementList.add(new QName("", 
/*  715 */           "record_quantity"));
/*      */     
/*  717 */     elementList.add(
/*  718 */         new QName(ConverterUtil.convertToString(this.localRecord_quantity)));
/*      */     
/*  720 */     elementList.add(new QName("", 
/*  721 */           "backwards_order"));
/*      */     
/*  723 */     elementList.add(
/*  724 */         new QName(ConverterUtil.convertToString(this.localBackwards_order)));
/*      */     
/*  726 */     elementList.add(new QName("", 
/*  727 */           "function"));
/*      */     
/*  729 */     elementList.add((this.localFunction == null) ? null : 
/*  730 */         new QName(ConverterUtil.convertToString(this.localFunction)));
/*      */     
/*  732 */     elementList.add(new QName("", 
/*  733 */           "xml_params"));
/*      */     
/*  735 */     elementList.add((this.localXml_params == null) ? null : 
/*  736 */         new QName(ConverterUtil.convertToString(this.localXml_params)));
/*      */ 
/*      */     
/*  739 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public static class Factory
/*      */   {
/*      */     public static GetFirstTransbyDateJS parse(XMLStreamReader reader) throws Exception {
/*  763 */       GetFirstTransbyDateJS object = 
/*  764 */         new GetFirstTransbyDateJS();
/*      */ 
/*      */       
/*  767 */       String nillableValue = null;
/*  768 */       String prefix = "";
/*  769 */       String namespaceuri = "";
/*      */       
/*      */       try {
/*  772 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/*  773 */           reader.next();
/*      */         }
/*      */         
/*  776 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/*  777 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/*  778 */               "type");
/*  779 */           if (fullTypeName != null) {
/*  780 */             String nsPrefix = null;
/*  781 */             if (fullTypeName.indexOf(":") > -1) {
/*  782 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*      */             }
/*  784 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*      */             
/*  786 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*      */             
/*  788 */             if (!"GetFirstTransbyDateJS".equals(type)) {
/*      */               
/*  790 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/*  791 */               return (GetFirstTransbyDateJS)ExtensionMapper.getTypeObject(
/*  792 */                   nsUri, type, reader);
/*      */             } 
/*      */           } 
/*      */         } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  806 */         Vector handledAttributes = new Vector();
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  811 */         reader.next();
/*      */ 
/*      */         
/*  814 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  816 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*      */           
/*  818 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  819 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  822 */             String content = reader.getElementText();
/*      */             
/*  824 */             object.setAccess_key(
/*  825 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  830 */             object.setAccess_key(-2147483648);
/*      */             
/*  832 */             reader.getElementText();
/*      */           } 
/*      */           
/*  835 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  841 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  845 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  847 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*      */           
/*  849 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  850 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  853 */             String content = reader.getElementText();
/*      */             
/*  855 */             object.setType(
/*  856 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  861 */             reader.getElementText();
/*      */           } 
/*      */           
/*  864 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  870 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  874 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  876 */         if (reader.isStartElement() && (new QName("", "start_date")).equals(reader.getName())) {
/*      */           
/*  878 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  879 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  882 */             String content = reader.getElementText();
/*      */             
/*  884 */             object.setStart_date(
/*  885 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  890 */             reader.getElementText();
/*      */           } 
/*      */           
/*  893 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  899 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  903 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  905 */         if (reader.isStartElement() && (new QName("", "end_date")).equals(reader.getName())) {
/*      */           
/*  907 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  908 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  911 */             String content = reader.getElementText();
/*      */             
/*  913 */             object.setEnd_date(
/*  914 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  919 */             reader.getElementText();
/*      */           } 
/*      */           
/*  922 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  928 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  932 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  934 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*      */           
/*  936 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  937 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  940 */             String content = reader.getElementText();
/*      */             
/*  942 */             object.setFlags(
/*  943 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  948 */             object.setFlags(-2147483648);
/*      */             
/*  950 */             reader.getElementText();
/*      */           } 
/*      */           
/*  953 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  959 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  963 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  965 */         if (reader.isStartElement() && (new QName("", "record_quantity")).equals(reader.getName())) {
/*      */           
/*  967 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  968 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  971 */             String content = reader.getElementText();
/*      */             
/*  973 */             object.setRecord_quantity(
/*  974 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  979 */             object.setRecord_quantity(-2147483648);
/*      */             
/*  981 */             reader.getElementText();
/*      */           } 
/*      */           
/*  984 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  990 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  994 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  996 */         if (reader.isStartElement() && (new QName("", "backwards_order")).equals(reader.getName())) {
/*      */           
/*  998 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  999 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/* 1002 */             String content = reader.getElementText();
/*      */             
/* 1004 */             object.setBackwards_order(
/* 1005 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/* 1010 */             object.setBackwards_order(-2147483648);
/*      */             
/* 1012 */             reader.getElementText();
/*      */           } 
/*      */           
/* 1015 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1021 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1025 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1027 */         if (reader.isStartElement() && (new QName("", "function")).equals(reader.getName())) {
/*      */           
/* 1029 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1030 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/* 1033 */             String content = reader.getElementText();
/*      */             
/* 1035 */             object.setFunction(
/* 1036 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/* 1041 */             reader.getElementText();
/*      */           } 
/*      */           
/* 1044 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1050 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1054 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1056 */         if (reader.isStartElement() && (new QName("", "xml_params")).equals(reader.getName())) {
/*      */           
/* 1058 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1059 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/* 1062 */             String content = reader.getElementText();
/*      */             
/* 1064 */             object.setXml_params(
/* 1065 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/* 1070 */             reader.getElementText();
/*      */           } 
/*      */           
/* 1073 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1079 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */         
/* 1082 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1083 */           reader.next();
/*      */         }
/* 1085 */         if (reader.isStartElement())
/*      */         {
/* 1087 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         
/*      */         }
/*      */       
/*      */       }
/* 1092 */       catch (XMLStreamException e) {
/* 1093 */         throw new Exception(e);
/*      */       } 
/*      */       
/* 1096 */       return object;
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetFirstTransbyDateJS.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */