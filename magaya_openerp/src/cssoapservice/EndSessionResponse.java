package cssoapservice;

import java.util.ArrayList;
import java.util.Vector;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.apache.axiom.om.OMDataSource;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axis2.databinding.ADBBean;
import org.apache.axis2.databinding.ADBDataSource;
import org.apache.axis2.databinding.ADBException;
import org.apache.axis2.databinding.utils.BeanUtil;
import org.apache.axis2.databinding.utils.ConverterUtil;
import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;

public class EndSessionResponse implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "EndSessionResponse", 
/*  24 */       "ns1");






  
  protected Api_session_error local_return;






  
  public Api_session_error get_return() {
/*  41 */     return this.local_return;
  }







  
  public void set_return(Api_session_error param) {
/*  52 */     this.local_return = param;
  }
















  
  public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  72 */     ADBDataSource aDBDataSource = 
/*  73 */       new ADBDataSource(this, MY_QNAME);
/*  74 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
  }



  
  public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  81 */     serialize(parentQName, xmlWriter, false);
  }







  
  public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  92 */     String prefix = null;
/*  93 */     String namespace = null;

    
/*  96 */     prefix = parentQName.getPrefix();
/*  97 */     namespace = parentQName.getNamespaceURI();
/*  98 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
    
/* 100 */     if (serializeType) {

      
/* 103 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 104 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 105 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 106 */             String.valueOf(namespacePrefix) + ":EndSessionResponse", 
/* 107 */             xmlWriter);
      } else {
/* 109 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 110 */             "EndSessionResponse", 
/* 111 */             xmlWriter);
      } 
    } 


    
/* 117 */     if (this.local_return == null) {
      
/* 119 */       writeStartElement(null, "", "return", xmlWriter);

      
/* 122 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/* 123 */       xmlWriter.writeEndElement();
    } else {
/* 125 */       this.local_return.serialize(new QName("", "return"), 
/* 126 */           xmlWriter);
    } 
    
/* 129 */     xmlWriter.writeEndElement();
  }


  
  private static String generatePrefix(String namespace) {
/* 135 */     if (namespace.equals("urn:CSSoapService")) {
/* 136 */       return "ns1";
    }
/* 138 */     return BeanUtil.getUniquePrefix();
  }




  
  private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 146 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 147 */     if (writerPrefix != null) {
/* 148 */       xmlWriter.writeStartElement(namespace, localPart);
    } else {
/* 150 */       if (namespace.length() == 0) {
/* 151 */         prefix = "";
/* 152 */       } else if (prefix == null) {
/* 153 */         prefix = generatePrefix(namespace);
      } 
      
/* 156 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 157 */       xmlWriter.writeNamespace(prefix, namespace);
/* 158 */       xmlWriter.setPrefix(prefix, namespace);
    } 
  }




  
  private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 167 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 168 */       xmlWriter.writeNamespace(prefix, namespace);
/* 169 */       xmlWriter.setPrefix(prefix, namespace);
    } 
/* 171 */     xmlWriter.writeAttribute(namespace, attName, attValue);
  }




  
  private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 179 */     if (namespace.equals("")) {
/* 180 */       xmlWriter.writeAttribute(attName, attValue);
    } else {
/* 182 */       registerPrefix(xmlWriter, namespace);
/* 183 */       xmlWriter.writeAttribute(namespace, attName, attValue);
    } 
  }






  
  private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 194 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 195 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 196 */     if (attributePrefix == null) {
/* 197 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    
/* 200 */     if (attributePrefix.trim().length() > 0) {
/* 201 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
    } else {
/* 203 */       attributeValue = qname.getLocalPart();
    } 
    
/* 206 */     if (namespace.equals("")) {
/* 207 */       xmlWriter.writeAttribute(attName, attributeValue);
    } else {
/* 209 */       registerPrefix(xmlWriter, namespace);
/* 210 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
    } 
  }




  
  private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 219 */     String namespaceURI = qname.getNamespaceURI();
/* 220 */     if (namespaceURI != null) {
/* 221 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 222 */       if (prefix == null) {
/* 223 */         prefix = generatePrefix(namespaceURI);
/* 224 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 225 */         xmlWriter.setPrefix(prefix, namespaceURI);
      } 
      
/* 228 */       if (prefix.trim().length() > 0) {
/* 229 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
      } else {
        
/* 232 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
      } 
    } else {
      
/* 236 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
    } 
  }


  
  private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 243 */     if (qnames != null) {

      
/* 246 */       StringBuffer stringToWrite = new StringBuffer();
/* 247 */       String namespaceURI = null;
/* 248 */       String prefix = null;
      
/* 250 */       for (int i = 0; i < qnames.length; i++) {
/* 251 */         if (i > 0) {
/* 252 */           stringToWrite.append(" ");
        }
/* 254 */         namespaceURI = qnames[i].getNamespaceURI();
/* 255 */         if (namespaceURI != null) {
/* 256 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 257 */           if (prefix == null || prefix.length() == 0) {
/* 258 */             prefix = generatePrefix(namespaceURI);
/* 259 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 260 */             xmlWriter.setPrefix(prefix, namespaceURI);
          } 
          
/* 263 */           if (prefix.trim().length() > 0) {
/* 264 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
          } else {
/* 266 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
          } 
        } else {
/* 269 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
        } 
      } 
/* 272 */       xmlWriter.writeCharacters(stringToWrite.toString());
    } 
  }





  
  private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 282 */     String prefix = xmlWriter.getPrefix(namespace);
/* 283 */     if (prefix == null) {
/* 284 */       prefix = generatePrefix(namespace);
/* 285 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
/* 287 */         String uri = nsContext.getNamespaceURI(prefix);
/* 288 */         if (uri == null || uri.length() == 0) {
          break;
        }
/* 291 */         prefix = BeanUtil.getUniquePrefix();
      } 
/* 293 */       xmlWriter.writeNamespace(prefix, namespace);
/* 294 */       xmlWriter.setPrefix(prefix, namespace);
    } 
/* 296 */     return prefix;
  }










  
  public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 310 */     ArrayList<QName> elementList = new ArrayList();
/* 311 */     ArrayList attribList = new ArrayList();

    
/* 314 */     elementList.add(new QName("", 
/* 315 */           "return"));

    
/* 318 */     elementList.add((this.local_return == null) ? null : 
/* 319 */         new QName(ConverterUtil.convertToString(this.local_return)));

    
/* 322 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
  }


















  
  public static class Factory
  {
    public static EndSessionResponse parse(XMLStreamReader reader) throws Exception {
/* 346 */       EndSessionResponse object = 
/* 347 */         new EndSessionResponse();

      
/* 350 */       String nillableValue = null;
/* 351 */       String prefix = "";
/* 352 */       String namespaceuri = "";
      
      try {
/* 355 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 356 */           reader.next();
        }
        
/* 359 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 360 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 361 */               "type");
/* 362 */           if (fullTypeName != null) {
/* 363 */             String nsPrefix = null;
/* 364 */             if (fullTypeName.indexOf(":") > -1) {
/* 365 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
/* 367 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
            
/* 369 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
            
/* 371 */             if (!"EndSessionResponse".equals(type)) {
              
/* 373 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 374 */               return (EndSessionResponse)ExtensionMapper.getTypeObject(
/* 375 */                   nsUri, type, reader);
            } 
          } 
        } 









        
/* 389 */         Vector handledAttributes = new Vector();



        
/* 394 */         reader.next();

        
/* 397 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
        
/* 399 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
          
/* 401 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 402 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 403 */             object.set_return(null);
/* 404 */             reader.next();
            
/* 406 */             reader.next();
          }
          else {
            
/* 410 */             object.set_return(Api_session_error.Factory.parse(reader));
            
/* 412 */             reader.next();
          }
        
        }
        else {
          
/* 418 */           throw new ADBException("Unexpected subelement " + reader.getName());
        } 
        
/* 421 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 422 */           reader.next();
        }
/* 424 */         if (reader.isStartElement())
        {
/* 426 */           throw new ADBException("Unexpected subelement " + reader.getName());
        
        }
      
      }
/* 431 */       catch (XMLStreamException e) {
/* 432 */         throw new Exception(e);
      } 
      
/* 435 */       return object;
    }
  }
}


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\EndSessionResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */