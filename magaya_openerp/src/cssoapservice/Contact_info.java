/*      */ package cssoapservice;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.Vector;
/*      */ import javax.xml.namespace.NamespaceContext;
/*      */ import javax.xml.namespace.QName;
/*      */ import javax.xml.stream.XMLStreamException;
/*      */ import javax.xml.stream.XMLStreamReader;
/*      */ import javax.xml.stream.XMLStreamWriter;
/*      */ import org.apache.axiom.om.OMDataSource;
/*      */ import org.apache.axiom.om.OMElement;
/*      */ import org.apache.axiom.om.OMFactory;
/*      */ import org.apache.axis2.databinding.ADBBean;
/*      */ import org.apache.axis2.databinding.ADBDataSource;
/*      */ import org.apache.axis2.databinding.ADBException;
/*      */ import org.apache.axis2.databinding.types.UnsignedByte;
/*      */ import org.apache.axis2.databinding.utils.BeanUtil;
/*      */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*      */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*      */ 
/*      */ public class Contact_info
/*      */   implements ADBBean
/*      */ {
/*      */   protected int localId;
/*      */   protected String localName;
/*      */   protected String localEmail;
/*      */   protected String localWeb_site;
/*      */   protected String localWan_ip;
/*      */   protected String localLan_ip;
/*      */   protected short localPort;
/*      */   protected UnsignedByte localConnect_type;
/*      */   protected UnsignedByte localNet_version;
/*      */   protected int localAvailability;
/*      */   protected int localVersion;
/*      */   protected short localLoc_lic;
/*      */   protected short localRem_lic;
/*      */   protected int localServices;
/*      */   
/*      */   public int getId() {
/*   40 */     return this.localId;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setId(int param) {
/*   51 */     this.localId = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getName() {
/*   70 */     return this.localName;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setName(String param) {
/*   81 */     this.localName = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getEmail() {
/*  100 */     return this.localEmail;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setEmail(String param) {
/*  111 */     this.localEmail = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getWeb_site() {
/*  130 */     return this.localWeb_site;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setWeb_site(String param) {
/*  141 */     this.localWeb_site = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getWan_ip() {
/*  160 */     return this.localWan_ip;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setWan_ip(String param) {
/*  171 */     this.localWan_ip = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getLan_ip() {
/*  190 */     return this.localLan_ip;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLan_ip(String param) {
/*  201 */     this.localLan_ip = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getPort() {
/*  220 */     return this.localPort;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setPort(short param) {
/*  231 */     this.localPort = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public UnsignedByte getConnect_type() {
/*  250 */     return this.localConnect_type;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setConnect_type(UnsignedByte param) {
/*  261 */     this.localConnect_type = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public UnsignedByte getNet_version() {
/*  280 */     return this.localNet_version;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setNet_version(UnsignedByte param) {
/*  291 */     this.localNet_version = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getAvailability() {
/*  310 */     return this.localAvailability;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setAvailability(int param) {
/*  321 */     this.localAvailability = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getVersion() {
/*  340 */     return this.localVersion;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setVersion(int param) {
/*  351 */     this.localVersion = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getLoc_lic() {
/*  370 */     return this.localLoc_lic;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLoc_lic(short param) {
/*  381 */     this.localLoc_lic = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getRem_lic() {
/*  400 */     return this.localRem_lic;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setRem_lic(short param) {
/*  411 */     this.localRem_lic = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getServices() {
/*  430 */     return this.localServices;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setServices(int param) {
/*  441 */     this.localServices = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  461 */     ADBDataSource aDBDataSource = 
/*  462 */       new ADBDataSource(this, parentQName);
/*  463 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, parentQName);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  470 */     serialize(parentQName, xmlWriter, false);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  481 */     String prefix = null;
/*  482 */     String namespace = null;
/*      */ 
/*      */     
/*  485 */     prefix = parentQName.getPrefix();
/*  486 */     namespace = parentQName.getNamespaceURI();
/*  487 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*      */     
/*  489 */     if (serializeType) {
/*      */ 
/*      */       
/*  492 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/*  493 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/*  494 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  495 */             String.valueOf(namespacePrefix) + ":contact_info", 
/*  496 */             xmlWriter);
/*      */       } else {
/*  498 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  499 */             "contact_info", 
/*  500 */             xmlWriter);
/*      */       } 
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*  506 */     namespace = "urn:CSSoapService";
/*  507 */     writeStartElement(null, namespace, "id", xmlWriter);
/*      */     
/*  509 */     if (this.localId == Integer.MIN_VALUE)
/*      */     {
/*  511 */       throw new ADBException("id cannot be null!!");
/*      */     }
/*      */     
/*  514 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localId));
/*      */ 
/*      */     
/*  517 */     xmlWriter.writeEndElement();
/*      */     
/*  519 */     namespace = "urn:CSSoapService";
/*  520 */     writeStartElement(null, namespace, "name", xmlWriter);
/*      */ 
/*      */     
/*  523 */     if (this.localName == null)
/*      */     {
/*      */       
/*  526 */       throw new ADBException("name cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  531 */     xmlWriter.writeCharacters(this.localName);
/*      */ 
/*      */ 
/*      */     
/*  535 */     xmlWriter.writeEndElement();
/*      */     
/*  537 */     namespace = "urn:CSSoapService";
/*  538 */     writeStartElement(null, namespace, "email", xmlWriter);
/*      */ 
/*      */     
/*  541 */     if (this.localEmail == null)
/*      */     {
/*      */       
/*  544 */       throw new ADBException("email cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  549 */     xmlWriter.writeCharacters(this.localEmail);
/*      */ 
/*      */ 
/*      */     
/*  553 */     xmlWriter.writeEndElement();
/*      */     
/*  555 */     namespace = "urn:CSSoapService";
/*  556 */     writeStartElement(null, namespace, "web_site", xmlWriter);
/*      */ 
/*      */     
/*  559 */     if (this.localWeb_site == null)
/*      */     {
/*      */       
/*  562 */       throw new ADBException("web_site cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  567 */     xmlWriter.writeCharacters(this.localWeb_site);
/*      */ 
/*      */ 
/*      */     
/*  571 */     xmlWriter.writeEndElement();
/*      */     
/*  573 */     namespace = "urn:CSSoapService";
/*  574 */     writeStartElement(null, namespace, "wan_ip", xmlWriter);
/*      */ 
/*      */     
/*  577 */     if (this.localWan_ip == null)
/*      */     {
/*      */       
/*  580 */       throw new ADBException("wan_ip cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  585 */     xmlWriter.writeCharacters(this.localWan_ip);
/*      */ 
/*      */ 
/*      */     
/*  589 */     xmlWriter.writeEndElement();
/*      */     
/*  591 */     namespace = "urn:CSSoapService";
/*  592 */     writeStartElement(null, namespace, "lan_ip", xmlWriter);
/*      */ 
/*      */     
/*  595 */     if (this.localLan_ip == null)
/*      */     {
/*      */       
/*  598 */       throw new ADBException("lan_ip cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  603 */     xmlWriter.writeCharacters(this.localLan_ip);
/*      */ 
/*      */ 
/*      */     
/*  607 */     xmlWriter.writeEndElement();
/*      */     
/*  609 */     namespace = "urn:CSSoapService";
/*  610 */     writeStartElement(null, namespace, "port", xmlWriter);
/*      */     
/*  612 */     if (this.localPort == Short.MIN_VALUE)
/*      */     {
/*  614 */       throw new ADBException("port cannot be null!!");
/*      */     }
/*      */     
/*  617 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localPort));
/*      */ 
/*      */     
/*  620 */     xmlWriter.writeEndElement();
/*      */     
/*  622 */     namespace = "urn:CSSoapService";
/*  623 */     writeStartElement(null, namespace, "connect_type", xmlWriter);
/*      */ 
/*      */     
/*  626 */     if (this.localConnect_type == null)
/*      */     {
/*      */       
/*  629 */       throw new ADBException("connect_type cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  634 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localConnect_type));
/*      */ 
/*      */ 
/*      */     
/*  638 */     xmlWriter.writeEndElement();
/*      */     
/*  640 */     namespace = "urn:CSSoapService";
/*  641 */     writeStartElement(null, namespace, "net_version", xmlWriter);
/*      */ 
/*      */     
/*  644 */     if (this.localNet_version == null)
/*      */     {
/*      */       
/*  647 */       throw new ADBException("net_version cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  652 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localNet_version));
/*      */ 
/*      */ 
/*      */     
/*  656 */     xmlWriter.writeEndElement();
/*      */     
/*  658 */     namespace = "urn:CSSoapService";
/*  659 */     writeStartElement(null, namespace, "availability", xmlWriter);
/*      */     
/*  661 */     if (this.localAvailability == Integer.MIN_VALUE)
/*      */     {
/*  663 */       throw new ADBException("availability cannot be null!!");
/*      */     }
/*      */     
/*  666 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAvailability));
/*      */ 
/*      */     
/*  669 */     xmlWriter.writeEndElement();
/*      */     
/*  671 */     namespace = "urn:CSSoapService";
/*  672 */     writeStartElement(null, namespace, "version", xmlWriter);
/*      */     
/*  674 */     if (this.localVersion == Integer.MIN_VALUE)
/*      */     {
/*  676 */       throw new ADBException("version cannot be null!!");
/*      */     }
/*      */     
/*  679 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localVersion));
/*      */ 
/*      */     
/*  682 */     xmlWriter.writeEndElement();
/*      */     
/*  684 */     namespace = "urn:CSSoapService";
/*  685 */     writeStartElement(null, namespace, "loc_lic", xmlWriter);
/*      */     
/*  687 */     if (this.localLoc_lic == Short.MIN_VALUE)
/*      */     {
/*  689 */       throw new ADBException("loc_lic cannot be null!!");
/*      */     }
/*      */     
/*  692 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localLoc_lic));
/*      */ 
/*      */     
/*  695 */     xmlWriter.writeEndElement();
/*      */     
/*  697 */     namespace = "urn:CSSoapService";
/*  698 */     writeStartElement(null, namespace, "rem_lic", xmlWriter);
/*      */     
/*  700 */     if (this.localRem_lic == Short.MIN_VALUE)
/*      */     {
/*  702 */       throw new ADBException("rem_lic cannot be null!!");
/*      */     }
/*      */     
/*  705 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localRem_lic));
/*      */ 
/*      */     
/*  708 */     xmlWriter.writeEndElement();
/*      */     
/*  710 */     namespace = "urn:CSSoapService";
/*  711 */     writeStartElement(null, namespace, "services", xmlWriter);
/*      */     
/*  713 */     if (this.localServices == Integer.MIN_VALUE)
/*      */     {
/*  715 */       throw new ADBException("services cannot be null!!");
/*      */     }
/*      */     
/*  718 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localServices));
/*      */ 
/*      */     
/*  721 */     xmlWriter.writeEndElement();
/*      */     
/*  723 */     xmlWriter.writeEndElement();
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private static String generatePrefix(String namespace) {
/*  729 */     if (namespace.equals("urn:CSSoapService")) {
/*  730 */       return "ns1";
/*      */     }
/*  732 */     return BeanUtil.getUniquePrefix();
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  740 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/*  741 */     if (writerPrefix != null) {
/*  742 */       xmlWriter.writeStartElement(namespace, localPart);
/*      */     } else {
/*  744 */       if (namespace.length() == 0) {
/*  745 */         prefix = "";
/*  746 */       } else if (prefix == null) {
/*  747 */         prefix = generatePrefix(namespace);
/*      */       } 
/*      */       
/*  750 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/*  751 */       xmlWriter.writeNamespace(prefix, namespace);
/*  752 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  761 */     if (xmlWriter.getPrefix(namespace) == null) {
/*  762 */       xmlWriter.writeNamespace(prefix, namespace);
/*  763 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  765 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  773 */     if (namespace.equals("")) {
/*  774 */       xmlWriter.writeAttribute(attName, attValue);
/*      */     } else {
/*  776 */       registerPrefix(xmlWriter, namespace);
/*  777 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  788 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/*  789 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/*  790 */     if (attributePrefix == null) {
/*  791 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*      */     }
/*      */     
/*  794 */     if (attributePrefix.trim().length() > 0) {
/*  795 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*      */     } else {
/*  797 */       attributeValue = qname.getLocalPart();
/*      */     } 
/*      */     
/*  800 */     if (namespace.equals("")) {
/*  801 */       xmlWriter.writeAttribute(attName, attributeValue);
/*      */     } else {
/*  803 */       registerPrefix(xmlWriter, namespace);
/*  804 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  813 */     String namespaceURI = qname.getNamespaceURI();
/*  814 */     if (namespaceURI != null) {
/*  815 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/*  816 */       if (prefix == null) {
/*  817 */         prefix = generatePrefix(namespaceURI);
/*  818 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/*  819 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*      */       } 
/*      */       
/*  822 */       if (prefix.trim().length() > 0) {
/*  823 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*      */       } else {
/*      */         
/*  826 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */       } 
/*      */     } else {
/*      */       
/*  830 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  837 */     if (qnames != null) {
/*      */ 
/*      */       
/*  840 */       StringBuffer stringToWrite = new StringBuffer();
/*  841 */       String namespaceURI = null;
/*  842 */       String prefix = null;
/*      */       
/*  844 */       for (int i = 0; i < qnames.length; i++) {
/*  845 */         if (i > 0) {
/*  846 */           stringToWrite.append(" ");
/*      */         }
/*  848 */         namespaceURI = qnames[i].getNamespaceURI();
/*  849 */         if (namespaceURI != null) {
/*  850 */           prefix = xmlWriter.getPrefix(namespaceURI);
/*  851 */           if (prefix == null || prefix.length() == 0) {
/*  852 */             prefix = generatePrefix(namespaceURI);
/*  853 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/*  854 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*      */           } 
/*      */           
/*  857 */           if (prefix.trim().length() > 0) {
/*  858 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*      */           } else {
/*  860 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */           } 
/*      */         } else {
/*  863 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */         } 
/*      */       } 
/*  866 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/*  876 */     String prefix = xmlWriter.getPrefix(namespace);
/*  877 */     if (prefix == null) {
/*  878 */       prefix = generatePrefix(namespace);
/*  879 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*      */       while (true) {
/*  881 */         String uri = nsContext.getNamespaceURI(prefix);
/*  882 */         if (uri == null || uri.length() == 0) {
/*      */           break;
/*      */         }
/*  885 */         prefix = BeanUtil.getUniquePrefix();
/*      */       } 
/*  887 */       xmlWriter.writeNamespace(prefix, namespace);
/*  888 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  890 */     return prefix;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/*  904 */     ArrayList<QName> elementList = new ArrayList();
/*  905 */     ArrayList attribList = new ArrayList();
/*      */ 
/*      */     
/*  908 */     elementList.add(new QName("urn:CSSoapService", 
/*  909 */           "id"));
/*      */     
/*  911 */     elementList.add(
/*  912 */         new QName(ConverterUtil.convertToString(this.localId)));
/*      */     
/*  914 */     elementList.add(new QName("urn:CSSoapService", 
/*  915 */           "name"));
/*      */     
/*  917 */     if (this.localName != null) {
/*  918 */       elementList.add(new QName(ConverterUtil.convertToString(this.localName)));
/*      */     } else {
/*  920 */       throw new ADBException("name cannot be null!!");
/*      */     } 
/*      */     
/*  923 */     elementList.add(new QName("urn:CSSoapService", 
/*  924 */           "email"));
/*      */     
/*  926 */     if (this.localEmail != null) {
/*  927 */       elementList.add(new QName(ConverterUtil.convertToString(this.localEmail)));
/*      */     } else {
/*  929 */       throw new ADBException("email cannot be null!!");
/*      */     } 
/*      */     
/*  932 */     elementList.add(new QName("urn:CSSoapService", 
/*  933 */           "web_site"));
/*      */     
/*  935 */     if (this.localWeb_site != null) {
/*  936 */       elementList.add(new QName(ConverterUtil.convertToString(this.localWeb_site)));
/*      */     } else {
/*  938 */       throw new ADBException("web_site cannot be null!!");
/*      */     } 
/*      */     
/*  941 */     elementList.add(new QName("urn:CSSoapService", 
/*  942 */           "wan_ip"));
/*      */     
/*  944 */     if (this.localWan_ip != null) {
/*  945 */       elementList.add(new QName(ConverterUtil.convertToString(this.localWan_ip)));
/*      */     } else {
/*  947 */       throw new ADBException("wan_ip cannot be null!!");
/*      */     } 
/*      */     
/*  950 */     elementList.add(new QName("urn:CSSoapService", 
/*  951 */           "lan_ip"));
/*      */     
/*  953 */     if (this.localLan_ip != null) {
/*  954 */       elementList.add(new QName(ConverterUtil.convertToString(this.localLan_ip)));
/*      */     } else {
/*  956 */       throw new ADBException("lan_ip cannot be null!!");
/*      */     } 
/*      */     
/*  959 */     elementList.add(new QName("urn:CSSoapService", 
/*  960 */           "port"));
/*      */     
/*  962 */     elementList.add(
/*  963 */         new QName(ConverterUtil.convertToString(this.localPort)));
/*      */     
/*  965 */     elementList.add(new QName("urn:CSSoapService", 
/*  966 */           "connect_type"));
/*      */     
/*  968 */     if (this.localConnect_type != null) {
/*  969 */       elementList.add(new QName(ConverterUtil.convertToString(this.localConnect_type)));
/*      */     } else {
/*  971 */       throw new ADBException("connect_type cannot be null!!");
/*      */     } 
/*      */     
/*  974 */     elementList.add(new QName("urn:CSSoapService", 
/*  975 */           "net_version"));
/*      */     
/*  977 */     if (this.localNet_version != null) {
/*  978 */       elementList.add(new QName(ConverterUtil.convertToString(this.localNet_version)));
/*      */     } else {
/*  980 */       throw new ADBException("net_version cannot be null!!");
/*      */     } 
/*      */     
/*  983 */     elementList.add(new QName("urn:CSSoapService", 
/*  984 */           "availability"));
/*      */     
/*  986 */     elementList.add(
/*  987 */         new QName(ConverterUtil.convertToString(this.localAvailability)));
/*      */     
/*  989 */     elementList.add(new QName("urn:CSSoapService", 
/*  990 */           "version"));
/*      */     
/*  992 */     elementList.add(
/*  993 */         new QName(ConverterUtil.convertToString(this.localVersion)));
/*      */     
/*  995 */     elementList.add(new QName("urn:CSSoapService", 
/*  996 */           "loc_lic"));
/*      */     
/*  998 */     elementList.add(
/*  999 */         new QName(ConverterUtil.convertToString(this.localLoc_lic)));
/*      */     
/* 1001 */     elementList.add(new QName("urn:CSSoapService", 
/* 1002 */           "rem_lic"));
/*      */     
/* 1004 */     elementList.add(
/* 1005 */         new QName(ConverterUtil.convertToString(this.localRem_lic)));
/*      */     
/* 1007 */     elementList.add(new QName("urn:CSSoapService", 
/* 1008 */           "services"));
/*      */     
/* 1010 */     elementList.add(
/* 1011 */         new QName(ConverterUtil.convertToString(this.localServices)));
/*      */ 
/*      */     
/* 1014 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public static class Factory
/*      */   {
/*      */     public static Contact_info parse(XMLStreamReader reader) throws Exception {
/* 1038 */       Contact_info object = 
/* 1039 */         new Contact_info();
/*      */ 
/*      */       
/* 1042 */       String nillableValue = null;
/* 1043 */       String prefix = "";
/* 1044 */       String namespaceuri = "";
/*      */       
/*      */       try {
/* 1047 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1048 */           reader.next();
/*      */         }
/*      */         
/* 1051 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 1052 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 1053 */               "type");
/* 1054 */           if (fullTypeName != null) {
/* 1055 */             String nsPrefix = null;
/* 1056 */             if (fullTypeName.indexOf(":") > -1) {
/* 1057 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*      */             }
/* 1059 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*      */             
/* 1061 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*      */             
/* 1063 */             if (!"contact_info".equals(type)) {
/*      */               
/* 1065 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 1066 */               return (Contact_info)ExtensionMapper.getTypeObject(
/* 1067 */                   nsUri, type, reader);
/*      */             } 
/*      */           } 
/*      */         } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         
/* 1081 */         Vector handledAttributes = new Vector();
/*      */ 
/*      */ 
/*      */ 
/*      */         
/* 1086 */         reader.next();
/*      */ 
/*      */         
/* 1089 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1091 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "id")).equals(reader.getName())) {
/*      */           
/* 1093 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1094 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1095 */             throw new ADBException("The element: id  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1099 */           String content = reader.getElementText();
/*      */           
/* 1101 */           object.setId(
/* 1102 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1104 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1110 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1114 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1116 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "name")).equals(reader.getName())) {
/*      */           
/* 1118 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1119 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1120 */             throw new ADBException("The element: name  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1124 */           String content = reader.getElementText();
/*      */           
/* 1126 */           object.setName(
/* 1127 */               ConverterUtil.convertToString(content));
/*      */           
/* 1129 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1135 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1139 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1141 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "email")).equals(reader.getName())) {
/*      */           
/* 1143 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1144 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1145 */             throw new ADBException("The element: email  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1149 */           String content = reader.getElementText();
/*      */           
/* 1151 */           object.setEmail(
/* 1152 */               ConverterUtil.convertToString(content));
/*      */           
/* 1154 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1160 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1164 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1166 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "web_site")).equals(reader.getName())) {
/*      */           
/* 1168 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1169 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1170 */             throw new ADBException("The element: web_site  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1174 */           String content = reader.getElementText();
/*      */           
/* 1176 */           object.setWeb_site(
/* 1177 */               ConverterUtil.convertToString(content));
/*      */           
/* 1179 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1185 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1189 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1191 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "wan_ip")).equals(reader.getName())) {
/*      */           
/* 1193 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1194 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1195 */             throw new ADBException("The element: wan_ip  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1199 */           String content = reader.getElementText();
/*      */           
/* 1201 */           object.setWan_ip(
/* 1202 */               ConverterUtil.convertToString(content));
/*      */           
/* 1204 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1210 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1214 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1216 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "lan_ip")).equals(reader.getName())) {
/*      */           
/* 1218 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1219 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1220 */             throw new ADBException("The element: lan_ip  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1224 */           String content = reader.getElementText();
/*      */           
/* 1226 */           object.setLan_ip(
/* 1227 */               ConverterUtil.convertToString(content));
/*      */           
/* 1229 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1235 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1239 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1241 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "port")).equals(reader.getName())) {
/*      */           
/* 1243 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1244 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1245 */             throw new ADBException("The element: port  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1249 */           String content = reader.getElementText();
/*      */           
/* 1251 */           object.setPort(
/* 1252 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1254 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1260 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1264 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1266 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "connect_type")).equals(reader.getName())) {
/*      */           
/* 1268 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1269 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1270 */             throw new ADBException("The element: connect_type  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1274 */           String content = reader.getElementText();
/*      */           
/* 1276 */           object.setConnect_type(
/* 1277 */               ConverterUtil.convertToUnsignedByte(content));
/*      */           
/* 1279 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1285 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1289 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1291 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "net_version")).equals(reader.getName())) {
/*      */           
/* 1293 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1294 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1295 */             throw new ADBException("The element: net_version  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1299 */           String content = reader.getElementText();
/*      */           
/* 1301 */           object.setNet_version(
/* 1302 */               ConverterUtil.convertToUnsignedByte(content));
/*      */           
/* 1304 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1310 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1314 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1316 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "availability")).equals(reader.getName())) {
/*      */           
/* 1318 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1319 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1320 */             throw new ADBException("The element: availability  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1324 */           String content = reader.getElementText();
/*      */           
/* 1326 */           object.setAvailability(
/* 1327 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1329 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1335 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1339 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1341 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "version")).equals(reader.getName())) {
/*      */           
/* 1343 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1344 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1345 */             throw new ADBException("The element: version  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1349 */           String content = reader.getElementText();
/*      */           
/* 1351 */           object.setVersion(
/* 1352 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1354 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1360 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1364 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1366 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "loc_lic")).equals(reader.getName())) {
/*      */           
/* 1368 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1369 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1370 */             throw new ADBException("The element: loc_lic  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1374 */           String content = reader.getElementText();
/*      */           
/* 1376 */           object.setLoc_lic(
/* 1377 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1379 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1385 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1389 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1391 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "rem_lic")).equals(reader.getName())) {
/*      */           
/* 1393 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1394 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1395 */             throw new ADBException("The element: rem_lic  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1399 */           String content = reader.getElementText();
/*      */           
/* 1401 */           object.setRem_lic(
/* 1402 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1404 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1410 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1414 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1416 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "services")).equals(reader.getName())) {
/*      */           
/* 1418 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1419 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1420 */             throw new ADBException("The element: services  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1424 */           String content = reader.getElementText();
/*      */           
/* 1426 */           object.setServices(
/* 1427 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1429 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1435 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */         
/* 1438 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1439 */           reader.next();
/*      */         }
/* 1441 */         if (reader.isStartElement())
/*      */         {
/* 1443 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         
/*      */         }
/*      */       
/*      */       }
/* 1448 */       catch (XMLStreamException e) {
/* 1449 */         throw new Exception(e);
/*      */       } 
/*      */       
/* 1452 */       return object;
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\Contact_info.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */