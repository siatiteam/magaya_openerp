/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetNextTransbyDateResponse implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetNextTransbyDateResponse", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected Api_session_error local_return;
/*     */ 
/*     */   
/*     */   protected String localCookie;
/*     */ 
/*     */   
/*     */   protected String localTrans_list_xml;
/*     */ 
/*     */   
/*     */   protected int localMore_results;
/*     */ 
/*     */ 
/*     */   
/*     */   public Api_session_error get_return() {
/*  41 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(Api_session_error param) {
/*  52 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getCookie() {
/*  71 */     return this.localCookie;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setCookie(String param) {
/*  82 */     this.localCookie = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getTrans_list_xml() {
/* 101 */     return this.localTrans_list_xml;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setTrans_list_xml(String param) {
/* 112 */     this.localTrans_list_xml = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getMore_results() {
/* 131 */     return this.localMore_results;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setMore_results(int param) {
/* 142 */     this.localMore_results = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 162 */     ADBDataSource aDBDataSource = 
/* 163 */       new ADBDataSource(this, MY_QNAME);
/* 164 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 171 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 182 */     String prefix = null;
/* 183 */     String namespace = null;
/*     */ 
/*     */     
/* 186 */     prefix = parentQName.getPrefix();
/* 187 */     namespace = parentQName.getNamespaceURI();
/* 188 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 190 */     if (serializeType) {
/*     */ 
/*     */       
/* 193 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 194 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 195 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 196 */             String.valueOf(namespacePrefix) + ":GetNextTransbyDateResponse", 
/* 197 */             xmlWriter);
/*     */       } else {
/* 199 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 200 */             "GetNextTransbyDateResponse", 
/* 201 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 207 */     if (this.local_return == null) {
/*     */       
/* 209 */       writeStartElement(null, "", "return", xmlWriter);
/*     */ 
/*     */       
/* 212 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/* 213 */       xmlWriter.writeEndElement();
/*     */     } else {
/* 215 */       this.local_return.serialize(new QName("", "return"), 
/* 216 */           xmlWriter);
/*     */     } 
/*     */     
/* 219 */     namespace = "";
/* 220 */     writeStartElement(null, namespace, "cookie", xmlWriter);
/*     */ 
/*     */     
/* 223 */     if (this.localCookie == null) {
/*     */ 
/*     */       
/* 226 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 231 */       xmlWriter.writeCharacters(this.localCookie);
/*     */     } 
/*     */ 
/*     */     
/* 235 */     xmlWriter.writeEndElement();
/*     */     
/* 237 */     namespace = "";
/* 238 */     writeStartElement(null, namespace, "trans_list_xml", xmlWriter);
/*     */ 
/*     */     
/* 241 */     if (this.localTrans_list_xml == null) {
/*     */ 
/*     */       
/* 244 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 249 */       xmlWriter.writeCharacters(this.localTrans_list_xml);
/*     */     } 
/*     */ 
/*     */     
/* 253 */     xmlWriter.writeEndElement();
/*     */     
/* 255 */     namespace = "";
/* 256 */     writeStartElement(null, namespace, "more_results", xmlWriter);
/*     */     
/* 258 */     if (this.localMore_results == Integer.MIN_VALUE) {
/*     */       
/* 260 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 263 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localMore_results));
/*     */     } 
/*     */     
/* 266 */     xmlWriter.writeEndElement();
/*     */     
/* 268 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 274 */     if (namespace.equals("urn:CSSoapService")) {
/* 275 */       return "ns1";
/*     */     }
/* 277 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 285 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 286 */     if (writerPrefix != null) {
/* 287 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 289 */       if (namespace.length() == 0) {
/* 290 */         prefix = "";
/* 291 */       } else if (prefix == null) {
/* 292 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 295 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 296 */       xmlWriter.writeNamespace(prefix, namespace);
/* 297 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 306 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 307 */       xmlWriter.writeNamespace(prefix, namespace);
/* 308 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 310 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 318 */     if (namespace.equals("")) {
/* 319 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 321 */       registerPrefix(xmlWriter, namespace);
/* 322 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 333 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 334 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 335 */     if (attributePrefix == null) {
/* 336 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 339 */     if (attributePrefix.trim().length() > 0) {
/* 340 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 342 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 345 */     if (namespace.equals("")) {
/* 346 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 348 */       registerPrefix(xmlWriter, namespace);
/* 349 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 358 */     String namespaceURI = qname.getNamespaceURI();
/* 359 */     if (namespaceURI != null) {
/* 360 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 361 */       if (prefix == null) {
/* 362 */         prefix = generatePrefix(namespaceURI);
/* 363 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 364 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 367 */       if (prefix.trim().length() > 0) {
/* 368 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 371 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 375 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 382 */     if (qnames != null) {
/*     */ 
/*     */       
/* 385 */       StringBuffer stringToWrite = new StringBuffer();
/* 386 */       String namespaceURI = null;
/* 387 */       String prefix = null;
/*     */       
/* 389 */       for (int i = 0; i < qnames.length; i++) {
/* 390 */         if (i > 0) {
/* 391 */           stringToWrite.append(" ");
/*     */         }
/* 393 */         namespaceURI = qnames[i].getNamespaceURI();
/* 394 */         if (namespaceURI != null) {
/* 395 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 396 */           if (prefix == null || prefix.length() == 0) {
/* 397 */             prefix = generatePrefix(namespaceURI);
/* 398 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 399 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 402 */           if (prefix.trim().length() > 0) {
/* 403 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 405 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 408 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 411 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 421 */     String prefix = xmlWriter.getPrefix(namespace);
/* 422 */     if (prefix == null) {
/* 423 */       prefix = generatePrefix(namespace);
/* 424 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 426 */         String uri = nsContext.getNamespaceURI(prefix);
/* 427 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 430 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 432 */       xmlWriter.writeNamespace(prefix, namespace);
/* 433 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 435 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 449 */     ArrayList<QName> elementList = new ArrayList();
/* 450 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 453 */     elementList.add(new QName("", 
/* 454 */           "return"));
/*     */ 
/*     */     
/* 457 */     elementList.add((this.local_return == null) ? null : 
/* 458 */         new QName(ConverterUtil.convertToString(this.local_return)));
/*     */     
/* 460 */     elementList.add(new QName("", 
/* 461 */           "cookie"));
/*     */     
/* 463 */     elementList.add((this.localCookie == null) ? null : 
/* 464 */         new QName(ConverterUtil.convertToString(this.localCookie)));
/*     */     
/* 466 */     elementList.add(new QName("", 
/* 467 */           "trans_list_xml"));
/*     */     
/* 469 */     elementList.add((this.localTrans_list_xml == null) ? null : 
/* 470 */         new QName(ConverterUtil.convertToString(this.localTrans_list_xml)));
/*     */     
/* 472 */     elementList.add(new QName("", 
/* 473 */           "more_results"));
/*     */     
/* 475 */     elementList.add(
/* 476 */         new QName(ConverterUtil.convertToString(this.localMore_results)));
/*     */ 
/*     */     
/* 479 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetNextTransbyDateResponse parse(XMLStreamReader reader) throws Exception {
/* 503 */       GetNextTransbyDateResponse object = 
/* 504 */         new GetNextTransbyDateResponse();
/*     */ 
/*     */       
/* 507 */       String nillableValue = null;
/* 508 */       String prefix = "";
/* 509 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 512 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 513 */           reader.next();
/*     */         }
/*     */         
/* 516 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 517 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 518 */               "type");
/* 519 */           if (fullTypeName != null) {
/* 520 */             String nsPrefix = null;
/* 521 */             if (fullTypeName.indexOf(":") > -1) {
/* 522 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 524 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 526 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 528 */             if (!"GetNextTransbyDateResponse".equals(type)) {
/*     */               
/* 530 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 531 */               return (GetNextTransbyDateResponse)ExtensionMapper.getTypeObject(
/* 532 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 546 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 551 */         reader.next();
/*     */ 
/*     */         
/* 554 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 556 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
/*     */           
/* 558 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 559 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 560 */             object.set_return(null);
/* 561 */             reader.next();
/*     */             
/* 563 */             reader.next();
/*     */           }
/*     */           else {
/*     */             
/* 567 */             object.set_return(Api_session_error.Factory.parse(reader));
/*     */             
/* 569 */             reader.next();
/*     */           }
/*     */         
/*     */         }
/*     */         else {
/*     */           
/* 575 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 579 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 581 */         if (reader.isStartElement() && (new QName("", "cookie")).equals(reader.getName())) {
/*     */           
/* 583 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 584 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 587 */             String content = reader.getElementText();
/*     */             
/* 589 */             object.setCookie(
/* 590 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 595 */             reader.getElementText();
/*     */           } 
/*     */           
/* 598 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 604 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 608 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 610 */         if (reader.isStartElement() && (new QName("", "trans_list_xml")).equals(reader.getName())) {
/*     */           
/* 612 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 613 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 616 */             String content = reader.getElementText();
/*     */             
/* 618 */             object.setTrans_list_xml(
/* 619 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 624 */             reader.getElementText();
/*     */           } 
/*     */           
/* 627 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 633 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 637 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 639 */         if (reader.isStartElement() && (new QName("", "more_results")).equals(reader.getName())) {
/*     */           
/* 641 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 642 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 645 */             String content = reader.getElementText();
/*     */             
/* 647 */             object.setMore_results(
/* 648 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 653 */             object.setMore_results(-2147483648);
/*     */             
/* 655 */             reader.getElementText();
/*     */           } 
/*     */           
/* 658 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 664 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 667 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 668 */           reader.next();
/*     */         }
/* 670 */         if (reader.isStartElement())
/*     */         {
/* 672 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 677 */       catch (XMLStreamException e) {
/* 678 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 681 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetNextTransbyDateResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */