/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetTransRangeByDateJS implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetTransRangeByDateJS", 
/*  24 */       "ns1");
/*     */   
/*     */   protected int localAccess_key;
/*     */   
/*     */   protected String localType;
/*     */   
/*     */   protected String localStart_date;
/*     */   
/*     */   protected String localEnd_date;
/*     */   
/*     */   protected int localFlags;
/*     */   
/*     */   protected String localFunction;
/*     */   
/*     */   protected String localXml_params;
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/*  71 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/*  82 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getStart_date() {
/* 101 */     return this.localStart_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStart_date(String param) {
/* 112 */     this.localStart_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEnd_date() {
/* 131 */     return this.localEnd_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEnd_date(String param) {
/* 142 */     this.localEnd_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/* 161 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/* 172 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getFunction() {
/* 191 */     return this.localFunction;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFunction(String param) {
/* 202 */     this.localFunction = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getXml_params() {
/* 221 */     return this.localXml_params;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setXml_params(String param) {
/* 232 */     this.localXml_params = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 252 */     ADBDataSource aDBDataSource = 
/* 253 */       new ADBDataSource(this, MY_QNAME);
/* 254 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 261 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 272 */     String prefix = null;
/* 273 */     String namespace = null;
/*     */ 
/*     */     
/* 276 */     prefix = parentQName.getPrefix();
/* 277 */     namespace = parentQName.getNamespaceURI();
/* 278 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 280 */     if (serializeType) {
/*     */ 
/*     */       
/* 283 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 284 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 285 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 286 */             String.valueOf(namespacePrefix) + ":GetTransRangeByDateJS", 
/* 287 */             xmlWriter);
/*     */       } else {
/* 289 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 290 */             "GetTransRangeByDateJS", 
/* 291 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 297 */     namespace = "";
/* 298 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 300 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 302 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 305 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 308 */     xmlWriter.writeEndElement();
/*     */     
/* 310 */     namespace = "";
/* 311 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 314 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 317 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 322 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 326 */     xmlWriter.writeEndElement();
/*     */     
/* 328 */     namespace = "";
/* 329 */     writeStartElement(null, namespace, "start_date", xmlWriter);
/*     */ 
/*     */     
/* 332 */     if (this.localStart_date == null) {
/*     */ 
/*     */       
/* 335 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 340 */       xmlWriter.writeCharacters(this.localStart_date);
/*     */     } 
/*     */ 
/*     */     
/* 344 */     xmlWriter.writeEndElement();
/*     */     
/* 346 */     namespace = "";
/* 347 */     writeStartElement(null, namespace, "end_date", xmlWriter);
/*     */ 
/*     */     
/* 350 */     if (this.localEnd_date == null) {
/*     */ 
/*     */       
/* 353 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 358 */       xmlWriter.writeCharacters(this.localEnd_date);
/*     */     } 
/*     */ 
/*     */     
/* 362 */     xmlWriter.writeEndElement();
/*     */     
/* 364 */     namespace = "";
/* 365 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 367 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 369 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 372 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 375 */     xmlWriter.writeEndElement();
/*     */     
/* 377 */     namespace = "";
/* 378 */     writeStartElement(null, namespace, "function", xmlWriter);
/*     */ 
/*     */     
/* 381 */     if (this.localFunction == null) {
/*     */ 
/*     */       
/* 384 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 389 */       xmlWriter.writeCharacters(this.localFunction);
/*     */     } 
/*     */ 
/*     */     
/* 393 */     xmlWriter.writeEndElement();
/*     */     
/* 395 */     namespace = "";
/* 396 */     writeStartElement(null, namespace, "xml_params", xmlWriter);
/*     */ 
/*     */     
/* 399 */     if (this.localXml_params == null) {
/*     */ 
/*     */       
/* 402 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 407 */       xmlWriter.writeCharacters(this.localXml_params);
/*     */     } 
/*     */ 
/*     */     
/* 411 */     xmlWriter.writeEndElement();
/*     */     
/* 413 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 419 */     if (namespace.equals("urn:CSSoapService")) {
/* 420 */       return "ns1";
/*     */     }
/* 422 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 430 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 431 */     if (writerPrefix != null) {
/* 432 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 434 */       if (namespace.length() == 0) {
/* 435 */         prefix = "";
/* 436 */       } else if (prefix == null) {
/* 437 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 440 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 441 */       xmlWriter.writeNamespace(prefix, namespace);
/* 442 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 451 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 452 */       xmlWriter.writeNamespace(prefix, namespace);
/* 453 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 455 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 463 */     if (namespace.equals("")) {
/* 464 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 466 */       registerPrefix(xmlWriter, namespace);
/* 467 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 478 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 479 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 480 */     if (attributePrefix == null) {
/* 481 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 484 */     if (attributePrefix.trim().length() > 0) {
/* 485 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 487 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 490 */     if (namespace.equals("")) {
/* 491 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 493 */       registerPrefix(xmlWriter, namespace);
/* 494 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 503 */     String namespaceURI = qname.getNamespaceURI();
/* 504 */     if (namespaceURI != null) {
/* 505 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 506 */       if (prefix == null) {
/* 507 */         prefix = generatePrefix(namespaceURI);
/* 508 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 509 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 512 */       if (prefix.trim().length() > 0) {
/* 513 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 516 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 520 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 527 */     if (qnames != null) {
/*     */ 
/*     */       
/* 530 */       StringBuffer stringToWrite = new StringBuffer();
/* 531 */       String namespaceURI = null;
/* 532 */       String prefix = null;
/*     */       
/* 534 */       for (int i = 0; i < qnames.length; i++) {
/* 535 */         if (i > 0) {
/* 536 */           stringToWrite.append(" ");
/*     */         }
/* 538 */         namespaceURI = qnames[i].getNamespaceURI();
/* 539 */         if (namespaceURI != null) {
/* 540 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 541 */           if (prefix == null || prefix.length() == 0) {
/* 542 */             prefix = generatePrefix(namespaceURI);
/* 543 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 544 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 547 */           if (prefix.trim().length() > 0) {
/* 548 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 550 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 553 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 556 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 566 */     String prefix = xmlWriter.getPrefix(namespace);
/* 567 */     if (prefix == null) {
/* 568 */       prefix = generatePrefix(namespace);
/* 569 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 571 */         String uri = nsContext.getNamespaceURI(prefix);
/* 572 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 575 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 577 */       xmlWriter.writeNamespace(prefix, namespace);
/* 578 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 580 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 594 */     ArrayList<QName> elementList = new ArrayList();
/* 595 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 598 */     elementList.add(new QName("", 
/* 599 */           "access_key"));
/*     */     
/* 601 */     elementList.add(
/* 602 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*     */     
/* 604 */     elementList.add(new QName("", 
/* 605 */           "type"));
/*     */     
/* 607 */     elementList.add((this.localType == null) ? null : 
/* 608 */         new QName(ConverterUtil.convertToString(this.localType)));
/*     */     
/* 610 */     elementList.add(new QName("", 
/* 611 */           "start_date"));
/*     */     
/* 613 */     elementList.add((this.localStart_date == null) ? null : 
/* 614 */         new QName(ConverterUtil.convertToString(this.localStart_date)));
/*     */     
/* 616 */     elementList.add(new QName("", 
/* 617 */           "end_date"));
/*     */     
/* 619 */     elementList.add((this.localEnd_date == null) ? null : 
/* 620 */         new QName(ConverterUtil.convertToString(this.localEnd_date)));
/*     */     
/* 622 */     elementList.add(new QName("", 
/* 623 */           "flags"));
/*     */     
/* 625 */     elementList.add(
/* 626 */         new QName(ConverterUtil.convertToString(this.localFlags)));
/*     */     
/* 628 */     elementList.add(new QName("", 
/* 629 */           "function"));
/*     */     
/* 631 */     elementList.add((this.localFunction == null) ? null : 
/* 632 */         new QName(ConverterUtil.convertToString(this.localFunction)));
/*     */     
/* 634 */     elementList.add(new QName("", 
/* 635 */           "xml_params"));
/*     */     
/* 637 */     elementList.add((this.localXml_params == null) ? null : 
/* 638 */         new QName(ConverterUtil.convertToString(this.localXml_params)));
/*     */ 
/*     */     
/* 641 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetTransRangeByDateJS parse(XMLStreamReader reader) throws Exception {
/* 665 */       GetTransRangeByDateJS object = 
/* 666 */         new GetTransRangeByDateJS();
/*     */ 
/*     */       
/* 669 */       String nillableValue = null;
/* 670 */       String prefix = "";
/* 671 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 674 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 675 */           reader.next();
/*     */         }
/*     */         
/* 678 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 679 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 680 */               "type");
/* 681 */           if (fullTypeName != null) {
/* 682 */             String nsPrefix = null;
/* 683 */             if (fullTypeName.indexOf(":") > -1) {
/* 684 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 686 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 688 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 690 */             if (!"GetTransRangeByDateJS".equals(type)) {
/*     */               
/* 692 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 693 */               return (GetTransRangeByDateJS)ExtensionMapper.getTypeObject(
/* 694 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 708 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 713 */         reader.next();
/*     */ 
/*     */         
/* 716 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 718 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 720 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 721 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 724 */             String content = reader.getElementText();
/*     */             
/* 726 */             object.setAccess_key(
/* 727 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 732 */             object.setAccess_key(-2147483648);
/*     */             
/* 734 */             reader.getElementText();
/*     */           } 
/*     */           
/* 737 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 743 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 747 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 749 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 751 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 752 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 755 */             String content = reader.getElementText();
/*     */             
/* 757 */             object.setType(
/* 758 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 763 */             reader.getElementText();
/*     */           } 
/*     */           
/* 766 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 772 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 776 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 778 */         if (reader.isStartElement() && (new QName("", "start_date")).equals(reader.getName())) {
/*     */           
/* 780 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 781 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 784 */             String content = reader.getElementText();
/*     */             
/* 786 */             object.setStart_date(
/* 787 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 792 */             reader.getElementText();
/*     */           } 
/*     */           
/* 795 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 801 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 805 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 807 */         if (reader.isStartElement() && (new QName("", "end_date")).equals(reader.getName())) {
/*     */           
/* 809 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 810 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 813 */             String content = reader.getElementText();
/*     */             
/* 815 */             object.setEnd_date(
/* 816 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 821 */             reader.getElementText();
/*     */           } 
/*     */           
/* 824 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 830 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 834 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 836 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 838 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 839 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 842 */             String content = reader.getElementText();
/*     */             
/* 844 */             object.setFlags(
/* 845 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 850 */             object.setFlags(-2147483648);
/*     */             
/* 852 */             reader.getElementText();
/*     */           } 
/*     */           
/* 855 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 861 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 865 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 867 */         if (reader.isStartElement() && (new QName("", "function")).equals(reader.getName())) {
/*     */           
/* 869 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 870 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 873 */             String content = reader.getElementText();
/*     */             
/* 875 */             object.setFunction(
/* 876 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 881 */             reader.getElementText();
/*     */           } 
/*     */           
/* 884 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 890 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 894 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 896 */         if (reader.isStartElement() && (new QName("", "xml_params")).equals(reader.getName())) {
/*     */           
/* 898 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 899 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 902 */             String content = reader.getElementText();
/*     */             
/* 904 */             object.setXml_params(
/* 905 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 910 */             reader.getElementText();
/*     */           } 
/*     */           
/* 913 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 919 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 922 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 923 */           reader.next();
/*     */         }
/* 925 */         if (reader.isStartElement())
/*     */         {
/* 927 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 932 */       catch (XMLStreamException e) {
/* 933 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 936 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetTransRangeByDateJS.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */