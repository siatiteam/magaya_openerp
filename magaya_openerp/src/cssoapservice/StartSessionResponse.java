/*     */ package cssoapservice;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class StartSessionResponse implements ADBBean {
/*  17 */   public static final QName MY_QNAME = new QName(
/*  18 */       "urn:CSSoapService", "StartSessionResponse", "ns1");
/*     */ 
/*     */ 
/*     */   
/*     */   protected Api_session_error local_return;
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public Api_session_error get_return() {
/*  32 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(Api_session_error param) {
/*  43 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  59 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  70 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  85 */     ADBDataSource aDBDataSource = new ADBDataSource(
/*  86 */         this, MY_QNAME);
/*  87 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  95 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 103 */     String prefix = null;
/* 104 */     String namespace = null;
/*     */     
/* 106 */     prefix = parentQName.getPrefix();
/* 107 */     namespace = parentQName.getNamespaceURI();
/* 108 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), 
/* 109 */         xmlWriter);
/*     */     
/* 111 */     if (serializeType) {
/*     */       
/* 113 */       String namespacePrefix = registerPrefix(xmlWriter, 
/* 114 */           "urn:CSSoapService");
/* 115 */       if (namespacePrefix != null && 
/* 116 */         namespacePrefix.trim().length() > 0) {
/* 117 */         writeAttribute("xsi", 
/* 118 */             "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 119 */             String.valueOf(namespacePrefix) + ":StartSessionResponse", xmlWriter);
/*     */       } else {
/* 121 */         writeAttribute("xsi", 
/* 122 */             "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 123 */             "StartSessionResponse", xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */     
/* 128 */     if (this.local_return == null) {
/*     */       
/* 130 */       writeStartElement(null, "", "return", xmlWriter);
/*     */ 
/*     */       
/* 133 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", 
/* 134 */           "nil", "1", xmlWriter);
/* 135 */       xmlWriter.writeEndElement();
/*     */     } else {
/* 137 */       this.local_return.serialize(new QName("", "return"), 
/* 138 */           xmlWriter);
/*     */     } 
/*     */     
/* 141 */     namespace = "";
/* 142 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 144 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 146 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", 
/* 147 */           "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 150 */       xmlWriter
/* 151 */         .writeCharacters(
/* 152 */           ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 155 */     xmlWriter.writeEndElement();
/*     */     
/* 157 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 162 */     if (namespace.equals("urn:CSSoapService")) {
/* 163 */       return "ns1";
/*     */     }
/* 165 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 175 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 176 */     if (writerPrefix != null) {
/* 177 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 179 */       if (namespace.length() == 0) {
/* 180 */         prefix = "";
/* 181 */       } else if (prefix == null) {
/* 182 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 185 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 186 */       xmlWriter.writeNamespace(prefix, namespace);
/* 187 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 199 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 200 */       xmlWriter.writeNamespace(prefix, namespace);
/* 201 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 203 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 213 */     if (namespace.equals("")) {
/* 214 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 216 */       registerPrefix(xmlWriter, namespace);
/* 217 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 229 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 230 */     String attributePrefix = xmlWriter
/* 231 */       .getPrefix(attributeNamespace);
/* 232 */     if (attributePrefix == null) {
/* 233 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 236 */     if (attributePrefix.trim().length() > 0) {
/* 237 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 239 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 242 */     if (namespace.equals("")) {
/* 243 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 245 */       registerPrefix(xmlWriter, namespace);
/* 246 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 257 */     String namespaceURI = qname.getNamespaceURI();
/* 258 */     if (namespaceURI != null) {
/* 259 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 260 */       if (prefix == null) {
/* 261 */         prefix = generatePrefix(namespaceURI);
/* 262 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 263 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 266 */       if (prefix.trim().length() > 0) {
/* 267 */         xmlWriter.writeCharacters(String.valueOf(prefix) + 
/* 268 */             ":" + 
/*     */             
/* 270 */             ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 273 */         xmlWriter
/* 274 */           .writeCharacters(
/* 275 */             ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 279 */       xmlWriter
/* 280 */         .writeCharacters(
/* 281 */           ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 289 */     if (qnames != null) {
/*     */ 
/*     */ 
/*     */       
/* 293 */       StringBuffer stringToWrite = new StringBuffer();
/* 294 */       String namespaceURI = null;
/* 295 */       String prefix = null;
/*     */       
/* 297 */       for (int i = 0; i < qnames.length; i++) {
/* 298 */         if (i > 0) {
/* 299 */           stringToWrite.append(" ");
/*     */         }
/* 301 */         namespaceURI = qnames[i].getNamespaceURI();
/* 302 */         if (namespaceURI != null) {
/* 303 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 304 */           if (prefix == null || prefix.length() == 0) {
/* 305 */             prefix = generatePrefix(namespaceURI);
/* 306 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 307 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 310 */           if (prefix.trim().length() > 0) {
/* 311 */             stringToWrite
/* 312 */               .append(prefix)
/* 313 */               .append(":")
/* 314 */               .append(
/* 315 */                 ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 317 */             stringToWrite
/* 318 */               .append(
/* 319 */                 ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 322 */           stringToWrite
/* 323 */             .append(
/* 324 */               ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 327 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 339 */     String prefix = xmlWriter.getPrefix(namespace);
/* 340 */     if (prefix == null) {
/* 341 */       prefix = generatePrefix(namespace);
/* 342 */       NamespaceContext nsContext = xmlWriter
/* 343 */         .getNamespaceContext();
/*     */       while (true) {
/* 345 */         String uri = nsContext.getNamespaceURI(prefix);
/* 346 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 349 */         prefix = 
/* 350 */           BeanUtil.getUniquePrefix();
/*     */       } 
/* 352 */       xmlWriter.writeNamespace(prefix, namespace);
/* 353 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 355 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 366 */     ArrayList<QName> elementList = new ArrayList();
/* 367 */     ArrayList attribList = new ArrayList();
/*     */     
/* 369 */     elementList.add(new QName("", "return"));
/*     */     
/* 371 */     elementList.add((this.local_return == null) ? null : new QName(ConverterUtil.convertToString(this.local_return)));
/*     */     
/* 373 */     elementList.add(new QName("", "access_key"));
/*     */     
/* 375 */     elementList.add(
/* 376 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*     */     
/* 378 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(
/* 379 */         qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static StartSessionResponse parse(XMLStreamReader reader) throws Exception {
/* 401 */       StartSessionResponse object = new StartSessionResponse();
/*     */ 
/*     */       
/* 404 */       String nillableValue = null;
/* 405 */       String prefix = "";
/* 406 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 409 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 410 */           reader.next();
/*     */         }
/* 412 */         if (reader.getAttributeValue(
/* 413 */             "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 414 */           String fullTypeName = reader
/* 415 */             .getAttributeValue(
/* 416 */               "http://www.w3.org/2001/XMLSchema-instance", 
/* 417 */               "type");
/* 418 */           if (fullTypeName != null) {
/* 419 */             String nsPrefix = null;
/* 420 */             if (fullTypeName.indexOf(":") > -1) {
/* 421 */               nsPrefix = fullTypeName.substring(0, 
/* 422 */                   fullTypeName.indexOf(":"));
/*     */             }
/* 424 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 426 */             String type = fullTypeName
/* 427 */               .substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 429 */             if (!"StartSessionResponse".equals(type)) {
/*     */               
/* 431 */               String nsUri = reader
/* 432 */                 .getNamespaceContext().getNamespaceURI(
/* 433 */                   nsPrefix);
/* 434 */               return 
/* 435 */                 (StartSessionResponse)ExtensionMapper.getTypeObject(nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 445 */         Vector handledAttributes = new Vector();
/*     */         
/* 447 */         reader.next();
/*     */         
/* 449 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 450 */           reader.next();
/*     */         }
/* 452 */         if (reader.isStartElement() && (
/* 453 */           new QName("", "return"))
/* 454 */           .equals(reader.getName())) {
/*     */           
/* 456 */           nillableValue = reader.getAttributeValue(
/* 457 */               "http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 458 */           if ("true".equals(nillableValue) || 
/* 459 */             "1".equals(nillableValue)) {
/* 460 */             object.set_return(null);
/* 461 */             reader.next();
/*     */             
/* 463 */             reader.next();
/*     */           }
/*     */           else {
/*     */             
/* 467 */             object.set_return(
/* 468 */                 Api_session_error.Factory.parse(reader));
/*     */             
/* 470 */             reader.next();
/*     */           
/*     */           }
/*     */         
/*     */         }
/*     */         else {
/*     */           
/* 477 */           throw new ADBException(
/* 478 */               "Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 481 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 482 */           reader.next();
/*     */         }
/* 484 */         if (reader.isStartElement() && (
/* 485 */           new QName("", "access_key"))
/* 486 */           .equals(reader.getName())) {
/*     */           
/* 488 */           nillableValue = reader.getAttributeValue(
/* 489 */               "http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 490 */           if (!"true".equals(nillableValue) && 
/* 491 */             !"1".equals(nillableValue)) {
/*     */             
/* 493 */             String content = reader.getElementText();
/*     */             
/* 495 */             object.setAccess_key(
/* 496 */                 ConverterUtil.convertToInt(content));
/*     */           }
/*     */           else {
/*     */             
/* 500 */             object.setAccess_key(-2147483648);
/*     */             
/* 502 */             reader.getElementText();
/*     */           } 
/*     */ 
/*     */           
/* 506 */           reader.next();
/*     */ 
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 513 */           throw new ADBException(
/* 514 */               "Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 517 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 518 */           reader.next();
/*     */         }
/* 520 */         if (reader.isStartElement())
/*     */         {
/*     */           
/* 523 */           throw new ADBException(
/* 524 */               "Unexpected subelement " + reader.getName());
/*     */         }
/* 526 */       } catch (XMLStreamException e) {
/* 527 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 530 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\StartSessionResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */