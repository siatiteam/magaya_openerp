/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetEntitiesOfType implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetEntitiesOfType", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */   
/*     */   protected int localFlags;
/*     */ 
/*     */   
/*     */   protected String localStart_with;
/*     */ 
/*     */   
/*     */   protected int localEntity_type;
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/*  71 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/*  82 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getStart_with() {
/* 101 */     return this.localStart_with;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStart_with(String param) {
/* 112 */     this.localStart_with = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getEntity_type() {
/* 131 */     return this.localEntity_type;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEntity_type(int param) {
/* 142 */     this.localEntity_type = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 162 */     ADBDataSource aDBDataSource = 
/* 163 */       new ADBDataSource(this, MY_QNAME);
/* 164 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 171 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 182 */     String prefix = null;
/* 183 */     String namespace = null;
/*     */ 
/*     */     
/* 186 */     prefix = parentQName.getPrefix();
/* 187 */     namespace = parentQName.getNamespaceURI();
/* 188 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 190 */     if (serializeType) {
/*     */ 
/*     */       
/* 193 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 194 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 195 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 196 */             String.valueOf(namespacePrefix) + ":GetEntitiesOfType", 
/* 197 */             xmlWriter);
/*     */       } else {
/* 199 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 200 */             "GetEntitiesOfType", 
/* 201 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 207 */     namespace = "";
/* 208 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 210 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 212 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 215 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 218 */     xmlWriter.writeEndElement();
/*     */     
/* 220 */     namespace = "";
/* 221 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 223 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 225 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 228 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 231 */     xmlWriter.writeEndElement();
/*     */     
/* 233 */     namespace = "";
/* 234 */     writeStartElement(null, namespace, "start_with", xmlWriter);
/*     */ 
/*     */     
/* 237 */     if (this.localStart_with == null) {
/*     */ 
/*     */       
/* 240 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 245 */       xmlWriter.writeCharacters(this.localStart_with);
/*     */     } 
/*     */ 
/*     */     
/* 249 */     xmlWriter.writeEndElement();
/*     */     
/* 251 */     namespace = "";
/* 252 */     writeStartElement(null, namespace, "entity_type", xmlWriter);
/*     */     
/* 254 */     if (this.localEntity_type == Integer.MIN_VALUE) {
/*     */       
/* 256 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 259 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localEntity_type));
/*     */     } 
/*     */     
/* 262 */     xmlWriter.writeEndElement();
/*     */     
/* 264 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 270 */     if (namespace.equals("urn:CSSoapService")) {
/* 271 */       return "ns1";
/*     */     }
/* 273 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 281 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 282 */     if (writerPrefix != null) {
/* 283 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 285 */       if (namespace.length() == 0) {
/* 286 */         prefix = "";
/* 287 */       } else if (prefix == null) {
/* 288 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 291 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 292 */       xmlWriter.writeNamespace(prefix, namespace);
/* 293 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 302 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 303 */       xmlWriter.writeNamespace(prefix, namespace);
/* 304 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 306 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 314 */     if (namespace.equals("")) {
/* 315 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 317 */       registerPrefix(xmlWriter, namespace);
/* 318 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 329 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 330 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 331 */     if (attributePrefix == null) {
/* 332 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 335 */     if (attributePrefix.trim().length() > 0) {
/* 336 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 338 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 341 */     if (namespace.equals("")) {
/* 342 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 344 */       registerPrefix(xmlWriter, namespace);
/* 345 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 354 */     String namespaceURI = qname.getNamespaceURI();
/* 355 */     if (namespaceURI != null) {
/* 356 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 357 */       if (prefix == null) {
/* 358 */         prefix = generatePrefix(namespaceURI);
/* 359 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 360 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 363 */       if (prefix.trim().length() > 0) {
/* 364 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 367 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 371 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 378 */     if (qnames != null) {
/*     */ 
/*     */       
/* 381 */       StringBuffer stringToWrite = new StringBuffer();
/* 382 */       String namespaceURI = null;
/* 383 */       String prefix = null;
/*     */       
/* 385 */       for (int i = 0; i < qnames.length; i++) {
/* 386 */         if (i > 0) {
/* 387 */           stringToWrite.append(" ");
/*     */         }
/* 389 */         namespaceURI = qnames[i].getNamespaceURI();
/* 390 */         if (namespaceURI != null) {
/* 391 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 392 */           if (prefix == null || prefix.length() == 0) {
/* 393 */             prefix = generatePrefix(namespaceURI);
/* 394 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 395 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 398 */           if (prefix.trim().length() > 0) {
/* 399 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 401 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 404 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 407 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 417 */     String prefix = xmlWriter.getPrefix(namespace);
/* 418 */     if (prefix == null) {
/* 419 */       prefix = generatePrefix(namespace);
/* 420 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 422 */         String uri = nsContext.getNamespaceURI(prefix);
/* 423 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 426 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 428 */       xmlWriter.writeNamespace(prefix, namespace);
/* 429 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 431 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 445 */     ArrayList<QName> elementList = new ArrayList();
/* 446 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 449 */     elementList.add(new QName("", 
/* 450 */           "access_key"));
/*     */     
/* 452 */     elementList.add(
/* 453 */         new QName(ConverterUtil.convertToString(this.localAccess_key)));
/*     */     
/* 455 */     elementList.add(new QName("", 
/* 456 */           "flags"));
/*     */     
/* 458 */     elementList.add(
/* 459 */         new QName(ConverterUtil.convertToString(this.localFlags)));
/*     */     
/* 461 */     elementList.add(new QName("", 
/* 462 */           "start_with"));
/*     */     
/* 464 */     elementList.add((this.localStart_with == null) ? null : 
/* 465 */         new QName(ConverterUtil.convertToString(this.localStart_with)));
/*     */     
/* 467 */     elementList.add(new QName("", 
/* 468 */           "entity_type"));
/*     */     
/* 470 */     elementList.add(
/* 471 */         new QName(ConverterUtil.convertToString(this.localEntity_type)));
/*     */ 
/*     */     
/* 474 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetEntitiesOfType parse(XMLStreamReader reader) throws Exception {
/* 498 */       GetEntitiesOfType object = 
/* 499 */         new GetEntitiesOfType();
/*     */ 
/*     */       
/* 502 */       String nillableValue = null;
/* 503 */       String prefix = "";
/* 504 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 507 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 508 */           reader.next();
/*     */         }
/*     */         
/* 511 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 512 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 513 */               "type");
/* 514 */           if (fullTypeName != null) {
/* 515 */             String nsPrefix = null;
/* 516 */             if (fullTypeName.indexOf(":") > -1) {
/* 517 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 519 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 521 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 523 */             if (!"GetEntitiesOfType".equals(type)) {
/*     */               
/* 525 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 526 */               return (GetEntitiesOfType)ExtensionMapper.getTypeObject(
/* 527 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 541 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 546 */         reader.next();
/*     */ 
/*     */         
/* 549 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 551 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 553 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 554 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 557 */             String content = reader.getElementText();
/*     */             
/* 559 */             object.setAccess_key(
/* 560 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 565 */             object.setAccess_key(-2147483648);
/*     */             
/* 567 */             reader.getElementText();
/*     */           } 
/*     */           
/* 570 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 576 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 580 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 582 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 584 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 585 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 588 */             String content = reader.getElementText();
/*     */             
/* 590 */             object.setFlags(
/* 591 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 596 */             object.setFlags(-2147483648);
/*     */             
/* 598 */             reader.getElementText();
/*     */           } 
/*     */           
/* 601 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 607 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 611 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 613 */         if (reader.isStartElement() && (new QName("", "start_with")).equals(reader.getName())) {
/*     */           
/* 615 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 616 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 619 */             String content = reader.getElementText();
/*     */             
/* 621 */             object.setStart_with(
/* 622 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 627 */             reader.getElementText();
/*     */           } 
/*     */           
/* 630 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 636 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 640 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 642 */         if (reader.isStartElement() && (new QName("", "entity_type")).equals(reader.getName())) {
/*     */           
/* 644 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 645 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 648 */             String content = reader.getElementText();
/*     */             
/* 650 */             object.setEntity_type(
/* 651 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 656 */             object.setEntity_type(-2147483648);
/*     */             
/* 658 */             reader.getElementText();
/*     */           } 
/*     */           
/* 661 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 667 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 670 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 671 */           reader.next();
/*     */         }
/* 673 */         if (reader.isStartElement())
/*     */         {
/* 675 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 680 */       catch (XMLStreamException e) {
/* 681 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 684 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetEntitiesOfType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */