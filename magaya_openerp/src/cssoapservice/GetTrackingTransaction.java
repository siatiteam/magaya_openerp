/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetTrackingTransaction implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetTrackingTransaction", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localApp;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localNumber;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getApp() {
/*  41 */     return this.localApp;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setApp(String param) {
/*  52 */     this.localApp = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNumber() {
/*  71 */     return this.localNumber;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNumber(String param) {
/*  82 */     this.localNumber = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 102 */     ADBDataSource aDBDataSource = 
/* 103 */       new ADBDataSource(this, MY_QNAME);
/* 104 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 111 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 122 */     String prefix = null;
/* 123 */     String namespace = null;
/*     */ 
/*     */     
/* 126 */     prefix = parentQName.getPrefix();
/* 127 */     namespace = parentQName.getNamespaceURI();
/* 128 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 130 */     if (serializeType) {
/*     */ 
/*     */       
/* 133 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 134 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 135 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 136 */             String.valueOf(namespacePrefix) + ":GetTrackingTransaction", 
/* 137 */             xmlWriter);
/*     */       } else {
/* 139 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 140 */             "GetTrackingTransaction", 
/* 141 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 147 */     namespace = "";
/* 148 */     writeStartElement(null, namespace, "app", xmlWriter);
/*     */ 
/*     */     
/* 151 */     if (this.localApp == null) {
/*     */ 
/*     */       
/* 154 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 159 */       xmlWriter.writeCharacters(this.localApp);
/*     */     } 
/*     */ 
/*     */     
/* 163 */     xmlWriter.writeEndElement();
/*     */     
/* 165 */     namespace = "";
/* 166 */     writeStartElement(null, namespace, "number", xmlWriter);
/*     */ 
/*     */     
/* 169 */     if (this.localNumber == null) {
/*     */ 
/*     */       
/* 172 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 177 */       xmlWriter.writeCharacters(this.localNumber);
/*     */     } 
/*     */ 
/*     */     
/* 181 */     xmlWriter.writeEndElement();
/*     */     
/* 183 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 189 */     if (namespace.equals("urn:CSSoapService")) {
/* 190 */       return "ns1";
/*     */     }
/* 192 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 200 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 201 */     if (writerPrefix != null) {
/* 202 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 204 */       if (namespace.length() == 0) {
/* 205 */         prefix = "";
/* 206 */       } else if (prefix == null) {
/* 207 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 210 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 211 */       xmlWriter.writeNamespace(prefix, namespace);
/* 212 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 221 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 222 */       xmlWriter.writeNamespace(prefix, namespace);
/* 223 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 225 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 233 */     if (namespace.equals("")) {
/* 234 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 236 */       registerPrefix(xmlWriter, namespace);
/* 237 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 248 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 249 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 250 */     if (attributePrefix == null) {
/* 251 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 254 */     if (attributePrefix.trim().length() > 0) {
/* 255 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 257 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 260 */     if (namespace.equals("")) {
/* 261 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 263 */       registerPrefix(xmlWriter, namespace);
/* 264 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 273 */     String namespaceURI = qname.getNamespaceURI();
/* 274 */     if (namespaceURI != null) {
/* 275 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 276 */       if (prefix == null) {
/* 277 */         prefix = generatePrefix(namespaceURI);
/* 278 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 279 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 282 */       if (prefix.trim().length() > 0) {
/* 283 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 286 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 290 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 297 */     if (qnames != null) {
/*     */ 
/*     */       
/* 300 */       StringBuffer stringToWrite = new StringBuffer();
/* 301 */       String namespaceURI = null;
/* 302 */       String prefix = null;
/*     */       
/* 304 */       for (int i = 0; i < qnames.length; i++) {
/* 305 */         if (i > 0) {
/* 306 */           stringToWrite.append(" ");
/*     */         }
/* 308 */         namespaceURI = qnames[i].getNamespaceURI();
/* 309 */         if (namespaceURI != null) {
/* 310 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 311 */           if (prefix == null || prefix.length() == 0) {
/* 312 */             prefix = generatePrefix(namespaceURI);
/* 313 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 314 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 317 */           if (prefix.trim().length() > 0) {
/* 318 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 320 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 323 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 326 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 336 */     String prefix = xmlWriter.getPrefix(namespace);
/* 337 */     if (prefix == null) {
/* 338 */       prefix = generatePrefix(namespace);
/* 339 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 341 */         String uri = nsContext.getNamespaceURI(prefix);
/* 342 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 345 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 347 */       xmlWriter.writeNamespace(prefix, namespace);
/* 348 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 350 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 364 */     ArrayList<QName> elementList = new ArrayList();
/* 365 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 368 */     elementList.add(new QName("", 
/* 369 */           "app"));
/*     */     
/* 371 */     elementList.add((this.localApp == null) ? null : 
/* 372 */         new QName(ConverterUtil.convertToString(this.localApp)));
/*     */     
/* 374 */     elementList.add(new QName("", 
/* 375 */           "number"));
/*     */     
/* 377 */     elementList.add((this.localNumber == null) ? null : 
/* 378 */         new QName(ConverterUtil.convertToString(this.localNumber)));
/*     */ 
/*     */     
/* 381 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetTrackingTransaction parse(XMLStreamReader reader) throws Exception {
/* 405 */       GetTrackingTransaction object = 
/* 406 */         new GetTrackingTransaction();
/*     */ 
/*     */       
/* 409 */       String nillableValue = null;
/* 410 */       String prefix = "";
/* 411 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 414 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 415 */           reader.next();
/*     */         }
/*     */         
/* 418 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 419 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 420 */               "type");
/* 421 */           if (fullTypeName != null) {
/* 422 */             String nsPrefix = null;
/* 423 */             if (fullTypeName.indexOf(":") > -1) {
/* 424 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 426 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 428 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 430 */             if (!"GetTrackingTransaction".equals(type)) {
/*     */               
/* 432 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 433 */               return (GetTrackingTransaction)ExtensionMapper.getTypeObject(
/* 434 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 448 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 453 */         reader.next();
/*     */ 
/*     */         
/* 456 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 458 */         if (reader.isStartElement() && (new QName("", "app")).equals(reader.getName())) {
/*     */           
/* 460 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 461 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 464 */             String content = reader.getElementText();
/*     */             
/* 466 */             object.setApp(
/* 467 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 472 */             reader.getElementText();
/*     */           } 
/*     */           
/* 475 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 481 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 485 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 487 */         if (reader.isStartElement() && (new QName("", "number")).equals(reader.getName())) {
/*     */           
/* 489 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 490 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 493 */             String content = reader.getElementText();
/*     */             
/* 495 */             object.setNumber(
/* 496 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 501 */             reader.getElementText();
/*     */           } 
/*     */           
/* 504 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 510 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 513 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 514 */           reader.next();
/*     */         }
/* 516 */         if (reader.isStartElement())
/*     */         {
/* 518 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 523 */       catch (XMLStreamException e) {
/* 524 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 527 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetTrackingTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */