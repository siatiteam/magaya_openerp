package com.siatigroup;

import com.siatigroup.utils.RuntimeConfig;
import com.siatigroup.utils.XmlRepository;
import com.siatigroup.utils.TransactionType;
import com.siatigroup.utils.MagayaWS;
import com.siatigroup.utils.OpenerpClient;
import com.csvreader.CsvReader;
import com.siatigroup.utils.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.xmlrpc.XmlRpcException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class SincronizadorWRMagayaToOERP {

    private final SimpleDateFormat formaterFechaHoraSys = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private final SimpleDateFormat formaterFecha = new SimpleDateFormat("yyyy-MM-dd");

    static OpenerpClient openerpClient = new OpenerpClient();
    static MagayaWS magayaWS = new MagayaWS();
    HashMap<String, String> transTypeDateFieldName = null;
    private SimpleDateFormat formaterUtc;

    public SincronizadorWRMagayaToOERP() {
        this.transTypeDateFieldName = new HashMap<String, String>();
        this.transTypeDateFieldName.put("WH", "created_on");
        this.transTypeDateFieldName.put("SH", "created_on");

        this.formaterUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.formaterUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public Boolean testOpenerpConection() {
        Boolean loginOk = false;
        OpenerpClient openerpClient = new OpenerpClient();
        MagayaWS magayaWS = new MagayaWS();

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        try {
            props.load(new FileInputStream("operp_db.conf"));

            db = props.getProperty("db");
            user = props.getProperty("user");
            pass = props.getProperty("pass");
            host = props.getProperty("host");
            port = props.getProperty("port");
            Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

            openerpClient.login(db, user, pass, host, port);
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();

            loginOk = magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
            magayaWS.logout();
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException | XmlRpcException e) {
            return false;
        }
        return loginOk;
    }

    public Object marcarParaCrearTransaccion(TransactionType transType, OpenerpClient openerpClient, String guid, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
        HashMap<Object, Object> values = new HashMap<>();

        values.put("guid", guid);

        values.put("deleted", Boolean.FALSE);
        values.put("update_required", 100);

        if (desdeFechaHora != null) {
            values.put("last_query_log_from", this.formaterUtc.format(desdeFechaHora));
        }
        if (hastaFechaHora != null) {
            values.put("last_query_log_to", this.formaterUtc.format(hastaFechaHora));
        }
        switch (transType) {
            case WarehouseReceipt:
                values.put("state", "draft");

                return openerpClient.crearTransaccionWR(values, null, null, null, null);
            case Shipment:
                return openerpClient.crearTransaccionSH(values, null, null, null);
            case CargoRelease:
                values.put("state", "draft");
                return openerpClient.crearTransaccionCR(values, null, null);
        }
        return null;
    }

    private void marcarParaEliminarTransaccion(TransactionType transType, Element nodoWR, OpenerpClient openerpClient, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
        HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();
        paresNombreValor.put("update_required", 0);
        paresNombreValor.put("deleted", Boolean.TRUE);

        if (desdeFechaHora != null) {
            paresNombreValor.put("last_query_log_from", this.formaterUtc.format(desdeFechaHora));
        }
        if (hastaFechaHora != null) {
            paresNombreValor.put("last_query_log_to", this.formaterUtc.format(hastaFechaHora));
        }
        Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        Vector<Object> guidFilter = new Vector();
        guidFilter.add("guid");
        guidFilter.add("=");
        guidFilter.add(nodoWR.getChildText("GUID"));
        vectorOfVectorFilterTuples.add(guidFilter);
        openerpClient.write(transType.getOpenerpModel(), openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples)[0], paresNombreValor);
    }

    private void marcarParaActualizarTransaccion(TransactionType transType, Element nodoWR, OpenerpClient openerpClient, Integer prioridad, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
        marcarParaActualizarTransaccion(transType, nodoWR.getChildText("GUID"), openerpClient, prioridad, desdeFechaHora, hastaFechaHora);
    }

    private Object marcarParaActualizarTransaccion(TransactionType transType, String guid, OpenerpClient openerpClient, Integer prioridad, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
        HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();

        paresNombreValor.put("update_required", prioridad);

        if (desdeFechaHora != null) {
            paresNombreValor.put("last_query_log_from", this.formaterUtc.format(desdeFechaHora));
        }
        if (hastaFechaHora != null) {
            paresNombreValor.put("last_query_log_to", this.formaterUtc.format(hastaFechaHora));
        }
        Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        Vector<Object> guidFilter = new Vector();
        guidFilter.add("guid");
        guidFilter.add("=");
        guidFilter.add(guid);

        vectorOfVectorFilterTuples.add(guidFilter);

        Object id = openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples)[0];
        openerpClient.write(transType.getOpenerpModel(), id, paresNombreValor);

        return id;
    }

    private void desmarcarParaActualizarTransaccion(TransactionType transType, String guid, OpenerpClient openerpClient) throws XmlRpcException {
        HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();
        paresNombreValor.put("update_required", Integer.valueOf(-20));

        Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        Vector<Object> guidFilter = new Vector();
        guidFilter.add("guid");
        guidFilter.add("=");
        guidFilter.add(guid);

        vectorOfVectorFilterTuples.add(guidFilter);

        openerpClient.write(transType.getOpenerpModel(), openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples)[0], paresNombreValor);
    }

    public void actualizar(TransactionType transType, Object OpenerpTransId, Element nodoTrans, OpenerpClient openerpClient) throws XmlRpcException, JDOMException, IOException {
        actualizar(transType, OpenerpTransId, nodoTrans, openerpClient, true);
    }

    public void actualizar(TransactionType transType, Object OpenerpTransId, Element nodoTrans, OpenerpClient openerpClient, boolean guardarXml) throws XmlRpcException, JDOMException, IOException {
        Vector wrItemsGuids, wrItemIds;
        HashMap<Object, Object> values = null;
        Object parentId = null;
        switch (transType) {
            case WarehouseReceipt:
                values = getWRValues(nodoTrans);
                break;
            case Shipment:
                values = getSHValues(nodoTrans);
                if (nodoTrans.getChild("MasterGUID") != null) {
                    Vector<String> filter = new Vector();
                    filter.addElement("guid");
                    filter.addElement("=");
                    filter.addElement(nodoTrans.getChildText("MasterGUID"));
                    Vector<Vector<String>> filters = new Vector();
                    filters.add(filter);
                    Object[] ids = openerpClient.search(transType.getOpenerpModel(), (Vector) filters);
                    if (ids.length != 0) {
                        parentId = ids[0];
                        Logger.getLogger(getClass().getSimpleName()).info("Seteando parent: " + nodoTrans.getChildText("MasterGUID"));
                    }
                }
                break;
            case CargoRelease:
                values = getCRValues(nodoTrans);
                break;

            default:
                return;
        }
        values.put("guid", nodoTrans.getAttributeValue("GUID"));

        values.put("deleted", Boolean.FALSE);
        values.put("update_required", Integer.valueOf(0));

        HashMap<Object, Object> hashShipperAddress = null;
        if (nodoTrans.getChild("ShipperAddress") != null) {
            hashShipperAddress = getAddress(nodoTrans.getChild("ShipperAddress"));
        }
        HashMap<Object, Object> hashConsigneeAddress = null;
        if (nodoTrans.getChild("ConsigneeAddress") != null) {
            hashConsigneeAddress = getAddress(nodoTrans.getChild("ConsigneeAddress"));
        }
        HashMap<Object, Object> hashReleaseToAddress = null;
        if (nodoTrans.getChild("ReleasedToAddress") != null) {
            hashReleaseToAddress = getAddress(nodoTrans.getChild("ReleasedToAddress"));
        }
        ArrayList<HashMap<Object, Object>> listaItems = new ArrayList<HashMap<Object, Object>>();
        if (transType == TransactionType.WarehouseReceipt) {
            if (nodoTrans.getChild("Items") != null) {
                listaItems = getListaItems(nodoTrans.getChild("Items"));
            }
            values.put("atlas_tracking", getAtlasTrackingContent(values, listaItems));
        }
        XmlRepository.getInstance().saveUltimoXmlPara(transType, nodoTrans.getAttributeValue("GUID"), nodoTrans);

        ArrayList<HashMap<String, String>> attachments = downloadAttachmentsFor(nodoTrans);

        Vector wrGuids = new Vector();
        switch (transType) {

            case WarehouseReceipt:
                openerpClient.actualizarTransaccionWR(OpenerpTransId, values, hashShipperAddress, hashConsigneeAddress, listaItems, attachments);
                break;

            case Shipment:
                if (nodoTrans.getChild("Items") != null) {

                    verificarExistenciaYCrearWrsFaltantes(nodoTrans.getChild("Items"), openerpClient, TransactionType.Shipment, OpenerpTransId);
                    wrGuids = getListaGuidsWRs(nodoTrans.getChild("Items"));
                }
                openerpClient.actualizarTransaccionSH(OpenerpTransId, parentId, values, hashShipperAddress, hashConsigneeAddress, wrGuids);
                break;
            case CargoRelease:
                wrItemsGuids = new Vector();
                wrItemIds = new Vector();
                if (nodoTrans.getChild("Items") != null) {
                    verificarExistenciaYCrearWrsFaltantes(nodoTrans.getChild("Items"), openerpClient, TransactionType.Shipment, OpenerpTransId);

                    wrGuids = getListaGuidsWRs(nodoTrans.getChild("Items"));
                }
                openerpClient.actualizarTransaccionCR(OpenerpTransId, values, hashReleaseToAddress, wrGuids, attachments);
                break;
        }
    }

    private Vector getWrItemIds(Vector wrItemsGuids) throws XmlRpcException {
        Vector<Object> filter = new Vector();
        filter.addElement("guid");
        filter.addElement("in");
        filter.addElement(wrItemsGuids);
        Vector<Vector<Object>> filters = new Vector();
        filters.add(filter);
        Object[] wrItensIds = openerpClient.search(TransactionType.WarehouseItem.getOpenerpModel(), (Vector) filters);
        Vector<Object> res = new Vector();
        for (int i = 0; i < wrItensIds.length; i++) {
            res.add(wrItensIds[i]);
        }
        return res;
    }

    private Vector getListaGuidsWRItems(Element items) {
        Vector<String> listaItems = new Vector();

        List<Element> nodosItems = items.getChildren("Item");
        for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext();) {
            Element nodoItem = iterator.next();
            if (nodoItem.getAttributeValue("GUID") != null) {
                listaItems.add(nodoItem.getAttributeValue("GUID"));
            }
        }
        return listaItems;
    }

    private HashMap<Object, Object> getCRValues(Element nodoCR) {
        DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");

        HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("guid", nodoCR.getAttributeValue("GUID"));

        try {
            values.put("name", Integer.valueOf((nodoCR.getChildText("Number") != null) ? Integer.valueOf(nodoCR.getChildText("Number")).intValue() : 0));
        } catch (NumberFormatException e1) {

            values.put("name", Integer.valueOf(0));
        }

        try {
            values.put("created_on", (nodoCR.getChildText("CreatedOn") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(Utils.replaceCharAt(nodoCR.getChildText("CreatedOn"), nodoCR.getChildText("CreatedOn").lastIndexOf(":"), ""))) : "");
        } catch (ParseException e) {
            Logger.getLogger(getClass().getSimpleName()).error("El numero de transaccion no es un numero, seteandolo a 0", e);
            values.put("created_on", Integer.valueOf(0));
        }

        try {
            values.put("release_date", (nodoCR.getChildText("ReleaseDate") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(Utils.replaceCharAt(nodoCR.getChildText("ReleaseDate"), nodoCR.getChildText("ReleaseDate").lastIndexOf(":"), ""))) : "");
        } catch (ParseException e) {
            Logger.getLogger(getClass().getSimpleName()).error("El numero de transaccion no es un numero, seteandolo a 0", e);
            values.put("release_date", Integer.valueOf(0));
        }

        values.put("issued_by", (nodoCR.getChildText("IssuedByName") != null) ? nodoCR.getChildText("IssuedByName") : "");
        values.put("release_by", (nodoCR.getChildText("CreatedByName") != null) ? nodoCR.getChildText("CreatedByName") : "");
        values.put("status", (nodoCR.getChildText("Status") != null) ? nodoCR.getChildText("Status") : "");
        values.put("carrier_name", (nodoCR.getChildText("CarrierName") != null) ? nodoCR.getChildText("CarrierName") : "");
        values.put("carrier_tracking_number", (nodoCR.getChildText("CarrierTrackingNumber") != null) ? nodoCR.getChildText("CarrierTrackingNumber") : "");
        values.put("release_to", (nodoCR.getChildText("ReleasedToName") != null) ? nodoCR.getChildText("ReleasedToName") : "");
        values.put("driver_name", (nodoCR.getChildText("DriverName") != null) ? nodoCR.getChildText("DriverName") : "");
        values.put("driver_license_number", (nodoCR.getChildText("DriverLicenseNumber") != null) ? nodoCR.getChildText("DriverLicenseNumber") : "");
        values.put("notes", (nodoCR.getChildText("Notes") != null) ? nodoCR.getChildText("Notes") : "");
        values.put("state", "draft");

        if (nodoCR.getChildText("TotalValue") != null) {
            values.put("aplicable_charges", nodoCR.getChildText("TotalValue"));
            values.put("currency", nodoCR.getChild("TotalValue").getAttributeValue("Currency"));
        }

        values.put("total_pieces", (nodoCR.getChildText("TotalPieces") != null) ? nodoCR.getChildText("TotalPieces") : "");

        if (nodoCR.getChildText("TotalWeight") != null) {
            values.put("total_weight", nodoCR.getChildText("TotalWeight"));
            values.put("total_weight_unit", nodoCR.getChild("TotalWeight").getAttributeValue("Unit"));
        }
        if (nodoCR.getChildText("TotalVolume") != null) {
            values.put("total_volume", nodoCR.getChildText("TotalVolume"));
            values.put("total_volume_unit", nodoCR.getChild("TotalVolume").getAttributeValue("Unit"));
        }
        if (nodoCR.getChildText("TotalVolumeWeight") != null) {
            values.put("total_volume_weight", nodoCR.getChildText("TotalVolumeWeight"));
            values.put("total_volume_weight_unit", nodoCR.getChild("TotalVolumeWeight").getAttributeValue("Unit"));
        }

        return values;
    }

    private ArrayList<HashMap<String, String>> downloadAttachmentsFor(Element nodoTrans) {
        ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();

        if (nodoTrans.getChild("Attachments") != null && nodoTrans.getChild("Attachments").getChild("Attachment") != null) {
            List<Element> attachmentElements = nodoTrans.getChild("Attachments").getChildren("Attachment");
            for (Element element : attachmentElements) {
                try {
                    String name = element.getChildText("Name");
                    String extension = element.getChildText("Extension");
                    String isImage = element.getChildText("IsImage");

                    String ownerGUID = element.getChildText("OwnerGUID");
                    String ownerType = element.getChildText("OwnerType");
                    String identifier = element.getChildText("Identifier");

                    String attachXml = magayaWS.getAttachment(ownerType, ownerGUID, identifier);

                    SAXBuilder builder = new SAXBuilder();

                    attachXml = attachXml.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
                    Logger.getLogger("SincronizadorWRMagayaToOERP").info("Parseando respuesta...");
                    Reader in = new StringReader(attachXml);
                    Document document = builder.build(in);
                    Element nodoattach = document.getRootElement();

                    String data = nodoattach.getChildText("Data");

                    HashMap<String, String> attach = new HashMap<String, String>();
                    attach.put("name", name);
                    attach.put("extension", extension);
                    attach.put("isImage", isImage);
                    attach.put("data", data);

                    res.add(attach);
                } catch (JDOMException e) {

                } catch (IOException e) {

                }
            }
        }

        return res;
    }

    private ArrayList<HashMap<String, String>> getAttachments(Element nodoTrans) {
        ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();

        if (nodoTrans.getChild("Attachments") != null && nodoTrans.getChild("Attachments").getChild("Attachment") != null) {
            List<Element> attachmentElements = nodoTrans.getChild("Attachments").getChildren("Attachment");
            for (Element element : attachmentElements) {
                String name = element.getChildText("Name");
                String extension = element.getChildText("Extension");
                String isImage = element.getChildText("IsImage");
                String data = element.getChildText("Data");

                HashMap<String, String> attach = new HashMap<String, String>();
                attach.put("name", name);
                attach.put("extension", extension);
                attach.put("isImage", isImage);
                attach.put("data", data);

                res.add(attach);
            }
        }

        return res;
    }

    private String getAtlasTrackingContent(HashMap<Object, Object> values, ArrayList<HashMap<Object, Object>> listaItems) {
        String atlas_tracking = (values.get("carrier_tracking_number") != "") ? (new StringBuilder()).append(values.get("carrier_tracking_number")).append("\n").append(values.get("notes")).toString() : (String) values.get("notes");

        TreeMap<Integer, HashMap<Object, Object>> tree = new TreeMap<Integer, HashMap<Object, Object>>();
        for (int i = 0; i < listaItems.size(); i++) {
            tree.put(Integer.valueOf(((HashMap) listaItems.get(i)).get("name").toString()), listaItems.get(i));
        }
        Iterator<Integer> it = tree.keySet().iterator();
        while (it.hasNext()) {
            Integer num = it.next();
            atlas_tracking = String.valueOf(atlas_tracking) + "\n" + ((HashMap) tree.get(num)).get("notes").toString();
        }
        return atlas_tracking;
    }

    private void verificarExistenciaYCrearWrsFaltantes(Element wrItems, OpenerpClient openerpClient, TransactionType parentTransType, Object parentTransId) throws XmlRpcException, JDOMException, IOException {
        Vector<String> wrGuids = getListaGuidsWRs(wrItems);

        Object[] readRes = openerpClient.read(parentTransType.getOpenerpModel(), new Object[]{parentTransId}, new Vector());

        Vector<String> fieldsSoloGUID = new Vector();
        fieldsSoloGUID.add("guid");
        Object[] wr_item_ids = ((Map<?, Object[]>) readRes[0]).get("wr_item_ids");
        for (int i = 0; i < wr_item_ids.length; i++) {
            Object[] readWR = openerpClient.read(TransactionType.WarehouseReceipt.getOpenerpModel(), new Object[]{wr_item_ids[i]}, fieldsSoloGUID);
            String guidWR = ((Map<?, String>) readWR[0]).get("guid");
            if (!wrGuids.contains(guidWR)) {
                wrGuids.add(guidWR);
            }
        }

        Vector<Object> filter = new Vector();
        filter.addElement("guid");
        filter.addElement("in");
        filter.addElement(wrGuids);
        Vector<Vector<Object>> filters = new Vector();
        filters.add(filter);
        Object[] wrIds = openerpClient.search(TransactionType.WarehouseReceipt.getOpenerpModel(), (Vector) filters);

        Vector<String> fieldNames = new Vector();
        fieldNames.add("guid");
        Object[] wrRead = openerpClient.read(TransactionType.WarehouseReceipt.getOpenerpModel(), wrIds, fieldNames);

        for (Iterator<String> iterator = wrGuids.iterator(); iterator.hasNext();) {
            String guidWR = iterator.next();
            Object idWr = null;

            boolean existe = false;
            for (int j = 0; j < wrRead.length; j++) {
                if (((Map<?, Vector>) wrRead[j]).get("guid").equals(guidWR)) {
                    existe = true;
                    idWr = ((Map<?, ?>) wrRead[j]).get("id");
                }
            }
            SAXBuilder builder = new SAXBuilder();
            if (!existe) {
                idWr = marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guidWR, null, null);
            }

            String xmlTranss = magayaWS.getTransaction(TransactionType.WarehouseReceipt.getCode(), guidWR, Integer.valueOf(546));
            if (xmlTranss != null) {
                Logger.getLogger(getClass().getSimpleName()).warn("Descargando WR faltante, referenciado por otra transaccion que se ha descargado");
                xmlTranss = xmlTranss.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
                Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
                Reader in = new StringReader(xmlTranss);
                Document document = builder.build(in);
                Element nodoTrans = document.getRootElement();

                try {
                    Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                    Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));

                    Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + TransactionType.WarehouseReceipt.getCode() + ")...");
                    actualizar(TransactionType.WarehouseReceipt, idWr, nodoTrans, openerpClient);
                } catch (Exception e) {
                    Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
                }
                continue;
            }
            Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta. ");
        }
    }

    private void verificarExistenciaYCrearWrsFaltantesRutilizandoXML(Element wrItems, OpenerpClient openerpClient) throws XmlRpcException, JDOMException, IOException {
        Vector wrGuids = getListaGuidsWRs(wrItems);

        List<Element> nodosItems = wrItems.getChildren("Item");

        Vector<Object> filter = new Vector();
        filter.addElement("guid");
        filter.addElement("in");
        filter.addElement(wrGuids);
        Vector<Vector<Object>> filters = new Vector();
        filters.add(filter);
        Object[] wrIds = openerpClient.search(TransactionType.WarehouseReceipt.getOpenerpModel(), (Vector) filters);

        Vector<String> fieldNames = new Vector();
        fieldNames.add("guid");
        Object[] wrRead = openerpClient.read(TransactionType.WarehouseReceipt.getOpenerpModel(), wrIds, fieldNames);

        for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext();) {
            Element nodoItem = iterator.next();

            String guidWR = nodoItem.getChildText("WarehouseReceiptGUID");
            Object idWr = null;

            boolean existe = false;
            for (int i = 0; i < wrRead.length; i++) {
                if (((Map<?, Vector>) wrRead[i]).get("guid").equals(guidWR)) {
                    existe = true;
                    idWr = ((Map<?, ?>) wrRead[i]).get("id");
                }
            }
            SAXBuilder builder = new SAXBuilder();
            if (!existe) {
                idWr = marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guidWR, null, null);
            }

            String xmlTranss = magayaWS.getTransaction(TransactionType.WarehouseReceipt.getCode(), guidWR, Integer.valueOf(546));
            if (xmlTranss != null) {
                Logger.getLogger(getClass().getSimpleName()).warn("Descargando WR faltante, referenciado por otra transaccion que se ha descargado");
                xmlTranss = xmlTranss.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
                Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
                Reader in = new StringReader(xmlTranss);
                Document document = builder.build(in);
                Element nodoTrans = document.getRootElement();

                try {
                    Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                    Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));

                    Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + TransactionType.WarehouseReceipt.getCode() + ")...");
                    actualizar(TransactionType.WarehouseReceipt, idWr, nodoTrans, openerpClient);
                } catch (Exception e) {
                    Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
                }
                continue;
            }
            Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta. ");
        }
    }

    private Vector getListaGuidsWRs(Element items) {
        Vector<String> listaItems = new Vector();

        List<Element> nodosItems = items.getChildren("Item");
        for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext();) {
            Element nodoItem = iterator.next();
            if (nodoItem.getChildText("WarehouseReceiptGUID") != null) {
                listaItems.add(nodoItem.getChildText("WarehouseReceiptGUID"));
            }
            if (nodoItem.getChild("ContainedItems") != null) {
                List<Element> nodosItemsContenidos = nodoItem.getChild("ContainedItems").getChildren("Item");
                for (Iterator<Element> iteratorContenidos = nodosItemsContenidos.iterator(); iteratorContenidos.hasNext();) {
                    Element nodoItemContenido = iteratorContenidos.next();
                    if (nodoItemContenido.getChildText("WarehouseReceiptGUID") != null) {
                        listaItems.add(nodoItemContenido.getChildText("WarehouseReceiptGUID"));
                    }
                }
            }
        }
        return listaItems;
    }

    public Integer crearTransaccion(Element nodoWR, OpenerpClient openerpClient) throws XmlRpcException {
        HashMap<Object, Object> values = getWRValues(nodoWR);

        values.put("guid", nodoWR.getAttributeValue("GUID"));

        values.put("deleted", Boolean.FALSE);
        values.put("update_required", Boolean.FALSE);

        HashMap<Object, Object> hashShipperAddress = null;
        if (nodoWR.getChild("ShipperAddress") != null) {
            hashShipperAddress = getAddress(nodoWR.getChild("ShipperAddress"));
        }
        HashMap<Object, Object> hashConsigneeAddress = null;
        if (nodoWR.getChild("ConsigneeAddress") != null) {
            hashConsigneeAddress = getAddress(nodoWR.getChild("ConsigneeAddress"));
        }
        ArrayList<HashMap<Object, Object>> listaItems = new ArrayList<HashMap<Object, Object>>();
        if (nodoWR.getChild("Items") != null) {
            listaItems = getListaItems(nodoWR.getChild("Items"));
        }
        HashMap<Object, Object> hashMapXml = new HashMap<Object, Object>();
        hashMapXml.put("xml_response", (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoWR));

        return openerpClient.crearTransaccionWR(values, hashShipperAddress, hashConsigneeAddress, listaItems, hashMapXml);
    }

    private HashMap<Object, Object> getSHValues(Element nodoSH) {
        HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("guid", nodoSH.getAttributeValue("GUID"));
        values.put("sh_number", nodoSH.getChildText("Number"));

        try {
            values.put("name", Integer.valueOf((nodoSH.getChildText("Number") != null) ? Integer.valueOf(nodoSH.getChildText("Number")).intValue() : 0));
        } catch (NumberFormatException e1) {

            values.put("name", Integer.valueOf(0));
        }

        try {
            values.put("created_on", (nodoSH.getChildText("CreatedOn") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(Utils.replaceCharAt(nodoSH.getChildText("CreatedOn"), nodoSH.getChildText("CreatedOn").lastIndexOf(":"), ""))) : "");
        } catch (ParseException e) {

        }

        values.put("status", (nodoSH.getChildText("Status") != null) ? nodoSH.getChildText("Status") : "");
        values.put("issued_by_name", (nodoSH.getChildText("IssuedByName") != null) ? nodoSH.getChildText("IssuedByName") : "");
        values.put("created_by_name", (nodoSH.getChildText("CreatedByName") != null) ? nodoSH.getChildText("CreatedByName") : "");
        values.put("shipment_type", nodoSH.getName().replace("Shipment", ""));
        values.put("shipper_name", (nodoSH.getChildText("ShipperName") != null) ? nodoSH.getChildText("ShipperName") : "");
        values.put("consignee_name", (nodoSH.getChildText("ConsigneeName") != null) ? nodoSH.getChildText("ConsigneeName") : "");
        String letrasTipo = "";
        if (nodoSH.getChildText("MasterGUID") != null && !nodoSH.getChildText("MasterGUID").isEmpty()) {
            letrasTipo = "H";
        } else {
            letrasTipo = "M";
        }
        if (values.get("shipment_type").equals("Air")) {
            letrasTipo = String.valueOf(letrasTipo) + "A";
        } else if (values.get("shipment_type").equals("Ocean")) {
            letrasTipo = String.valueOf(letrasTipo) + "B";
        } else {
            letrasTipo = "";
        }
        if (!letrasTipo.isEmpty()) {

            values.put("transport_document_number", (nodoSH.getChildText("OutHouseWayBillNumber") != null) ? nodoSH.getChildText("OutHouseWayBillNumber") : "");
        }
        values.put("document_master_number", (nodoSH.getChildText("MasterWayBillNumber") != null) ? nodoSH.getChildText("MasterWayBillNumber") : "");
        values.put("description_of_goods", (nodoSH.getChildText("DescriptionOfGoods") != null) ? nodoSH.getChildText("DescriptionOfGoods") : "");
        if (nodoSH.getChildText("ChargeableWeight") != null) {
            values.put("charge_wiegth", nodoSH.getChildText("ChargeableWeight"));
            values.put("charge_wiegth_unit", nodoSH.getChild("ChargeableWeight").getAttributeValue("Unit"));
            values.put("chargeable_weight", nodoSH.getChildText("ChargeableWeight"));
            values.put("chargeable_weight_unit", nodoSH.getChild("ChargeableWeight").getAttributeValue("Unit"));
        }
        values.put("booking_number", (nodoSH.getChildText("BookingNumber") != null) ? nodoSH.getChildText("BookingNumber") : "");
        values.put("carrier_name", (nodoSH.getChildText("CarrierName") != null) ? nodoSH.getChildText("CarrierName") : "");

        if (nodoSH.getChild("OriginPort") != null) {
            values.put("origin_port_name", (nodoSH.getChild("OriginPort").getChildText("Name") != null) ? nodoSH.getChild("OriginPort").getChildText("Name") : "");
            values.put("origin_port_code", (nodoSH.getAttributeValue("Code") != null) ? nodoSH.getAttributeValue("Code") : "");
            values.put("origin_port_country_name", (nodoSH.getChild("OriginPort").getChildText("Country") != null) ? nodoSH.getChild("OriginPort").getChildText("Country") : "");
            values.put("origin_port_country_code", (nodoSH.getChild("OriginPort").getChild("Country").getAttributeValue("Code") != null) ? nodoSH.getChild("OriginPort").getChild("Country").getAttributeValue("Code") : "");
        }

        if (nodoSH.getChild("DestinationPort") != null) {
            values.put("destination_port_name", (nodoSH.getChild("DestinationPort").getChildText("Name") != null) ? nodoSH.getChild("DestinationPort").getChildText("Name") : "");
            values.put("destination_port_code", (nodoSH.getChild("DestinationPort").getAttributeValue("Code") != null) ? nodoSH.getChild("DestinationPort").getAttributeValue("Code") : "");
            values.put("destination_port_country_name", (nodoSH.getChild("DestinationPort").getChildText("Country") != null) ? nodoSH.getChild("DestinationPort").getChildText("Country") : "");
            values.put("destination_port_country_code", (nodoSH.getChild("DestinationPort").getChild("Country").getAttributeValue("Code") != null) ? nodoSH.getChild("DestinationPort").getChild("Country").getAttributeValue("Code") : "");
        }

        if (nodoSH.getChild("ModeOfTransportation") != null) {
            values.put("mode_of_transportation", (nodoSH.getChild("ModeOfTransportation").getChildText("Method") != null) ? nodoSH.getChild("ModeOfTransportation").getChildText("Method") : "");
        }
        values.put("declared_value", (nodoSH.getChildText("DeclaredValue") != null) ? nodoSH.getChildText("DeclaredValue") : "");
        if (nodoSH.getChildText("AirShipmentInfo") != null) {
            values.put("declared_value_for_customs", (nodoSH.getChild("AirShipmentInfo").getChildText("DeclareValueForCustoms") != null) ? nodoSH.getChild("AirShipmentInfo").getChildText("DeclareValueForCustoms") : "");
        }
        if (nodoSH.getChildText("TotalValue") != null) {
            values.put("currency", nodoSH.getChild("TotalValue").getAttributeValue("Currency"));
        }

        try {
            values.put("pieces", Integer.valueOf((nodoSH.getChildText("TotalPieces") != null) ? Integer.valueOf(nodoSH.getChildText("TotalPieces")).intValue() : 0));
        } catch (NumberFormatException e1) {
            values.put("pieces", Integer.valueOf(0));
        }

        if (nodoSH.getChildText("TotalWeight") != null) {
            values.put("weight", nodoSH.getChildText("TotalWeight"));
            values.put("weight_unit", nodoSH.getChild("TotalWeight").getAttributeValue("Unit"));

            values.put("gross_wiegth", nodoSH.getChildText("TotalWeight"));
            values.put("gross_wiegth_unit", nodoSH.getChild("TotalWeight").getAttributeValue("Unit"));
        }
        if (nodoSH.getChildText("TotalVolume") != null) {
            values.put("volume", nodoSH.getChildText("TotalVolume"));
            values.put("volume_unit", nodoSH.getChild("TotalVolume").getAttributeValue("Unit"));
        }
        if (nodoSH.getChildText("TotalVolumeWeight") != null) {
            values.put("volume_weight", nodoSH.getChildText("TotalVolumeWeight"));
            values.put("volume_weight_unit", nodoSH.getChild("TotalVolumeWeight").getAttributeValue("Unit"));
        }

        return values;
    }

    public HashMap<Object, Object> getWRValues(Element nodoWR) {
        HashMap<Object, Object> values = new HashMap<>();
        values.put("guid", nodoWR.getAttributeValue("GUID"));

        String notesValue = "";

        List<Element> wrNodeList = nodoWR.getChildren();
        for (Element wrItem : wrNodeList) {

            if (wrItem.getName().equalsIgnoreCase("Number")) {
                try {
                    String numberValue = wrItem.getValue();
                    values.put("name", (numberValue != null) ? Integer.parseInt(numberValue) : 0);
                } catch (NumberFormatException e1) {

                    values.put("name", 0);
                }
            } else if (wrItem.getName().equalsIgnoreCase("CreatedOn")) {
                try {
                    String createdOnValue = wrItem.getValue();
                    values.put("created_on", (createdOnValue != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(Utils.replaceCharAt(createdOnValue, createdOnValue.lastIndexOf(":"), ""))) : (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()));
                } catch (ParseException e) {
                    Logger.getLogger(getClass().getSimpleName()).error("El numero de transaccion no es un numero, seteandolo a 0", e);
                    values.put("created_on", 0);
                }
            } else if (wrItem.getName().equalsIgnoreCase("CreatedByName")) {
                String createdByNameValue = wrItem.getValue();
                values.put("created_by_name", (createdByNameValue != null) ? createdByNameValue : "");
            } else if (wrItem.getName().equalsIgnoreCase("Status")) {
                String value = wrItem.getValue();
                values.put("status", (value != null) ? value : "");
            } else if (wrItem.getName().equalsIgnoreCase("IssuedByName")) {
                String value = wrItem.getValue();
                values.put("issued_by_name", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("ShipperName")) {
                String value = wrItem.getValue();
                values.put("shipper_name", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("CarrierName")) {
                String value = wrItem.getValue();
                values.put("carrier_name", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("ConsigneeName")) {
                String value = wrItem.getValue();
                values.put("consignee_name", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("TotalPieces")) {
                String value = wrItem.getValue();
                values.put("total_pieces", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("TotalValue")) {
                String value = wrItem.getValue();
                values.put("total_value", value);
                values.put("currency", wrItem.getAttributeValue("Currency"));
            } else if (wrItem.getName().equalsIgnoreCase("TotalWeight")) {
                String value = wrItem.getValue();
                values.put("total_weight", value);
                values.put("total_weight_unit", wrItem.getAttributeValue("Unit"));
            } else if (wrItem.getName().equalsIgnoreCase("TotalVolume")) {
                String value = wrItem.getValue();
                values.put("total_volume", value);
                values.put("total_volume_unit", wrItem.getAttributeValue("Unit"));
            } else if (wrItem.getName().equalsIgnoreCase("TotalVolumeWeight")) {
                String value = wrItem.getValue();
                values.put("total_volume_weight", value);
                values.put("total_volume_weight_unit", wrItem.getAttributeValue("Unit"));
            } else if (wrItem.getName().equalsIgnoreCase("SupplierInvoiceNumber")) {
                String txtMonto = wrItem.getValue();
                values.put("invoice_value", txtMonto.replace(",", ""));
            } else if (wrItem.getName().equalsIgnoreCase("DriverName")) {
                String value = wrItem.getValue();
                values.put("driver_name", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("DestinationAgentName")) {
                String value = wrItem.getValue();
                values.put("destination_agent_name", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("CarrierTrackingNumber")) {
                String value = wrItem.getValue();
                values.put("carrier_tracking_number", (value != null) ? value : "");

            } else if (wrItem.getName().equalsIgnoreCase("Notes")) {
                String value = wrItem.getValue();
                notesValue += (value != null) ? value : "";
            } else if (wrItem.getName().equalsIgnoreCase("Charges")) {
                List<Element> nodosChargerItems = wrItem.getChildren();
                String referencesValue = "";
                for (Element wrChargeItem : nodosChargerItems) {
                    List<Element> nodosChargeDefinitionItems = wrChargeItem.getChildren("ChargeDefinition", null);
                    for (Element wrChargeDefinitionItem : nodosChargeDefinitionItems) {
                        referencesValue += wrChargeDefinitionItem.getChild("Description", null).getValue();
                        referencesValue += ": $. ";
                        referencesValue += wrChargeDefinitionItem.getChild("Amount", null).getValue() + "\n";
                    }
                }

                values.put("references", referencesValue);

            } else if (wrItem.getName().equalsIgnoreCase("SupplierPONumber")) {
                String value = wrItem.getValue();
                values.put("supplier_po_number", (value != null) ? value : "");
            }

        }

        values.put("notes", notesValue);
        values.put("atlas_status", "NoReportado");

        return values;
    }

    public ArrayList<HashMap<Object, Object>> getListaItems(Element items) {
        ArrayList<HashMap<Object, Object>> listaItems = new ArrayList<HashMap<Object, Object>>();

        List<Element> nodosItems = items.getChildren("Item");
        for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext();) {
            Element nodoItem = iterator.next();
            HashMap<Object, Object> hashItem = new HashMap<Object, Object>();

            hashItem.put("guid", nodoItem.getAttributeValue("GUID"));

            hashItem.put("name", (nodoItem.getChildText("WHRItemID") != null) ? nodoItem.getChildText("WHRItemID") : "");
            hashItem.put("package_name", (nodoItem.getChildText("PackageName") != null) ? nodoItem.getChildText("PackageName") : "");
            hashItem.put("pieces", (nodoItem.getChildText("Pieces") != null) ? nodoItem.getChildText("Pieces") : "");

            if (nodoItem.getChildText("Length") != null) {
                hashItem.put("length", (nodoItem.getChildText("Length") != null) ? nodoItem.getChildText("Length") : "");
                hashItem.put("length_unit", nodoItem.getChild("Length").getAttributeValue("Unit"));
            }
            if (nodoItem.getChildText("Height") != null) {
                hashItem.put("height", (nodoItem.getChildText("Height") != null) ? nodoItem.getChildText("Height") : "");
                hashItem.put("height_unit", nodoItem.getChild("Height").getAttributeValue("Unit"));
            }
            if (nodoItem.getChildText("Width") != null) {
                hashItem.put("width", (nodoItem.getChildText("Width") != null) ? nodoItem.getChildText("Width") : "");
                hashItem.put("width_unit", nodoItem.getChild("Width").getAttributeValue("Unit"));
            }
            if (nodoItem.getChildText("Weight") != null) {
                hashItem.put("weight", (nodoItem.getChildText("Weight") != null) ? nodoItem.getChildText("Weight") : "");
                hashItem.put("weight_unit", nodoItem.getChild("Weight").getAttributeValue("Unit"));
            }
            if (nodoItem.getChildText("Volume") != null) {
                hashItem.put("volume", (nodoItem.getChildText("Volume") != null) ? nodoItem.getChildText("Volume") : "");
                hashItem.put("volume_unit", nodoItem.getChild("Volume").getAttributeValue("Unit"));
            }
            if (nodoItem.getChildText("VolumeWeight") != null) {
                hashItem.put("volume_weight", (nodoItem.getChildText("VolumeWeight") != null) ? nodoItem.getChildText("VolumeWeight") : "");
                hashItem.put("volume_weight_unit", nodoItem.getChild("VolumeWeight").getAttributeValue("Unit"));
            }
            hashItem.put("out_master_way_bill_number", (nodoItem.getChildText("OutMasterWayBillNumber") != null) ? nodoItem.getChildText("OutMasterWayBillNumber") : "");
            hashItem.put("out_house_way_bill_number", (nodoItem.getChildText("OutHouseWayBillNumber") != null) ? nodoItem.getChildText("OutHouseWayBillNumber") : "");
            hashItem.put("tracking_number", (nodoItem.getChildText("TrackingNumber") != null) ? nodoItem.getChildText("TrackingNumber") : "");
            hashItem.put("location_code", (nodoItem.getChildText("LocationCode") != null) ? nodoItem.getChildText("LocationCode") : "");
            hashItem.put("notes", (nodoItem.getChildText("Notes") != null) ? nodoItem.getChildText("Notes") : "");
            hashItem.put("status", (nodoItem.getChildText("Status") != null) ? nodoItem.getChildText("Status") : "");

            hashItem.put("description", (nodoItem.getChildText("Description") != null) ? nodoItem.getChildText("Description") : "");

            listaItems.add(hashItem);
        }
        return listaItems;
    }

    public HashMap<Object, Object> getAddress(Element nodoAddr) {
        HashMap<Object, Object> hashAddress = null;

        hashAddress = new HashMap<Object, Object>();

        hashAddress.put("name", (nodoAddr.getChildren("Street").size() > 0) ? ((Element) nodoAddr.getChildren("Street").get(0)).getText() : "");
        hashAddress.put("street_2", (nodoAddr.getChildren("Street").size() > 1) ? ((Element) nodoAddr.getChildren("Street").get(1)).getText() : "");
        hashAddress.put("city", (nodoAddr.getChildText("City") != null) ? nodoAddr.getChildText("City") : "");
        hashAddress.put("state", (nodoAddr.getChildText("State") != null) ? nodoAddr.getChildText("State") : "");
        hashAddress.put("zip_code", (nodoAddr.getChildText("ZipCode") != null) ? nodoAddr.getChildText("ZipCode") : "");
        hashAddress.put("country", (nodoAddr.getChildText("Country") != null) ? nodoAddr.getChildText("Country") : "");
        hashAddress.put("country_code", (nodoAddr.getChildText("CountryCode") != null) ? nodoAddr.getChildText("CountryCode") : "");
        hashAddress.put("description", (nodoAddr.getChildText("Description") != null) ? nodoAddr.getChildText("Description") : "");
        hashAddress.put("contact_name", (nodoAddr.getChildText("ContactName") != null) ? nodoAddr.getChildText("ContactName") : "");
        hashAddress.put("contact_phone", (nodoAddr.getChildText("ContactPhone") != null) ? nodoAddr.getChildText("ContactPhone") : "");
        hashAddress.put("port_code", (nodoAddr.getChildText("PortCode") != null) ? nodoAddr.getChildText("PortCode") : "");

        return hashAddress;
    }

    public void chequearCambiosTransacciones(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora, Boolean chequearEliminaciones, Boolean chequearCreaciones, Boolean chequearModificaciones, Boolean guardarFechaFin) throws FileNotFoundException, IOException {
        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
            magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

            Date ultimaActualizacion = new Date();
            if (desdeFechaHora == null) {
                desdeFechaHora = openerpClient.getDateUltimaConsultaQueryLog(transType);
            }
            if (chequearEliminaciones.booleanValue()) {
                checkDeleted(transType, desdeFechaHora, hastaFechaHora);
            }

            if (chequearCreaciones.booleanValue()) {
                checkCreated(transType, desdeFechaHora, hastaFechaHora);
            }

            if (chequearModificaciones.booleanValue()) {
                checkModified(transType, desdeFechaHora, hastaFechaHora);
            }

            if (guardarFechaFin.booleanValue()) {
                openerpClient.setDateUltimaConsultaQueryLog(transType, ultimaActualizacion);
            }
        } catch (XmlRpcException e1) {
            Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con OpenERP", (Throwable) e1);
        } catch (JDOMException e) {
            Logger.getLogger(getClass().getSimpleName()).error("Error al procesar el XML retornado por QueryLog", (Throwable) e);
        } catch (IOException e) {
            Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con Magaya, probablemente este caido", e);
        } catch (Exception e) {
            Logger.getLogger(getClass().getSimpleName()).error("Error en el chequeo de actualizaciones (via QueryLog)", e);
        }
    }

    public void chequearVerificaciones(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora, Boolean guardarFechaFin) throws FileNotFoundException, IOException {
        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
            magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

            if (desdeFechaHora == null) {
                desdeFechaHora = openerpClient.getDateUltimaConsultaQueryLogVerificacionesVerificador(transType);
            }
            if (hastaFechaHora == null) {
                hastaFechaHora = new Date();
            }
            TreeSet<String> guidsMagayaPeriodo = new TreeSet<String>();

            SAXBuilder builder = new SAXBuilder();

            Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + "  creadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)) + " ...");
            String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)), Integer.valueOf(1), Integer.valueOf(0));

            xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
            Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

            Reader in = new StringReader(xmlTransPreriodo);
            Document document = builder.build(in);
            Element rootNode = document.getRootElement();
            List<Element> nodosTrans = rootNode.getChildren("GUIDItem");
            Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());

            for (Element nodoTransGuid : nodosTrans) {
                guidsMagayaPeriodo.add(nodoTransGuid.getChildText("GUID"));
            }

            Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + " actualizadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)) + " ...");
            xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)), Integer.valueOf(4), Integer.valueOf(0));

            xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
            Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

            in = new StringReader(xmlTransPreriodo);
            document = builder.build(in);
            rootNode = document.getRootElement();
            nodosTrans = rootNode.getChildren("GUIDItem");
            Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());

            for (Element nodoTransGuid : nodosTrans) {
                guidsMagayaPeriodo.add(nodoTransGuid.getChildText("GUID"));
            }

            Vector<Object> vectorOfVectorFilterTuples = new Vector();

            vectorOfVectorFilterTuples.add("&");
            vectorOfVectorFilterTuples.add("&");

            Vector<Object> filterGuids = new Vector();
            filterGuids.addElement("guid");
            filterGuids.addElement("in");
            filterGuids.addElement(new Vector<Object>(guidsMagayaPeriodo));
            vectorOfVectorFilterTuples.add(filterGuids);

            Vector<String> filterDesde = new Vector();
            filterDesde.addElement("last_query_log_from");
            filterDesde.addElement(">=");
            filterDesde.addElement(this.formaterUtc.format(sumarUnaHoraYRestar10Min(desdeFechaHora)));
            vectorOfVectorFilterTuples.add(filterDesde);

            Vector<String> filterHasta = new Vector();
            filterHasta.addElement("last_query_log_to");
            filterHasta.addElement("<=");
            filterHasta.addElement(this.formaterUtc.format(sumarUnaHoraYRestar10Min(hastaFechaHora)));
            vectorOfVectorFilterTuples.add(filterHasta);

            Object[] trans_ids = openerpClient.searchGenerico(transType.getOpenerpModel(), vectorOfVectorFilterTuples);

            Logger.getLogger(getClass().getSimpleName()).warn("Trans openerp marcadas en el periodo: " + trans_ids.length);

            Vector<String> fieldNames = new Vector();
            fieldNames.add("guid");

            Object[] transReads = openerpClient.read(transType.getOpenerpModel(), trans_ids, fieldNames);

            Logger.getLogger(getClass().getSimpleName()).warn("Inciales: " + guidsMagayaPeriodo.size());
            for (int i = 0; i < transReads.length; i++) {
                HashMap<Object, Object> transRead = (HashMap<Object, Object>) transReads[i];

                Object transOpenerpId = transRead.get("id");
                String transGuid = transRead.get("guid").toString();

                if (guidsMagayaPeriodo.contains(transGuid)) {
                    guidsMagayaPeriodo.remove(transGuid);
                }
            }

            Logger.getLogger(getClass().getSimpleName()).warn("Finales: " + guidsMagayaPeriodo.size());

            for (Iterator<String> iterator = guidsMagayaPeriodo.iterator(); iterator.hasNext();) {
                String guid = iterator.next();

                if (!openerpClient.existeTransaccion(transType, guid)) {
                    marcarParaCrearTransaccion(transType, openerpClient, guid, sumarUnaHoraYRestar10Min(desdeFechaHora), sumarUnaHoraYRestar10Min(hastaFechaHora));
                    continue;
                }
                Vector<String> filter = new Vector();
                filter.addElement("guid");
                filter.addElement("=");
                filter.addElement(guid);
                Vector<Vector<String>> filters = new Vector();
                filters.add(filter);
                Object[] ids = openerpClient.search(transType.getOpenerpModel(), (Vector) filters);

                Vector<String> fieldNamesTrans = new Vector();
                fieldNamesTrans.addElement("update_required");
                HashMap<Object, Object> transRead = (HashMap<Object, Object>) openerpClient.read(transType.getOpenerpModel(), ids, fieldNamesTrans)[0];

                Integer prioridad = Integer.valueOf(transRead.get("update_required").toString());
                if (prioridad.intValue() == 0) {
                    prioridad = Integer.valueOf(1);
                }

                marcarParaActualizarTransaccion(transType, guid, openerpClient, prioridad, sumarUnaHoraYRestar10Min(desdeFechaHora), sumarUnaHoraYRestar10Min(hastaFechaHora));
                Logger.getLogger(getClass().getSimpleName()).info("Transaccion marcada para actualizar, guid: " + guid);
            }

            if (guardarFechaFin.booleanValue()) {
                openerpClient.setDateUltimaConsultaQueryLogVerificacionesVerificador(transType, hastaFechaHora);
            }
        } catch (XmlRpcException e1) {
            Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con OpenERP", (Throwable) e1);
        } catch (JDOMException e) {
            Logger.getLogger(getClass().getSimpleName()).error("Error al procesar el XML retornado por QueryLog", (Throwable) e);
        } catch (IOException e) {
            Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con Magaya, probablemente este caido", e);
        } catch (Exception e) {
            Logger.getLogger(getClass().getSimpleName()).error("Error en el chequeo de actualizaciones (via QueryLog)", e);
        }
    }

    private void checkModified(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException, JDOMException, IOException {
        Calendar calHasta = Calendar.getInstance();
        if (hastaFechaHora != null) {
            calHasta.setTime(hastaFechaHora);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(desdeFechaHora);

        SAXBuilder builder = new SAXBuilder();
        Date fechaPivotFin = cal.getTime();

        while (cal.before(calHasta)) {
            Date fechaPivotInicio = cal.getTime();
            cal.add(5, 1);
            if (cal.after(calHasta)) {
                cal.setTime(calHasta.getTime());
            }
            fechaPivotFin = cal.getTime();

            Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + " actualizadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)) + " ...");
            String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(),
                    this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)),
                    this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)),
                    Integer.valueOf(4),
                    Integer.valueOf(0));

            xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
            Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

            Reader in = new StringReader(xmlTransPreriodo);
            Document document = builder.build(in);
            Element rootNode = document.getRootElement();
            List<Element> nodosTrans = rootNode.getChildren("GUIDItem");
            Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());

            for (Element nodoTransGuid : nodosTrans) {
                int reintentos = 5;
                while (reintentos > 0) {
                    try {
                        Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                        Logger.getLogger(getClass().getSimpleName()).info(nodoTransGuid.getChildText("GUID"));
                        if (!transaccionProblematicaIgnorada(nodoTransGuid.getChildText("GUID"))) {
                            if (!openerpClient.existeTransaccion(transType, nodoTransGuid.getChildText("GUID"))) {

                                Logger.getLogger(getClass().getSimpleName()).warn("La transaccion no existe aún, pero se recibio una actualizacion de la misma. Se marca para crearla");
                                marcarParaCrearTransaccion(transType, openerpClient, nodoTransGuid.getChildText("GUID"), sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
                            } else {
                                Logger.getLogger(getClass().getSimpleName()).info("La transaccion ya existe, se marca para actualizarla");

                                Vector<String> filter = new Vector();
                                filter.addElement("guid");
                                filter.addElement("=");
                                filter.addElement(nodoTransGuid.getChildText("GUID"));
                                Vector<Vector<String>> filters = new Vector();
                                filters.add(filter);
                                Object[] ids = openerpClient.search(transType.getOpenerpModel(), (Vector) filters);

                                Vector<String> fieldNames = new Vector();
                                fieldNames.addElement("update_required");
                                HashMap<Object, Object> transRead = (HashMap<Object, Object>) openerpClient.read(transType.getOpenerpModel(), ids, fieldNames)[0];

                                Integer prioridad = Integer.valueOf(transRead.get("update_required").toString());
                                if (prioridad.intValue() == 0) {
                                    prioridad = Integer.valueOf(1);
                                }

                                marcarParaActualizarTransaccion(transType, nodoTransGuid, openerpClient, prioridad, sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
                            }
                        } else {
                            Logger.getLogger(getClass().getSimpleName()).warn("Se ignora la ttransaccion problematica: GUID: " + nodoTransGuid.getChildText("GUID"));
                        }
                        reintentos = 0;
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
                        reintentos--;
                        try {
                            Thread.sleep(5000L);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public Date sumarUnaHoraYRestar10Min(Date fecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        Logger.getLogger(getClass().getSimpleName()).info("Horario verano: " + String.valueOf(RuntimeConfig.getInstance().isHorarioVerano()));
        if (!RuntimeConfig.getInstance().isHorarioVerano().booleanValue()) {
            cal.add(11, 1);
        }
        cal.add(12, -10);
        return cal.getTime();
    }

    public Date sumarUnaHora(Date fecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        Logger.getLogger(getClass().getSimpleName()).info("Horario verano: " + String.valueOf(RuntimeConfig.getInstance().isHorarioVerano()));
        if (!RuntimeConfig.getInstance().isHorarioVerano().booleanValue()) {
            cal.add(11, 1);
        }
        return cal.getTime();
    }

    public Date restar10Min(Date fecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.add(12, -10);
        return cal.getTime();
    }

    private void checkDeleted(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException, JDOMException, IOException {
        Calendar calHasta = Calendar.getInstance();
        if (hastaFechaHora != null) {
            calHasta.setTime(hastaFechaHora);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(desdeFechaHora);

        SAXBuilder builder = new SAXBuilder();
        Date fechaPivotFin = cal.getTime();

        while (cal.before(calHasta)) {
            Date fechaPivotInicio = cal.getTime();
            cal.add(5, 1);
            if (cal.after(calHasta)) {
                cal.setTime(calHasta.getTime());
            }
            fechaPivotFin = cal.getTime();

            Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + " eliminad@s entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)) + " ...");
            String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)), Integer.valueOf(2), Integer.valueOf(0));

            xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
            Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

            Reader in = new StringReader(xmlTransPreriodo);
            Document document = builder.build(in);
            Element rootNode = document.getRootElement();
            List<Element> nodosWR = rootNode.getChildren("GUIDItem");
            Logger.getRootLogger().info("Transacciones obtenidas: " + nodosWR.size());

            for (Element nodoTransGuid : nodosWR) {
                int reintentos = 5;
                while (reintentos > 0) {
                    try {
                        Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                        Logger.getLogger(getClass().getSimpleName()).info(nodoTransGuid.getChildText("GUID"));
                        if (!transaccionProblematicaIgnorada(nodoTransGuid.getChildText("GUID"))) {
                            if (openerpClient.existeTransaccion(transType, nodoTransGuid.getChildText("GUID"))) {

                                marcarParaEliminarTransaccion(transType, nodoTransGuid, openerpClient, sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
                                Logger.getLogger(getClass().getSimpleName()).info("Transaccion marcada como eliminada");
                            } else {

                                Logger.getLogger(getClass().getSimpleName()).warn("Transaccion eliminada en magaya, pero en openerp no existia (no se hace nada)...");
                            }
                        } else {
                            Logger.getLogger(getClass().getSimpleName()).warn("Se ignora la ttransaccion problematica: GUID: " + nodoTransGuid.getChildText("GUID"));
                        }
                        reintentos = 0;
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
                        reintentos--;
                        try {
                            Thread.sleep(5000L);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private boolean transaccionProblematicaIgnorada(String guid) {
        try {
            CsvReader wrs = new CsvReader("guids_ignorados.csv");
            wrs.readHeaders();
            while (wrs.readRecord()) {
                String guidIgnorado = wrs.get("guid");
                if (guidIgnorado.equals(guid)) {
                    wrs.close();
                    return true;
                }
            }
            wrs.close();
        } catch (IOException e) {
            Logger.getLogger(getClass().getSimpleName()).error("No se pudo procesar el archivo guids_ignorados.csv", e);
        }
        return false;
    }

    private void checkCreated(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException, JDOMException, IOException {
        Calendar calHasta = Calendar.getInstance();
        if (hastaFechaHora != null) {
            calHasta.setTime(hastaFechaHora);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(desdeFechaHora);

        SAXBuilder builder = new SAXBuilder();
        Date fechaPivotFin = cal.getTime();

        while (cal.before(calHasta)) {
            Date fechaPivotInicio = cal.getTime();
            cal.add(5, 1);
            if (cal.after(calHasta)) {
                cal.setTime(calHasta.getTime());
            }
            fechaPivotFin = cal.getTime();

            Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + "  creadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)) + " ...");
            String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)), Integer.valueOf(1), Integer.valueOf(0));

            xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
            Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

            Reader in = new StringReader(xmlTransPreriodo);
            Document document = builder.build(in);
            Element rootNode = document.getRootElement();
            List<Element> nodosWR = rootNode.getChildren("GUIDItem");
            Logger.getRootLogger().info("Transacciones obtenidas: " + nodosWR.size());

            for (Element nodoWRGuid : nodosWR) {
                int reintentos = 5;
                while (reintentos > 0) {
                    try {
                        Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                        Logger.getLogger(getClass().getSimpleName()).info(nodoWRGuid.getChildText("GUID"));
                        if (!transaccionProblematicaIgnorada(nodoWRGuid.getChildText("GUID"))) {
                            if (!openerpClient.existeTransaccion(transType, nodoWRGuid.getChildText("GUID"))) {

                                Logger.getLogger(getClass().getSimpleName()).info("La transaccion no existe aún, marcandola para crear");
                                marcarParaCrearTransaccion(transType, openerpClient, nodoWRGuid.getChildText("GUID"), sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
                            } else {

                                Logger.getLogger(getClass().getSimpleName()).warn("La transaccion ya existe, esto no es normal (no se hace nada)");

                            }

                        } else {

                            Logger.getLogger(getClass().getSimpleName()).warn("Se ignora la ttransaccion problematica: GUID: " + nodoWRGuid.getChildText("GUID"));
                        }
                        reintentos = 0;
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoWRGuid), e);
                        reintentos--;
                        try {
                            Thread.sleep(5000L);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public void actualizarTransacciones(TransactionType transactionType, Date desdeFecha, Date hastaFecha, Boolean descargarAdjuntos) throws FileNotFoundException, IOException {
        DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();

            magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

            Date ultimaActualizacion = null;
            if (desdeFecha == null) {

                ultimaActualizacion = openerpClient.getDateUltimaConsultaQueryLog(transactionType);
            } else {
                ultimaActualizacion = desdeFecha;
            }
            Calendar calHasta = Calendar.getInstance();
            if (hastaFecha != null) {
                calHasta.setTime(hastaFecha);
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(ultimaActualizacion);

            SAXBuilder builder = new SAXBuilder();

            Integer actualizadas = Integer.valueOf(0);

            boolean terminado = false;
            while (!terminado) {
                Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();

                Vector<Object> filterUpdateReq = new Vector();
                filterUpdateReq.addElement("update_required");
                filterUpdateReq.addElement(">");
                filterUpdateReq.addElement(Integer.valueOf(0));
                vectorOfVectorFilterTuples.add(filterUpdateReq);

                Vector<Object> filterDeleted = new Vector();
                filterDeleted.addElement("deleted");
                filterDeleted.addElement("=");
                filterDeleted.addElement(Boolean.FALSE);
                vectorOfVectorFilterTuples.add(filterDeleted);

                Object[] trans_ids = openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(1), "update_required desc, created_on desc");
                int cantAActualizar = (openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples)).length;

                if (trans_ids.length > 0) {
                    Vector<String> fieldNames = new Vector();
                    fieldNames.add("guid");

                    HashMap<Object, Object> transRead = (HashMap<Object, Object>) openerpClient.read(transactionType.getOpenerpModel(), trans_ids, fieldNames)[0];

                    Object transOpenerpId = transRead.get("id");
                    String transGuid = transRead.get("guid").toString();
                    Logger.getLogger(getClass().getSimpleName()).info("Obteniendo transaccion " + transactionType.getCode() + " : " + transGuid + " ...");

                    int flags = 608;
                    if (!descargarAdjuntos.booleanValue()) {
                        flags = 546;
                    }
                    String xmlTranss = magayaWS.getTransaction(transactionType.getCode(), transGuid, Integer.valueOf(flags));
                    int reintentosRestantes = 5;

                    boolean problematica = false;
                    while (xmlTranss != null && reintentosRestantes > 0) {
                        xmlTranss = xmlTranss.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
                        Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
                        Reader in = new StringReader(xmlTranss);
                        Document document = builder.build(in);
                        Element nodoTrans = document.getRootElement();

                        try {
                            Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));

                            Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + transactionType.getCode() + ")...");
                            actualizar(transactionType, transOpenerpId, nodoTrans, openerpClient);
                            Logger.getLogger(getClass().getSimpleName()).info("Actualizada con exito, guid: " + nodoTrans.getAttributeValue("GUID"));
                            actualizadas = Integer.valueOf(actualizadas.intValue() + 1);
                            Logger.getLogger(getClass().getSimpleName()).info("-------------- Progreso - Actualizadas: " + actualizadas + ", Restantes: " + cantAActualizar);
                            reintentosRestantes = 0;
                            problematica = false;
                        } catch (Exception e) {
                            problematica = true;

                            Logger.getLogger(getClass().getSimpleName()).warn("Ocurrio un error al procesar el WR GUID: " + nodoTrans.getAttributeValue("GUID") + ", Number: " + nodoTrans.getChildText("Number"), e);
                            reintentosRestantes--;
                            Logger.getLogger(getClass().getSimpleName()).warn("Reintentos restantes: " + reintentosRestantes);
                            try {
                                Thread.sleep(5000L);
                            } catch (InterruptedException e1) {
                                Logger.getLogger(getClass().getSimpleName()).error(e);
                            }
                        }

                        if (problematica) {

                            Logger.getLogger(getClass().getSimpleName()).warn("Se supero la cantidad de reintentos permitidos para: " + transactionType + ", GUID: " + transGuid + ", Number: " + nodoTrans.getChildText("Number"));
                            desmarcarParaActualizarTransaccion(transactionType, transGuid, openerpClient);
                        }
                    }

                    if (xmlTranss == null) {
                        Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta, se elimina fisicamente la transaccion. ");
                        desmarcarParaActualizarTransaccion(transactionType, transGuid, openerpClient);
                        eliminarTransaccionFisicamente(transactionType, transGuid, openerpClient);
                        continue;
                    }
                    continue;
                }
                terminado = true;
            }

        } catch (XmlRpcException e1) {
        } catch (JDOMException e) {
        } catch (IOException e) {
        }
    }

    private void eliminarTransaccionFisicamente(TransactionType transactionType, String transGuid, OpenerpClient openerpClient2) throws XmlRpcException {
        Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        Vector<Object> guidFilter = new Vector();
        guidFilter.add("guid");
        guidFilter.add("=");
        guidFilter.add(transGuid);
        vectorOfVectorFilterTuples.add(guidFilter);
        openerpClient.unlink(transactionType.getOpenerpModel(), openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples));
    }

    public void descargarMasivamenteTransacciones(TransactionType transType, Date desdeFecha, Date hastaFecha, Boolean soloOnHand) throws FileNotFoundException, IOException, XmlRpcException, JDOMException {
        Date timestampUltimaConsulta = new Date();

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();

            magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

            Date ultimaActualizacion = null;
            if (desdeFecha == null) {

                ultimaActualizacion = openerpClient.getDateUltimaConsultaQueryLog(transType);
            } else {
                ultimaActualizacion = desdeFecha;
            }
            Logger.getLogger(getClass().getSimpleName()).info("Fecha de ultima consulta de " + transType.getDescription() + " obtenida: " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ")).format(ultimaActualizacion));

            Calendar calHasta = Calendar.getInstance();
            if (hastaFecha != null) {
                calHasta.setTime(hastaFecha);
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(ultimaActualizacion);

            SAXBuilder builder = new SAXBuilder();

            System.out.println(String.valueOf(cal.getTime().toString()) + " -- " + calHasta.getTime().toString());

            int desdeY = cal.get(1);
            int hastaY = calHasta.get(1);
            int desdeD = cal.get(6);
            int hastaD = calHasta.get(6);

            while (cal.get(1) != calHasta.get(1) || cal.get(6) <= calHasta.get(6)) {
                Date fechaPivot = cal.getTime();

                timestampUltimaConsulta = new Date();

                Logger.getLogger(getClass().getSimpleName()).info("Descargando transacciones " + transType.getDescription() + "  creadas el " + this.formaterFecha.format(fechaPivot) + " ...");
                String xmlTransPreriodo = magayaWS.getTransRangeByDate(transType.getCode(), this.formaterFecha.format(fechaPivot), this.formaterFecha.format(fechaPivot), Integer.valueOf(546));

                xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
                Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

                Logger.getLogger(getClass().getSimpleName()).info("GetTransByDateRange: " + transType);
                Reader in = new StringReader(xmlTransPreriodo);
                Document document = builder.build(in);
                Element rootNode = document.getRootElement();
                List<Element> nodosTrans = rootNode.getChildren();
                Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());

                for (Element nodoTrans : nodosTrans) {
                    try {
                        Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                        Logger.getLogger(getClass().getSimpleName()).info(nodoTrans.getAttributeValue("GUID"));
                        Object id = null;
                        if (!openerpClient.existeTransaccion(transType, nodoTrans.getAttributeValue("GUID"))) {
                            Logger.getLogger(getClass().getSimpleName()).info("La transaccion no existe aún, se crea y actualiza");
                            id = marcarParaCrearTransaccion(transType, openerpClient, nodoTrans.getAttributeValue("GUID"), desdeFecha, hastaFecha);
                        } else {

                            Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();

                            Vector<Object> filterGuid = new Vector();
                            filterGuid.addElement("guid");
                            filterGuid.addElement("=");
                            filterGuid.addElement(nodoTrans.getAttributeValue("GUID"));
                            vectorOfVectorFilterTuples.add(filterGuid);

                            Object[] trans_ids = openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(1), "update_required desc, created_on desc");
                            id = trans_ids[0];
                            Logger.getLogger(getClass().getSimpleName()).warn("La transaccion ya existe, se actualiza");
                        }

                        if (transType != TransactionType.WarehouseReceipt || (soloOnHand.booleanValue() && nodoTrans.getChildText("Status").equals("OnHand")) || !soloOnHand.booleanValue()) {
                            actualizar(transType, id, nodoTrans, openerpClient);
                            continue;
                        }
                        Logger.getLogger(getClass().getSimpleName()).info("La transaccion no esta en estado OnHand, se ignora");
                    } catch (Exception e) {
                        Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
                    }
                }
                cal.add(5, 1);
            }

            openerpClient.setDateUltimaConsultaQueryLog(transType, timestampUltimaConsulta);
        } catch (XmlRpcException e1) {

            e1.printStackTrace();
        } catch (JDOMException e) {

        } catch (IOException e) {

        }
    }

    public void reprocesarTransaccionesDesdeUltimosXMLs(TransactionType transactionType, Integer de, Integer a, String soloEstado) throws FileNotFoundException, IOException, XmlRpcException, JDOMException {
        DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
            SAXBuilder builder = new SAXBuilder();

            Integer totalTransacciones = Integer.valueOf(0);
            Integer totalProcesadas = Integer.valueOf(0);

            Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
            Vector<Object> filterDeleted = new Vector();
            filterDeleted.addElement("deleted");
            filterDeleted.addElement("=");
            filterDeleted.addElement(Boolean.FALSE);
            vectorOfVectorFilterTuples.add(filterDeleted);
            Object[] trans_ids = openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(100000000), "update_required desc");

            totalTransacciones = Integer.valueOf(trans_ids.length);
            Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR a reprocesar: " + totalTransacciones);

            Object[] trans_id = new Object[1];
            for (int i = 0; i < trans_ids.length; i++) {
                trans_id[0] = trans_ids[i];

                if (i >= de.intValue() && i <= a.intValue()) {

                    Logger.getLogger(getClass().getSimpleName()).info("Procesando " + (i + 1) + " de " + totalTransacciones);
                    Vector<String> fieldNames = new Vector();
                    fieldNames.add("guid");
                    fieldNames.add("state");
                    HashMap<Object, Object> transRead = (HashMap<Object, Object>) openerpClient.read(transactionType.getOpenerpModel(), trans_id, fieldNames)[0];

                    Object wrOpenerpId = transRead.get("id");
                    String transGuid = transRead.get("guid").toString();
                    String transState = transRead.get("state").toString();

                    Vector<Vector<Object>> filtrosXml = new Vector<Vector<Object>>();
                    Vector<Object> filtroWrId = new Vector();
                    filtroWrId.add("wr_id");
                    filtroWrId.add("=");
                    filtroWrId.add(wrOpenerpId);
                    filtrosXml.add(filtroWrId);
                    Object[] xml_ids = openerpClient.search("courier.magaya.wr.xml", filtrosXml, Integer.valueOf(0), Integer.valueOf(1), "name desc");

                    if (xml_ids.length > 0 && soloEstado != null && soloEstado.equals(transState)) {

                        totalProcesadas = Integer.valueOf(totalProcesadas.intValue() + 1);

                        String xmlTranss = XmlRepository.getInstance().getUltimoXmlPara(TransactionType.WarehouseReceipt, transGuid);
                        Logger.getLogger(getClass().getSimpleName()).info("Tamaño XML: " + MagayaWS.getHumanSize(Integer.valueOf((xmlTranss != null) ? xmlTranss.length() : 0)));

                        int reintentosRestantes = 5;
                        while (xmlTranss != null && reintentosRestantes > 0) {
                            Logger.getLogger(getClass().getSimpleName()).info("Parseando XML...");
                            Reader in = new StringReader(xmlTranss);
                            Document document = builder.build(in);
                            Element nodoTrans = document.getRootElement();

                            try {
                                Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                                Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));

                                Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + transactionType.getCode() + ")...");
                                Logger.getLogger(getClass().getSimpleName()).info("Actualizada con exito, guid: " + nodoTrans.getAttributeValue("GUID"));
                                actualizar(transactionType, wrOpenerpId, nodoTrans, openerpClient, false);
                                reintentosRestantes = 0;
                            } catch (Exception e) {
                                Logger.getLogger(getClass().getSimpleName()).warn("Ocurrio un error al procesar la transaccion: " + nodoTrans.getAttributeValue("GUID") + "\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
                                reintentosRestantes--;
                                Logger.getLogger(getClass().getSimpleName()).warn("Reintentos restantes: " + reintentosRestantes);
                                try {
                                    Thread.sleep(5000L);
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }

            Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR: " + totalTransacciones);
            Logger.getLogger(getClass().getSimpleName()).info("Total WR procesados: " + totalProcesadas);
        } catch (XmlRpcException e1) {

            e1.printStackTrace();
        } catch (JDOMException e) {

        } catch (IOException e) {

        }
    }

    /**
     * java -cp magaya_openerp_lib/ -jar magaya_openerp.jar
     * pidfile=manual_download_consola accion=DescargarTransaccion
     * trans=WarehouseReceipt number=1514772
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException, XmlRpcException, JDOMException {
        if (getArg(args, "pidfile") != null) {
            String pidfile = getArg(args, "pidfile");

            MDC.put("pidfile", pidfile);

            if (!corriendo(pidfile)) {
                marcarCorriendo(pidfile);

                try {
                    if (getArg(args, "accion") != null) {
                        String accion = getArg(args, "accion");

                        if (accion.contains("VerificadorActualizaciones")) {
                            Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "desde")) : null;
                            Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "hasta")) : null;
                            String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment,CargoRelease").split(",");
                            Boolean guardarFechaFin = Boolean.valueOf((getArg(args, "guardarFechaFin") != null) ? Boolean.valueOf(getArg(args, "guardarFechaFin")).booleanValue() : true);

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
                            for (int i = 0; i < trans.length; i++) {
                                sincronizadorWRMagayaToOERP.chequearCambiosTransacciones(TransactionType.valueOf(trans[i]), desde, hasta, Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), guardarFechaFin);
                            }
                        }

                        if (accion.contains("Actualizador")) {
                            Boolean descargarAdjuntos = Boolean.valueOf((getArg(args, "descargarAdjuntos") != null) ? Boolean.valueOf(getArg(args, "descargarAdjuntos")).booleanValue() : true);
                            Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "desde")) : null;
                            Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "hasta")) : null;
                            String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment,CargoRelease").split(",");

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
                            for (int i = 0; i < trans.length; i++) {
                                sincronizadorWRMagayaToOERP.actualizarTransacciones(TransactionType.valueOf(trans[i]), desde, hasta, descargarAdjuntos);
                            }
                        }

                        if (accion.contains("ChequearVerificaciones")) {
                            Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "desde")) : null;
                            Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "hasta")) : null;
                            String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment,CargoRelease").split(",");
                            Boolean guardarFechaFin = Boolean.valueOf((getArg(args, "guardarFechaFin") != null) ? Boolean.valueOf(getArg(args, "guardarFechaFin")).booleanValue() : true);

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
                            for (int i = 0; i < trans.length; i++) {
                                sincronizadorWRMagayaToOERP.chequearVerificaciones(TransactionType.valueOf(trans[i]), desde, hasta, guardarFechaFin);
                            }
                        }

                        if (accion.equals("DescargaMasiva")) {
                            Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd")).parse(getArg(args, "desde")) : null;
                            Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd")).parse(getArg(args, "hasta")) : null;
                            Boolean soloOnHand = Boolean.valueOf((getArg(args, "soloOnHand") != null) ? Boolean.valueOf(getArg(args, "soloOnHand").toLowerCase()).booleanValue() : false);
                            String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment").split(",");

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
                            for (int i = 0; i < trans.length; i++) {
                                sincronizadorWRMagayaToOERP.descargarMasivamenteTransacciones(TransactionType.valueOf(trans[i]), desde, hasta, soloOnHand);
                            }
                        }

                        if (accion.equals("DescargarTransaccion")) {
                            String user = null;
                            String pass = null;
                            String db = null;
                            String host = null;
                            String port = null;
                            Properties props = new Properties();

                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
                            props.load(new FileInputStream("operp_db.conf"));

                            db = props.getProperty("db");
                            user = props.getProperty("user");
                            pass = props.getProperty("pass");
                            host = props.getProperty("host");
                            port = props.getProperty("port");
                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + db + "\"");

                            SincronizadorWRMagayaToOERP.openerpClient = new OpenerpClient();
                            SincronizadorWRMagayaToOERP.openerpClient.login(db, user, pass, host, port);

                            magayaWS = new MagayaWS();
                            HashMap<String, Object> configMagaya = SincronizadorWRMagayaToOERP.openerpClient.getConfigMagaya();
                            magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

                            if (getArg(args, "number") != null || getArg(args, "guid") != null) {
                                TransactionType transType = TransactionType.valueOf(getArg(args, "trans"));

                                SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
                                String xmlTranss = null;
                                if (getArg(args, "number") != null) {
                                    String number = getArg(args, "number");
                                    String[] trx = number.split(",");
                                    if (getArg(args, "rango") != null && getArg(args, "rango").equals("1")) {
                                        Integer trxIni = Integer.parseInt(trx[0]);
                                        Integer trxFin = Integer.parseInt(trx[1]);

                                        for (int i = trxIni; i <= trxFin; i++) {
                                            String txtItem = String.valueOf(i);
                                            xmlTranss = sincronizadorWRMagayaToOERP.getTransaction(transType, txtItem, 65, magayaWS);
                                            sincronizadorWRMagayaToOERP.procesarXmlTrans(xmlTranss, getArg(args, "solo-descargar"), sincronizadorWRMagayaToOERP, transType);
                                            //                                      Logger.getLogger("SincronizadorWRMagayaToOERP").info(xmlTranss);
                                        }

                                    } else {
                                        for (String txtItem : trx) {
                                            xmlTranss = sincronizadorWRMagayaToOERP.getTransaction(transType, txtItem, 65, magayaWS);
                                            sincronizadorWRMagayaToOERP.procesarXmlTrans(xmlTranss, getArg(args, "solo-descargar"), sincronizadorWRMagayaToOERP, transType);
                                            //                                      Logger.getLogger("SincronizadorWRMagayaToOERP").info(xmlTranss);
                                        }
                                    }

                                } else {
                                    xmlTranss = sincronizadorWRMagayaToOERP.getTransaction(transType, getArg(args, "guid"), 65, magayaWS);
                                    sincronizadorWRMagayaToOERP.procesarXmlTrans(xmlTranss, getArg(args, "solo-descargar"), sincronizadorWRMagayaToOERP, transType);
//                                    Logger.getLogger("SincronizadorWRMagayaToOERP").info(xmlTranss);

                                }

                            } else {

                                Logger.getLogger("SincronizadorWRMagayaToOERP").error("Debe especificar el parametro \"guid\" o \"number\"");
                            }
                        }
                        if (accion.equals("VerificarFaltantes")) {
                            String user = null;
                            String pass = null;
                            String db = null;
                            String host = null;
                            String port = null;
                            Properties props = new Properties();

                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
                            props.load(new FileInputStream("operp_db.conf"));

                            db = props.getProperty("db");
                            user = props.getProperty("user");
                            pass = props.getProperty("pass");
                            host = props.getProperty("host");
                            port = props.getProperty("port");
                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + db + "\"");

                            OpenerpClient openerpClient = new OpenerpClient();
                            openerpClient.login(db, user, pass, host, port);

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();

                            HashMap<String, String> guidsEnMagaya = sincronizadorWRMagayaToOERP.getWRsGUIDsExistentesEnMagaya();
                            HashMap<String, String> guidsEnOERP = sincronizadorWRMagayaToOERP.getWRsGUIDsExistentesEnOERP();

                            Iterator<String> itEnOpenerp = guidsEnOERP.keySet().iterator();
                            while (itEnOpenerp.hasNext()) {
                                String guidEnOERP = itEnOpenerp.next();
                                if (guidsEnMagaya.containsKey(guidEnOERP)) {
                                    guidsEnMagaya.remove(guidEnOERP);
                                }
                            }
                            Logger.getLogger("SincronizadorWRMagayaToOERP").warn("WRs faltantes: " + guidsEnMagaya.keySet().size());
                            Iterator<String> itFaltantes = guidsEnMagaya.keySet().iterator();
                            while (itFaltantes.hasNext()) {
                                String guidFaltante = itFaltantes.next();
                                Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR Faltante: " + guidFaltante + "  ----  Marcado para crear");
                                Object id = sincronizadorWRMagayaToOERP.marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guidFaltante, null, null);
                                Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR OERP ID: " + id.toString());
                            }
                        }

                        if (accion.equals("VerificarFaltantesPorNumero")) {
                            String user = null;
                            String pass = null;
                            String db = null;
                            String host = null;
                            String port = null;
                            Properties props = new Properties();

                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
                            props.load(new FileInputStream("operp_db.conf"));

                            db = props.getProperty("db");
                            user = props.getProperty("user");
                            pass = props.getProperty("pass");
                            host = props.getProperty("host");
                            port = props.getProperty("port");
                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + db + "\"");

                            OpenerpClient openerpClient = new OpenerpClient();
                            openerpClient.login(db, user, pass, host, port);

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();

                            HashMap<String, String> wrNumsEnMagaya = sincronizadorWRMagayaToOERP.getWRsNumbersFromCSVs(getArg(args, "csvs"));
                            HashMap<String, String> wrNumsEnOERP = sincronizadorWRMagayaToOERP.getWRsNumbersExistentesEnOERP();

                            Iterator<String> itEnOpenerp = wrNumsEnOERP.keySet().iterator();
                            while (itEnOpenerp.hasNext()) {
                                String guidEnOERP = itEnOpenerp.next();
                                if (wrNumsEnMagaya.containsKey(guidEnOERP)) {
                                    wrNumsEnMagaya.remove(guidEnOERP);
                                }
                            }
                            Logger.getLogger("SincronizadorWRMagayaToOERP").warn("WRs faltantes: " + wrNumsEnMagaya.keySet().size());

                            if (getArg(args, "descargarFaltantes") != null) {

                                HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
                                magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

                                SAXBuilder builder = new SAXBuilder();

                                Iterator<String> itFaltantes = wrNumsEnMagaya.keySet().iterator();
                                while (itFaltantes.hasNext()) {
                                    String wrNumberFaltante = itFaltantes.next();

                                    try {
                                        String xmlTrans = sincronizadorWRMagayaToOERP.getTransaction(TransactionType.WarehouseReceipt, wrNumberFaltante, Integer.valueOf(546), magayaWS);

                                        Reader in = new StringReader(xmlTrans);
                                        Document document = builder.build(in);
                                        Element rootNode = document.getRootElement();

                                        String guid = rootNode.getAttributeValue("GUID");

                                        Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR Faltante: " + wrNumberFaltante + " (" + guid + ")" + "  ----  Marcado para crear");
                                        try {
                                            Object id = sincronizadorWRMagayaToOERP.marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guid, null, null);
                                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR OERP ID: " + id.toString());
                                        } catch (Exception e) {
                                            Logger.getLogger("SincronizadorWRMagayaToOERP").error("Error al intentar marcar para crear, intentando marcar para actualizar...");
                                            sincronizadorWRMagayaToOERP.marcarParaActualizarTransaccion(TransactionType.WarehouseReceipt, guid, openerpClient, Integer.valueOf(1), (Date) null, (Date) null);
                                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Transaccion marcada para actualizar");
                                        }

                                    } catch (Exception e) {
                                        Logger.getLogger("SincronizadorWRMagayaToOERP").error("Error al procesar transaccion faltante, numero: " + wrNumberFaltante, e);
                                    }
                                }
                            }
                        }
                        if (accion.equals("Reprocesar")) {
                            Integer de = Integer.valueOf((getArg(args, "de") != null) ? Integer.valueOf(getArg(args, "de")).intValue() : 0);
                            Integer a = Integer.valueOf((getArg(args, "a") != null) ? Integer.valueOf(getArg(args, "a")).intValue() : Integer.MAX_VALUE);
                            String soloEstado = getArg(args, "soloEstado");

                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();

                            sincronizadorWRMagayaToOERP.reprocesarTransaccionesDesdeUltimosXMLs(TransactionType.WarehouseReceipt, de, a, soloEstado);
                        }

                        if (accion.equals("PruebaQuerylog")) {
                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("QueryLog, modificaciones entre 2013-01-31T08:00:00-0500 y 2013-01-31T15:00:00-0500");

                            magayaWS = new MagayaWS();
                            magayaWS.login("API", "Ecuador153");
                            String respuesta = magayaWS.queryLog("WH", "2013-01-31T12:00:00-0500", "2013-01-31T21:00:00-0500", Integer.valueOf(4), Integer.valueOf(0));

                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("######################################################################");
                            Logger.getLogger("SincronizadorWRMagayaToOERP").info(respuesta);
                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("######################################################################");
                        }

                        if (accion.equals("MigrarXMLsDeOpenerpADirectorio")) {
                            SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
                            sincronizadorWRMagayaToOERP.migrarUltimosXmls(TransactionType.WarehouseReceipt);
                        }

                        if (accion.equals("MigrarAttachments")) {
                            String userO = "admin";
                            String passO = "arri4ta";
                            String dbO = "siati_at2";
                            String dbD = "siati_v3";
                            String hostO = "http:localhost";
                            String portO = "8069";

                            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + dbO + "\"");
                            OpenerpClient openerpClientO = new OpenerpClient();
                            openerpClientO.login(dbO, userO, passO, hostO, portO);

                            OpenerpClient openerpClientD = new OpenerpClient();
                            openerpClientD.login(dbD, userO, passO, hostO, portO);

                            Vector<Vector<Object>> vectorOfVectorFilterTuplesModelos = new Vector<Vector<Object>>();
                            Object[] ids_modelos = openerpClientO.search("ir.model", vectorOfVectorFilterTuplesModelos);

                            Vector<String> fieldNamesModelos = new Vector();
                            fieldNamesModelos.add("model");
                            Object[] readModelos = openerpClientO.read("ir.model", ids_modelos, fieldNamesModelos);

                            for (int m = 0; m < readModelos.length; m++) {
                                HashMap<Object, Object> hashModelo = (HashMap<Object, Object>) readModelos[m];

                                Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
                                Vector<Object> filtro = new Vector();
                                filtro.add("res_model");
                                filtro.add("=");
                                filtro.add(hashModelo.get("model"));
                                vectorOfVectorFilterTuples.add(filtro);

                                Object[] ids_origen = openerpClientO.search("ir.attachment", vectorOfVectorFilterTuples);

                                Vector<String> fieldNames = new Vector();
                                fieldNames.add("datas");
                                fieldNames.add("res_id");
                                fieldNames.add("res_model");
                                fieldNames.add("name");
                                for (int i = 0; i < ids_origen.length; i++) {
                                    Object readO = openerpClientO.read("ir.attachment", new Object[]{ids_origen[i]}, fieldNames);

                                    HashMap<Object, Object> hash = (HashMap<Object, Object>) ((Object[]) readO)[0];
                                    openerpClientD.write("ir.attachment", ids_origen[i], hash);
                                }

                            }
                        }
                    } else {
                        System.out.println("Error: Debe especificar el parametro \"accion\" con valores \"VerificadorActualizaciones\", \"Actualizador\" o \"DescargaMasiva\"\nEjemplos de uso:\n\njava -cp magaya_java_lib -jar magaya_openerp.jar accion=VerificadorActualizaciones desde=2012-10-24T08:00:00 hasta=2012-10-24T20:00:00 trans=WarehouseReceipt,Shipment\n\njava -cp magaya_java_lib -jar magaya_openerp.jar accion=Actualizador desde=2012-10-24T08:00:00 hasta=2012-10-24T20:00:00 trans=WarehouseReceipt,Shipment\n\njava -cp magaya_java_lib -jar magaya_openerp.jar accion=DescargaMasiva desde=2012-10-24 hasta=2012-10-24 trans=WarehouseReceipt,Shipment soloOnHand=true\n\n(Los parametros desde, hasta y trans son opcionales.\nSi no se especifica desde, se hara desde la fecha de la ultima transaccion existente en openerp. Si no existen transacciones, se hara desde la fecha de inicio seteada en la configuracion de magaya en openerp.\nSi no se especifica hasta, se hara hasta el momento en que se ejecute esta aplicacion.\nSi no se especifica trans, se procesaran tanto Shipment como WarehouseReceipt. Si se especifica, se procesaran los tipos de transacciones especificadas, en el orden especificado)\nNOTA: en el caso de DescargaMasiva, desde y hasta son solo fechas, en los otros casos son fechas y horas. Los formatos son como se muestra en los ejemplos");
                    }
                } catch (Exception e) {

                }
                //TODO eliminar comentario
                desmarcarCorriendo(pidfile);
            } else {
                Logger.getLogger("SincronizadorWRMagayaToOERP").warn("Ya hay una instancia del sincronizador ejecutandose (el archivo \"" + pidfile + "\" existe)");
            }
        } else {
            Logger.getLogger("SincronizadorWRMagayaToOERP").warn("DEBE especificar el parametro pidfile");
        }

    }

    private void procesarXmlTrans(String xmlTranss, String soloDescargar, SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP, TransactionType transType) throws JDOMException, IOException, XmlRpcException {
        SAXBuilder builder = new SAXBuilder();

        xmlTranss = xmlTranss.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
        Logger.getLogger("SincronizadorWRMagayaToOERP").info("Parseando respuesta...");
        Reader in = new StringReader(xmlTranss);
        Document document = builder.build(in);
        Element nodoTrans = document.getRootElement();

        if (soloDescargar == null) {

            Object id = null;
            if (!SincronizadorWRMagayaToOERP.openerpClient.existeTransaccion(transType, nodoTrans.getAttributeValue("GUID"))) {
                Logger.getLogger("SincronizadorWRMagayaToOERP").info("La transaccion no existe aún, se crea y actualiza");
                id = sincronizadorWRMagayaToOERP.marcarParaCrearTransaccion(transType, SincronizadorWRMagayaToOERP.openerpClient, nodoTrans.getAttributeValue("GUID"), null, null);
            } else {

                Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();

                Vector<Object> filterGuid = new Vector();
                filterGuid.addElement("guid");
                filterGuid.addElement("=");
                filterGuid.addElement(nodoTrans.getAttributeValue("GUID"));
                vectorOfVectorFilterTuples.add(filterGuid);

                Object[] trans_ids = SincronizadorWRMagayaToOERP.openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(1), "update_required desc, created_on desc");
                id = trans_ids[0];
                Logger.getLogger("SincronizadorWRMagayaToOERP").warn("La transaccion ya existe, se actualiza");
            }

            sincronizadorWRMagayaToOERP.actualizar(transType, id, nodoTrans, SincronizadorWRMagayaToOERP.openerpClient);
            Logger.getLogger("SincronizadorWRMagayaToOERP").info("Transaccion descargada y actualizada correctamente");
        }

    }

    private void migrarUltimosXmls(TransactionType transactionType) throws FileNotFoundException, IOException {
        DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
            SAXBuilder builder = new SAXBuilder();

            Integer totalTransacciones = Integer.valueOf(0);
            Integer totalProcesadas = Integer.valueOf(0);

            Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
            Vector<Object> filterDeleted = new Vector();
            filterDeleted.addElement("deleted");
            filterDeleted.addElement("=");
            filterDeleted.addElement(Boolean.FALSE);
            vectorOfVectorFilterTuples.add(filterDeleted);
            Object[] trans_ids = openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(100000000), "update_required desc");

            totalTransacciones = Integer.valueOf(trans_ids.length);
            Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR a reprocesar: " + totalTransacciones);

            Object[] trans_id = new Object[1];
            for (int i = 0; i < trans_ids.length; i++) {
                trans_id[0] = trans_ids[i];

                Logger.getLogger(getClass().getSimpleName()).info("Procesando " + (i + 1) + " de " + totalTransacciones);
                Vector<String> fieldNames = new Vector();
                fieldNames.add("guid");
                fieldNames.add("state");
                HashMap<Object, Object> transRead = (HashMap<Object, Object>) openerpClient.read(transactionType.getOpenerpModel(), trans_id, fieldNames)[0];

                Object wrOpenerpId = transRead.get("id");
                String transGuid = transRead.get("guid").toString();
                String transState = transRead.get("state").toString();

                Vector<Vector<Object>> filtrosXml = new Vector<Vector<Object>>();
                Vector<Object> filtroWrId = new Vector();
                filtroWrId.add("wr_id");
                filtroWrId.add("=");
                filtroWrId.add(wrOpenerpId);
                filtrosXml.add(filtroWrId);
                Object[] xml_ids = openerpClient.search("courier.magaya.wr.xml", filtrosXml, Integer.valueOf(0), Integer.valueOf(1), "name desc");

                if (xml_ids.length > 0) {
                    totalProcesadas = Integer.valueOf(totalProcesadas.intValue() + 1);

                    Vector<String> fieldNamesXml = new Vector();
                    fieldNamesXml.add("xml_response");
                    HashMap<Object, Object> xmlRead = (HashMap<Object, Object>) openerpClient.read("courier.magaya.wr.xml", xml_ids, fieldNamesXml)[0];

                    String xmlTranss = xmlRead.get("xml_response").toString();

                    Logger.getLogger(getClass().getSimpleName()).info("Tamaño XML: " + MagayaWS.getHumanSize(Integer.valueOf((xmlTranss != null) ? xmlTranss.length() : 0)));
                    if (xmlTranss != null) {

                        Reader in = new StringReader(xmlTranss);
                        Document document = builder.build(in);
                        Element nodoTrans = document.getRootElement();

                        XmlRepository.getInstance().saveUltimoXmlPara(transactionType, transGuid, nodoTrans);
                        Logger.getLogger(getClass().getSimpleName()).info("Wr XML migrado al filesystem correctamente, GUID: " + transGuid);
                    } else {
                        Logger.getLogger(getClass().getSimpleName()).info("Error al migrar el Wr XML al filesystem, GUID: " + transGuid);
                    }
                }
            }

            Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR: " + totalTransacciones);
            Logger.getLogger(getClass().getSimpleName()).info("Total WR procesados: " + totalProcesadas);
        } catch (XmlRpcException e1) {

            e1.printStackTrace();
        } catch (JDOMException e) {

        } catch (IOException e) {

        }
    }

    private HashMap<String, String> getWRsNumbersFromCSVs(String csvFileNames) throws IOException {
        HashMap<String, String> hash = new HashMap<String, String>();
        String[] fileNames = csvFileNames.split(",");
        for (int i = 0; i < fileNames.length; i++) {
            CsvReader wrs = new CsvReader(fileNames[i]);
            wrs.readHeaders();
            while (wrs.readRecord()) {
                String wrNumber = wrs.get("Number");
                hash.put(wrNumber, wrNumber);
            }
            wrs.close();
        }
        return hash;
    }

    private static String getArg(String[] args, String parametro) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].contains("=") && args[i].split("=")[0].equals(parametro)) {
                return args[i].split("=")[1];
            }
        }
        return null;
    }

    private static void desmarcarCorriendo(String pidfile) {
        try {
            Runtime.getRuntime().exec(new String[]{"binbash", "-c", "rm " + pidfile}).waitFor();
        } catch (IOException e) {

        } catch (InterruptedException e) {

        }
        Logger.getLogger("SincronizadorWRMagayaToOERP").info(String.valueOf(pidfile) + " eliminado");
    }

    private static void marcarCorriendo(String pidfile) {
        Logger.getLogger("SincronizadorWRMagayaToOERP").info("Creando archivo \"" + pidfile + "\"...");
        try {
            (new File(pidfile)).createNewFile();
        } catch (IOException e) {

        }
    }

    private static boolean corriendo(String pidfile) {
        return (new File(pidfile)).exists();
    }

    public void getTransaccion(TransactionType transactionType, String wrNumber) throws FileNotFoundException, IOException {
        DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        try {
            HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();

            magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

            SAXBuilder builder = new SAXBuilder();

            Logger.getLogger(getClass().getSimpleName()).info("Obteniendo transaccion " + transactionType.getCode() + " : Numero: " + wrNumber + " ...");
            String xmlTranss = magayaWS.getTransaction(transactionType.getCode(), wrNumber, Integer.valueOf(546));
            if (xmlTranss != null) {
                xmlTranss = xmlTranss.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
                Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
                Reader in = new StringReader(xmlTranss);
                Document document = builder.build(in);
                Element nodoTrans = document.getRootElement();

                try {
                    Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
                    Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));

                    Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + transactionType.getCode() + ")...");
                } catch (Exception e) {
                    Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
                }
            } else {

                Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta, se elimina fisicamente la transaccion. ");

            }

        } catch (XmlRpcException e1) {

            e1.printStackTrace();
        } catch (JDOMException e) {

        } catch (IOException e) {

        }
    }

    public HashMap<String, String> getWRsGUIDsExistentesEnMagaya() throws JDOMException, IOException, XmlRpcException {
        HashMap<String, String> hashGuidWrNumber = new HashMap<String, String>();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
        magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());

        Date desde = null;
        try {
            desde = formater.parse("2012-10-01");
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Date hasta = new Date();

        SAXBuilder builder = new SAXBuilder();

        Logger.getLogger(getClass().getSimpleName()).info("Verificando WRs faltantes entre " + formater.format(desde) + " y " + formater.format(hasta) + " ...");
        Logger.getLogger(getClass().getSimpleName()).info("Descargando log de creaciones...");
        String xmlTransCreadas = magayaWS.queryLog(TransactionType.WarehouseReceipt.getCode(), formater.format(desde), formater.format(hasta), Integer.valueOf(1), Integer.valueOf(0));
        xmlTransCreadas = xmlTransCreadas.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
        Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

        Reader in = new StringReader(xmlTransCreadas);
        Document document = builder.build(in);
        Element rootNode = document.getRootElement();
        List<Element> nodosTrans = rootNode.getChildren("GUIDItem");
        Logger.getRootLogger().info("Creaciones obtenidas: " + nodosTrans.size());
        for (Element nodoTransGuid : nodosTrans) {
            try {
                hashGuidWrNumber.put(nodoTransGuid.getChildText("GUID"), nodoTransGuid.getChildText("GUID"));
            } catch (Exception e) {
                Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
            }
        }

        Logger.getLogger(getClass().getSimpleName()).info("Descargando log de modificaciones...");
        String xmlTransModificadas = magayaWS.queryLog(TransactionType.WarehouseReceipt.getCode(), formater.format(desde), formater.format(hasta), Integer.valueOf(4), Integer.valueOf(0));
        xmlTransModificadas = xmlTransModificadas.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
        Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

        in = new StringReader(xmlTransModificadas);
        document = builder.build(in);
        rootNode = document.getRootElement();
        nodosTrans = rootNode.getChildren("GUIDItem");
        Logger.getRootLogger().info("Modificaciones obtenidas: " + nodosTrans.size());
        for (Element nodoTransGuid : nodosTrans) {
            try {
                hashGuidWrNumber.put(nodoTransGuid.getChildText("GUID"), nodoTransGuid.getChildText("GUID"));
            } catch (Exception e) {
                Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
            }
        }

        Logger.getLogger(getClass().getSimpleName()).info("Descargando log de eliminaciones...");
        String xmlTransEliminadas = magayaWS.queryLog(TransactionType.WarehouseReceipt.getCode(), formater.format(desde), formater.format(hasta), Integer.valueOf(2), Integer.valueOf(0));
        xmlTransEliminadas = xmlTransEliminadas.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
        Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

        in = new StringReader(xmlTransEliminadas);
        document = builder.build(in);
        rootNode = document.getRootElement();
        nodosTrans = rootNode.getChildren("GUIDItem");
        Logger.getRootLogger().info("Creaciones obtenidas: " + nodosTrans.size());
        for (Element nodoTransGuid : nodosTrans) {
            try {
                if (hashGuidWrNumber.containsKey(nodoTransGuid.getChildText("GUID"))) {
                    hashGuidWrNumber.remove(nodoTransGuid.getChildText("GUID"));
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
            }
        }
        Logger.getLogger(getClass().getSimpleName()).info("Descarga completa");
        Logger.getLogger(getClass().getSimpleName()).info("Total de transacciones que deben existir (desde que se utiliza la funcion logging): " + hashGuidWrNumber.keySet().size());

        return hashGuidWrNumber;
    }

    private HashMap<String, String> getWRsGUIDsExistentesEnOERP() throws FileNotFoundException, IOException, XmlRpcException {
        HashMap<String, String> hashGuidWrNumber = new HashMap<String, String>();
        OpenerpClient openerpClient = new OpenerpClient();
        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        Vector<Object> filtroNoEliminadas = new Vector();
        filtroNoEliminadas.add("deleted");
        filtroNoEliminadas.add("=");
        filtroNoEliminadas.add(Boolean.FALSE);
        vectorOfVectorFilterTuples.add(filtroNoEliminadas);
        Object[] idsNoEliminadas = openerpClient.search("courier.magaya.wr", vectorOfVectorFilterTuples);

        Vector<String> fieldNames = new Vector();
        fieldNames.add("guid");
        Object[] guidsVector = openerpClient.read("courier.magaya.wr", idsNoEliminadas, fieldNames);
        for (int i = 0; i < guidsVector.length; i++) {
            hashGuidWrNumber.put(((HashMap<?, Vector>) guidsVector[i]).get("guid").toString(), ((HashMap<?, Vector>) guidsVector[i]).get("guid").toString());
        }
        return hashGuidWrNumber;
    }

    private HashMap<String, String> getWRsNumbersExistentesEnOERP() throws FileNotFoundException, IOException, XmlRpcException {
        HashMap<String, String> hashGuidWrNumber = new HashMap<String, String>();
        OpenerpClient openerpClient = new OpenerpClient();
        String user = null;
        String pass = null;
        String db = null;
        String host = null;
        String port = null;
        Properties props = new Properties();

        Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
        props.load(new FileInputStream("operp_db.conf"));

        db = props.getProperty("db");
        user = props.getProperty("user");
        pass = props.getProperty("pass");
        host = props.getProperty("host");
        port = props.getProperty("port");
        Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");

        openerpClient.login(db, user, pass, host, port);

        Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        Vector<Object> filtroNoEliminadas = new Vector();
        filtroNoEliminadas.add("deleted");
        filtroNoEliminadas.add("=");
        filtroNoEliminadas.add(Boolean.FALSE);
        vectorOfVectorFilterTuples.add(filtroNoEliminadas);
        Object[] idsNoEliminadas = openerpClient.search("courier.magaya.wr", vectorOfVectorFilterTuples);

        Vector<String> fieldNames = new Vector();
        fieldNames.add("name");
        Object[] guidsVector = openerpClient.read("courier.magaya.wr", idsNoEliminadas, fieldNames);
        for (int i = 0; i < guidsVector.length; i++) {
            hashGuidWrNumber.put(((HashMap<?, Vector>) guidsVector[i]).get("name").toString(), ((HashMap<?, Vector>) guidsVector[i]).get("name").toString());
        }
        return hashGuidWrNumber;
    }

    public String getTransaction(TransactionType transType, String numberOrGUID, Integer flags, MagayaWS magayaWS) throws JDOMException, IOException, XmlRpcException {
        SAXBuilder builder = new SAXBuilder();

        Logger.getLogger(getClass().getSimpleName()).info("Descargando: " + numberOrGUID + " ...");
        String xmlTrans = magayaWS.getTransaction(transType.getCode(), numberOrGUID, flags);
        xmlTrans = xmlTrans.replace("xmlns=\"http:www.magaya.comXMLSchemaV1\"", "");
        Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");

        Reader in = new StringReader(xmlTrans);
        Document document = builder.build(in);
        Element rootNode = document.getRootElement();

        xmlTrans = (new XMLOutputter(Format.getPrettyFormat())).outputString(rootNode);

        return xmlTrans;
    }
}
