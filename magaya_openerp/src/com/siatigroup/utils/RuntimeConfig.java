/*    */ package com.siatigroup.utils;
/*    */ 
/*    */ import java.io.FileInputStream;
/*    */ import java.io.FileNotFoundException;
/*    */ import java.io.IOException;
/*    */ import java.util.Properties;
/*    */ 
/*    */ public class RuntimeConfig
/*    */ {
/* 10 */   private static RuntimeConfig _instance = null;
/* 11 */   Properties props = null;
/*    */   
/*    */   private RuntimeConfig() {
/* 14 */     this.props = new Properties();
/*    */     try {
/* 16 */       this.props.load(new FileInputStream("operp_db.conf"));
/* 17 */     } catch (FileNotFoundException e) {
/* 18 */       e.printStackTrace();
/* 19 */     } catch (IOException e) {
/* 20 */       e.printStackTrace();
/*    */     } 
/*    */   }
/*    */   
/*    */   public static RuntimeConfig getInstance() {
/* 25 */     if (_instance == null)
/* 26 */       _instance = new RuntimeConfig(); 
/* 27 */     return _instance;
/*    */   }
/*    */   
/*    */   public String getMagayaHostOrIp() {
/* 31 */     return (this.props.getProperty("magayaHostOrIp") != null) ? this.props.getProperty("magayaHostOrIp") : this.props.getProperty("magayahostorip");
/*    */   }
/*    */ 
/*    */   
/*    */   public String getMagayaPort() {
/* 36 */     return (this.props.getProperty("magayaPort") != null) ? this.props.getProperty("magayaPort") : this.props.getProperty("magayaport");
/*    */   }
/*    */ 
/*    */   
/*    */   public Boolean isHorarioVerano() {
/* 41 */     return (this.props.getProperty("horarioVerano") != null) ? Boolean.valueOf(this.props.getProperty("horarioVerano")) : Boolean.valueOf(this.props.getProperty("horarioverano"));
/*    */   }
/*    */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\RuntimeConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */