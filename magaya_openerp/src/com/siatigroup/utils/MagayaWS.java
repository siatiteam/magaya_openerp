package com.siatigroup.utils;

import cssoapservice.CSSoapServiceStub;
import cssoapservice.EndSession;
import cssoapservice.ExistsTransaction;
import cssoapservice.ExistsTransactionResponse;
import cssoapservice.GetAttachment;
import cssoapservice.GetAttachmentResponse;
import cssoapservice.GetEntities;
import cssoapservice.GetEntitiesResponse;
import cssoapservice.GetTransRangeByDate;
import cssoapservice.GetTransRangeByDateResponse;
import cssoapservice.GetTransaction;
import cssoapservice.GetTransactionResponse;
import cssoapservice.QueryLog;
import cssoapservice.QueryLogResponse;
import cssoapservice.StartSession;
import cssoapservice.StartSessionResponse;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class MagayaWS {

    private Integer access_key;
    private CSSoapServiceStub stub;
    private Logger logger;

    public MagayaWS() {
        /*  38 */ BasicConfigurator.configure();
        /*  39 */ this.logger = Logger.getLogger(getClass().getSimpleName());

        try {
            /*  46 */ this.stub = new CSSoapServiceStub();
            /*  47 */ this.stub._getServiceClient().getOptions().setProperty("__CHUNKED__", Boolean.FALSE);

        } /*  51 */ catch (RemoteException e) {
            /*  52 */ this.logger.fatal("", e);
        }
    }

    public boolean login(String user, String pass) {
        try {
            /*  58 */ StartSession startSession = new StartSession();
            /*  59 */ startSession.setUser(user);
            /*  60 */ startSession.setPass(pass);

            /*  62 */ StartSessionResponse response = this.stub.startSession(startSession);
            /*  63 */ this.access_key = response.getAccess_key();

            //this.logger.info("Login OK!");
        } /*  67 */ catch (RemoteException e) {
            /*  68 */ this.logger.error("", e);
        }
        /*  70 */ return (this.access_key != null && this.access_key != 0);
    }

    public void logout() {
        try {
            /*  75 */ EndSession endSession = new EndSession();
            /*  76 */ endSession.setAccess_key(this.access_key);
            /*  77 */ this.stub.endSession(endSession);


            //this.logger.info("Logout OK, bye!");
/*  80 */        } catch (RemoteException e) {
            /*  81 */ this.logger.error("", e);
        }
    }

    public String getTransaction(String transTypeCode, String transNum, Integer flags) {
        /* 101 */ String xmlTransaction = null;
        try {
            /* 103 */ GetTransaction getTransaction = new GetTransaction();
            /* 104 */ getTransaction.setAccess_key(this.access_key);
            /* 105 */ getTransaction.setType(transTypeCode);
            /* 106 */ getTransaction.setNumber(transNum);
            /* 107 */ getTransaction.setFlags(flags);
            /* 108 */ GetTransactionResponse getTransactionResponse = this.stub.getTransaction(getTransaction);
            /* 109 */ xmlTransaction = getTransactionResponse.getTrans_xml();


            //this.logger.info("Tamaño transaccion: " + ((xmlTransaction != null) ? getHumanSize(Integer.valueOf(xmlTransaction.length())) : "(0 Bytes)"));
/* 112 */        } catch (RemoteException e) {
            /* 113 */ this.logger.error("", e);
        }
        /* 115 */ return xmlTransaction;
    }

    public static String getHumanSize(Integer bytes) {
        /* 121 */ if (bytes / 1024 < 1) /* 122 */ {
            return String.valueOf(DecimalFormat.getInstance().format(bytes)) + " B";
        }
        /* 123 */ if (bytes / 1048576 < 1) /* 124 */ {
            return String.valueOf(DecimalFormat.getInstance().format((bytes / 1024))) + " KB";
        }
        /* 125 */ if (bytes / 1073741824 < 1) {
            /* 126 */ return String.valueOf(DecimalFormat.getInstance().format((bytes / 1048576))) + " MB";
        }
        /* 128 */ return String.valueOf(DecimalFormat.getInstance().format((bytes / 1073741824))) + " GB";
    }

    public String getTransRangeByDate(String transTypeCode, String startDate, String endDate, Integer flags) {
        /* 142 */ String xmlTransaction = null;
        try {
            /* 144 */ GetTransRangeByDate getTransRangeByDate = new GetTransRangeByDate();
            /* 145 */ getTransRangeByDate.setAccess_key(this.access_key);
            /* 146 */ getTransRangeByDate.setType(transTypeCode);
            /* 147 */ getTransRangeByDate.setStart_date(startDate);
            /* 148 */ getTransRangeByDate.setEnd_date(endDate);
            /* 149 */ getTransRangeByDate.setFlags(flags);
            /* 150 */ GetTransRangeByDateResponse getTransRangeByDateResponse = this.stub.getTransRangeByDate(getTransRangeByDate);
            /* 151 */ xmlTransaction = getTransRangeByDateResponse.getTrans_list_xml();

        } /* 154 */ catch (RemoteException e) {
            /* 155 */ this.logger.error("", e);
        }
        /* 157 */ return xmlTransaction;
    }

    public String queryLog(String transTypeCode, String startDate, String endDate, Integer logEntryType, Integer flags) {
        /* 170 */ if (startDate != null && startDate.length() > 3) {
            /* 171 */ String nuevaFecha = "";
            /* 172 */ for (int i = 0; i < startDate.length(); i++) {
                /* 173 */ nuevaFecha = String.valueOf(nuevaFecha) + startDate.charAt(i);
                /* 174 */ if (i == 21) {
                    /* 175 */ nuevaFecha = String.valueOf(nuevaFecha) + ":";
                }
            }
            /* 178 */ startDate = nuevaFecha;
        }
        /* 180 */ if (endDate != null && endDate.length() > 3) {
            /* 181 */ String nuevaFecha = "";
            /* 182 */ for (int i = 0; i < endDate.length(); i++) {
                /* 183 */ nuevaFecha = String.valueOf(nuevaFecha) + endDate.charAt(i);
                /* 184 */ if (i == 21) {
                    /* 185 */ nuevaFecha = String.valueOf(nuevaFecha) + ":";
                }
            }
            /* 188 */ endDate = nuevaFecha;
        }

        /* 193 */ String xmlTransaction = null;
        try {
            /* 195 */ QueryLog queryLog = new QueryLog();
            /* 196 */ queryLog.setAccess_key(this.access_key);
            /* 197 */ queryLog.setTrans_type(transTypeCode);
            /* 198 */ queryLog.setLog_entry_type(logEntryType);
            /* 199 */ queryLog.setStart_date(startDate);
            /* 200 */ queryLog.setEnd_date(endDate);
            /* 201 */ queryLog.setFlags(flags);
            /* 202 */ QueryLogResponse queryLogResponse = this.stub.queryLog(queryLog);
            /* 203 */ xmlTransaction = queryLogResponse.getTrans_list_xml();
        } /* 205 */ catch (RemoteException e) {
            /* 206 */ this.logger.error("", e);
        }
        /* 208 */ return xmlTransaction;
    }

    public boolean existsTransaction(String transTypeCode, String transNum) {
        /* 213 */ boolean exist = false;
        try {
            /* 215 */ ExistsTransaction existsTransaction = new ExistsTransaction();
            /* 216 */ existsTransaction.setAccess_key(this.access_key);
            /* 217 */ existsTransaction.setType(transTypeCode);
            /* 218 */ existsTransaction.setNumber(transNum);

            /* 220 */ ExistsTransactionResponse existsTransactionResponse = this.stub.existsTransaction(existsTransaction);
            /* 221 */ exist = (existsTransactionResponse.getExist_trans() == 1);

        } /* 224 */ catch (RemoteException e) {
            /* 225 */ this.logger.error("", e);
        }
        /* 227 */ return exist;
    }

    public String getEntities(String startWith) {
        /* 231 */ String xmlEntities = null;

        try {
            /* 234 */ GetEntities getEntities = new GetEntities();
            /* 235 */ getEntities.setAccess_key(this.access_key);
            /* 236 */ getEntities.setStart_with(startWith);
            /* 237 */ getEntities.setFlags(0);

            /* 239 */ GetEntitiesResponse getEntitiesResponse = this.stub.getEntities(getEntities);
            /* 240 */ xmlEntities = getEntitiesResponse.getEntity_list_xml();
        } /* 242 */ catch (RemoteException e) {
            /* 243 */
        }

        /* 246 */
        return xmlEntities;
    }

    public String getAttachment(String ownerType, String ownerGuid, String attachmentId) {
        /* 252 */ String xmlTransaction = null;
        try {
            /* 254 */ GetAttachment getAttachment = new GetAttachment();
            /* 255 */ getAttachment.setAttach_id(Integer.parseInt(attachmentId));
            /* 256 */ getAttachment.setTrans_uuid(ownerGuid);
            /* 257 */ getAttachment.setApp(ownerType);
            /* 258 */ GetAttachmentResponse getTransactionResponse = this.stub.getAttachment(getAttachment);
            /* 259 */ xmlTransaction = getTransactionResponse.getAttach_xml();


            //this.logger.info("Tamaño attachment: " + ((xmlTransaction != null) ? getHumanSize(Integer.valueOf(xmlTransaction.length())) : "(0 Bytes)"));
/* 262 */        } catch (RemoteException e) {
            /* 263 */ this.logger.error("", e);
        }
        /* 265 */ return xmlTransaction;
    }
}
