package com.siatigroup.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class XmlRepository {

    private static XmlRepository _instance = null;
    private File dirXml;
    private File dirXmlWr;
    private File dirXmlSh;
    private File dirXmlCr;
    private String magayaXMLPath;
    private String magayaXMLPathWr;
    private String magayaXMLPathSh;
    private String magayaXMLPathCr;

    private XmlRepository() throws FileNotFoundException, IOException {
        Properties props = new Properties();
        props.load(new FileInputStream("operp_db.conf"));

        this.magayaXMLPath = (props.getProperty("magayaXMLPath") != null) ? props.getProperty("magayaXMLPath") : props.getProperty("magayaxmlpath");
        this.magayaXMLPathWr = String.valueOf(this.magayaXMLPath) + "/wr";
        this.magayaXMLPathSh = String.valueOf(this.magayaXMLPath) + "/sh";
        this.magayaXMLPathCr = String.valueOf(this.magayaXMLPath) + "/cr";

        this.dirXml = new File(this.magayaXMLPath);
        this.dirXmlWr = new File(this.magayaXMLPathWr);
        this.dirXmlSh = new File(this.magayaXMLPathSh);
        this.dirXmlCr = new File(this.magayaXMLPathCr);

        if (!this.dirXml.exists()) {
            this.dirXml.mkdir();
        }
        if (!this.dirXmlWr.exists()) {
            this.dirXmlWr.mkdir();
        }
        if (!this.dirXmlSh.exists()) {
            this.dirXmlSh.mkdir();
        }
        if (!this.dirXmlCr.exists()) {
            this.dirXmlCr.mkdir();
        }
    }

    public static XmlRepository getInstance() {
        if (_instance == null) {
            try {
                _instance = new XmlRepository();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return _instance;
    }

    public String getUltimoXmlPara(TransactionType type, String guid) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = null;
        switch (type) {
            case WarehouseReceipt:
                xmlFile = new File(String.valueOf(this.magayaXMLPathWr) + "/" + guid);
                break;
            case Shipment:
                xmlFile = new File(String.valueOf(this.magayaXMLPathSh) + "/" + guid);
                break;
            case CargoRelease:
                xmlFile = new File(String.valueOf(this.magayaXMLPathCr) + "/" + guid);
                break;
        }
        if (xmlFile.exists()) {
            Document document = builder.build(xmlFile);
            Element rootNode = document.getRootElement();
            return (new XMLOutputter(Format.getPrettyFormat())).outputString(rootNode);
        }

        return null;
    }

    public void saveUltimoXmlPara(TransactionType type, String guid, String xml) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = null;
        switch (type) {
            case WarehouseReceipt:
                xmlFile = new File(String.valueOf(this.magayaXMLPathWr) + "/" + guid);
                break;
            case Shipment:
                xmlFile = new File(String.valueOf(this.magayaXMLPathSh) + "/" + guid);
                break;
            case CargoRelease:
                xmlFile = new File(String.valueOf(this.magayaXMLPathCr) + "/" + guid);
                break;
        }

        Document document = builder.build(xml);
        XMLOutputter xmlOutput = new XMLOutputter();

        xmlOutput.setFormat(Format.getPrettyFormat());
        xmlOutput.output(document, new FileWriter(xmlFile));
    }

    public void saveUltimoXmlPara(TransactionType type, String guid, Element xml) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = null;
        switch (type) {
            case WarehouseReceipt:
                xmlFile = new File(String.valueOf(this.magayaXMLPathWr) + "/" + guid);
                break;
            case Shipment:
                xmlFile = new File(String.valueOf(this.magayaXMLPathSh) + "/" + guid);
                break;
            case CargoRelease:
                xmlFile = new File(String.valueOf(this.magayaXMLPathCr) + "/" + guid);
                break;
        }

        XMLOutputter xmlOutput = new XMLOutputter();

        xmlOutput.setFormat(Format.getPrettyFormat());
        xmlOutput.output(xml, new FileWriter(xmlFile));
    }
}
