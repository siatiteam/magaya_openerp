package com.siatigroup.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;

public class OpenerpClient {

    private Integer userId;
    private String dbName;
    private String user;
    private String pass;
    private String host;
    private String port;
    SimpleDateFormat formaterUtc;

    public OpenerpClient() {
        this.formaterUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.formaterUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public void login(String dbName, String user, String pass, String host, String port) {
        this.dbName = dbName;
        /*   41 */ this.user = user;
        /*   42 */ this.pass = pass;
        /*   43 */ this.host = host;
        /*   44 */ this.port = port;

        /*   46 */ XmlRpcClient client = new XmlRpcClient();
        /*   47 */ XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        try {
            /*   49 */ config.setServerURL(new URL(String.valueOf(host) + ":" + port + "/xmlrpc/common"));
            /*   50 */        } catch (MalformedURLException e) {
            /*   51 */ e.printStackTrace();
        }
        /*   53 */ client.setTransportFactory((XmlRpcTransportFactory) new XmlRpcSunHttpTransportFactory(client));
        /*   54 */ config.setEnabledForExtensions(true);
        /*   55 */ config.setEnabledForExceptions(true);
        /*   56 */ client.setConfig((XmlRpcClientConfig) config);

        /*   58 */ List<String> params = new ArrayList();
        /*   59 */ params.add(this.dbName);
        /*   60 */ params.add(this.user);
        /*   61 */ params.add(this.pass);

        try {
            /*   65 */ Object result = client.execute("login", params);
            /*   66 */ if (result instanceof Integer) {
                /*   67 */ this.userId = Integer.valueOf(((Integer) result).intValue());
            }
        } /*   70 */ catch (XmlRpcException e) {
            /*   71 */ this.userId = Integer.valueOf(0);
            /*   72 */ e.printStackTrace();
        }
    }

    public Date getDateInicioOperaciones() throws XmlRpcException {
        /*   79 */ XmlRpcClient client = getClienteXmlrpc();

        // Logger.getLogger(getClass().getSimpleName()).info("Obteniendo fecha comienzo de utilizacion de Magaya...");

        /*   83 */ Vector<Object> paramSearchConf = new Vector();
        /*   84 */ paramSearchConf.addElement(this.dbName);
        /*   85 */ paramSearchConf.addElement(this.userId);
        /*   86 */ paramSearchConf.addElement(this.pass);
        /*   87 */ paramSearchConf.addElement("courier.magaya.config");
        /*   88 */ paramSearchConf.add("search");

        /*   90 */ paramSearchConf.add(new Vector());

        /*   92 */ Object[] idsConf = (Object[]) client.execute("execute", paramSearchConf);
        /*   93 */ System.out.println(Arrays.toString(idsConf));

        /*   95 */ Vector<String> fieldsConf = new Vector();
        /*   96 */ fieldsConf.add("start_up_date");

        /*   98 */ Vector<Object> paramReadConf = new Vector();
        /*   99 */ paramReadConf.addElement(this.dbName);
        /*  100 */ paramReadConf.addElement(this.userId);
        /*  101 */ paramReadConf.addElement(this.pass);
        /*  102 */ paramReadConf.addElement("courier.magaya.config");
        /*  103 */ paramReadConf.add("read");
        /*  104 */ paramReadConf.add(idsConf);
        /*  105 */ paramReadConf.add(fieldsConf);
        /*  106 */ Object[] dataConf = (Object[]) client.execute("execute", paramReadConf);

        /*  108 */ Date fechaYHoraUltimaTransaccion = null;
        /*  109 */ String fechaConfStr = "";
        /*  110 */ if (dataConf.length != 0) {
            /*  111 */ fechaConfStr = ((HashMap<?, Vector>) dataConf[dataConf.length - 1]).get("start_up_date").toString();
        }
        try {
            /*  114 */ fechaYHoraUltimaTransaccion = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).parse(fechaConfStr);
            // Logger.getLogger(getClass()).info("Fecha ultima transaccion (se descargaran transacciones a partir de esta fecha): " + ((HashMap<?, ?>)dataConf[dataConf.length - 1]).get("start_up_date"));
/*  116 */        } catch (ParseException e1) {
            /*  117 */ Logger.getLogger(getClass()).fatal("No se pudo determinar la fecha de StartUp. Verifique que ha configurado correctamente el modulo courier_magaya_base en su base de datos de openerp, abortando...");
        }
        /*  119 */ return fechaYHoraUltimaTransaccion;
    }

    public Date getFechaYHoraUltimaTransaccionDescargada(String model, String dateFieldName) throws XmlRpcException {
        // Logger.getLogger(getClass().getSimpleName()).info("Obteniendo fecha de la ultima transaccion...");
/*  124 */ String url = String.valueOf(this.host) + ":" + this.port + "/xmlrpc/object";

        /*  126 */ XmlRpcClient client = new XmlRpcClient();
        /*  127 */ XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        try {
            /*  129 */ config.setServerURL(new URL(url));
            /*  130 */        } catch (MalformedURLException e) {
            /*  131 */ e.printStackTrace();
        }
        /*  133 */ client.setTransportFactory((XmlRpcTransportFactory) new XmlRpcSunHttpTransportFactory(client));
        /*  134 */ config.setEnabledForExtensions(true);
        /*  135 */ config.setEnabledForExceptions(true);
        /*  136 */ client.setConfig((XmlRpcClientConfig) config);

        /*  138 */ Vector<Object> paramSearch = new Vector();
        /*  139 */ paramSearch.addElement(this.dbName);
        /*  140 */ paramSearch.addElement(this.userId);
        /*  141 */ paramSearch.addElement(this.pass);

        /*  143 */ paramSearch.addElement(model);
        /*  144 */ paramSearch.add("search");

        /*  149 */ paramSearch.add(new Vector());
        /*  150 */ paramSearch.add(Integer.valueOf(0));
        /*  151 */ paramSearch.add(Integer.valueOf(10));
        /*  152 */ paramSearch.add(String.valueOf(dateFieldName) + " desc");

        /*  155 */ Object[] ids = (Object[]) client.execute("execute", paramSearch);
        /*  156 */ System.out.println(Arrays.toString(ids));

        /*  158 */ Vector<String> fields = new Vector();

        /*  160 */ fields.add(dateFieldName);

        /*  162 */ Vector<Object> paramRead = new Vector();
        /*  163 */ paramRead.addElement(this.dbName);
        /*  164 */ paramRead.addElement(this.userId);
        /*  165 */ paramRead.addElement(this.pass);
        /*  166 */ paramRead.addElement(model);
        /*  167 */ paramRead.add("read");
        /*  168 */ paramRead.add(ids);
        /*  169 */ paramRead.add(fields);
        /*  170 */ Object[] data = (Object[]) client.execute("execute", paramRead);

        /*  174 */ Date fechaYHoraUltimaTransaccion = null;
        /*  175 */ String fechaStr = "";
        /*  176 */ if (data.length != 0) {

            /*  179 */ fechaStr = ((HashMap<?, Vector>) data[0]).get(dateFieldName).toString();
        }
        try {
            /*  181 */ fechaYHoraUltimaTransaccion = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", new Locale("es", "EC"))).parse(fechaStr);
            // Logger.getLogger(getClass().getSimpleName()).info("Fecha ultima transaccion (se descargaran transacciones a partir de esta fecha): " + ((HashMap<?, ?>)data[0]).get(dateFieldName));
/*  183 */        } catch (ParseException e) {
            // Logger.getLogger(getClass().getSimpleName()).info("No se pudo determinar la fecha de la ultima transaccion.");
// Logger.getLogger(getClass().getSimpleName()).info("Obteniendo fecha comienzo de utilizacion de Magaya...");

            /*  187 */ Vector<Object> paramSearchConf = new Vector();
            /*  188 */ paramSearchConf.addElement(this.dbName);
            /*  189 */ paramSearchConf.addElement(this.userId);
            /*  190 */ paramSearchConf.addElement(this.pass);
            /*  191 */ paramSearchConf.addElement("courier.magaya.config");
            /*  192 */ paramSearchConf.add("search");

            /*  194 */ paramSearchConf.add(new Vector());

            /*  196 */ Object[] idsConf = (Object[]) client.execute("execute", paramSearchConf);
            /*  197 */ System.out.println(Arrays.toString(idsConf));

            /*  199 */ Vector<String> fieldsConf = new Vector();
            /*  200 */ fieldsConf.add("start_up_date");

            /*  202 */ Vector<Object> paramReadConf = new Vector();
            /*  203 */ paramReadConf.addElement(this.dbName);
            /*  204 */ paramReadConf.addElement(this.userId);
            /*  205 */ paramReadConf.addElement(this.pass);
            /*  206 */ paramReadConf.addElement("courier.magaya.config");
            /*  207 */ paramReadConf.add("read");
            /*  208 */ paramReadConf.add(idsConf);
            /*  209 */ paramReadConf.add(fieldsConf);
            /*  210 */ Object[] dataConf = (Object[]) client.execute("execute", paramReadConf);

            /*  212 */ String fechaConfStr = "";
            /*  213 */ if (dataConf.length != 0) {
                /*  214 */ fechaConfStr = ((HashMap<?, Vector>) dataConf[dataConf.length - 1]).get("start_up_date").toString();
            }
            try {
                /*  217 */ fechaYHoraUltimaTransaccion = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", new Locale("es", "EC"))).parse(fechaConfStr);
                // Logger.getLogger(getClass()).info("Fecha ultima transaccion (se descargaran transacciones a partir de esta fecha): " + ((HashMap<?, ?>)dataConf[dataConf.length - 1]).get("start_up_date"));
/*  219 */            } catch (ParseException e1) {
                /*  220 */ Logger.getLogger(getClass()).fatal("No se pudo determinar la fecha de StartUp. Verifique que ha configurado correctamente el modulo courier_magaya_base en su base de datos de openerp, abortando...");
            }
        }

        /*  224 */ return fechaYHoraUltimaTransaccion;
    }

    public HashMap<String, Object> getConfigMagaya() throws XmlRpcException {
        /*  228 */ HashMap<String, Object> hash = null;

        /*  230 */ XmlRpcClient client = getClienteXmlrpc();

        /*  232 */ Vector<Object> paramSearchConf = new Vector();
        /*  233 */ paramSearchConf.addElement(this.dbName);
        /*  234 */ paramSearchConf.addElement(this.userId);
        /*  235 */ paramSearchConf.addElement(this.pass);
        /*  236 */ paramSearchConf.addElement("courier.magaya.config");
        /*  237 */ paramSearchConf.add("search");

        /*  239 */ paramSearchConf.add(new Vector());

        /*  241 */ Object[] idsConf = (Object[]) client.execute("execute", paramSearchConf);
        /*  242 */ System.out.println(Arrays.toString(idsConf));

        /*  244 */ Vector<Object> fieldsConf = new Vector();
        /*  245 */ fieldsConf.add("start_up_date");
        /*  246 */ fieldsConf.add("user");
        /*  247 */ fieldsConf.add("password");
        /*  248 */ fieldsConf.add("last_query_log_call_datetime_wr");
        /*  249 */ fieldsConf.add("last_query_log_call_datetime_sh");
        /*  250 */ fieldsConf.add("last_query_log_call_datetime_cr");
        /*  251 */ fieldsConf.add("last_query_log_call_datetime_wr_verif");
        /*  252 */ fieldsConf.add("last_query_log_call_datetime_sh_verif");
        /*  253 */ fieldsConf.add("last_query_log_call_datetime_cr_verif");

        /*  255 */ Vector<Object> paramReadConf = new Vector();
        /*  256 */ paramReadConf.addElement(this.dbName);
        /*  257 */ paramReadConf.addElement(this.userId);
        /*  258 */ paramReadConf.addElement(this.pass);
        /*  259 */ paramReadConf.addElement("courier.magaya.config");
        /*  260 */ paramReadConf.add("read");
        /*  261 */ paramReadConf.add(idsConf);
        /*  262 */ paramReadConf.add(fieldsConf);
        /*  263 */ Object[] dataConf = (Object[]) client.execute("execute", paramReadConf);

        /*  265 */ String fechaConfStr = "";
        /*  266 */ String userMagaya = "";
        /*  267 */ String passMagaya = "";
        /*  268 */ String fechaWr = "";
        /*  269 */ String fechaSh = "";
        /*  270 */ String fechaCr = "";

        /*  272 */ String fechaWrVerif = "";
        /*  273 */ String fechaShVerif = "";
        /*  274 */ String fechaCrVerif = "";

        /*  276 */ if (dataConf.length != 0) {
            /*  277 */ hash = new HashMap<String, Object>();
            /*  278 */ Date fechaYHoraUltimaTransaccion = null;
            /*  279 */ Date last_query_log_call_datetime_wr = null;
            /*  280 */ Date last_query_log_call_datetime_sh = null;
            /*  281 */ Date last_query_log_call_datetime_cr = null;

            /*  283 */ Date last_query_log_call_datetime_wr_verif = null;
            /*  284 */ Date last_query_log_call_datetime_sh_verif = null;
            /*  285 */ Date last_query_log_call_datetime_cr_verif = null;

            /*  287 */ fechaConfStr = ((HashMap<?, Object>) dataConf[0]).get("start_up_date").toString();
            /*  288 */ fechaWr = ((HashMap<?, Object>) dataConf[0]).get("last_query_log_call_datetime_wr").toString();
            /*  289 */ fechaSh = ((HashMap<?, Object>) dataConf[0]).get("last_query_log_call_datetime_sh").toString();
            /*  290 */ fechaCr = ((HashMap<?, Object>) dataConf[0]).get("last_query_log_call_datetime_cr").toString();

            /*  292 */ fechaWrVerif = ((HashMap<?, Object>) dataConf[0]).get("last_query_log_call_datetime_wr_verif").toString();
            /*  293 */ fechaShVerif = ((HashMap<?, Object>) dataConf[0]).get("last_query_log_call_datetime_sh_verif").toString();
            /*  294 */ fechaCrVerif = ((HashMap<?, Object>) dataConf[0]).get("last_query_log_call_datetime_cr_verif").toString();

            /*  296 */ SimpleDateFormat formaterSys = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            try {
                /*  298 */ fechaYHoraUltimaTransaccion = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaConfStr)));
                /*  299 */ last_query_log_call_datetime_wr = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaWr)));
                /*  300 */ last_query_log_call_datetime_sh = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaSh)));
                /*  301 */ last_query_log_call_datetime_cr = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaCr)));

                /*  303 */ last_query_log_call_datetime_wr_verif = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaWrVerif)));
                /*  304 */ last_query_log_call_datetime_sh_verif = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaShVerif)));
                /*  305 */ last_query_log_call_datetime_cr_verif = formaterSys.parse(formaterSys.format(this.formaterUtc.parse(fechaCrVerif)));
            } /*  307 */ catch (ParseException e) {
                /*  308 */ Logger.getLogger(getClass().getSimpleName()).error("No se pudo parsear una fecha", e);
            }
            /*  310 */ userMagaya = ((HashMap<?, Object>) dataConf[0]).get("user").toString();
            /*  311 */ passMagaya = ((HashMap<?, Object>) dataConf[0]).get("password").toString();

            /*  313 */ hash.put("star_up_date", fechaYHoraUltimaTransaccion);
            /*  314 */ hash.put("user", userMagaya);
            /*  315 */ hash.put("password", passMagaya);
            /*  316 */ hash.put("last_query_log_call_datetime_wr", last_query_log_call_datetime_wr);
            /*  317 */ hash.put("last_query_log_call_datetime_sh", last_query_log_call_datetime_sh);
            /*  318 */ hash.put("last_query_log_call_datetime_cr", last_query_log_call_datetime_cr);

            /*  320 */ hash.put("last_query_log_call_datetime_wr_verif", last_query_log_call_datetime_wr_verif);
            /*  321 */ hash.put("last_query_log_call_datetime_sh_verif", last_query_log_call_datetime_sh_verif);
            /*  322 */ hash.put("last_query_log_call_datetime_cr_verif", last_query_log_call_datetime_cr_verif);
        }

        /*  325 */ return hash;
    }

    public boolean existeTransaccion(TransactionType transType, String transGuid) throws XmlRpcException {
        /*  329 */ XmlRpcClient client = getClienteXmlrpc();

        /*  331 */ Vector<Object> filter = new Vector();
        /*  332 */ filter.addElement("guid");
        /*  333 */ filter.addElement("=");
        /*  334 */ filter.addElement(transGuid);
        /*  342 */ Vector<Vector<Object>> filters = new Vector();
        /*  343 */ filters.add(filter);

        /*  345 */ Vector<Object> paramSearch = new Vector();
        /*  346 */ paramSearch.addElement(this.dbName);
        /*  347 */ paramSearch.addElement(this.userId);
        /*  348 */ paramSearch.addElement(this.pass);
        /*  349 */ paramSearch.addElement(transType.getOpenerpModel());
        /*  350 */ paramSearch.add("search");

        /*  352 */ paramSearch.add(filters);

        /*  354 */ Object[] idsTrans = (Object[]) client.execute("execute", paramSearch);
        /*  355 */ return (idsTrans.length != 0);
    }

    public Integer crearTransaccionWR(HashMap<Object, Object> values, HashMap<Object, Object> hashShipperAddress, HashMap<Object, Object> hashConsigneeAddress, ArrayList<HashMap<Object, Object>> listaItems, HashMap<Object, Object> hashMapXml) throws XmlRpcException {
        /*  360 */ XmlRpcClient client = getClienteXmlrpc();

        /*  362 */ Object shipperAddrId = null;

        /*  364 */ if (hashShipperAddress != null) {
            /*  365 */ Vector<Object> paramsShipperAddr = new Vector();
            /*  366 */ paramsShipperAddr.addElement(this.dbName);
            /*  367 */ paramsShipperAddr.addElement(this.userId);
            /*  368 */ paramsShipperAddr.addElement(this.pass);
            /*  369 */ paramsShipperAddr.addElement("courier.magaya.wr.address");
            /*  370 */ paramsShipperAddr.add("create");

            /*  374 */ paramsShipperAddr.add(hashShipperAddress);
            /*  375 */ shipperAddrId = client.execute("execute", paramsShipperAddr);
            // Logger.getLogger(getClass().getSimpleName()).info("Shipper Address creada correctamente, id: " + shipperAddrId);
        }

        /*  379 */ Object consigneeAddrId = null;
        /*  380 */ if (hashConsigneeAddress != null) {

            /*  382 */ Vector<Object> paramsConsigneeAddr = new Vector();
            /*  383 */ paramsConsigneeAddr.addElement(this.dbName);
            /*  384 */ paramsConsigneeAddr.addElement(this.userId);
            /*  385 */ paramsConsigneeAddr.addElement(this.pass);
            /*  386 */ paramsConsigneeAddr.addElement("courier.magaya.wr.address");
            /*  387 */ paramsConsigneeAddr.add("create");

            /*  391 */ paramsConsigneeAddr.add(hashConsigneeAddress);
            /*  392 */ consigneeAddrId = client.execute("execute", paramsConsigneeAddr);
            // Logger.getLogger(getClass().getSimpleName()).info("Consignee Address creada correctamente, id: " + consigneeAddrId);
        }

        /*  396 */ Vector<Object> params = new Vector();
        /*  397 */ params.addElement(this.dbName);
        /*  398 */ params.addElement(this.userId);
        /*  399 */ params.addElement(this.pass);
        /*  400 */ params.addElement("courier.magaya.wr");
        /*  401 */ params.add("create");

        /*  403 */ if (shipperAddrId != null) {
            /*  404 */ values.put("shipper_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), shipperAddrId});
        }
        /*  406 */ if (consigneeAddrId != null) {
            /*  407 */ values.put("consignee_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), consigneeAddrId});
        }
        /*  429 */ params.add(values);
        /*  430 */ Object newWrId = client.execute("execute", params);
        // Logger.getLogger(getClass().getSimpleName()).info("Transaccion creada correctamente, id: " + newWrId);

        /*  433 */ if (listaItems != null) {
            /*  434 */ Vector<Object> idsItems = new Vector();
            /*  435 */ for (Iterator<HashMap<Object, Object>> iterator = listaItems.iterator(); iterator.hasNext();) {
                /*  436 */ HashMap<Object, Object> hashMap = (HashMap<Object, Object>) iterator.next();

                /*  438 */ Vector<Object> paramsNuevoItem = new Vector();
                /*  439 */ paramsNuevoItem.addElement(this.dbName);
                /*  440 */ paramsNuevoItem.addElement(this.userId);
                /*  441 */ paramsNuevoItem.addElement(this.pass);
                /*  442 */ paramsNuevoItem.addElement("courier.magaya.wr.line");
                /*  443 */ paramsNuevoItem.add("create");

                /*  445 */ hashMap.put("wr_id", newWrId);

                /*  447 */ paramsNuevoItem.add(hashMap);
                /*  448 */ Object idItem = client.execute("execute", paramsNuevoItem);
                // Logger.getLogger(getClass().getSimpleName()).info("Item WR creado correctamente, id: " + idItem);
/*  450 */ idsItems.add(idItem);
            }
        }
        /*  453 */ return (Integer) newWrId;
    }

    public void actualizarTransaccionSH(Object wrOpenerpId, Object parent, HashMap<Object, Object> values, HashMap<Object, Object> hashShipperAddress, HashMap<Object, Object> hashConsigneeAddress, Vector wrItemsGuids) throws XmlRpcException {
        /*  472 */ XmlRpcClient client = getClienteXmlrpc();

        /*  475 */ Vector<Object> fieldsWR = new Vector();
        /*  476 */ fieldsWR.add("guid");
        /*  477 */ fieldsWR.add("created_on");
        /*  478 */ fieldsWR.add("shipper_address_ids");
        /*  479 */ fieldsWR.add("consignee_address_ids");

        /*  482 */ HashMap<Object, Object> hashDataWR = (HashMap<Object, Object>) read("courier.magaya.sh", new Object[]{wrOpenerpId}, fieldsWR)[0];

        /*  486 */ if (hashShipperAddress != null) {
            /*  487 */ hashShipperAddress.put("sh_id", wrOpenerpId);
            /*  488 */ if (((Object[]) hashDataWR.get("shipper_address_ids")).length != 0) {
                /*  489 */ Object shipper_address_id = ((Object[]) hashDataWR.get("shipper_address_ids"))[0];
                /*  490 */ write("courier.magaya.sh.address", shipper_address_id, hashShipperAddress);
            } else {
                /*  492 */ Object shipper_address_id = create("courier.magaya.sh.address", hashShipperAddress);
                /*  493 */ values.put("shipper_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), shipper_address_id});
            }
        }

        /*  498 */ if (hashConsigneeAddress != null) {
            /*  499 */ hashConsigneeAddress.put("sh_id", wrOpenerpId);
            /*  500 */ if (((Object[]) hashDataWR.get("consignee_address_ids")).length != 0) {
                /*  501 */ Object consignee_address_id = ((Object[]) hashDataWR.get("consignee_address_ids"))[0];
                /*  502 */ write("courier.magaya.sh.address", consignee_address_id, hashConsigneeAddress);
            } else {
                /*  504 */ Object consignee_address_id = create("courier.magaya.sh.address", hashConsigneeAddress);
                /*  505 */ values.put("consignee_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), consignee_address_id});
            }
        }

        /*  509 */ if (parent != null) {
            /*  510 */ values.put("parent_id", parent);
        }

        /*  513 */ Vector<Object> filter = new Vector();
        /*  514 */ filter.addElement("guid");
        /*  515 */ filter.addElement("in");
        /*  516 */ filter.addElement(wrItemsGuids);
        /*  517 */ Vector<Vector<Object>> filters = new Vector();
        /*  518 */ filters.add(filter);
        /*  519 */ Object[] wrItensIds = search(TransactionType.WarehouseReceipt.getOpenerpModel(), (Vector) filters);
        /*  520 */ values.put("wr_item_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), wrItensIds});

        /*  532 */ values.remove("xml_ids");
        /*  533 */ values.remove("audit_logs");

        /*  535 */ write("courier.magaya.sh", wrOpenerpId, values);
    }

    public void actualizarTransaccionWR(Object wrOpenerpId, HashMap<Object, Object> values, HashMap<Object, Object> hashShipperAddress, HashMap<Object, Object> hashConsigneeAddress, ArrayList<HashMap<Object, Object>> listaItems, ArrayList<HashMap<String, String>> attachments) throws XmlRpcException {
        /*  539 */ XmlRpcClient client = getClienteXmlrpc();

        /*  542 */ Vector<String> fieldsWR = new Vector();
        /*  543 */ fieldsWR.add("name");
        /*  544 */ fieldsWR.add("created_on");
        /*  545 */ fieldsWR.add("shipper_address_ids");
        /*  546 */ fieldsWR.add("consignee_address_ids");
        /*  547 */ fieldsWR.add("wr_line_ids");

        /*  549 */ fieldsWR.add("atlas_partner_id");
        /*  550 */ fieldsWR.add("guid");

        /*  552 */ HashMap<Object, Object> hashDataWR = (HashMap<Object, Object>) read("courier.magaya.wr", new Object[]{wrOpenerpId}, fieldsWR)[0];

        /*  561 */ if (hashShipperAddress != null) {
            /*  562 */ hashShipperAddress.put("wr_id", wrOpenerpId);
            /*  563 */ if (((Object[]) hashDataWR.get("shipper_address_ids")).length != 0) {
                /*  564 */ Object shipper_address_id = ((Object[]) hashDataWR.get("shipper_address_ids"))[0];
                /*  565 */ write("courier.magaya.wr.address", shipper_address_id, hashShipperAddress);
            } else {
                /*  567 */ Object shipper_address_id = create("courier.magaya.wr.address", hashShipperAddress);

                /*  570 */ values.put("shipper_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), shipper_address_id});
            }
        }

        /*  575 */ if (hashConsigneeAddress != null) {
            /*  576 */ hashConsigneeAddress.put("wr_id", wrOpenerpId);
            /*  577 */ if (((Object[]) hashDataWR.get("consignee_address_ids")).length != 0) {
                /*  578 */ Object consignee_address_id = ((Object[]) hashDataWR.get("consignee_address_ids"))[0];
                /*  579 */ write("courier.magaya.wr.address", consignee_address_id, hashConsigneeAddress);
            } else {
                /*  581 */ Object consignee_address_id = create("courier.magaya.wr.address", hashConsigneeAddress);

                /*  584 */ values.put("consignee_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), consignee_address_id});
            }
        }

        /*  629 */ Object[] wr_line_ids = (Object[]) hashDataWR.get("wr_line_ids");
        /*  630 */ Vector<String> fieldNamesItemWr = new Vector();
        /*  631 */ fieldNamesItemWr.add("guid");
        /*  632 */ Object[] wr_line_objs = read("courier.magaya.wr.line", wr_line_ids, fieldNamesItemWr);
        /*  633 */ HashMap<String, Integer> hashGuidPosExistentes = new HashMap<String, Integer>();

        /*  635 */ for (int i = 0; i < wr_line_objs.length; i++) {
            /*  637 */ hashGuidPosExistentes.put(((HashMap<?, Object>) wr_line_objs[i]).get("guid").toString(), Integer.valueOf(i));
        }

        /*  642 */ Iterator<HashMap<Object, Object>> itItems = listaItems.iterator();
        /*  643 */ while (itItems.hasNext()) {
            /*  644 */ HashMap<Object, Object> hashMapItem = itItems.next();

            /*  646 */ hashMapItem.put("wr_id", wrOpenerpId);

            /*  649 */ if (hashGuidPosExistentes.containsKey(hashMapItem.get("guid"))) {
                /*  650 */ Object idItem = ((HashMap<?, ?>) wr_line_objs[((Integer) hashGuidPosExistentes.get(hashMapItem.get("guid"))).intValue()]).get("id");

                /*  652 */ write("courier.magaya.wr.line", idItem, hashMapItem);
                /*  653 */ itItems.remove();

            } else {

                /*  659 */ Object new_item_id = create("courier.magaya.wr.line", hashMapItem);

                /*  661 */ itItems.remove();

                /*  663 */ Object[] new_wr_line_ids = new Object[wr_line_ids.length + 1];
                /*  664 */ for (int k = 0; k < wr_line_ids.length; k++) {
                    /*  665 */ new_wr_line_ids[k] = wr_line_ids[k];
                }
                /*  667 */ new_wr_line_ids[wr_line_ids.length] = new_item_id;
                /*  668 */ values.put("wr_line_ids", new_wr_line_ids);
            }

            /*  671 */ hashGuidPosExistentes.remove(hashMapItem.get("guid"));
        }

        /*  678 */ Iterator<String> itGuidsItemsAEliminar = hashGuidPosExistentes.keySet().iterator();
        /*  679 */ while (itGuidsItemsAEliminar.hasNext()) {
            /*  680 */ String guidAEliminar = itGuidsItemsAEliminar.next();

            /*  691 */ Object[] ids = {((HashMap<?, ?>) wr_line_objs[((Integer) hashGuidPosExistentes.get(guidAEliminar)).intValue()]).get("id")};
            /*  692 */ unlink("courier.magaya.wr.line", ids);

            // Logger.getLogger(getClass().getSimpleName()).info("Se elimino linea WR (WI), GUID WI: " + guidAEliminar + ", GUID WR: " + hashDataWR.get("guid"));
        }

        /*  699 */ values.remove("wr_line_ids");

        /*  702 */ values.remove("xml_ids");
        /*  703 */ values.remove("audit_logs");

        /*  712 */ write("courier.magaya.wr", wrOpenerpId, values);

        /*  716 */ fieldsWR = new Vector<String>();
        /*  717 */ fieldsWR.add("name");
        /*  718 */ fieldsWR.add("created_on");

        /*  723 */ fieldsWR.add("atlas_partner_id");

        /*  725 */ hashDataWR = (HashMap<Object, Object>) read("courier.magaya.wr", new Object[]{wrOpenerpId}, fieldsWR)[0];

        /*  727 */ for (int j = 0; j < attachments.size(); j++) {
            /*  728 */ HashMap<String, String> hashAttachment = attachments.get(j);
            /*  729 */ HashMap<Object, Object> hashMap = new HashMap<Object, Object>();

            /*  731 */ int reintentosRestantes = 4;
            /*  732 */ while (reintentosRestantes > 0) {

                /*  734 */ Vector<Object> vectorOfVectorFilterTuples = new Vector();

                /*  736 */ Vector<Object> res_idFilter = new Vector();
                /*  737 */ res_idFilter.add("res_id");
                /*  738 */ res_idFilter.add("=");
                /*  739 */ res_idFilter.add(wrOpenerpId);

                /*  741 */ Vector<Object> res_modelFilter = new Vector();
                /*  742 */ res_modelFilter.add("res_model");
                /*  743 */ res_modelFilter.add("=");
                /*  744 */ res_modelFilter.add("courier.magaya.wr");

                /*  746 */ Vector<Object> nameFilter = new Vector();
                /*  747 */ nameFilter.add("name");
                /*  748 */ nameFilter.add("=");
                /*  749 */ nameFilter.add(String.valueOf(hashAttachment.get("name")) + "." + ((String) hashAttachment.get("extension")).toLowerCase());

                /*  751 */ vectorOfVectorFilterTuples.add("&");
                /*  752 */ vectorOfVectorFilterTuples.add("&");
                /*  753 */ vectorOfVectorFilterTuples.add(res_idFilter);
                /*  754 */ vectorOfVectorFilterTuples.add(res_modelFilter);
                /*  755 */ vectorOfVectorFilterTuples.add(nameFilter);

                /*  757 */ Object[] attachIds = searchGenerico("ir.attachment", vectorOfVectorFilterTuples);

                /*  759 */ if (attachIds.length == 0) {
                    /*  760 */ hashMap.put("name", String.valueOf(hashAttachment.get("name")) + "." + ((String) hashAttachment.get("extension")).toLowerCase());
                    /*  761 */ hashMap.put("datas", hashAttachment.get("data"));
                    /*  762 */ hashMap.put("datas_fname", String.valueOf(hashAttachment.get("name")) + "." + ((String) hashAttachment.get("extension")).toLowerCase());
                    /*  763 */ hashMap.put("res_model", "courier.magaya.wr");
                    /*  764 */ hashMap.put("res_id", wrOpenerpId);

                    /*  766 */ if (hashDataWR.containsKey("atlas_partner_id") && hashDataWR.get("atlas_partner_id") != null && !(hashDataWR.get("atlas_partner_id") instanceof Boolean)) {
                        /*  767 */ hashMap.put("partner_id", ((Object[]) hashDataWR.get("atlas_partner_id"))[0]);
                    }

                    try {
                        /*  771 */ create("ir.attachment", hashMap);
                        /*  772 */ reintentosRestantes = 0;
                        // Logger.getLogger(getClass().getSimpleName()).info("Attachment creado: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase());
/*  774 */                    } catch (Exception e) {
                        // Logger.getLogger(getClass().getSimpleName()).info("Error al crear attachment: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase(), e);
/*  776 */ reintentosRestantes--;
                        // Logger.getLogger(getClass().getSimpleName()).info("Reintentos restantes attachment: " + reintentosRestantes);
                    }
                    continue;
                }
                /*  780 */ hashMap.put("datas", hashAttachment.get("data"));

                /*  782 */ if (hashDataWR.containsKey("atlas_partner_id") && hashDataWR.get("atlas_partner_id") != null && !(hashDataWR.get("atlas_partner_id") instanceof Boolean)) {
                    /*  783 */ hashMap.put("partner_id", ((Object[]) hashDataWR.get("atlas_partner_id"))[0]);
                }

                try {
                    /*  787 */ write("ir.attachment", attachIds[0], hashMap);
                    /*  788 */ reintentosRestantes = 0;
                    // Logger.getLogger(getClass().getSimpleName()).info("Attachment actualizado: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase());
/*  790 */                } catch (Exception e) {
                    // Logger.getLogger(getClass().getSimpleName()).info("Error al actualizar attachment: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase(), e);
/*  792 */ reintentosRestantes--;
                    // Logger.getLogger(getClass().getSimpleName()).info("Reintentos restantes attachment: " + reintentosRestantes);
                }
            }
        }

        /*  798 */ HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();

        try {
            /*  801 */ procesar("courier.magaya.wr", wrOpenerpId, paresNombreValor);
            /*  802 */        } catch (Exception e) {
            /*  803 */ Logger.getLogger(getClass().getSimpleName()).error("No se pudo PROCESAR la transaccion, GUID: " + hashDataWR.get("guid") + ", WR Number: " + hashDataWR.get("name"));
        }
    }

    public void actualizarTransaccionCR(Object openerpTransId, HashMap<Object, Object> values, HashMap<Object, Object> hashReleaseToAddress, Vector wrItemsGuids, ArrayList<HashMap<String, String>> attachments) throws XmlRpcException {
        /*  808 */ XmlRpcClient client = getClienteXmlrpc();

        /*  811 */ Vector<String> fieldsCR = new Vector();
        /*  812 */ fieldsCR.add("name");
        /*  813 */ fieldsCR.add("created_on");
        /*  814 */ fieldsCR.add("release_to_address_ids");
        /*  815 */ fieldsCR.add("wr_ids");
        /*  816 */ fieldsCR.add("atlas_partner_id");

        /*  818 */ HashMap<Object, Object> hashDataCR = (HashMap<Object, Object>) read("courier.magaya.cr", new Object[]{openerpTransId}, fieldsCR)[0];

        /*  822 */ if (hashReleaseToAddress != null) {
            /*  823 */ hashReleaseToAddress.put("cr_id", openerpTransId);
            /*  824 */ if (((Object[]) hashDataCR.get("release_to_address_ids")).length != 0) {
                /*  825 */ Object release_to_address_id = ((Object[]) hashDataCR.get("release_to_address_ids"))[0];
                /*  826 */ write("courier.magaya.cr.address", release_to_address_id, hashReleaseToAddress);
            } else {
                /*  828 */ Object release_to_address_id = create("courier.magaya.cr.address", hashReleaseToAddress);
                /*  829 */ values.put("release_to_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), release_to_address_id});
            }
        }

        /*  839 */ Vector<Object> filter = new Vector();
        /*  840 */ filter.addElement("guid");
        /*  841 */ filter.addElement("in");
        /*  842 */ filter.addElement(wrItemsGuids);
        /*  843 */ Vector<Vector<Object>> filters = new Vector();
        /*  844 */ filters.add(filter);
        /*  845 */ Object[] wrItensIds = search(TransactionType.WarehouseReceipt.getOpenerpModel(), (Vector) filters);
        /*  846 */ values.put("wr_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), wrItensIds});

        /*  854 */ values.remove("xml_ids");
        /*  855 */ values.remove("audit_logs");

        /*  857 */ write("courier.magaya.cr", openerpTransId, values);

        /*  861 */ fieldsCR = new Vector<String>();
        /*  862 */ fieldsCR.add("name");
        /*  863 */ fieldsCR.add("created_on");
        /*  864 */ fieldsCR.add("release_to_address_ids");

        /*  868 */ fieldsCR.add("atlas_partner_id");

        /*  870 */ hashDataCR = (HashMap<Object, Object>) read("courier.magaya.cr", new Object[]{openerpTransId}, fieldsCR)[0];

        /*  872 */ for (int i = 0; i < attachments.size(); i++) {
            /*  873 */ HashMap<String, String> hashAttachment = attachments.get(i);
            /*  874 */ HashMap<Object, Object> hashMap = new HashMap<Object, Object>();

            /*  876 */ int reintentosRestantes = 4;
            /*  877 */ while (reintentosRestantes > 0) {

                /*  879 */ Vector<Object> vectorOfVectorFilterTuples = new Vector();

                /*  881 */ Vector<Object> res_idFilter = new Vector();
                /*  882 */ res_idFilter.add("res_id");
                /*  883 */ res_idFilter.add("=");
                /*  884 */ res_idFilter.add(openerpTransId);

                /*  886 */ Vector<Object> res_modelFilter = new Vector();
                /*  887 */ res_modelFilter.add("res_model");
                /*  888 */ res_modelFilter.add("=");
                /*  889 */ res_modelFilter.add("courier.magaya.cr");

                /*  891 */ Vector<Object> nameFilter = new Vector();
                /*  892 */ nameFilter.add("name");
                /*  893 */ nameFilter.add("=");
                /*  894 */ nameFilter.add(String.valueOf(hashAttachment.get("name")) + "." + ((String) hashAttachment.get("extension")).toLowerCase());

                /*  896 */ vectorOfVectorFilterTuples.add("&");
                /*  897 */ vectorOfVectorFilterTuples.add("&");
                /*  898 */ vectorOfVectorFilterTuples.add(res_idFilter);
                /*  899 */ vectorOfVectorFilterTuples.add(res_modelFilter);
                /*  900 */ vectorOfVectorFilterTuples.add(nameFilter);

                /*  902 */ Object[] attachIds = searchGenerico("ir.attachment", vectorOfVectorFilterTuples);

                /*  904 */ if (attachIds.length == 0) {
                    /*  905 */ hashMap.put("name", String.valueOf(hashAttachment.get("name")) + "." + ((String) hashAttachment.get("extension")).toLowerCase());
                    /*  906 */ hashMap.put("datas", hashAttachment.get("data"));
                    /*  907 */ hashMap.put("datas_fname", String.valueOf(hashAttachment.get("name")) + "." + ((String) hashAttachment.get("extension")).toLowerCase());
                    /*  908 */ hashMap.put("res_model", "courier.magaya.cr");
                    /*  909 */ hashMap.put("res_id", openerpTransId);

                    /*  911 */ if (hashDataCR.containsKey("atlas_partner_id") && hashDataCR.get("atlas_partner_id") != null && !(hashDataCR.get("atlas_partner_id") instanceof Boolean)) {
                        /*  912 */ hashMap.put("partner_id", ((Object[]) hashDataCR.get("atlas_partner_id"))[0]);
                    }

                    try {
                        /*  916 */ create("ir.attachment", hashMap);
                        /*  917 */ reintentosRestantes = 0;
                        // Logger.getLogger(getClass().getSimpleName()).info("Attachment creado: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase());
/*  919 */                    } catch (Exception e) {
                        // Logger.getLogger(getClass().getSimpleName()).info("Error al crear attachment: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase(), e);
/*  921 */ reintentosRestantes--;
                        // Logger.getLogger(getClass().getSimpleName()).info("Reintentos restantes attachment: " + reintentosRestantes);
                    }
                    continue;
                }
                /*  925 */ hashMap.put("datas", hashAttachment.get("data"));

                /*  927 */ if (hashDataCR.containsKey("atlas_partner_id") && hashDataCR.get("atlas_partner_id") != null && !(hashDataCR.get("atlas_partner_id") instanceof Boolean)) {
                    /*  928 */ hashMap.put("partner_id", ((Object[]) hashDataCR.get("atlas_partner_id"))[0]);
                }

                try {
                    /*  932 */ write("ir.attachment", attachIds[0], hashMap);
                    /*  933 */ reintentosRestantes = 0;
                    // Logger.getLogger(getClass().getSimpleName()).info("Attachment actualizado: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase());
/*  935 */                } catch (Exception e) {
                    // Logger.getLogger(getClass().getSimpleName()).info("Error al actualizar attachment: " + (String)hashAttachment.get("name") + "." + ((String)hashAttachment.get("extension")).toLowerCase(), e);
/*  937 */ reintentosRestantes--;
                    // Logger.getLogger(getClass().getSimpleName()).info("Reintentos restantes attachment: " + reintentosRestantes);
                }
            }
        }

        /*  944 */ HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();
    }

    public HashMap<String, Object> readWR(Object wr_id) throws XmlRpcException {
        /*  949 */ HashMap<String, Object> hash = null;

        /*  951 */ XmlRpcClient client = getClienteXmlrpc();

        /*  953 */ Vector<String> fieldsWR = new Vector();
        /*  954 */ fieldsWR.add("name");

        /*  956 */ fieldsWR.add("created_on");
        /*  957 */ fieldsWR.add("shipper_address_ids");
        /*  958 */ fieldsWR.add("consignee_address_ids");
        /*  959 */ fieldsWR.add("wr_line_ids");

        /*  962 */ Vector<Object> paramReadWR = new Vector();
        /*  963 */ paramReadWR.addElement(this.dbName);
        /*  964 */ paramReadWR.addElement(this.userId);
        /*  965 */ paramReadWR.addElement(this.pass);
        /*  966 */ paramReadWR.addElement("courier.magaya.wr");
        /*  967 */ paramReadWR.add("read");
        /*  968 */ paramReadWR.add(new Object[]{wr_id});
        /*  969 */ paramReadWR.add(fieldsWR);
        /*  970 */ Object[] dataWR = (Object[]) client.execute("execute", paramReadWR);

        /*  973 */ for (int i = 0; i < dataWR.length; i++) {
            /*  974 */ System.out.println(String.valueOf(dataWR[i].toString()) + " : " + dataWR[i].getClass().getName());
        }

        /*  977 */ HashMap hashm = (HashMap) dataWR[0];
        /*  978 */ String id = hashm.get("id").toString();
        /*  979 */ String name = hashm.get("name").toString();
        /*  980 */ String created_on = hashm.get("created_on").toString();
        /*  981 */ Object[] shipper_address_ids = (Object[]) hashm.get("shipper_address_ids");
        /*  982 */ Object[] consignee_address_ids = (Object[]) hashm.get("consignee_address_ids");
        /*  983 */ Object[] wr_line_ids = (Object[]) hashm.get("wr_line_ids");

        /* 1009 */ return hash;
    }

    public void write(String objectName, Object id, HashMap<Object, Object> paresNombreValor) throws XmlRpcException {
        /* 1013 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1015 */ Vector<Object> params = new Vector();
        /* 1016 */ params.addElement(this.dbName);
        /* 1017 */ params.addElement(this.userId);
        /* 1018 */ params.addElement(this.pass);
        /* 1019 */ params.addElement(objectName);
        /* 1020 */ params.add("write");
        /* 1021 */ params.add(new Object[]{id});
        /* 1022 */ params.add(paresNombreValor);
        /* 1023 */ client.execute("execute", params);
    }

    public void marcar_para_actualizar(String objectName, Object id, HashMap<Object, Object> paresNombreValor) throws XmlRpcException {
        /* 1027 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1029 */ Vector<Object> params = new Vector();
        /* 1030 */ params.addElement(this.dbName);
        /* 1031 */ params.addElement(this.userId);
        /* 1032 */ params.addElement(this.pass);
        /* 1033 */ params.addElement(objectName);
        /* 1034 */ params.add("marcar_para_actualizar");
        /* 1035 */ params.add(id);
        /* 1036 */ params.add(paresNombreValor);
        /* 1037 */ client.execute("execute", params);
    }

    public Object[] search(String objectName, Vector<Vector<Object>> vectorOfVectorFilterTuples, Integer offset, Integer cantResults, String orderByString) throws XmlRpcException {
        /* 1052 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1054 */ Vector<Object> param = new Vector();
        /* 1055 */ param.addElement(this.dbName);
        /* 1056 */ param.addElement(this.userId);
        /* 1057 */ param.addElement(this.pass);
        /* 1058 */ param.addElement(objectName);
        /* 1059 */ param.add("search");
        /* 1060 */ param.add(vectorOfVectorFilterTuples);
        /* 1061 */ if (offset != null) /* 1062 */ {
            param.add(offset);
        }
        /* 1063 */ if (cantResults != null) /* 1064 */ {
            param.add(cantResults);
        }
        /* 1065 */ if (orderByString != null) {
            /* 1066 */ param.add(orderByString);
        }
        /* 1068 */ return (Object[]) client.execute("execute", param);
    }

    public Object[] searchGenerico(String objectName, Vector<Object> vectorOfVectorFilterTuples) throws XmlRpcException {
        /* 1072 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1074 */ Vector<Object> param = new Vector();
        /* 1075 */ param.addElement(this.dbName);
        /* 1076 */ param.addElement(this.userId);
        /* 1077 */ param.addElement(this.pass);
        /* 1078 */ param.addElement(objectName);
        /* 1079 */ param.add("search");
        /* 1080 */ param.add(vectorOfVectorFilterTuples);

        /* 1082 */ return (Object[]) client.execute("execute", param);
    }

    public Object[] search(String objectName, Vector<Vector<Object>> vectorOfVectorFilterTuples) throws XmlRpcException {
        /* 1086 */ return search(objectName, vectorOfVectorFilterTuples, null, null, null);
    }

    public Object[] read(String objectName, Object[] ids, Vector fieldNames) throws XmlRpcException {
        /* 1099 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1101 */ Vector<Object> paramReadWR = new Vector();
        /* 1102 */ paramReadWR.addElement(this.dbName);
        /* 1103 */ paramReadWR.addElement(this.userId);
        /* 1104 */ paramReadWR.addElement(this.pass);
        /* 1105 */ paramReadWR.addElement(objectName);
        /* 1106 */ paramReadWR.add("read");
        /* 1107 */ paramReadWR.add(ids);
        /* 1108 */ paramReadWR.add(fieldNames);

        /* 1110 */ Object[] res = (Object[]) client.execute("execute", paramReadWR);

        /* 1112 */ return res;
    }

    public Object create(String objectName, HashMap<Object, Object> paresNombreValor) throws XmlRpcException {
        /* 1116 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1118 */ Vector<Object> paramsNuevoItem = new Vector();
        /* 1119 */ paramsNuevoItem.addElement(this.dbName);
        /* 1120 */ paramsNuevoItem.addElement(this.userId);
        /* 1121 */ paramsNuevoItem.addElement(this.pass);
        /* 1122 */ paramsNuevoItem.addElement(objectName);
        /* 1123 */ paramsNuevoItem.add("create");

        /* 1125 */ paramsNuevoItem.add(paresNombreValor);
        /* 1126 */ return client.execute("execute", paramsNuevoItem);
    }

    public void unlink(String objectName, Object[] ids) throws XmlRpcException {
        /* 1130 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1132 */ Vector<Object> paramUnlink = new Vector();
        /* 1133 */ paramUnlink.addElement(this.dbName);
        /* 1134 */ paramUnlink.addElement(this.userId);
        /* 1135 */ paramUnlink.addElement(this.pass);
        /* 1136 */ paramUnlink.addElement(objectName);
        /* 1137 */ paramUnlink.add("unlink");
        /* 1138 */ paramUnlink.add(ids);
        /* 1139 */ client.execute("execute", paramUnlink);
    }

    public void unlink(String objectName, Object id) throws XmlRpcException {
        /* 1143 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1145 */ Vector<Object> paramUnlink = new Vector();
        /* 1146 */ paramUnlink.addElement(this.dbName);
        /* 1147 */ paramUnlink.addElement(this.userId);
        /* 1148 */ paramUnlink.addElement(this.pass);
        /* 1149 */ paramUnlink.addElement(objectName);
        /* 1150 */ paramUnlink.add("unlink");
        /* 1151 */ paramUnlink.add(id);
        /* 1152 */ client.execute("execute", paramUnlink);
    }

    public Integer crearTransaccionSH(HashMap<Object, Object> values, HashMap<Object, Object> hashShipperAddress, HashMap<Object, Object> hashConsigneeAddress, HashMap<Object, Object> hashMapXml) throws XmlRpcException {
        /* 1157 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1159 */ Object shipperAddrId = null;

        /* 1161 */ if (hashShipperAddress != null) {
            /* 1162 */ Vector<Object> paramsShipperAddr = new Vector();
            /* 1163 */ paramsShipperAddr.addElement(this.dbName);
            /* 1164 */ paramsShipperAddr.addElement(this.userId);
            /* 1165 */ paramsShipperAddr.addElement(this.pass);
            /* 1166 */ paramsShipperAddr.addElement("courier.magaya.sh.address");
            /* 1167 */ paramsShipperAddr.add("create");

            /* 1171 */ paramsShipperAddr.add(hashShipperAddress);
            /* 1172 */ shipperAddrId = client.execute("execute", paramsShipperAddr);
            // Logger.getLogger(getClass().getSimpleName()).info("Shipper Address creada correctamente, id: " + shipperAddrId);
        }

        /* 1176 */ Object consigneeAddrId = null;
        /* 1177 */ if (hashConsigneeAddress != null) {

            /* 1179 */ Vector<Object> paramsConsigneeAddr = new Vector();
            /* 1180 */ paramsConsigneeAddr.addElement(this.dbName);
            /* 1181 */ paramsConsigneeAddr.addElement(this.userId);
            /* 1182 */ paramsConsigneeAddr.addElement(this.pass);
            /* 1183 */ paramsConsigneeAddr.addElement("courier.magaya.sh.address");
            /* 1184 */ paramsConsigneeAddr.add("create");

            /* 1188 */ paramsConsigneeAddr.add(hashConsigneeAddress);
            /* 1189 */ consigneeAddrId = client.execute("execute", paramsConsigneeAddr);
            // Logger.getLogger(getClass().getSimpleName()).info("Consignee Address creada correctamente, id: " + consigneeAddrId);
        }

        /* 1193 */ Vector<Object> params = new Vector();
        /* 1194 */ params.addElement(this.dbName);
        /* 1195 */ params.addElement(this.userId);
        /* 1196 */ params.addElement(this.pass);
        /* 1197 */ params.addElement("courier.magaya.sh");
        /* 1198 */ params.add("create");

        /* 1200 */ if (shipperAddrId != null) {
            /* 1201 */ values.put("shipper_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), shipperAddrId});
        }
        /* 1203 */ if (consigneeAddrId != null) {
            /* 1204 */ values.put("consignee_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), consigneeAddrId});
        }

        /* 1226 */ params.add(values);
        /* 1227 */ Object newWrId = client.execute("execute", params);
        // Logger.getLogger(getClass().getSimpleName()).info("Transaccion creada correctamente, id: " + newWrId);

        /* 1230 */ return (Integer) newWrId;
    }

    public Date getDateUltimaConsultaQueryLog(TransactionType transactionType) throws XmlRpcException {
        // Logger.getLogger(getClass().getSimpleName()).info("Obteniendo fecha de la ultimo " + transactionType.getDescription() + "...");
/* 1235 */ HashMap<String, Object> conf = getConfigMagaya();
        /* 1236 */ switch (transactionType) {
            case Shipment:
                /* 1238 */ return (Date) conf.get("last_query_log_call_datetime_sh");
            case WarehouseReceipt:
                /* 1240 */ return (Date) conf.get("last_query_log_call_datetime_wr");
            case CargoRelease:
                /* 1242 */ return (Date) conf.get("last_query_log_call_datetime_cr");
        }
        /* 1244 */ return null;
    }

    public Date getDateUltimaConsultaQueryLogVerificacionesVerificador(TransactionType transactionType) throws XmlRpcException {
        // Logger.getLogger(getClass().getSimpleName()).info("Obteniendo fecha de la ultimo " + transactionType.getDescription() + "...");
/* 1250 */ HashMap<String, Object> conf = getConfigMagaya();
        /* 1251 */ switch (transactionType) {
            case Shipment:
                /* 1253 */ return (Date) conf.get("last_query_log_call_datetime_sh_verif");
            case WarehouseReceipt:
                /* 1255 */ return (Date) conf.get("last_query_log_call_datetime_wr_verif");
            case CargoRelease:
                /* 1257 */ return (Date) conf.get("last_query_log_call_datetime_cr_verif");
        }
        /* 1259 */ return null;
    }

    public XmlRpcClient getClienteXmlrpc() {
        /* 1266 */ String url = String.valueOf(this.host) + ":" + this.port + "/xmlrpc/object";

        /* 1268 */ XmlRpcClient client = new XmlRpcClient();
        /* 1269 */ XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        try {
            /* 1271 */ config.setServerURL(new URL(url));
            /* 1272 */        } catch (MalformedURLException e) {
            /* 1273 */ e.printStackTrace();
        }
        /* 1275 */ client.setTransportFactory((XmlRpcTransportFactory) new XmlRpcSunHttpTransportFactory(client));
        /* 1276 */ config.setEnabledForExtensions(true);
        /* 1277 */ config.setEnabledForExceptions(true);
        /* 1278 */ client.setConfig((XmlRpcClientConfig) config);
        /* 1279 */ return client;
    }

    public void setDateUltimaConsultaQueryLogVerificacionesVerificador(TransactionType transactionType, Date dateUltimaConsulta) throws XmlRpcException {
        /* 1283 */ HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();

        /* 1285 */ switch (transactionType) {
            case WarehouseReceipt:
                /* 1287 */ paresNombreValor.put("last_query_log_call_datetime_wr_verif", this.formaterUtc.format(dateUltimaConsulta));
                break;
            case Shipment:
                /* 1290 */ paresNombreValor.put("last_query_log_call_datetime_sh_verif", this.formaterUtc.format(dateUltimaConsulta));
                break;
            case CargoRelease:
                /* 1293 */ paresNombreValor.put("last_query_log_call_datetime_cr_verif", this.formaterUtc.format(dateUltimaConsulta));
                break;
        }

        /* 1299 */ Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        /* 1300 */ write("courier.magaya.config", search("courier.magaya.config", vectorOfVectorFilterTuples)[0], paresNombreValor);
        // Logger.getLogger(getClass().getSimpleName()).info("Fecha de ultima verificacion de " + transactionType.getDescription() + " seteada a " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ")).format(dateUltimaConsulta));
    }

    public void setDateUltimaConsultaQueryLog(TransactionType transactionType, Date dateUltimaConsulta) throws XmlRpcException {
        /* 1305 */ HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();

        /* 1307 */ switch (transactionType) {
            case WarehouseReceipt:
                /* 1309 */ paresNombreValor.put("last_query_log_call_datetime_wr", this.formaterUtc.format(dateUltimaConsulta));
                break;
            case Shipment:
                /* 1312 */ paresNombreValor.put("last_query_log_call_datetime_sh", this.formaterUtc.format(dateUltimaConsulta));
                break;
            case CargoRelease:
                /* 1315 */ paresNombreValor.put("last_query_log_call_datetime_cr", this.formaterUtc.format(dateUltimaConsulta));
                break;
        }

        /* 1321 */ Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
        /* 1322 */ write("courier.magaya.config", search("courier.magaya.config", vectorOfVectorFilterTuples)[0], paresNombreValor);
        // Logger.getLogger(getClass().getSimpleName()).info("Fecha de ultima consulta de " + transactionType.getDescription() + " seteada a " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ")).format(dateUltimaConsulta));
    }

    public void procesar(String objectName, Object id, HashMap<Object, Object> paresNombreValor) throws XmlRpcException {
        /* 1327 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1329 */ Vector<Object> params = new Vector();
        /* 1330 */ params.addElement(this.dbName);
        /* 1331 */ params.addElement(this.userId);
        /* 1332 */ params.addElement(this.pass);
        /* 1333 */ params.addElement(objectName);
        /* 1334 */ params.add("procesar");
        /* 1335 */ params.add(new Object[]{id});
        /* 1336 */ params.add(paresNombreValor);
        /* 1337 */ client.execute("execute", params);
    }

    public Object crearTransaccionCR(HashMap<Object, Object> values, HashMap<Object, Object> hashReleaseToAddress, HashMap<Object, Object> hashMapXml) throws XmlRpcException {
        /* 1341 */ XmlRpcClient client = getClienteXmlrpc();

        /* 1343 */ Object releaseToAddrId = null;

        /* 1345 */ if (hashReleaseToAddress != null) {
            /* 1346 */ Vector<Object> paramsReleaseToAddr = new Vector();
            /* 1347 */ paramsReleaseToAddr.addElement(this.dbName);
            /* 1348 */ paramsReleaseToAddr.addElement(this.userId);
            /* 1349 */ paramsReleaseToAddr.addElement(this.pass);
            /* 1350 */ paramsReleaseToAddr.addElement("courier.magaya.cr.address");
            /* 1351 */ paramsReleaseToAddr.add("create");

            /* 1353 */ paramsReleaseToAddr.add(hashReleaseToAddress);
            /* 1354 */ releaseToAddrId = client.execute("execute", paramsReleaseToAddr);
            // Logger.getLogger(getClass().getSimpleName()).info("Release to Address creada correctamente, id: " + releaseToAddrId);
        }

        /* 1358 */ Vector<Object> params = new Vector();
        /* 1359 */ params.addElement(this.dbName);
        /* 1360 */ params.addElement(this.userId);
        /* 1361 */ params.addElement(this.pass);
        /* 1362 */ params.addElement("courier.magaya.cr");
        /* 1363 */ params.add("create");

        /* 1365 */ if (releaseToAddrId != null) {
            /* 1366 */ values.put("release_to_address_ids", new Object[]{Integer.valueOf(6), Integer.valueOf(0), releaseToAddrId});
        }

        /* 1369 */ params.add(values);
        /* 1370 */ Object newCrId = client.execute("execute", params);
        // Logger.getLogger(getClass().getSimpleName()).info("Transaccion CR creada correctamente, id: " + newCrId);

        /* 1374 */ return newCrId;
    }
}


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\OpenerpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */
