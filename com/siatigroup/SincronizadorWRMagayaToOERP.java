/*      */ package com.siatigroup;
/*      */ 
/*      */ import com.csvreader.CsvReader;
/*      */ import java.io.File;
/*      */ import java.io.FileInputStream;
/*      */ import java.io.FileNotFoundException;
/*      */ import java.io.IOException;
/*      */ import java.io.Reader;
/*      */ import java.io.StringReader;
/*      */ import java.text.DecimalFormat;
/*      */ import java.text.ParseException;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Calendar;
/*      */ import java.util.Date;
/*      */ import java.util.HashMap;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ import java.util.Map;
/*      */ import java.util.Properties;
/*      */ import java.util.TimeZone;
/*      */ import java.util.TreeMap;
/*      */ import java.util.TreeSet;
/*      */ import java.util.Vector;
/*      */ import org.apache.log4j.Logger;
/*      */ import org.apache.log4j.MDC;
/*      */ import org.apache.xmlrpc.XmlRpcException;
/*      */ import org.jdom2.Document;
/*      */ import org.jdom2.Element;
/*      */ import org.jdom2.JDOMException;
/*      */ import org.jdom2.input.SAXBuilder;
/*      */ import org.jdom2.output.Format;
/*      */ import org.jdom2.output.XMLOutputter;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class SincronizadorWRMagayaToOERP
/*      */ {
/*   41 */   private SimpleDateFormat formaterFechaHoraSys = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
/*   42 */   private SimpleDateFormat formaterFecha = new SimpleDateFormat("yyyy-MM-dd");
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*   49 */   static OpenerpClient openerpClient = new OpenerpClient();
/*   50 */   static MagayaWS magayaWS = new MagayaWS();
/*   51 */   HashMap<String, String> transTypeDateFieldName = null;
/*      */   private SimpleDateFormat formaterUtc;
/*      */   
/*      */   public SincronizadorWRMagayaToOERP() {
/*   55 */     this.transTypeDateFieldName = new HashMap<String, String>();
/*   56 */     this.transTypeDateFieldName.put("WH", "created_on");
/*   57 */     this.transTypeDateFieldName.put("SH", "created_on");
/*      */     
/*   59 */     this.formaterUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/*   60 */     this.formaterUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public Boolean testConection() {
/*  315 */     Boolean loginOk = Boolean.valueOf(false);
/*  316 */     OpenerpClient openerpClient = new OpenerpClient();
/*  317 */     MagayaWS magayaWS = new MagayaWS();
/*      */     
/*  319 */     String user = null;
/*  320 */     String pass = null;
/*  321 */     String db = null;
/*  322 */     String host = null;
/*  323 */     String port = null;
/*  324 */     Properties props = new Properties();
/*      */     
/*  326 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/*      */     
/*      */     try {
/*  329 */       props.load(new FileInputStream("operp_db.conf"));
/*      */       
/*  331 */       db = props.getProperty("db");
/*  332 */       user = props.getProperty("user");
/*  333 */       pass = props.getProperty("pass");
/*  334 */       host = props.getProperty("host");
/*  335 */       port = props.getProperty("port");
/*  336 */       //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */       
/*  338 */       openerpClient.login(db, user, pass, host, port);
/*  339 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/*      */       
/*  341 */       loginOk = Boolean.valueOf(magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString()));
/*  342 */       magayaWS.getActiveCurrencies();
/*  343 */       magayaWS.logout();
/*      */     }
/*  345 */     catch (FileNotFoundException e) {
/*  346 */       e.printStackTrace();
/*  347 */       return Boolean.valueOf(false);
/*  348 */     } catch (IOException e) {
/*  349 */       e.printStackTrace();
/*  350 */       return Boolean.valueOf(false);
/*  351 */     } catch (XmlRpcException e) {
/*  352 */       e.printStackTrace();
/*  353 */       return Boolean.valueOf(false);
/*      */     } 
/*  355 */     return loginOk;
/*      */   }
/*      */ 
/*      */   
/*      */   public Object marcarParaCrearTransaccion(TransactionType transType, OpenerpClient openerpClient, String guid, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
/*  360 */     HashMap<Object, Object> values = new HashMap<Object, Object>();
/*      */ 
/*      */     
/*  363 */     values.put("guid", guid);
/*      */     
/*  365 */     values.put("deleted", Boolean.FALSE);
/*  366 */     values.put("update_required", Integer.valueOf(100));
/*      */ 
/*      */     
/*  369 */     if (desdeFechaHora != null)
/*  370 */       values.put("last_query_log_from", this.formaterUtc.format(desdeFechaHora)); 
/*  371 */     if (hastaFechaHora != null) {
/*  372 */       values.put("last_query_log_to", this.formaterUtc.format(hastaFechaHora));
/*      */     }
/*  374 */     switch (transType) {
/*      */       case WarehouseReceipt:
/*  376 */         values.put("state", "draft");
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  382 */         return openerpClient.crearTransaccionWR(values, null, null, null, null);
/*      */       case Shipment:
/*  384 */         return openerpClient.crearTransaccionSH(values, null, null, null);
/*      */       case CargoRelease:
/*  386 */         values.put("state", "draft");
/*  387 */         return openerpClient.crearTransaccionCR(values, null, null);
/*      */     } 
/*  389 */     return null;
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private void marcarParaEliminarTransaccion(TransactionType transType, Element nodoWR, OpenerpClient openerpClient, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
/*  395 */     HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();
/*  396 */     paresNombreValor.put("update_required", Integer.valueOf(0));
/*  397 */     paresNombreValor.put("deleted", Boolean.TRUE);
/*      */     
/*  399 */     if (desdeFechaHora != null)
/*  400 */       paresNombreValor.put("last_query_log_from", this.formaterUtc.format(desdeFechaHora)); 
/*  401 */     if (hastaFechaHora != null) {
/*  402 */       paresNombreValor.put("last_query_log_to", this.formaterUtc.format(hastaFechaHora));
/*      */     }
/*  404 */     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/*  405 */     Vector<Object> guidFilter = new Vector();
/*  406 */     guidFilter.add("guid");
/*  407 */     guidFilter.add("=");
/*  408 */     guidFilter.add(nodoWR.getChildText("GUID"));
/*  409 */     vectorOfVectorFilterTuples.add(guidFilter);
/*  410 */     openerpClient.write(transType.getOpenerpModel(), openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples)[0], paresNombreValor);
/*      */   }
/*      */   
/*      */   private void marcarParaActualizarTransaccion(TransactionType transType, Element nodoWR, OpenerpClient openerpClient, Integer prioridad, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
/*  414 */     marcarParaActualizarTransaccion(transType, nodoWR.getChildText("GUID"), openerpClient, prioridad, desdeFechaHora, hastaFechaHora);
/*      */   }
/*      */   
/*      */   private Object marcarParaActualizarTransaccion(TransactionType transType, String guid, OpenerpClient openerpClient, Integer prioridad, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException {
/*  418 */     HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();
/*      */     
/*  420 */     paresNombreValor.put("update_required", prioridad);
/*      */     
/*  422 */     if (desdeFechaHora != null)
/*  423 */       paresNombreValor.put("last_query_log_from", this.formaterUtc.format(desdeFechaHora)); 
/*  424 */     if (hastaFechaHora != null) {
/*  425 */       paresNombreValor.put("last_query_log_to", this.formaterUtc.format(hastaFechaHora));
/*      */     }
/*  427 */     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/*  428 */     Vector<Object> guidFilter = new Vector();
/*  429 */     guidFilter.add("guid");
/*  430 */     guidFilter.add("=");
/*  431 */     guidFilter.add(guid);
/*      */     
/*  433 */     vectorOfVectorFilterTuples.add(guidFilter);
/*      */     
/*  435 */     Object id = openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples)[0];
/*  436 */     openerpClient.write(transType.getOpenerpModel(), id, paresNombreValor);
/*      */ 
/*      */     
/*  439 */     return id;
/*      */   }
/*      */   
/*      */   private void desmarcarParaActualizarTransaccion(TransactionType transType, String guid, OpenerpClient openerpClient) throws XmlRpcException {
/*  443 */     HashMap<Object, Object> paresNombreValor = new HashMap<Object, Object>();
/*  444 */     paresNombreValor.put("update_required", Integer.valueOf(-20));
/*      */     
/*  446 */     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/*  447 */     Vector<Object> guidFilter = new Vector();
/*  448 */     guidFilter.add("guid");
/*  449 */     guidFilter.add("=");
/*  450 */     guidFilter.add(guid);
/*      */     
/*  452 */     vectorOfVectorFilterTuples.add(guidFilter);
/*      */     
/*  454 */     openerpClient.write(transType.getOpenerpModel(), openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples)[0], paresNombreValor);
/*      */   }
/*      */   
/*      */   public void actualizar(TransactionType transType, Object OpenerpTransId, Element nodoTrans, OpenerpClient openerpClient) throws XmlRpcException, JDOMException, IOException {
/*  458 */     actualizar(transType, OpenerpTransId, nodoTrans, openerpClient, true);
/*      */   }
/*      */   public void actualizar(TransactionType transType, Object OpenerpTransId, Element nodoTrans, OpenerpClient openerpClient, boolean guardarXml) throws XmlRpcException, JDOMException, IOException {
/*      */     Vector wrItemsGuids, wrItemIds;
/*  462 */     HashMap<Object, Object> values = null;
/*  463 */     Object parentId = null;
/*  464 */     switch (transType) {
/*      */       case WarehouseReceipt:
/*  466 */         values = getWRValues(nodoTrans);
/*      */         break;
/*      */       case Shipment:
/*  469 */         values = getSHValues(nodoTrans);
/*  470 */         if (nodoTrans.getChild("MasterGUID") != null) {
/*  471 */           Vector<String> filter = new Vector();
/*  472 */           filter.addElement("guid");
/*  473 */           filter.addElement("=");
/*  474 */           filter.addElement(nodoTrans.getChildText("MasterGUID"));
/*  475 */           Vector<Vector<String>> filters = new Vector();
/*  476 */           filters.add(filter);
/*  477 */           Object[] ids = openerpClient.search(transType.getOpenerpModel(), (Vector)filters);
/*  478 */           if (ids.length != 0) {
/*  479 */             parentId = ids[0];
/*  480 */             //Logger.getLogger(getClass().getSimpleName()).info("Seteando parent: " + nodoTrans.getChildText("MasterGUID"));
/*      */           } 
/*      */         } 
/*      */         break;
/*      */       case CargoRelease:
/*  485 */         values = getCRValues(nodoTrans);
/*      */         break;
/*      */       
/*      */       default:
/*      */         return;
/*      */     } 
/*  491 */     values.put("guid", nodoTrans.getAttributeValue("GUID"));
/*      */     
/*  493 */     values.put("deleted", Boolean.FALSE);
/*  494 */     values.put("update_required", Integer.valueOf(0));
/*      */     
/*  496 */     HashMap<Object, Object> hashShipperAddress = null;
/*  497 */     if (nodoTrans.getChild("ShipperAddress") != null) {
/*  498 */       hashShipperAddress = getAddress(nodoTrans.getChild("ShipperAddress"));
/*      */     }
/*  500 */     HashMap<Object, Object> hashConsigneeAddress = null;
/*  501 */     if (nodoTrans.getChild("ConsigneeAddress") != null) {
/*  502 */       hashConsigneeAddress = getAddress(nodoTrans.getChild("ConsigneeAddress"));
/*      */     }
/*  504 */     HashMap<Object, Object> hashReleaseToAddress = null;
/*  505 */     if (nodoTrans.getChild("ReleasedToAddress") != null) {
/*  506 */       hashReleaseToAddress = getAddress(nodoTrans.getChild("ReleasedToAddress"));
/*      */     }
/*  508 */     ArrayList<HashMap<Object, Object>> listaItems = new ArrayList<HashMap<Object, Object>>();
/*  509 */     if (transType == TransactionType.WarehouseReceipt) {
/*  510 */       if (nodoTrans.getChild("Items") != null) {
/*  511 */         listaItems = getListaItems(nodoTrans.getChild("Items"));
/*      */       }
/*  513 */       values.put("atlas_tracking", getAtlasTrackingContent(values, listaItems));
/*      */     } 
/*  515 */     XmlRepository.getInstance().saveUltimoXmlPara(transType, nodoTrans.getAttributeValue("GUID"), nodoTrans);
/*      */ 
/*      */     
/*  518 */     ArrayList<HashMap<String, String>> attachments = downloadAttachmentsFor(nodoTrans);
/*      */ 
/*      */     
/*  521 */     Vector wrGuids = new Vector();
/*  522 */     switch (transType) {
/*      */       
/*      */       case WarehouseReceipt:
/*  525 */         openerpClient.actualizarTransaccionWR(OpenerpTransId, values, hashShipperAddress, hashConsigneeAddress, listaItems, attachments);
/*      */         break;
/*      */       
/*      */       case Shipment:
/*  529 */         if (nodoTrans.getChild("Items") != null) {
/*      */ 
/*      */           
/*  532 */           verificarExistenciaYCrearWrsFaltantes(nodoTrans.getChild("Items"), openerpClient, TransactionType.Shipment, OpenerpTransId);
/*  533 */           wrGuids = getListaGuidsWRs(nodoTrans.getChild("Items"));
/*      */         } 
/*  535 */         openerpClient.actualizarTransaccionSH(OpenerpTransId, parentId, values, hashShipperAddress, hashConsigneeAddress, wrGuids);
/*      */         break;
/*      */       case CargoRelease:
/*  538 */         wrItemsGuids = new Vector();
/*  539 */         wrItemIds = new Vector();
/*  540 */         if (nodoTrans.getChild("Items") != null) {
/*  541 */           verificarExistenciaYCrearWrsFaltantes(nodoTrans.getChild("Items"), openerpClient, TransactionType.Shipment, OpenerpTransId);
/*      */ 
/*      */           
/*  544 */           wrGuids = getListaGuidsWRs(nodoTrans.getChild("Items"));
/*      */         } 
/*  546 */         openerpClient.actualizarTransaccionCR(OpenerpTransId, values, hashReleaseToAddress, wrGuids, attachments);
/*      */         break;
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private Vector getWrItemIds(Vector wrItemsGuids) throws XmlRpcException {
/*  554 */     Vector<String> filter = new Vector();
/*  555 */     filter.addElement("guid");
/*  556 */     filter.addElement("in");
/*  557 */     filter.addElement(wrItemsGuids);
/*  558 */     Vector<Vector<String>> filters = new Vector();
/*  559 */     filters.add(filter);
/*  560 */     Object[] wrItensIds = openerpClient.search(TransactionType.WarehouseItem.getOpenerpModel(), (Vector)filters);
/*  561 */     Vector<Object> res = new Vector();
/*  562 */     for (int i = 0; i < wrItensIds.length; i++) {
/*  563 */       res.add(wrItensIds[i]);
/*      */     }
/*  565 */     return res;
/*      */   }
/*      */   
/*      */   private Vector getListaGuidsWRItems(Element items) {
/*  569 */     Vector<String> listaItems = new Vector();
/*      */     
/*  571 */     List<Element> nodosItems = items.getChildren("Item");
/*  572 */     for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext(); ) {
/*  573 */       Element nodoItem = iterator.next();
/*  574 */       if (nodoItem.getAttributeValue("GUID") != null)
/*      */       {
/*  576 */         listaItems.add(nodoItem.getAttributeValue("GUID")); } 
/*      */     } 
/*  578 */     return listaItems;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private HashMap<Object, Object> getCRValues(Element nodoCR) {
/*  590 */     DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");
/*      */     
/*  592 */     HashMap<Object, Object> values = new HashMap<Object, Object>();
/*  593 */     values.put("guid", nodoCR.getAttributeValue("GUID"));
/*      */     
/*      */     try {
/*  596 */       values.put("name", Integer.valueOf((nodoCR.getChildText("Number") != null) ? Integer.valueOf(nodoCR.getChildText("Number")).intValue() : 0));
/*  597 */     } catch (NumberFormatException e1) {
/*      */       
/*  599 */       values.put("name", Integer.valueOf(0));
/*      */     } 
/*      */     
/*      */     try {
/*  603 */       values.put("created_on", (nodoCR.getChildText("CreatedOn") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(replaceCharAt(nodoCR.getChildText("CreatedOn"), nodoCR.getChildText("CreatedOn").lastIndexOf(":"), ""))) : "");
/*  604 */     } catch (ParseException e) {
/*  605 */       Logger.getLogger(getClass().getSimpleName()).error("El numero de transaccion no es un numero, seteandolo a 0", e);
/*  606 */       values.put("created_on", Integer.valueOf(0));
/*      */     } 
/*      */     
/*      */     try {
/*  610 */       values.put("release_date", (nodoCR.getChildText("ReleaseDate") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(replaceCharAt(nodoCR.getChildText("ReleaseDate"), nodoCR.getChildText("ReleaseDate").lastIndexOf(":"), ""))) : "");
/*  611 */     } catch (ParseException e) {
/*  612 */       Logger.getLogger(getClass().getSimpleName()).error("El numero de transaccion no es un numero, seteandolo a 0", e);
/*  613 */       values.put("release_date", Integer.valueOf(0));
/*      */     } 
/*      */     
/*  616 */     values.put("issued_by", (nodoCR.getChildText("IssuedByName") != null) ? nodoCR.getChildText("IssuedByName") : "");
/*  617 */     values.put("release_by", (nodoCR.getChildText("CreatedByName") != null) ? nodoCR.getChildText("CreatedByName") : "");
/*  618 */     values.put("status", (nodoCR.getChildText("Status") != null) ? nodoCR.getChildText("Status") : "");
/*  619 */     values.put("carrier_name", (nodoCR.getChildText("CarrierName") != null) ? nodoCR.getChildText("CarrierName") : "");
/*  620 */     values.put("carrier_tracking_number", (nodoCR.getChildText("CarrierTrackingNumber") != null) ? nodoCR.getChildText("CarrierTrackingNumber") : "");
/*  621 */     values.put("release_to", (nodoCR.getChildText("ReleasedToName") != null) ? nodoCR.getChildText("ReleasedToName") : "");
/*  622 */     values.put("driver_name", (nodoCR.getChildText("DriverName") != null) ? nodoCR.getChildText("DriverName") : "");
/*  623 */     values.put("driver_license_number", (nodoCR.getChildText("DriverLicenseNumber") != null) ? nodoCR.getChildText("DriverLicenseNumber") : "");
/*  624 */     values.put("notes", (nodoCR.getChildText("Notes") != null) ? nodoCR.getChildText("Notes") : "");
/*  625 */     values.put("state", "draft");
/*      */     
/*  627 */     if (nodoCR.getChildText("TotalValue") != null) {
/*  628 */       values.put("aplicable_charges", nodoCR.getChildText("TotalValue"));
/*  629 */       values.put("currency", nodoCR.getChild("TotalValue").getAttributeValue("Currency"));
/*      */     } 
/*      */     
/*  632 */     values.put("total_pieces", (nodoCR.getChildText("TotalPieces") != null) ? nodoCR.getChildText("TotalPieces") : "");
/*      */     
/*  634 */     if (nodoCR.getChildText("TotalWeight") != null) {
/*  635 */       values.put("total_weight", nodoCR.getChildText("TotalWeight"));
/*  636 */       values.put("total_weight_unit", nodoCR.getChild("TotalWeight").getAttributeValue("Unit"));
/*      */     } 
/*  638 */     if (nodoCR.getChildText("TotalVolume") != null) {
/*  639 */       values.put("total_volume", nodoCR.getChildText("TotalVolume"));
/*  640 */       values.put("total_volume_unit", nodoCR.getChild("TotalVolume").getAttributeValue("Unit"));
/*      */     } 
/*  642 */     if (nodoCR.getChildText("TotalVolumeWeight") != null) {
/*  643 */       values.put("total_volume_weight", nodoCR.getChildText("TotalVolumeWeight"));
/*  644 */       values.put("total_volume_weight_unit", nodoCR.getChild("TotalVolumeWeight").getAttributeValue("Unit"));
/*      */     } 
/*      */     
/*  647 */     return values;
/*      */   }
/*      */ 
/*      */   
/*      */   private ArrayList<HashMap<String, String>> downloadAttachmentsFor(Element nodoTrans) {
/*  652 */     ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();
/*      */     
/*  654 */     if (nodoTrans.getChild("Attachments") != null && nodoTrans.getChild("Attachments").getChild("Attachment") != null) {
/*  655 */       List<Element> attachmentElements = nodoTrans.getChild("Attachments").getChildren("Attachment");
/*  656 */       for (Element element : attachmentElements) {
/*      */         try {
/*  658 */           String name = element.getChildText("Name");
/*  659 */           String extension = element.getChildText("Extension");
/*  660 */           String isImage = element.getChildText("IsImage");
/*      */           
/*  662 */           String ownerGUID = element.getChildText("OwnerGUID");
/*  663 */           String ownerType = element.getChildText("OwnerType");
/*  664 */           String identifier = element.getChildText("Identifier");
/*      */           
/*  666 */           String attachXml = magayaWS.getAttachment(ownerType, ownerGUID, identifier);
/*      */           
/*  668 */           SAXBuilder builder = new SAXBuilder();
/*      */           
/*  670 */           attachXml = attachXml.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/*  671 */           //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Parseando respuesta...");
/*  672 */           Reader in = new StringReader(attachXml);
/*  673 */           Document document = builder.build(in);
/*  674 */           Element nodoattach = document.getRootElement();
/*      */           
/*  676 */           String data = nodoattach.getChildText("Data");
/*      */           
/*  678 */           HashMap<String, String> attach = new HashMap<String, String>();
/*  679 */           attach.put("name", name);
/*  680 */           attach.put("extension", extension);
/*  681 */           attach.put("isImage", isImage);
/*  682 */           attach.put("data", data);
/*      */           
/*  684 */           res.add(attach);
/*  685 */         } catch (JDOMException e) {
/*      */           
/*  687 */           e.printStackTrace();
/*  688 */         } catch (IOException e) {
/*      */           
/*  690 */           e.printStackTrace();
/*      */         } 
/*      */       } 
/*      */     } 
/*      */     
/*  695 */     return res;
/*      */   }
/*      */ 
/*      */   
/*      */   private ArrayList<HashMap<String, String>> getAttachments(Element nodoTrans) {
/*  700 */     ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();
/*      */     
/*  702 */     if (nodoTrans.getChild("Attachments") != null && nodoTrans.getChild("Attachments").getChild("Attachment") != null) {
/*  703 */       List<Element> attachmentElements = nodoTrans.getChild("Attachments").getChildren("Attachment");
/*  704 */       for (Element element : attachmentElements) {
/*  705 */         String name = element.getChildText("Name");
/*  706 */         String extension = element.getChildText("Extension");
/*  707 */         String isImage = element.getChildText("IsImage");
/*  708 */         String data = element.getChildText("Data");
/*      */         
/*  710 */         HashMap<String, String> attach = new HashMap<String, String>();
/*  711 */         attach.put("name", name);
/*  712 */         attach.put("extension", extension);
/*  713 */         attach.put("isImage", isImage);
/*  714 */         attach.put("data", data);
/*      */         
/*  716 */         res.add(attach);
/*      */       } 
/*      */     } 
/*      */     
/*  720 */     return res;
/*      */   }
/*      */ 
/*      */   
/*      */   private String getAtlasTrackingContent(HashMap<Object, Object> values, ArrayList<HashMap<Object, Object>> listaItems) {
/*  725 */     String atlas_tracking = (values.get("carrier_tracking_number") != "") ? (new StringBuilder()).append(values.get("carrier_tracking_number")).append("\n").append(values.get("notes")).toString() : (String)values.get("notes");
/*      */     
/*  727 */     TreeMap<Integer, HashMap<Object, Object>> tree = new TreeMap<Integer, HashMap<Object, Object>>();
/*  728 */     for (int i = 0; i < listaItems.size(); i++) {
/*  729 */       tree.put(Integer.valueOf(((HashMap)listaItems.get(i)).get("name").toString()), listaItems.get(i));
/*      */     }
/*  731 */     Iterator<Integer> it = tree.keySet().iterator();
/*  732 */     while (it.hasNext()) {
/*  733 */       Integer num = it.next();
/*  734 */       atlas_tracking = String.valueOf(atlas_tracking) + "\n" + ((HashMap)tree.get(num)).get("notes").toString();
/*      */     } 
/*  736 */     return atlas_tracking;
/*      */   }
/*      */ 
/*      */   
/*      */   private void verificarExistenciaYCrearWrsFaltantes(Element wrItems, OpenerpClient openerpClient, TransactionType parentTransType, Object parentTransId) throws XmlRpcException, JDOMException, IOException {
/*  741 */     Vector<String> wrGuids = getListaGuidsWRs(wrItems);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     
/*  752 */     Object[] readRes = openerpClient.read(parentTransType.getOpenerpModel(), new Object[] { parentTransId }, new Vector());
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     
/*  771 */     Vector<String> fieldsSoloGUID = new Vector();
/*  772 */     fieldsSoloGUID.add("guid");
/*  773 */     Object[] wr_item_ids = ((Map<?, Object[]>)readRes[0]).get("wr_item_ids");
/*  774 */     for (int i = 0; i < wr_item_ids.length; i++) {
/*  775 */       Object[] readWR = openerpClient.read(TransactionType.WarehouseReceipt.getOpenerpModel(), new Object[] { wr_item_ids[i] }, fieldsSoloGUID);
/*  776 */       String guidWR = ((Map<?, String>)readWR[0]).get("guid");
/*  777 */       if (!wrGuids.contains(guidWR)) {
/*  778 */         wrGuids.add(guidWR);
/*      */       }
/*      */     } 
/*      */ 
/*      */     
/*  783 */     Vector<String> filter = new Vector();
/*  784 */     filter.addElement("guid");
/*  785 */     filter.addElement("in");
/*  786 */     filter.addElement(wrGuids);
/*  787 */     Vector<Vector<String>> filters = new Vector();
/*  788 */     filters.add(filter);
/*  789 */     Object[] wrIds = openerpClient.search(TransactionType.WarehouseReceipt.getOpenerpModel(), (Vector)filters);
/*      */     
/*  791 */     Vector<String> fieldNames = new Vector();
/*  792 */     fieldNames.add("guid");
/*  793 */     Object[] wrRead = openerpClient.read(TransactionType.WarehouseReceipt.getOpenerpModel(), wrIds, fieldNames);
/*      */     
/*  795 */     for (Iterator<String> iterator = wrGuids.iterator(); iterator.hasNext(); ) {
/*  796 */       String guidWR = iterator.next();
/*  797 */       Object idWr = null;
/*      */       
/*  799 */       boolean existe = false;
/*  800 */       for (int j = 0; j < wrRead.length; j++) {
/*  801 */         if (((Map<?, V>)wrRead[j]).get("guid").equals(guidWR)) {
/*  802 */           existe = true;
/*  803 */           idWr = ((Map<?, ?>)wrRead[j]).get("id");
/*      */         } 
/*      */       } 
/*  806 */       SAXBuilder builder = new SAXBuilder();
/*  807 */       if (!existe) {
/*  808 */         idWr = marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guidWR, null, null);
/*      */       }
/*      */ 
/*      */       
/*  812 */       String xmlTranss = magayaWS.getTransaction(TransactionType.WarehouseReceipt.getCode(), guidWR, Integer.valueOf(546));
/*  813 */       if (xmlTranss != null) {
/*  814 */         Logger.getLogger(getClass().getSimpleName()).warn("Descargando WR faltante, referenciado por otra transaccion que se ha descargado");
/*  815 */         xmlTranss = xmlTranss.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/*  816 */         //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*  817 */         Reader in = new StringReader(xmlTranss);
/*  818 */         Document document = builder.build(in);
/*  819 */         Element nodoTrans = document.getRootElement();
/*      */         
/*      */         try {
/*  822 */           //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/*  823 */           //Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));
/*      */           
/*  825 */           Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + TransactionType.WarehouseReceipt.getCode() + ")...");
/*  826 */           actualizar(TransactionType.WarehouseReceipt, idWr, nodoTrans, openerpClient);
/*      */         }
/*  828 */         catch (Exception e) {
/*  829 */           Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
/*      */         } 
/*      */         continue;
/*      */       } 
/*  833 */       Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta. ");
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void verificarExistenciaYCrearWrsFaltantesRutilizandoXML(Element wrItems, OpenerpClient openerpClient) throws XmlRpcException, JDOMException, IOException {
/*  843 */     Vector wrGuids = getListaGuidsWRs(wrItems);
/*      */     
/*  845 */     List<Element> nodosItems = wrItems.getChildren("Item");
/*      */     
/*  847 */     Vector<String> filter = new Vector();
/*  848 */     filter.addElement("guid");
/*  849 */     filter.addElement("in");
/*  850 */     filter.addElement(wrGuids);
/*  851 */     Vector<Vector<String>> filters = new Vector();
/*  852 */     filters.add(filter);
/*  853 */     Object[] wrIds = openerpClient.search(TransactionType.WarehouseReceipt.getOpenerpModel(), (Vector)filters);
/*      */     
/*  855 */     Vector<String> fieldNames = new Vector();
/*  856 */     fieldNames.add("guid");
/*  857 */     Object[] wrRead = openerpClient.read(TransactionType.WarehouseReceipt.getOpenerpModel(), wrIds, fieldNames);
/*      */     
/*  859 */     for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext(); ) {
/*  860 */       Element nodoItem = iterator.next();
/*      */       
/*  862 */       String guidWR = nodoItem.getChildText("WarehouseReceiptGUID");
/*  863 */       Object idWr = null;
/*      */       
/*  865 */       boolean existe = false;
/*  866 */       for (int i = 0; i < wrRead.length; i++) {
/*  867 */         if (((Map<?, V>)wrRead[i]).get("guid").equals(guidWR)) {
/*  868 */           existe = true;
/*  869 */           idWr = ((Map<?, ?>)wrRead[i]).get("id");
/*      */         } 
/*      */       } 
/*  872 */       SAXBuilder builder = new SAXBuilder();
/*  873 */       if (!existe) {
/*  874 */         idWr = marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guidWR, null, null);
/*      */       }
/*      */ 
/*      */       
/*  878 */       String xmlTranss = magayaWS.getTransaction(TransactionType.WarehouseReceipt.getCode(), guidWR, Integer.valueOf(546));
/*  879 */       if (xmlTranss != null) {
/*  880 */         Logger.getLogger(getClass().getSimpleName()).warn("Descargando WR faltante, referenciado por otra transaccion que se ha descargado");
/*  881 */         xmlTranss = xmlTranss.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/*  882 */         //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*  883 */         Reader in = new StringReader(xmlTranss);
/*  884 */         Document document = builder.build(in);
/*  885 */         Element nodoTrans = document.getRootElement();
/*      */         
/*      */         try {
/*  888 */           //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/*  889 */           //Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));
/*      */           
/*  891 */           Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + TransactionType.WarehouseReceipt.getCode() + ")...");
/*  892 */           actualizar(TransactionType.WarehouseReceipt, idWr, nodoTrans, openerpClient);
/*      */         }
/*  894 */         catch (Exception e) {
/*  895 */           Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
/*      */         } 
/*      */         continue;
/*      */       } 
/*  899 */       Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta. ");
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private Vector getListaGuidsWRs(Element items) {
/*  908 */     Vector<String> listaItems = new Vector();
/*      */     
/*  910 */     List<Element> nodosItems = items.getChildren("Item");
/*  911 */     for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext(); ) {
/*  912 */       Element nodoItem = iterator.next();
/*  913 */       if (nodoItem.getChildText("WarehouseReceiptGUID") != null) {
/*  914 */         listaItems.add(nodoItem.getChildText("WarehouseReceiptGUID"));
/*      */       }
/*  916 */       if (nodoItem.getChild("ContainedItems") != null) {
/*  917 */         List<Element> nodosItemsContenidos = nodoItem.getChild("ContainedItems").getChildren("Item");
/*  918 */         for (Iterator<Element> iteratorContenidos = nodosItemsContenidos.iterator(); iteratorContenidos.hasNext(); ) {
/*  919 */           Element nodoItemContenido = iteratorContenidos.next();
/*  920 */           if (nodoItemContenido.getChildText("WarehouseReceiptGUID") != null) {
/*  921 */             listaItems.add(nodoItemContenido.getChildText("WarehouseReceiptGUID"));
/*      */           }
/*      */         } 
/*      */       } 
/*      */     } 
/*  926 */     return listaItems;
/*      */   }
/*      */ 
/*      */   
/*      */   public Integer crearTransaccion(Element nodoWR, OpenerpClient openerpClient) throws XmlRpcException {
/*  931 */     HashMap<Object, Object> values = getWRValues(nodoWR);
/*      */     
/*  933 */     values.put("guid", nodoWR.getAttributeValue("GUID"));
/*      */     
/*  935 */     values.put("deleted", Boolean.FALSE);
/*  936 */     values.put("update_required", Boolean.FALSE);
/*      */     
/*  938 */     HashMap<Object, Object> hashShipperAddress = null;
/*  939 */     if (nodoWR.getChild("ShipperAddress") != null) {
/*  940 */       hashShipperAddress = getAddress(nodoWR.getChild("ShipperAddress"));
/*      */     }
/*  942 */     HashMap<Object, Object> hashConsigneeAddress = null;
/*  943 */     if (nodoWR.getChild("ConsigneeAddress") != null) {
/*  944 */       hashConsigneeAddress = getAddress(nodoWR.getChild("ConsigneeAddress"));
/*      */     }
/*  946 */     ArrayList<HashMap<Object, Object>> listaItems = new ArrayList<HashMap<Object, Object>>();
/*  947 */     if (nodoWR.getChild("Items") != null) {
/*  948 */       listaItems = getListaItems(nodoWR.getChild("Items"));
/*      */     }
/*  950 */     HashMap<Object, Object> hashMapXml = new HashMap<Object, Object>();
/*  951 */     hashMapXml.put("xml_response", (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoWR));
/*      */     
/*  953 */     return openerpClient.crearTransaccionWR(values, hashShipperAddress, hashConsigneeAddress, listaItems, hashMapXml);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private HashMap<Object, Object> getSHValues(Element nodoSH) {
/*  971 */     HashMap<Object, Object> values = new HashMap<Object, Object>();
/*  972 */     values.put("guid", nodoSH.getAttributeValue("GUID"));
/*  973 */     values.put("sh_number", nodoSH.getChildText("Number"));
/*      */     
/*      */     try {
/*  976 */       values.put("name", Integer.valueOf((nodoSH.getChildText("Number") != null) ? Integer.valueOf(nodoSH.getChildText("Number")).intValue() : 0));
/*  977 */     } catch (NumberFormatException e1) {
/*      */       
/*  979 */       values.put("name", Integer.valueOf(0));
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*      */     try {
/*  985 */       values.put("created_on", (nodoSH.getChildText("CreatedOn") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(replaceCharAt(nodoSH.getChildText("CreatedOn"), nodoSH.getChildText("CreatedOn").lastIndexOf(":"), ""))) : "");
/*  986 */     } catch (ParseException e) {
/*  987 */       e.printStackTrace();
/*      */     } 
/*      */     
/*  990 */     values.put("status", (nodoSH.getChildText("Status") != null) ? nodoSH.getChildText("Status") : "");
/*  991 */     values.put("issued_by_name", (nodoSH.getChildText("IssuedByName") != null) ? nodoSH.getChildText("IssuedByName") : "");
/*  992 */     values.put("created_by_name", (nodoSH.getChildText("CreatedByName") != null) ? nodoSH.getChildText("CreatedByName") : "");
/*  993 */     values.put("shipment_type", nodoSH.getName().replace("Shipment", ""));
/*  994 */     values.put("shipper_name", (nodoSH.getChildText("ShipperName") != null) ? nodoSH.getChildText("ShipperName") : "");
/*  995 */     values.put("consignee_name", (nodoSH.getChildText("ConsigneeName") != null) ? nodoSH.getChildText("ConsigneeName") : "");
/*  996 */     String letrasTipo = "";
/*  997 */     if (nodoSH.getChildText("MasterGUID") != null && !nodoSH.getChildText("MasterGUID").isEmpty()) {
/*  998 */       letrasTipo = "H";
/*      */     } else {
/* 1000 */       letrasTipo = "M";
/* 1001 */     }  if (values.get("shipment_type").equals("Air")) {
/* 1002 */       letrasTipo = String.valueOf(letrasTipo) + "A";
/* 1003 */     } else if (values.get("shipment_type").equals("Ocean")) {
/* 1004 */       letrasTipo = String.valueOf(letrasTipo) + "B";
/*      */     } else {
/* 1006 */       letrasTipo = "";
/* 1007 */     }  if (!letrasTipo.isEmpty())
/*      */     {
/*      */       
/* 1010 */       values.put("transport_document_number", (nodoSH.getChildText("OutHouseWayBillNumber") != null) ? nodoSH.getChildText("OutHouseWayBillNumber") : ""); } 
/* 1011 */     values.put("document_master_number", (nodoSH.getChildText("MasterWayBillNumber") != null) ? nodoSH.getChildText("MasterWayBillNumber") : "");
/* 1012 */     values.put("description_of_goods", (nodoSH.getChildText("DescriptionOfGoods") != null) ? nodoSH.getChildText("DescriptionOfGoods") : "");
/* 1013 */     if (nodoSH.getChildText("ChargeableWeight") != null) {
/* 1014 */       values.put("charge_wiegth", nodoSH.getChildText("ChargeableWeight"));
/* 1015 */       values.put("charge_wiegth_unit", nodoSH.getChild("ChargeableWeight").getAttributeValue("Unit"));
/* 1016 */       values.put("chargeable_weight", nodoSH.getChildText("ChargeableWeight"));
/* 1017 */       values.put("chargeable_weight_unit", nodoSH.getChild("ChargeableWeight").getAttributeValue("Unit"));
/*      */     } 
/* 1019 */     values.put("booking_number", (nodoSH.getChildText("BookingNumber") != null) ? nodoSH.getChildText("BookingNumber") : "");
/* 1020 */     values.put("carrier_name", (nodoSH.getChildText("CarrierName") != null) ? nodoSH.getChildText("CarrierName") : "");
/*      */     
/* 1022 */     if (nodoSH.getChild("OriginPort") != null) {
/* 1023 */       values.put("origin_port_name", (nodoSH.getChild("OriginPort").getChildText("Name") != null) ? nodoSH.getChild("OriginPort").getChildText("Name") : "");
/* 1024 */       values.put("origin_port_code", (nodoSH.getAttributeValue("Code") != null) ? nodoSH.getAttributeValue("Code") : "");
/* 1025 */       values.put("origin_port_country_name", (nodoSH.getChild("OriginPort").getChildText("Country") != null) ? nodoSH.getChild("OriginPort").getChildText("Country") : "");
/* 1026 */       values.put("origin_port_country_code", (nodoSH.getChild("OriginPort").getChild("Country").getAttributeValue("Code") != null) ? nodoSH.getChild("OriginPort").getChild("Country").getAttributeValue("Code") : "");
/*      */     } 
/*      */     
/* 1029 */     if (nodoSH.getChild("DestinationPort") != null) {
/* 1030 */       values.put("destination_port_name", (nodoSH.getChild("DestinationPort").getChildText("Name") != null) ? nodoSH.getChild("DestinationPort").getChildText("Name") : "");
/* 1031 */       values.put("destination_port_code", (nodoSH.getChild("DestinationPort").getAttributeValue("Code") != null) ? nodoSH.getChild("DestinationPort").getAttributeValue("Code") : "");
/* 1032 */       values.put("destination_port_country_name", (nodoSH.getChild("DestinationPort").getChildText("Country") != null) ? nodoSH.getChild("DestinationPort").getChildText("Country") : "");
/* 1033 */       values.put("destination_port_country_code", (nodoSH.getChild("DestinationPort").getChild("Country").getAttributeValue("Code") != null) ? nodoSH.getChild("DestinationPort").getChild("Country").getAttributeValue("Code") : "");
/*      */     } 
/*      */     
/* 1036 */     if (nodoSH.getChild("ModeOfTransportation") != null)
/* 1037 */       values.put("mode_of_transportation", (nodoSH.getChild("ModeOfTransportation").getChildText("Method") != null) ? nodoSH.getChild("ModeOfTransportation").getChildText("Method") : ""); 
/* 1038 */     values.put("declared_value", (nodoSH.getChildText("DeclaredValue") != null) ? nodoSH.getChildText("DeclaredValue") : "");
/* 1039 */     if (nodoSH.getChildText("AirShipmentInfo") != null) {
/* 1040 */       values.put("declared_value_for_customs", (nodoSH.getChild("AirShipmentInfo").getChildText("DeclareValueForCustoms") != null) ? nodoSH.getChild("AirShipmentInfo").getChildText("DeclareValueForCustoms") : "");
/*      */     }
/* 1042 */     if (nodoSH.getChildText("TotalValue") != null) {
/* 1043 */       values.put("currency", nodoSH.getChild("TotalValue").getAttributeValue("Currency"));
/*      */     }
/*      */     
/*      */     try {
/* 1047 */       values.put("pieces", Integer.valueOf((nodoSH.getChildText("TotalPieces") != null) ? Integer.valueOf(nodoSH.getChildText("TotalPieces")).intValue() : 0));
/* 1048 */     } catch (NumberFormatException e1) {
/* 1049 */       values.put("pieces", Integer.valueOf(0));
/*      */     } 
/*      */     
/* 1052 */     if (nodoSH.getChildText("TotalWeight") != null) {
/* 1053 */       values.put("weight", nodoSH.getChildText("TotalWeight"));
/* 1054 */       values.put("weight_unit", nodoSH.getChild("TotalWeight").getAttributeValue("Unit"));
/*      */       
/* 1056 */       values.put("gross_wiegth", nodoSH.getChildText("TotalWeight"));
/* 1057 */       values.put("gross_wiegth_unit", nodoSH.getChild("TotalWeight").getAttributeValue("Unit"));
/*      */     } 
/* 1059 */     if (nodoSH.getChildText("TotalVolume") != null) {
/* 1060 */       values.put("volume", nodoSH.getChildText("TotalVolume"));
/* 1061 */       values.put("volume_unit", nodoSH.getChild("TotalVolume").getAttributeValue("Unit"));
/*      */     } 
/* 1063 */     if (nodoSH.getChildText("TotalVolumeWeight") != null) {
/* 1064 */       values.put("volume_weight", nodoSH.getChildText("TotalVolumeWeight"));
/* 1065 */       values.put("volume_weight_unit", nodoSH.getChild("TotalVolumeWeight").getAttributeValue("Unit"));
/*      */     } 
/*      */     
/* 1068 */     return values;
/*      */   }
/*      */   
/*      */   public String replaceCharAt(String s, int pos, String c) {
/* 1072 */     return String.valueOf(s.substring(0, pos)) + c + s.substring(pos + 1);
/*      */   }
/*      */ 
/*      */   
/*      */   public HashMap<Object, Object> getWRValues(Element nodoWR) {
/* 1077 */     DecimalFormat formaterMonto = new DecimalFormat("###,##0.00;(###,##0.00)");
/*      */     
/* 1079 */     HashMap<Object, Object> values = new HashMap<Object, Object>();
/* 1080 */     values.put("guid", nodoWR.getAttributeValue("GUID"));
/*      */     
/*      */     try {
/* 1083 */       values.put("name", Integer.valueOf((nodoWR.getChildText("Number") != null) ? Integer.valueOf(nodoWR.getChildText("Number")).intValue() : 0));
/* 1084 */     } catch (NumberFormatException e1) {
/*      */       
/* 1086 */       values.put("name", Integer.valueOf(0));
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*      */     try {
/* 1092 */       values.put("created_on", (nodoWR.getChildText("CreatedOn") != null) ? this.formaterUtc.format(this.formaterFechaHoraSys.parse(replaceCharAt(nodoWR.getChildText("CreatedOn"), nodoWR.getChildText("CreatedOn").lastIndexOf(":"), ""))) : "");
/* 1093 */     } catch (ParseException e) {
/* 1094 */       Logger.getLogger(getClass().getSimpleName()).error("El numero de transaccion no es un numero, seteandolo a 0", e);
/* 1095 */       values.put("created_on", Integer.valueOf(0));
/*      */     } 
/*      */     
/* 1098 */     values.put("created_by_name", (nodoWR.getChildText("CreatedByName") != null) ? nodoWR.getChildText("CreatedByName") : "");
/* 1099 */     values.put("status", (nodoWR.getChildText("Status") != null) ? nodoWR.getChildText("Status") : "");
/* 1100 */     values.put("issued_by_name", (nodoWR.getChildText("IssuedByName") != null) ? nodoWR.getChildText("IssuedByName") : "");
/* 1101 */     values.put("shipper_name", (nodoWR.getChildText("ShipperName") != null) ? nodoWR.getChildText("ShipperName") : "");
/*      */     
/* 1103 */     values.put("carrier_name", (nodoWR.getChildText("CarrierName") != null) ? nodoWR.getChildText("CarrierName") : "");
/*      */     
/* 1105 */     values.put("consignee_name", (nodoWR.getChildText("ConsigneeName") != null) ? nodoWR.getChildText("ConsigneeName") : "");
/* 1106 */     values.put("total_pieces", (nodoWR.getChildText("TotalPieces") != null) ? nodoWR.getChildText("TotalPieces") : "");
/*      */     
/* 1108 */     if (nodoWR.getChildText("TotalValue") != null) {
/* 1109 */       values.put("total_value", nodoWR.getChildText("TotalValue"));
/* 1110 */       values.put("currency", nodoWR.getChild("TotalValue").getAttributeValue("Currency"));
/*      */     } 
/* 1112 */     if (nodoWR.getChildText("TotalWeight") != null) {
/* 1113 */       values.put("total_weight", nodoWR.getChildText("TotalWeight"));
/* 1114 */       values.put("total_weight_unit", nodoWR.getChild("TotalWeight").getAttributeValue("Unit"));
/*      */     } 
/* 1116 */     if (nodoWR.getChildText("TotalVolume") != null) {
/* 1117 */       values.put("total_volume", nodoWR.getChildText("TotalVolume"));
/* 1118 */       values.put("total_volume_unit", nodoWR.getChild("TotalVolume").getAttributeValue("Unit"));
/*      */     } 
/* 1120 */     if (nodoWR.getChildText("TotalVolumeWeight") != null) {
/* 1121 */       values.put("total_volume_weight", nodoWR.getChildText("TotalVolumeWeight"));
/* 1122 */       values.put("total_volume_weight_unit", nodoWR.getChild("TotalVolumeWeight").getAttributeValue("Unit"));
/*      */     } 
/*      */     
/* 1125 */     if (nodoWR.getChildText("SupplierInvoiceNumber") != null) {
/*      */       try {
/* 1127 */         String txtMonto = nodoWR.getChildText("SupplierInvoiceNumber");
/* 1128 */         Number montoNumber = formaterMonto.parse(txtMonto);
/* 1129 */         Double monto = Double.valueOf(montoNumber.doubleValue());
/* 1130 */         values.put("invoice_value", txtMonto.replace(",", ""));
/*      */       }
/* 1132 */       catch (ParseException parseException) {}
/*      */     }
/*      */ 
/*      */ 
/*      */     
/* 1137 */     values.put("driver_name", (nodoWR.getChildText("DriverName") != null) ? nodoWR.getChildText("DriverName") : "");
/* 1138 */     values.put("destination_agent_name", (nodoWR.getChildText("DestinationAgentName") != null) ? nodoWR.getChildText("DestinationAgentName") : "");
/*      */     
/* 1140 */     values.put("carrier_tracking_number", (nodoWR.getChildText("CarrierTrackingNumber") != null) ? nodoWR.getChildText("CarrierTrackingNumber") : "");
/* 1141 */     values.put("notes", (nodoWR.getChildText("Notes") != null) ? nodoWR.getChildText("Notes") : "");
/*      */ 
/*      */ 
/*      */ 
/*      */     
/* 1146 */     values.put("atlas_status", "NoReportado");
/*      */     
/* 1148 */     values.put("supplier_po_number", (nodoWR.getChildText("SupplierPONumber") != null) ? nodoWR.getChildText("SupplierPONumber") : "");
/*      */ 
/*      */ 
/*      */     
/* 1152 */     return values;
/*      */   }
/*      */   
/*      */   public ArrayList<HashMap<Object, Object>> getListaItems(Element items) {
/* 1156 */     ArrayList<HashMap<Object, Object>> listaItems = new ArrayList<HashMap<Object, Object>>();
/*      */     
/* 1158 */     List<Element> nodosItems = items.getChildren("Item");
/* 1159 */     for (Iterator<Element> iterator = nodosItems.iterator(); iterator.hasNext(); ) {
/* 1160 */       Element nodoItem = iterator.next();
/* 1161 */       HashMap<Object, Object> hashItem = new HashMap<Object, Object>();
/*      */       
/* 1163 */       hashItem.put("guid", nodoItem.getAttributeValue("GUID"));
/*      */       
/* 1165 */       hashItem.put("name", (nodoItem.getChildText("WHRItemID") != null) ? nodoItem.getChildText("WHRItemID") : "");
/* 1166 */       hashItem.put("package_name", (nodoItem.getChildText("PackageName") != null) ? nodoItem.getChildText("PackageName") : "");
/* 1167 */       hashItem.put("pieces", (nodoItem.getChildText("Pieces") != null) ? nodoItem.getChildText("Pieces") : "");
/*      */       
/* 1169 */       if (nodoItem.getChildText("Length") != null) {
/* 1170 */         hashItem.put("length", (nodoItem.getChildText("Length") != null) ? nodoItem.getChildText("Length") : "");
/* 1171 */         hashItem.put("length_unit", nodoItem.getChild("Length").getAttributeValue("Unit"));
/*      */       } 
/* 1173 */       if (nodoItem.getChildText("Height") != null) {
/* 1174 */         hashItem.put("height", (nodoItem.getChildText("Height") != null) ? nodoItem.getChildText("Height") : "");
/* 1175 */         hashItem.put("height_unit", nodoItem.getChild("Height").getAttributeValue("Unit"));
/*      */       } 
/* 1177 */       if (nodoItem.getChildText("Width") != null) {
/* 1178 */         hashItem.put("width", (nodoItem.getChildText("Width") != null) ? nodoItem.getChildText("Width") : "");
/* 1179 */         hashItem.put("width_unit", nodoItem.getChild("Width").getAttributeValue("Unit"));
/*      */       } 
/* 1181 */       if (nodoItem.getChildText("Weight") != null) {
/* 1182 */         hashItem.put("weight", (nodoItem.getChildText("Weight") != null) ? nodoItem.getChildText("Weight") : "");
/* 1183 */         hashItem.put("weight_unit", nodoItem.getChild("Weight").getAttributeValue("Unit"));
/*      */       } 
/* 1185 */       if (nodoItem.getChildText("Volume") != null) {
/* 1186 */         hashItem.put("volume", (nodoItem.getChildText("Volume") != null) ? nodoItem.getChildText("Volume") : "");
/* 1187 */         hashItem.put("volume_unit", nodoItem.getChild("Volume").getAttributeValue("Unit"));
/*      */       } 
/* 1189 */       if (nodoItem.getChildText("VolumeWeight") != null) {
/* 1190 */         hashItem.put("volume_weight", (nodoItem.getChildText("VolumeWeight") != null) ? nodoItem.getChildText("VolumeWeight") : "");
/* 1191 */         hashItem.put("volume_weight_unit", nodoItem.getChild("VolumeWeight").getAttributeValue("Unit"));
/*      */       } 
/* 1193 */       hashItem.put("out_master_way_bill_number", (nodoItem.getChildText("OutMasterWayBillNumber") != null) ? nodoItem.getChildText("OutMasterWayBillNumber") : "");
/* 1194 */       hashItem.put("out_house_way_bill_number", (nodoItem.getChildText("OutHouseWayBillNumber") != null) ? nodoItem.getChildText("OutHouseWayBillNumber") : "");
/* 1195 */       hashItem.put("tracking_number", (nodoItem.getChildText("TrackingNumber") != null) ? nodoItem.getChildText("TrackingNumber") : "");
/* 1196 */       hashItem.put("location_code", (nodoItem.getChildText("LocationCode") != null) ? nodoItem.getChildText("LocationCode") : "");
/* 1197 */       hashItem.put("notes", (nodoItem.getChildText("Notes") != null) ? nodoItem.getChildText("Notes") : "");
/* 1198 */       hashItem.put("status", (nodoItem.getChildText("Status") != null) ? nodoItem.getChildText("Status") : "");
/*      */ 
/*      */       
/* 1201 */       hashItem.put("description", (nodoItem.getChildText("Description") != null) ? nodoItem.getChildText("Description") : "");
/*      */       
/* 1203 */       listaItems.add(hashItem);
/*      */     } 
/* 1205 */     return listaItems;
/*      */   }
/*      */   
/*      */   public HashMap<Object, Object> getAddress(Element nodoAddr) {
/* 1209 */     HashMap<Object, Object> hashAddress = null;
/*      */     
/* 1211 */     hashAddress = new HashMap<Object, Object>();
/*      */     
/* 1213 */     hashAddress.put("name", (nodoAddr.getChildren("Street").size() > 0) ? ((Element)nodoAddr.getChildren("Street").get(0)).getText() : "");
/* 1214 */     hashAddress.put("street_2", (nodoAddr.getChildren("Street").size() > 1) ? ((Element)nodoAddr.getChildren("Street").get(1)).getText() : "");
/* 1215 */     hashAddress.put("city", (nodoAddr.getChildText("City") != null) ? nodoAddr.getChildText("City") : "");
/* 1216 */     hashAddress.put("state", (nodoAddr.getChildText("State") != null) ? nodoAddr.getChildText("State") : "");
/* 1217 */     hashAddress.put("zip_code", (nodoAddr.getChildText("ZipCode") != null) ? nodoAddr.getChildText("ZipCode") : "");
/* 1218 */     hashAddress.put("country", (nodoAddr.getChildText("Country") != null) ? nodoAddr.getChildText("Country") : "");
/* 1219 */     hashAddress.put("country_code", (nodoAddr.getChildText("CountryCode") != null) ? nodoAddr.getChildText("CountryCode") : "");
/* 1220 */     hashAddress.put("description", (nodoAddr.getChildText("Description") != null) ? nodoAddr.getChildText("Description") : "");
/* 1221 */     hashAddress.put("contact_name", (nodoAddr.getChildText("ContactName") != null) ? nodoAddr.getChildText("ContactName") : "");
/* 1222 */     hashAddress.put("contact_phone", (nodoAddr.getChildText("ContactPhone") != null) ? nodoAddr.getChildText("ContactPhone") : "");
/* 1223 */     hashAddress.put("port_code", (nodoAddr.getChildText("PortCode") != null) ? nodoAddr.getChildText("PortCode") : "");
/*      */     
/* 1225 */     return hashAddress;
/*      */   }
/*      */   
/*      */   public void chequearCambiosTransacciones(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora, Boolean chequearEliminaciones, Boolean chequearCreaciones, Boolean chequearModificaciones, Boolean guardarFechaFin) throws FileNotFoundException, IOException {
/* 1229 */     String user = null;
/* 1230 */     String pass = null;
/* 1231 */     String db = null;
/* 1232 */     String host = null;
/* 1233 */     String port = null;
/* 1234 */     Properties props = new Properties();
/*      */ 
/*      */     
/* 1237 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 1238 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 1240 */     db = props.getProperty("db");
/* 1241 */     user = props.getProperty("user");
/* 1242 */     pass = props.getProperty("pass");
/* 1243 */     host = props.getProperty("host");
/* 1244 */     port = props.getProperty("port");
/* 1245 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 1247 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 1250 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/* 1251 */       magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */       
/* 1253 */       Date ultimaActualizacion = new Date();
/* 1254 */       if (desdeFechaHora == null) {
/* 1255 */         desdeFechaHora = openerpClient.getDateUltimaConsultaQueryLog(transType);
/*      */       }
/* 1257 */       if (chequearEliminaciones.booleanValue()) {
/* 1258 */         checkDeleted(transType, desdeFechaHora, hastaFechaHora);
/*      */       }
/*      */       
/* 1261 */       if (chequearCreaciones.booleanValue()) {
/* 1262 */         checkCreated(transType, desdeFechaHora, hastaFechaHora);
/*      */       }
/*      */       
/* 1265 */       if (chequearModificaciones.booleanValue()) {
/* 1266 */         checkModified(transType, desdeFechaHora, hastaFechaHora);
/*      */       }
/*      */       
/* 1269 */       if (guardarFechaFin.booleanValue()) {
/* 1270 */         openerpClient.setDateUltimaConsultaQueryLog(transType, ultimaActualizacion);
/*      */       }
/* 1272 */     } catch (XmlRpcException e1) {
/* 1273 */       Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con OpenERP", (Throwable)e1);
/* 1274 */     } catch (JDOMException e) {
/* 1275 */       Logger.getLogger(getClass().getSimpleName()).error("Error al procesar el XML retornado por QueryLog", (Throwable)e);
/* 1276 */     } catch (IOException e) {
/* 1277 */       Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con Magaya, probablemente este caido", e);
/* 1278 */     } catch (Exception e) {
/* 1279 */       Logger.getLogger(getClass().getSimpleName()).error("Error en el chequeo de actualizaciones (via QueryLog)", e);
/*      */     } 
/*      */   }
/*      */ 
/*      */   
/*      */   public void chequearVerificaciones(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora, Boolean guardarFechaFin) throws FileNotFoundException, IOException {
/* 1285 */     String user = null;
/* 1286 */     String pass = null;
/* 1287 */     String db = null;
/* 1288 */     String host = null;
/* 1289 */     String port = null;
/* 1290 */     Properties props = new Properties();
/*      */ 
/*      */     
/* 1293 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 1294 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 1296 */     db = props.getProperty("db");
/* 1297 */     user = props.getProperty("user");
/* 1298 */     pass = props.getProperty("pass");
/* 1299 */     host = props.getProperty("host");
/* 1300 */     port = props.getProperty("port");
/* 1301 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 1303 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 1306 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/* 1307 */       magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */       
/* 1309 */       if (desdeFechaHora == null) {
/* 1310 */         desdeFechaHora = openerpClient.getDateUltimaConsultaQueryLogVerificacionesVerificador(transType);
/*      */       }
/* 1312 */       if (hastaFechaHora == null) {
/* 1313 */         hastaFechaHora = new Date();
/*      */       }
/* 1315 */       TreeSet<String> guidsMagayaPeriodo = new TreeSet<String>();
/*      */ 
/*      */       
/* 1318 */       SAXBuilder builder = new SAXBuilder();
/*      */ 
/*      */       
/* 1321 */       //Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + "  creadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)) + " ...");
/* 1322 */       String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)), Integer.valueOf(1), Integer.valueOf(0));
/*      */       
/* 1324 */       xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 1325 */       //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */       
/* 1327 */       Reader in = new StringReader(xmlTransPreriodo);
/* 1328 */       Document document = builder.build(in);
/* 1329 */       Element rootNode = document.getRootElement();
/* 1330 */       List<Element> nodosTrans = rootNode.getChildren("GUIDItem");
/* 1331 */       //Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());
/*      */       
/* 1333 */       for (Element nodoTransGuid : nodosTrans) {
/* 1334 */         guidsMagayaPeriodo.add(nodoTransGuid.getChildText("GUID"));
/*      */       }
/*      */ 
/*      */ 
/*      */       
/* 1339 */       //Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + " actualizadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)) + " ...");
/* 1340 */       xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(desdeFechaHora)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(hastaFechaHora)), Integer.valueOf(4), Integer.valueOf(0));
/*      */       
/* 1342 */       xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 1343 */       //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */       
/* 1345 */       in = new StringReader(xmlTransPreriodo);
/* 1346 */       document = builder.build(in);
/* 1347 */       rootNode = document.getRootElement();
/* 1348 */       nodosTrans = rootNode.getChildren("GUIDItem");
/* 1349 */       //Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());
/*      */       
/* 1351 */       for (Element nodoTransGuid : nodosTrans) {
/* 1352 */         guidsMagayaPeriodo.add(nodoTransGuid.getChildText("GUID"));
/*      */       }
/*      */ 
/*      */ 
/*      */       
/* 1357 */       Vector<Object> vectorOfVectorFilterTuples = new Vector();
/*      */       
/* 1359 */       vectorOfVectorFilterTuples.add("&");
/* 1360 */       vectorOfVectorFilterTuples.add("&");
/*      */       
/* 1362 */       Vector<String> filterGuids = new Vector();
/* 1363 */       filterGuids.addElement("guid");
/* 1364 */       filterGuids.addElement("in");
/* 1365 */       filterGuids.addElement((String)new Vector<String>(guidsMagayaPeriodo));
/* 1366 */       vectorOfVectorFilterTuples.add(filterGuids);
/*      */       
/* 1368 */       Vector<String> filterDesde = new Vector();
/* 1369 */       filterDesde.addElement("last_query_log_from");
/* 1370 */       filterDesde.addElement(">=");
/* 1371 */       filterDesde.addElement(this.formaterUtc.format(sumarUnaHoraYRestar10Min(desdeFechaHora)));
/* 1372 */       vectorOfVectorFilterTuples.add(filterDesde);
/*      */       
/* 1374 */       Vector<String> filterHasta = new Vector();
/* 1375 */       filterHasta.addElement("last_query_log_to");
/* 1376 */       filterHasta.addElement("<=");
/* 1377 */       filterHasta.addElement(this.formaterUtc.format(sumarUnaHoraYRestar10Min(hastaFechaHora)));
/* 1378 */       vectorOfVectorFilterTuples.add(filterHasta);
/*      */       
/* 1380 */       Object[] trans_ids = openerpClient.searchGenerico(transType.getOpenerpModel(), vectorOfVectorFilterTuples);
/*      */       
/* 1382 */       Logger.getLogger(getClass().getSimpleName()).warn("Trans openerp marcadas en el periodo: " + trans_ids.length);
/*      */       
/* 1384 */       Vector<String> fieldNames = new Vector();
/* 1385 */       fieldNames.add("guid");
/*      */       
/* 1387 */       Object[] transReads = openerpClient.read(transType.getOpenerpModel(), trans_ids, fieldNames);
/*      */       
/* 1389 */       Logger.getLogger(getClass().getSimpleName()).warn("Inciales: " + guidsMagayaPeriodo.size());
/* 1390 */       for (int i = 0; i < transReads.length; i++) {
/* 1391 */         HashMap<Object, Object> transRead = (HashMap<Object, Object>)transReads[i];
/*      */         
/* 1393 */         Object transOpenerpId = transRead.get("id");
/* 1394 */         String transGuid = transRead.get("guid").toString();
/*      */         
/* 1396 */         if (guidsMagayaPeriodo.contains(transGuid)) {
/* 1397 */           guidsMagayaPeriodo.remove(transGuid);
/*      */         }
/*      */       } 
/*      */       
/* 1401 */       Logger.getLogger(getClass().getSimpleName()).warn("Finales: " + guidsMagayaPeriodo.size());
/*      */       
/* 1403 */       for (Iterator<String> iterator = guidsMagayaPeriodo.iterator(); iterator.hasNext(); ) {
/* 1404 */         String guid = iterator.next();
/*      */         
/* 1406 */         if (!openerpClient.existeTransaccion(transType, guid)) {
/* 1407 */           marcarParaCrearTransaccion(transType, openerpClient, guid, sumarUnaHoraYRestar10Min(desdeFechaHora), sumarUnaHoraYRestar10Min(hastaFechaHora)); continue;
/*      */         } 
/* 1409 */         Vector<String> filter = new Vector();
/* 1410 */         filter.addElement("guid");
/* 1411 */         filter.addElement("=");
/* 1412 */         filter.addElement(guid);
/* 1413 */         Vector<Vector<String>> filters = new Vector();
/* 1414 */         filters.add(filter);
/* 1415 */         Object[] ids = openerpClient.search(transType.getOpenerpModel(), (Vector)filters);
/*      */         
/* 1417 */         Vector<String> fieldNamesTrans = new Vector();
/* 1418 */         fieldNamesTrans.addElement("update_required");
/* 1419 */         HashMap<Object, Object> transRead = (HashMap<Object, Object>)openerpClient.read(transType.getOpenerpModel(), ids, fieldNamesTrans)[0];
/*      */         
/* 1421 */         Integer prioridad = Integer.valueOf(transRead.get("update_required").toString());
/* 1422 */         if (prioridad.intValue() == 0) {
/* 1423 */           prioridad = Integer.valueOf(1);
/*      */         }
/*      */ 
/*      */         
/* 1427 */         marcarParaActualizarTransaccion(transType, guid, openerpClient, prioridad, sumarUnaHoraYRestar10Min(desdeFechaHora), sumarUnaHoraYRestar10Min(hastaFechaHora));
/* 1428 */         //Logger.getLogger(getClass().getSimpleName()).info("Transaccion marcada para actualizar, guid: " + guid);
/*      */       } 
/*      */ 
/*      */ 
/*      */ 
/*      */       
/* 1434 */       if (guardarFechaFin.booleanValue()) {
/* 1435 */         openerpClient.setDateUltimaConsultaQueryLogVerificacionesVerificador(transType, hastaFechaHora);
/*      */       }
/* 1437 */     } catch (XmlRpcException e1) {
/* 1438 */       Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con OpenERP", (Throwable)e1);
/* 1439 */     } catch (JDOMException e) {
/* 1440 */       Logger.getLogger(getClass().getSimpleName()).error("Error al procesar el XML retornado por QueryLog", (Throwable)e);
/* 1441 */     } catch (IOException e) {
/* 1442 */       Logger.getLogger(getClass().getSimpleName()).error("Error de conexion con Magaya, probablemente este caido", e);
/* 1443 */     } catch (Exception e) {
/* 1444 */       Logger.getLogger(getClass().getSimpleName()).error("Error en el chequeo de actualizaciones (via QueryLog)", e);
/*      */     } 
/*      */   }
/*      */ 
/*      */   
/*      */   private void checkModified(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException, JDOMException, IOException {
/* 1450 */     Calendar calHasta = Calendar.getInstance();
/* 1451 */     if (hastaFechaHora != null) {
/* 1452 */       calHasta.setTime(hastaFechaHora);
/*      */     }
/* 1454 */     Calendar cal = Calendar.getInstance();
/* 1455 */     cal.setTime(desdeFechaHora);
/*      */     
/* 1457 */     SAXBuilder builder = new SAXBuilder();
/* 1458 */     Date fechaPivotFin = cal.getTime();
/*      */     
/* 1460 */     while (cal.before(calHasta)) {
/* 1461 */       Date fechaPivotInicio = cal.getTime();
/* 1462 */       cal.add(5, 1);
/* 1463 */       if (cal.after(calHasta))
/*      */       {
/* 1465 */         cal.setTime(calHasta.getTime()); } 
/* 1466 */       fechaPivotFin = cal.getTime();
/*      */       
/* 1468 */       //Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + " actualizadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)) + " ...");
/* 1469 */       String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)), Integer.valueOf(4), Integer.valueOf(0));
/*      */       
/* 1471 */       xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 1472 */       //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */       
/* 1474 */       Reader in = new StringReader(xmlTransPreriodo);
/* 1475 */       Document document = builder.build(in);
/* 1476 */       Element rootNode = document.getRootElement();
/* 1477 */       List<Element> nodosTrans = rootNode.getChildren("GUIDItem");
/* 1478 */       //Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());
/*      */       
/* 1480 */       for (Element nodoTransGuid : nodosTrans) {
/* 1481 */         int reintentos = 5;
/* 1482 */         while (reintentos > 0) {
/*      */           try {
/* 1484 */             //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/* 1485 */             //Logger.getLogger(getClass().getSimpleName()).info(nodoTransGuid.getChildText("GUID"));
/* 1486 */             if (!transaccionProblematicaIgnorada(nodoTransGuid.getChildText("GUID"))) {
/* 1487 */               if (!openerpClient.existeTransaccion(transType, nodoTransGuid.getChildText("GUID"))) {
/*      */ 
/*      */ 
/*      */ 
/*      */                 
/* 1492 */                 Logger.getLogger(getClass().getSimpleName()).warn("La transaccion no existe aún, pero se recibio una actualizacion de la misma. Se marca para crearla");
/* 1493 */                 marcarParaCrearTransaccion(transType, openerpClient, nodoTransGuid.getChildText("GUID"), sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
/*      */               } else {
/* 1495 */                 //Logger.getLogger(getClass().getSimpleName()).info("La transaccion ya existe, se marca para actualizarla");
/*      */                 
/* 1497 */                 Vector<String> filter = new Vector();
/* 1498 */                 filter.addElement("guid");
/* 1499 */                 filter.addElement("=");
/* 1500 */                 filter.addElement(nodoTransGuid.getChildText("GUID"));
/* 1501 */                 Vector<Vector<String>> filters = new Vector();
/* 1502 */                 filters.add(filter);
/* 1503 */                 Object[] ids = openerpClient.search(transType.getOpenerpModel(), (Vector)filters);
/*      */                 
/* 1505 */                 Vector<String> fieldNames = new Vector();
/* 1506 */                 fieldNames.addElement("update_required");
/* 1507 */                 HashMap<Object, Object> transRead = (HashMap<Object, Object>)openerpClient.read(transType.getOpenerpModel(), ids, fieldNames)[0];
/*      */                 
/* 1509 */                 Integer prioridad = Integer.valueOf(transRead.get("update_required").toString());
/* 1510 */                 if (prioridad.intValue() == 0) {
/* 1511 */                   prioridad = Integer.valueOf(1);
/*      */                 }
/*      */ 
/*      */ 
/*      */ 
/*      */                 
/* 1517 */                 marcarParaActualizarTransaccion(transType, nodoTransGuid, openerpClient, prioridad, sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
/*      */               } 
/*      */             } else {
/* 1520 */               Logger.getLogger(getClass().getSimpleName()).warn("Se ignora la ttransaccion problematica: GUID: " + nodoTransGuid.getChildText("GUID"));
/*      */             } 
/* 1522 */             reintentos = 0;
/* 1523 */           } catch (Exception e) {
/* 1524 */             Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
/* 1525 */             reintentos--;
/*      */             try {
/* 1527 */               Thread.sleep(5000L);
/* 1528 */             } catch (InterruptedException e1) {
/* 1529 */               e1.printStackTrace();
/*      */             } 
/*      */           } 
/*      */         } 
/*      */       } 
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   public Date sumarUnaHoraYRestar10Min(Date fecha) {
/* 1540 */     Calendar cal = Calendar.getInstance();
/* 1541 */     cal.setTime(fecha);
/* 1542 */     //Logger.getLogger(getClass().getSimpleName()).info("Horario verano: " + String.valueOf(RuntimeConfig.getInstance().isHorarioVerano()));
/* 1543 */     if (!RuntimeConfig.getInstance().isHorarioVerano().booleanValue())
/* 1544 */       cal.add(11, 1); 
/* 1545 */     cal.add(12, -10);
/* 1546 */     return cal.getTime();
/*      */   }
/*      */   
/*      */   public Date sumarUnaHora(Date fecha) {
/* 1550 */     Calendar cal = Calendar.getInstance();
/* 1551 */     cal.setTime(fecha);
/* 1552 */     //Logger.getLogger(getClass().getSimpleName()).info("Horario verano: " + String.valueOf(RuntimeConfig.getInstance().isHorarioVerano()));
/* 1553 */     if (!RuntimeConfig.getInstance().isHorarioVerano().booleanValue())
/* 1554 */       cal.add(11, 1); 
/* 1555 */     return cal.getTime();
/*      */   }
/*      */   
/*      */   public Date restar10Min(Date fecha) {
/* 1559 */     Calendar cal = Calendar.getInstance();
/* 1560 */     cal.setTime(fecha);
/* 1561 */     cal.add(12, -10);
/* 1562 */     return cal.getTime();
/*      */   }
/*      */   
/*      */   private void checkDeleted(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException, JDOMException, IOException {
/* 1566 */     Calendar calHasta = Calendar.getInstance();
/* 1567 */     if (hastaFechaHora != null) {
/* 1568 */       calHasta.setTime(hastaFechaHora);
/*      */     }
/* 1570 */     Calendar cal = Calendar.getInstance();
/* 1571 */     cal.setTime(desdeFechaHora);
/*      */     
/* 1573 */     SAXBuilder builder = new SAXBuilder();
/* 1574 */     Date fechaPivotFin = cal.getTime();
/*      */     
/* 1576 */     while (cal.before(calHasta)) {
/* 1577 */       Date fechaPivotInicio = cal.getTime();
/* 1578 */       cal.add(5, 1);
/* 1579 */       if (cal.after(calHasta))
/*      */       {
/* 1581 */         cal.setTime(calHasta.getTime()); } 
/* 1582 */       fechaPivotFin = cal.getTime();
/*      */       
/* 1584 */       //Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + " eliminad@s entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)) + " ...");
/* 1585 */       String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)), Integer.valueOf(2), Integer.valueOf(0));
/*      */       
/* 1587 */       xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 1588 */       //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */       
/* 1590 */       Reader in = new StringReader(xmlTransPreriodo);
/* 1591 */       Document document = builder.build(in);
/* 1592 */       Element rootNode = document.getRootElement();
/* 1593 */       List<Element> nodosWR = rootNode.getChildren("GUIDItem");
/* 1594 */       //Logger.getRootLogger().info("Transacciones obtenidas: " + nodosWR.size());
/*      */       
/* 1596 */       for (Element nodoTransGuid : nodosWR) {
/* 1597 */         int reintentos = 5;
/* 1598 */         while (reintentos > 0) {
/*      */           try {
/* 1600 */             //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/* 1601 */             //Logger.getLogger(getClass().getSimpleName()).info(nodoTransGuid.getChildText("GUID"));
/* 1602 */             if (!transaccionProblematicaIgnorada(nodoTransGuid.getChildText("GUID"))) {
/* 1603 */               if (openerpClient.existeTransaccion(transType, nodoTransGuid.getChildText("GUID"))) {
/*      */                 
/* 1605 */                 marcarParaEliminarTransaccion(transType, nodoTransGuid, openerpClient, sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
/* 1606 */                 //Logger.getLogger(getClass().getSimpleName()).info("Transaccion marcada como eliminada");
/*      */               } else {
/*      */                 
/* 1609 */                 Logger.getLogger(getClass().getSimpleName()).warn("Transaccion eliminada en magaya, pero en openerp no existia (no se hace nada)...");
/*      */               } 
/*      */             } else {
/* 1612 */               Logger.getLogger(getClass().getSimpleName()).warn("Se ignora la ttransaccion problematica: GUID: " + nodoTransGuid.getChildText("GUID"));
/*      */             } 
/* 1614 */             reintentos = 0;
/* 1615 */           } catch (Exception e) {
/* 1616 */             Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
/* 1617 */             reintentos--;
/*      */             try {
/* 1619 */               Thread.sleep(5000L);
/* 1620 */             } catch (InterruptedException e1) {
/* 1621 */               e1.printStackTrace();
/*      */             } 
/*      */           } 
/*      */         } 
/*      */       } 
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private boolean transaccionProblematicaIgnorada(String guid) {
/*      */     try {
/* 1633 */       CsvReader wrs = new CsvReader("guids_ignorados.csv");
/* 1634 */       wrs.readHeaders();
/* 1635 */       while (wrs.readRecord()) {
/* 1636 */         String guidIgnorado = wrs.get("guid");
/* 1637 */         if (guidIgnorado.equals(guid)) {
/* 1638 */           wrs.close();
/* 1639 */           return true;
/*      */         } 
/*      */       } 
/* 1642 */       wrs.close();
/* 1643 */     } catch (IOException e) {
/* 1644 */       Logger.getLogger(getClass().getSimpleName()).error("No se pudo procesar el archivo guids_ignorados.csv", e);
/*      */     } 
/* 1646 */     return false;
/*      */   }
/*      */   
/*      */   private void checkCreated(TransactionType transType, Date desdeFechaHora, Date hastaFechaHora) throws XmlRpcException, JDOMException, IOException {
/* 1650 */     Calendar calHasta = Calendar.getInstance();
/* 1651 */     if (hastaFechaHora != null) {
/* 1652 */       calHasta.setTime(hastaFechaHora);
/*      */     }
/* 1654 */     Calendar cal = Calendar.getInstance();
/* 1655 */     cal.setTime(desdeFechaHora);
/*      */     
/* 1657 */     SAXBuilder builder = new SAXBuilder();
/* 1658 */     Date fechaPivotFin = cal.getTime();
/*      */     
/* 1660 */     while (cal.before(calHasta)) {
/* 1661 */       Date fechaPivotInicio = cal.getTime();
/* 1662 */       cal.add(5, 1);
/* 1663 */       if (cal.after(calHasta))
/*      */       {
/* 1665 */         cal.setTime(calHasta.getTime()); } 
/* 1666 */       fechaPivotFin = cal.getTime();
/*      */       
/* 1668 */       //Logger.getLogger(getClass().getSimpleName()).info("Chequeando " + transType.getDescription() + "  creadas entre " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)) + " y " + this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)) + " ...");
/* 1669 */       String xmlTransPreriodo = magayaWS.queryLog(transType.getCode(), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotInicio)), this.formaterFechaHoraSys.format(sumarUnaHoraYRestar10Min(fechaPivotFin)), Integer.valueOf(1), Integer.valueOf(0));
/*      */       
/* 1671 */       xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 1672 */       //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */       
/* 1674 */       Reader in = new StringReader(xmlTransPreriodo);
/* 1675 */       Document document = builder.build(in);
/* 1676 */       Element rootNode = document.getRootElement();
/* 1677 */       List<Element> nodosWR = rootNode.getChildren("GUIDItem");
/* 1678 */       //Logger.getRootLogger().info("Transacciones obtenidas: " + nodosWR.size());
/*      */       
/* 1680 */       for (Element nodoWRGuid : nodosWR) {
/* 1681 */         int reintentos = 5;
/* 1682 */         while (reintentos > 0) {
/*      */           try {
/* 1684 */             //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/* 1685 */             //Logger.getLogger(getClass().getSimpleName()).info(nodoWRGuid.getChildText("GUID"));
/* 1686 */             if (!transaccionProblematicaIgnorada(nodoWRGuid.getChildText("GUID"))) {
/* 1687 */               if (!openerpClient.existeTransaccion(transType, nodoWRGuid.getChildText("GUID"))) {
/*      */                 
/* 1689 */                 //Logger.getLogger(getClass().getSimpleName()).info("La transaccion no existe aún, marcandola para crear");
/* 1690 */                 marcarParaCrearTransaccion(transType, openerpClient, nodoWRGuid.getChildText("GUID"), sumarUnaHoraYRestar10Min(fechaPivotInicio), sumarUnaHoraYRestar10Min(fechaPivotFin));
/*      */               } else {
/*      */                 
/* 1693 */                 Logger.getLogger(getClass().getSimpleName()).warn("La transaccion ya existe, esto no es normal (no se hace nada)");
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */               
/*      */               }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/*      */             }
/*      */             else {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */               
/* 1727 */               Logger.getLogger(getClass().getSimpleName()).warn("Se ignora la ttransaccion problematica: GUID: " + nodoWRGuid.getChildText("GUID"));
/*      */             } 
/* 1729 */             reintentos = 0;
/* 1730 */           } catch (Exception e) {
/* 1731 */             Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoWRGuid), e);
/* 1732 */             reintentos--;
/*      */             try {
/* 1734 */               Thread.sleep(5000L);
/* 1735 */             } catch (InterruptedException e1) {
/* 1736 */               e1.printStackTrace();
/*      */             } 
/*      */           } 
/*      */         } 
/*      */       } 
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void actualizarTransacciones(TransactionType transactionType, Date desdeFecha, Date hastaFecha, Boolean descargarAdjuntos) throws FileNotFoundException, IOException {
/* 1841 */     DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");
/*      */ 
/*      */     
/* 1844 */     String user = null;
/* 1845 */     String pass = null;
/* 1846 */     String db = null;
/* 1847 */     String host = null;
/* 1848 */     String port = null;
/* 1849 */     Properties props = new Properties();
/*      */     
/* 1851 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 1852 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 1854 */     db = props.getProperty("db");
/* 1855 */     user = props.getProperty("user");
/* 1856 */     pass = props.getProperty("pass");
/* 1857 */     host = props.getProperty("host");
/* 1858 */     port = props.getProperty("port");
/* 1859 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 1861 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 1864 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/*      */ 
/*      */       
/* 1867 */       magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */       
/* 1869 */       Date ultimaActualizacion = null;
/* 1870 */       if (desdeFecha == null) {
/*      */         
/* 1872 */         ultimaActualizacion = openerpClient.getDateUltimaConsultaQueryLog(transactionType);
/*      */       } else {
/* 1874 */         ultimaActualizacion = desdeFecha;
/*      */       } 
/* 1876 */       Calendar calHasta = Calendar.getInstance();
/* 1877 */       if (hastaFecha != null) {
/* 1878 */         calHasta.setTime(hastaFecha);
/*      */       }
/* 1880 */       Calendar cal = Calendar.getInstance();
/* 1881 */       cal.setTime(ultimaActualizacion);
/*      */       
/* 1883 */       SAXBuilder builder = new SAXBuilder();
/*      */       
/* 1885 */       Integer actualizadas = Integer.valueOf(0);
/*      */       
/* 1887 */       boolean terminado = false;
/* 1888 */       while (!terminado) {
/* 1889 */         Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/*      */         
/* 1891 */         Vector<String> filterUpdateReq = new Vector();
/* 1892 */         filterUpdateReq.addElement("update_required");
/* 1893 */         filterUpdateReq.addElement(">");
/* 1894 */         filterUpdateReq.addElement(Integer.valueOf(0));
/* 1895 */         vectorOfVectorFilterTuples.add(filterUpdateReq);
/*      */         
/* 1897 */         Vector<String> filterDeleted = new Vector();
/* 1898 */         filterDeleted.addElement("deleted");
/* 1899 */         filterDeleted.addElement("=");
/* 1900 */         filterDeleted.addElement(Boolean.FALSE);
/* 1901 */         vectorOfVectorFilterTuples.add(filterDeleted);
/*      */         
/* 1903 */         Object[] trans_ids = openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(1), "update_required desc, created_on desc");
/* 1904 */         int cantAActualizar = (openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples)).length;
/*      */         
/* 1906 */         if (trans_ids.length > 0) {
/* 1907 */           Vector<String> fieldNames = new Vector();
/* 1908 */           fieldNames.add("guid");
/*      */           
/* 1910 */           HashMap<Object, Object> transRead = (HashMap<Object, Object>)openerpClient.read(transactionType.getOpenerpModel(), trans_ids, fieldNames)[0];
/*      */           
/* 1912 */           Object transOpenerpId = transRead.get("id");
/* 1913 */           String transGuid = transRead.get("guid").toString();
/* 1914 */           //Logger.getLogger(getClass().getSimpleName()).info("Obteniendo transaccion " + transactionType.getCode() + " : " + transGuid + " ...");
/*      */ 
/*      */ 
/*      */           
/* 1918 */           int flags = 608;
/* 1919 */           if (!descargarAdjuntos.booleanValue()) {
/* 1920 */             flags = 546;
/*      */           }
/* 1922 */           String xmlTranss = magayaWS.getTransaction(transactionType.getCode(), transGuid, Integer.valueOf(flags));
/* 1923 */           int reintentosRestantes = 5;
/*      */ 
/*      */ 
/*      */           
/* 1927 */           boolean problematica = false;
/* 1928 */           while (xmlTranss != null && reintentosRestantes > 0) {
/* 1929 */             xmlTranss = xmlTranss.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 1930 */             //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/* 1931 */             Reader in = new StringReader(xmlTranss);
/* 1932 */             Document document = builder.build(in);
/* 1933 */             Element nodoTrans = document.getRootElement();
/*      */             
/*      */             try {
/* 1936 */               //Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));
/*      */               
/* 1938 */               //Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + transactionType.getCode() + ")...");
/* 1939 */               actualizar(transactionType, transOpenerpId, nodoTrans, openerpClient);
/* 1940 */               //Logger.getLogger(getClass().getSimpleName()).info("Actualizada con exito, guid: " + nodoTrans.getAttributeValue("GUID"));
/* 1941 */               actualizadas = Integer.valueOf(actualizadas.intValue() + 1);
/* 1942 */               Logger.getLogger(getClass().getSimpleName()).info("-------------- Progreso - Actualizadas: " + actualizadas + ", Restantes: " + cantAActualizar);
/* 1943 */               reintentosRestantes = 0;
/* 1944 */               problematica = false;
/* 1945 */             } catch (Exception e) {
/* 1946 */               problematica = true;
/*      */ 
/*      */ 
/*      */               
/* 1950 */               Logger.getLogger(getClass().getSimpleName()).warn("Ocurrio un error al procesar el WR GUID: " + nodoTrans.getAttributeValue("GUID") + ", Number: " + nodoTrans.getChildText("Number"), e);
/* 1951 */               reintentosRestantes--;
/* 1952 */               Logger.getLogger(getClass().getSimpleName()).warn("Reintentos restantes: " + reintentosRestantes);
/*      */               try {
/* 1954 */                 Thread.sleep(5000L);
/* 1955 */               } catch (InterruptedException e1) {
/* 1956 */                 Logger.getLogger(getClass().getSimpleName()).error(e);
/*      */               } 
/*      */             } 
/*      */             
/* 1960 */             if (problematica) {
/*      */ 
/*      */               
/* 1963 */               Logger.getLogger(getClass().getSimpleName()).warn("Se supero la cantidad de reintentos permitidos para: " + transactionType + ", GUID: " + transGuid + ", Number: " + nodoTrans.getChildText("Number"));
/* 1964 */               desmarcarParaActualizarTransaccion(transactionType, transGuid, openerpClient);
/*      */             } 
/*      */           } 
/*      */           
/* 1968 */           if (xmlTranss == null) {
/*      */ 
/*      */             
/* 1971 */             if (magayaWS.getActiveCurrencies() != null) {
/* 1972 */               Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta, se elimina fisicamente la transaccion. ");
/* 1973 */               desmarcarParaActualizarTransaccion(transactionType, transGuid, openerpClient);
/* 1974 */               eliminarTransaccionFisicamente(transactionType, transGuid, openerpClient); continue;
/*      */             } 
/* 1976 */             Logger.getLogger(getClass().getSimpleName()).error("MAGAYA ESTA CAIDO, cancelando actualizacion...");
/*      */             return;
/*      */           } 
/*      */           continue;
/*      */         } 
/* 1981 */         terminado = true;
/*      */       }
/*      */     
/* 1984 */     } catch (XmlRpcException e1) {
/*      */       
/* 1986 */       e1.printStackTrace();
/* 1987 */     } catch (JDOMException e) {
/*      */       
/* 1989 */       e.printStackTrace();
/* 1990 */     } catch (IOException e) {
/*      */       
/* 1992 */       e.printStackTrace();
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void eliminarTransaccionFisicamente(TransactionType transactionType, String transGuid, OpenerpClient openerpClient2) throws XmlRpcException {
/* 2075 */     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/* 2076 */     Vector<Object> guidFilter = new Vector();
/* 2077 */     guidFilter.add("guid");
/* 2078 */     guidFilter.add("=");
/* 2079 */     guidFilter.add(transGuid);
/* 2080 */     vectorOfVectorFilterTuples.add(guidFilter);
/* 2081 */     openerpClient.unlink(transactionType.getOpenerpModel(), openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples));
/*      */   }
/*      */   
/*      */   public void descargarMasivamenteTransacciones(TransactionType transType, Date desdeFecha, Date hastaFecha, Boolean soloOnHand) throws FileNotFoundException, IOException, XmlRpcException, JDOMException {
/* 2085 */     Date timestampUltimaConsulta = new Date();
/*      */     
/* 2087 */     String user = null;
/* 2088 */     String pass = null;
/* 2089 */     String db = null;
/* 2090 */     String host = null;
/* 2091 */     String port = null;
/* 2092 */     Properties props = new Properties();
/*      */     
/* 2094 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2095 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 2097 */     db = props.getProperty("db");
/* 2098 */     user = props.getProperty("user");
/* 2099 */     pass = props.getProperty("pass");
/* 2100 */     host = props.getProperty("host");
/* 2101 */     port = props.getProperty("port");
/* 2102 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 2104 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 2107 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/*      */ 
/*      */       
/* 2110 */       magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */       
/* 2112 */       Date ultimaActualizacion = null;
/* 2113 */       if (desdeFecha == null) {
/*      */         
/* 2115 */         ultimaActualizacion = openerpClient.getDateUltimaConsultaQueryLog(transType);
/*      */       } else {
/* 2117 */         ultimaActualizacion = desdeFecha;
/*      */       } 
/* 2119 */       //Logger.getLogger(getClass().getSimpleName()).info("Fecha de ultima consulta de " + transType.getDescription() + " obtenida: " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ")).format(ultimaActualizacion));
/*      */       
/* 2121 */       Calendar calHasta = Calendar.getInstance();
/* 2122 */       if (hastaFecha != null) {
/* 2123 */         calHasta.setTime(hastaFecha);
/*      */       }
/* 2125 */       Calendar cal = Calendar.getInstance();
/* 2126 */       cal.setTime(ultimaActualizacion);
/*      */       
/* 2128 */       SAXBuilder builder = new SAXBuilder();
/*      */ 
/*      */ 
/*      */ 
/*      */       
/* 2133 */       System.out.println(String.valueOf(cal.getTime().toString()) + " -- " + calHasta.getTime().toString());
/*      */       
/* 2135 */       int desdeY = cal.get(1);
/* 2136 */       int hastaY = calHasta.get(1);
/* 2137 */       int desdeD = cal.get(6);
/* 2138 */       int hastaD = calHasta.get(6);
/*      */       
/* 2140 */       while (cal.get(1) != calHasta.get(1) || cal.get(6) <= calHasta.get(6)) {
/* 2141 */         Date fechaPivot = cal.getTime();
/*      */         
/* 2143 */         timestampUltimaConsulta = new Date();
/*      */         
/* 2145 */         //Logger.getLogger(getClass().getSimpleName()).info("Descargando transacciones " + transType.getDescription() + "  creadas el " + this.formaterFecha.format(fechaPivot) + " ...");
/* 2146 */         String xmlTransPreriodo = magayaWS.getTransRangeByDate(transType.getCode(), this.formaterFecha.format(fechaPivot), this.formaterFecha.format(fechaPivot), Integer.valueOf(546));
/*      */         
/* 2148 */         xmlTransPreriodo = xmlTransPreriodo.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 2149 */         //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */         
/* 2151 */         //Logger.getLogger(getClass().getSimpleName()).info("GetTransByDateRange: " + transType);
/*      */ 
/*      */         
/* 2154 */         Reader in = new StringReader(xmlTransPreriodo);
/* 2155 */         Document document = builder.build(in);
/* 2156 */         Element rootNode = document.getRootElement();
/* 2157 */         List<Element> nodosTrans = rootNode.getChildren();
/* 2158 */         //Logger.getRootLogger().info("Transacciones obtenidas: " + nodosTrans.size());
/*      */         
/* 2160 */         for (Element nodoTrans : nodosTrans) {
/*      */           try {
/* 2162 */             //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/* 2163 */             //Logger.getLogger(getClass().getSimpleName()).info(nodoTrans.getAttributeValue("GUID"));
/* 2164 */             Object id = null;
/* 2165 */             if (!openerpClient.existeTransaccion(transType, nodoTrans.getAttributeValue("GUID"))) {
/* 2166 */               //Logger.getLogger(getClass().getSimpleName()).info("La transaccion no existe aún, se crea y actualiza");
/* 2167 */               id = marcarParaCrearTransaccion(transType, openerpClient, nodoTrans.getAttributeValue("GUID"), desdeFecha, hastaFecha);
/*      */             } else {
/*      */               
/* 2170 */               Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/*      */               
/* 2172 */               Vector<String> filterGuid = new Vector();
/* 2173 */               filterGuid.addElement("guid");
/* 2174 */               filterGuid.addElement("=");
/* 2175 */               filterGuid.addElement(nodoTrans.getAttributeValue("GUID"));
/* 2176 */               vectorOfVectorFilterTuples.add(filterGuid);
/*      */               
/* 2178 */               Object[] trans_ids = openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(1), "update_required desc, created_on desc");
/* 2179 */               id = trans_ids[0];
/* 2180 */               Logger.getLogger(getClass().getSimpleName()).warn("La transaccion ya existe, se actualiza");
/*      */             } 
/*      */             
/* 2183 */             if (transType != TransactionType.WarehouseReceipt || (soloOnHand.booleanValue() && nodoTrans.getChildText("Status").equals("OnHand")) || !soloOnHand.booleanValue()) {
/* 2184 */               actualizar(transType, id, nodoTrans, openerpClient); continue;
/*      */             } 
/* 2186 */             //Logger.getLogger(getClass().getSimpleName()).info("La transaccion no esta en estado OnHand, se ignora");
/*      */           }
/* 2188 */           catch (Exception e) {
/* 2189 */             Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
/*      */           } 
/*      */         } 
/* 2192 */         cal.add(5, 1);
/*      */       } 
/*      */ 
/*      */ 
/*      */       
/* 2197 */       openerpClient.setDateUltimaConsultaQueryLog(transType, timestampUltimaConsulta);
/* 2198 */     } catch (XmlRpcException e1) {
/*      */       
/* 2200 */       e1.printStackTrace();
/* 2201 */     } catch (JDOMException e) {
/*      */       
/* 2203 */       e.printStackTrace();
/* 2204 */     } catch (IOException e) {
/*      */       
/* 2206 */       e.printStackTrace();
/*      */     } 
/*      */   }
/*      */   
/*      */   public void reprocesarTransaccionesDesdeUltimosXMLs(TransactionType transactionType, Integer de, Integer a, String soloEstado) throws FileNotFoundException, IOException, XmlRpcException, JDOMException {
/* 2211 */     DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");
/*      */ 
/*      */     
/* 2214 */     String user = null;
/* 2215 */     String pass = null;
/* 2216 */     String db = null;
/* 2217 */     String host = null;
/* 2218 */     String port = null;
/* 2219 */     Properties props = new Properties();
/*      */     
/* 2221 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2222 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 2224 */     db = props.getProperty("db");
/* 2225 */     user = props.getProperty("user");
/* 2226 */     pass = props.getProperty("pass");
/* 2227 */     host = props.getProperty("host");
/* 2228 */     port = props.getProperty("port");
/* 2229 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 2231 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 2234 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/* 2235 */       SAXBuilder builder = new SAXBuilder();
/*      */       
/* 2237 */       Integer totalTransacciones = Integer.valueOf(0);
/* 2238 */       Integer totalProcesadas = Integer.valueOf(0);
/*      */       
/* 2240 */       Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/* 2241 */       Vector<String> filterDeleted = new Vector();
/* 2242 */       filterDeleted.addElement("deleted");
/* 2243 */       filterDeleted.addElement("=");
/* 2244 */       filterDeleted.addElement(Boolean.FALSE);
/* 2245 */       vectorOfVectorFilterTuples.add(filterDeleted);
/* 2246 */       Object[] trans_ids = openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(100000000), "update_required desc");
/*      */       
/* 2248 */       totalTransacciones = Integer.valueOf(trans_ids.length);
/* 2249 */       Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR a reprocesar: " + totalTransacciones);
/*      */       
/* 2251 */       Object[] trans_id = new Object[1];
/* 2252 */       for (int i = 0; i < trans_ids.length; i++) {
/* 2253 */         trans_id[0] = trans_ids[i];
/*      */         
/* 2255 */         if (i >= de.intValue() && i <= a.intValue()) {
/*      */           
/* 2257 */           //Logger.getLogger(getClass().getSimpleName()).info("Procesando " + (i + 1) + " de " + totalTransacciones);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */           
/* 2272 */           Vector<String> fieldNames = new Vector();
/* 2273 */           fieldNames.add("guid");
/* 2274 */           fieldNames.add("state");
/* 2275 */           HashMap<Object, Object> transRead = (HashMap<Object, Object>)openerpClient.read(transactionType.getOpenerpModel(), trans_id, fieldNames)[0];
/*      */           
/* 2277 */           Object wrOpenerpId = transRead.get("id");
/* 2278 */           String transGuid = transRead.get("guid").toString();
/* 2279 */           String transState = transRead.get("state").toString();
/*      */           
/* 2281 */           Vector<Vector<Object>> filtrosXml = new Vector<Vector<Object>>();
/* 2282 */           Vector<Object> filtroWrId = new Vector();
/* 2283 */           filtroWrId.add("wr_id");
/* 2284 */           filtroWrId.add("=");
/* 2285 */           filtroWrId.add(wrOpenerpId);
/* 2286 */           filtrosXml.add(filtroWrId);
/* 2287 */           Object[] xml_ids = openerpClient.search("courier.magaya.wr.xml", filtrosXml, Integer.valueOf(0), Integer.valueOf(1), "name desc");
/*      */           
/* 2289 */           if (xml_ids.length > 0 && soloEstado != null && soloEstado.equals(transState)) {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 2313 */             totalProcesadas = Integer.valueOf(totalProcesadas.intValue() + 1);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 2322 */             String xmlTranss = XmlRepository.getInstance().getUltimoXmlPara(TransactionType.WarehouseReceipt, transGuid);
/* 2323 */             //Logger.getLogger(getClass().getSimpleName()).info("Tamaño XML: " + MagayaWS.getHumanSize(Integer.valueOf((xmlTranss != null) ? xmlTranss.length() : 0)));
/*      */             
/* 2325 */             int reintentosRestantes = 5;
/* 2326 */             while (xmlTranss != null && reintentosRestantes > 0) {
/* 2327 */               //Logger.getLogger(getClass().getSimpleName()).info("Parseando XML...");
/* 2328 */               Reader in = new StringReader(xmlTranss);
/* 2329 */               Document document = builder.build(in);
/* 2330 */               Element nodoTrans = document.getRootElement();
/*      */               
/*      */               try {
/* 2333 */                 //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/* 2334 */                 //Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));
/*      */                 
/* 2336 */                 //Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + transactionType.getCode() + ")...");
/* 2337 */                 //Logger.getLogger(getClass().getSimpleName()).info("Actualizada con exito, guid: " + nodoTrans.getAttributeValue("GUID"));
/* 2338 */                 actualizar(transactionType, wrOpenerpId, nodoTrans, openerpClient, false);
/* 2339 */                 reintentosRestantes = 0;
/* 2340 */               } catch (Exception e) {
/* 2341 */                 Logger.getLogger(getClass().getSimpleName()).warn("Ocurrio un error al procesar la transaccion: " + nodoTrans.getAttributeValue("GUID") + "\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
/* 2342 */                 reintentosRestantes--;
/* 2343 */                 Logger.getLogger(getClass().getSimpleName()).warn("Reintentos restantes: " + reintentosRestantes);
/*      */                 try {
/* 2345 */                   Thread.sleep(5000L);
/* 2346 */                 } catch (InterruptedException e1) {
/* 2347 */                   e1.printStackTrace();
/*      */                 } 
/*      */               } 
/*      */             } 
/*      */           } 
/*      */         } 
/*      */       } 
/*      */       
/* 2355 */       Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR: " + totalTransacciones);
/* 2356 */       Logger.getLogger(getClass().getSimpleName()).info("Total WR procesados: " + totalProcesadas);
/*      */     }
/* 2358 */     catch (XmlRpcException e1) {
/*      */       
/* 2360 */       e1.printStackTrace();
/* 2361 */     } catch (JDOMException e) {
/*      */       
/* 2363 */       e.printStackTrace();
/* 2364 */     } catch (IOException e) {
/*      */       
/* 2366 */       e.printStackTrace();
/*      */     } 
/*      */   }
/*      */   
/*      */   public static void main(String[] args) throws FileNotFoundException, IOException, ParseException, XmlRpcException, JDOMException {
/* 2371 */     if (getArg(args, "pidfile") != null)
/*      */     
/*      */     { 
/* 2374 */       String pidfile = getArg(args, "pidfile");
/*      */       
/* 2376 */       MDC.put("pidfile", pidfile);
/*      */       
/* 2378 */       if (!corriendo(pidfile)) {
/* 2379 */         marcarCorriendo(pidfile);
/*      */         
/*      */         try {
/* 2382 */           if (getArg(args, "accion") != null)
/* 2383 */           { String accion = getArg(args, "accion");
/*      */             
/* 2385 */             if (accion.contains("VerificadorActualizaciones")) {
/* 2386 */               Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "desde")) : null;
/* 2387 */               Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "hasta")) : null;
/* 2388 */               String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment,CargoRelease").split(",");
/* 2389 */               Boolean guardarFechaFin = Boolean.valueOf((getArg(args, "guardarFechaFin") != null) ? Boolean.valueOf(getArg(args, "guardarFechaFin")).booleanValue() : true);
/*      */ 
/*      */ 
/*      */ 
/*      */               
/* 2394 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/* 2395 */               for (int i = 0; i < trans.length; i++) {
/* 2396 */                 sincronizadorWRMagayaToOERP.chequearCambiosTransacciones(TransactionType.valueOf(trans[i]), desde, hasta, Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), guardarFechaFin);
/*      */               }
/*      */             } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 2406 */             if (accion.contains("Actualizador")) {
/* 2407 */               Boolean descargarAdjuntos = Boolean.valueOf((getArg(args, "descargarAdjuntos") != null) ? Boolean.valueOf(getArg(args, "descargarAdjuntos")).booleanValue() : true);
/* 2408 */               Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "desde")) : null;
/* 2409 */               Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "hasta")) : null;
/* 2410 */               String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment,CargoRelease").split(",");
/*      */ 
/*      */ 
/*      */               
/* 2414 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/* 2415 */               for (int i = 0; i < trans.length; i++) {
/* 2416 */                 sincronizadorWRMagayaToOERP.actualizarTransacciones(TransactionType.valueOf(trans[i]), desde, hasta, descargarAdjuntos);
/*      */               }
/*      */             } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 2426 */             if (accion.contains("ChequearVerificaciones")) {
/* 2427 */               Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "desde")) : null;
/* 2428 */               Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(getArg(args, "hasta")) : null;
/* 2429 */               String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment,CargoRelease").split(",");
/* 2430 */               Boolean guardarFechaFin = Boolean.valueOf((getArg(args, "guardarFechaFin") != null) ? Boolean.valueOf(getArg(args, "guardarFechaFin")).booleanValue() : true);
/*      */               
/* 2432 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/* 2433 */               for (int i = 0; i < trans.length; i++) {
/* 2434 */                 sincronizadorWRMagayaToOERP.chequearVerificaciones(TransactionType.valueOf(trans[i]), desde, hasta, guardarFechaFin);
/*      */               }
/*      */             } 
/*      */ 
/*      */             
/* 2439 */             if (accion.equals("DescargaMasiva")) {
/* 2440 */               Date desde = (getArg(args, "desde") != null) ? (new SimpleDateFormat("yyyy-MM-dd")).parse(getArg(args, "desde")) : null;
/* 2441 */               Date hasta = (getArg(args, "hasta") != null) ? (new SimpleDateFormat("yyyy-MM-dd")).parse(getArg(args, "hasta")) : null;
/* 2442 */               Boolean soloOnHand = Boolean.valueOf((getArg(args, "soloOnHand") != null) ? Boolean.valueOf(getArg(args, "soloOnHand").toLowerCase()).booleanValue() : false);
/* 2443 */               String[] trans = ((getArg(args, "trans") != null) ? getArg(args, "trans") : "WarehouseReceipt,Shipment").split(",");
/*      */ 
/*      */ 
/*      */               
/* 2447 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/* 2448 */               for (int i = 0; i < trans.length; i++) {
/* 2449 */                 sincronizadorWRMagayaToOERP.descargarMasivamenteTransacciones(TransactionType.valueOf(trans[i]), desde, hasta, soloOnHand);
/*      */               }
/*      */             } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 2458 */             if (accion.equals("DescargarTransaccion")) {
/* 2459 */               String user = null;
/* 2460 */               String pass = null;
/* 2461 */               String db = null;
/* 2462 */               String host = null;
/* 2463 */               String port = null;
/* 2464 */               Properties props = new Properties();
/*      */ 
/*      */               
/* 2467 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2468 */               props.load(new FileInputStream("operp_db.conf"));
/*      */               
/* 2470 */               db = props.getProperty("db");
/* 2471 */               user = props.getProperty("user");
/* 2472 */               pass = props.getProperty("pass");
/* 2473 */               host = props.getProperty("host");
/* 2474 */               port = props.getProperty("port");
/* 2475 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + db + "\"");
/*      */               
/* 2477 */               SincronizadorWRMagayaToOERP.openerpClient = new OpenerpClient();
/* 2478 */               SincronizadorWRMagayaToOERP.openerpClient.login(db, user, pass, host, port);
/*      */ 
/*      */               
/* 2481 */               magayaWS = new MagayaWS();
/* 2482 */               HashMap<String, Object> configMagaya = SincronizadorWRMagayaToOERP.openerpClient.getConfigMagaya();
/* 2483 */               magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */               
/* 2485 */               if (getArg(args, "number") != null || getArg(args, "guid") != null) {
/* 2486 */                 TransactionType transType = TransactionType.valueOf(getArg(args, "trans"));
/*      */                 
/* 2488 */                 SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/* 2489 */                 String xmlTranss = null;
/* 2490 */                 if (getArg(args, "number") != null) {
/* 2491 */                   xmlTranss = sincronizadorWRMagayaToOERP.getTransaction(transType, getArg(args, "number"), Integer.valueOf(64), magayaWS);
/* 2492 */                   //Logger.getLogger("SincronizadorWRMagayaToOERP").info(xmlTranss);
/*      */                 } else {
/* 2494 */                   xmlTranss = sincronizadorWRMagayaToOERP.getTransaction(transType, getArg(args, "guid"), Integer.valueOf(64), magayaWS);
/* 2495 */                   //Logger.getLogger("SincronizadorWRMagayaToOERP").info(xmlTranss);
/*      */                 } 
/*      */                 
/* 2498 */                 SAXBuilder builder = new SAXBuilder();
/*      */                 
/* 2500 */                 xmlTranss = xmlTranss.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 2501 */                 //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Parseando respuesta...");
/* 2502 */                 Reader in = new StringReader(xmlTranss);
/* 2503 */                 Document document = builder.build(in);
/* 2504 */                 Element nodoTrans = document.getRootElement();
/*      */                 
/* 2506 */                 if (getArg(args, "solo-descargar") == null) {
/*      */                   
/* 2508 */                   Object id = null;
/* 2509 */                   if (!SincronizadorWRMagayaToOERP.openerpClient.existeTransaccion(transType, nodoTrans.getAttributeValue("GUID"))) {
/* 2510 */                     //Logger.getLogger("SincronizadorWRMagayaToOERP").info("La transaccion no existe aún, se crea y actualiza");
/* 2511 */                     id = sincronizadorWRMagayaToOERP.marcarParaCrearTransaccion(transType, SincronizadorWRMagayaToOERP.openerpClient, nodoTrans.getAttributeValue("GUID"), null, null);
/*      */                   } else {
/*      */                     
/* 2514 */                     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/*      */                     
/* 2516 */                     Vector<String> filterGuid = new Vector();
/* 2517 */                     filterGuid.addElement("guid");
/* 2518 */                     filterGuid.addElement("=");
/* 2519 */                     filterGuid.addElement(nodoTrans.getAttributeValue("GUID"));
/* 2520 */                     vectorOfVectorFilterTuples.add(filterGuid);
/*      */                     
/* 2522 */                     Object[] trans_ids = SincronizadorWRMagayaToOERP.openerpClient.search(transType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(1), "update_required desc, created_on desc");
/* 2523 */                     id = trans_ids[0];
/* 2524 */                     Logger.getLogger("SincronizadorWRMagayaToOERP").warn("La transaccion ya existe, se actualiza");
/*      */                   } 
/*      */                   
/* 2527 */                   sincronizadorWRMagayaToOERP.actualizar(transType, id, nodoTrans, SincronizadorWRMagayaToOERP.openerpClient);
/* 2528 */                   //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Transaccion descargada y actualizada correctamente");
/*      */                 } 
/*      */               } else {
/*      */                 
/* 2532 */                 Logger.getLogger("SincronizadorWRMagayaToOERP").error("Debe especificar el parametro \"guid\" o \"number\"");
/*      */               } 
/*      */             } 
/* 2535 */             if (accion.equals("VerificarFaltantes")) {
/* 2536 */               String user = null;
/* 2537 */               String pass = null;
/* 2538 */               String db = null;
/* 2539 */               String host = null;
/* 2540 */               String port = null;
/* 2541 */               Properties props = new Properties();
/*      */               
/* 2543 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2544 */               props.load(new FileInputStream("operp_db.conf"));
/*      */               
/* 2546 */               db = props.getProperty("db");
/* 2547 */               user = props.getProperty("user");
/* 2548 */               pass = props.getProperty("pass");
/* 2549 */               host = props.getProperty("host");
/* 2550 */               port = props.getProperty("port");
/* 2551 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + db + "\"");
/*      */               
/* 2553 */               OpenerpClient openerpClient = new OpenerpClient();
/* 2554 */               openerpClient.login(db, user, pass, host, port);
/*      */               
/* 2556 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/*      */               
/* 2558 */               HashMap<String, String> guidsEnMagaya = sincronizadorWRMagayaToOERP.getWRsGUIDsExistentesEnMagaya();
/* 2559 */               HashMap<String, String> guidsEnOERP = sincronizadorWRMagayaToOERP.getWRsGUIDsExistentesEnOERP();
/*      */               
/* 2561 */               Iterator<String> itEnOpenerp = guidsEnOERP.keySet().iterator();
/* 2562 */               while (itEnOpenerp.hasNext()) {
/* 2563 */                 String guidEnOERP = itEnOpenerp.next();
/* 2564 */                 if (guidsEnMagaya.containsKey(guidEnOERP))
/* 2565 */                   guidsEnMagaya.remove(guidEnOERP); 
/*      */               } 
/* 2567 */               Logger.getLogger("SincronizadorWRMagayaToOERP").warn("WRs faltantes: " + guidsEnMagaya.keySet().size());
/* 2568 */               Iterator<String> itFaltantes = guidsEnMagaya.keySet().iterator();
/* 2569 */               while (itFaltantes.hasNext()) {
/* 2570 */                 String guidFaltante = itFaltantes.next();
/* 2571 */                 //Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR Faltante: " + guidFaltante + "  ----  Marcado para crear");
/* 2572 */                 Object id = sincronizadorWRMagayaToOERP.marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guidFaltante, null, null);
/* 2573 */                 //Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR OERP ID: " + id.toString());
/*      */               } 
/*      */             } 
/*      */             
/* 2577 */             if (accion.equals("VerificarFaltantesPorNumero")) {
/* 2578 */               String user = null;
/* 2579 */               String pass = null;
/* 2580 */               String db = null;
/* 2581 */               String host = null;
/* 2582 */               String port = null;
/* 2583 */               Properties props = new Properties();
/*      */               
/* 2585 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2586 */               props.load(new FileInputStream("operp_db.conf"));
/*      */               
/* 2588 */               db = props.getProperty("db");
/* 2589 */               user = props.getProperty("user");
/* 2590 */               pass = props.getProperty("pass");
/* 2591 */               host = props.getProperty("host");
/* 2592 */               port = props.getProperty("port");
/* 2593 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + db + "\"");
/*      */               
/* 2595 */               OpenerpClient openerpClient = new OpenerpClient();
/* 2596 */               openerpClient.login(db, user, pass, host, port);
/*      */               
/* 2598 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/*      */               
/* 2600 */               HashMap<String, String> wrNumsEnMagaya = sincronizadorWRMagayaToOERP.getWRsNumbersFromCSVs(getArg(args, "csvs"));
/* 2601 */               HashMap<String, String> wrNumsEnOERP = sincronizadorWRMagayaToOERP.getWRsNumbersExistentesEnOERP();
/*      */               
/* 2603 */               Iterator<String> itEnOpenerp = wrNumsEnOERP.keySet().iterator();
/* 2604 */               while (itEnOpenerp.hasNext()) {
/* 2605 */                 String guidEnOERP = itEnOpenerp.next();
/* 2606 */                 if (wrNumsEnMagaya.containsKey(guidEnOERP))
/* 2607 */                   wrNumsEnMagaya.remove(guidEnOERP); 
/*      */               } 
/* 2609 */               Logger.getLogger("SincronizadorWRMagayaToOERP").warn("WRs faltantes: " + wrNumsEnMagaya.keySet().size());
/*      */               
/* 2611 */               if (getArg(args, "descargarFaltantes") != null) {
/*      */ 
/*      */                 
/* 2614 */                 HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/* 2615 */                 magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */                 
/* 2617 */                 SAXBuilder builder = new SAXBuilder();
/*      */                 
/* 2619 */                 Iterator<String> itFaltantes = wrNumsEnMagaya.keySet().iterator();
/* 2620 */                 while (itFaltantes.hasNext()) {
/* 2621 */                   String wrNumberFaltante = itFaltantes.next();
/*      */ 
/*      */                   
/*      */                   try {
/* 2625 */                     String xmlTrans = sincronizadorWRMagayaToOERP.getTransaction(TransactionType.WarehouseReceipt, wrNumberFaltante, Integer.valueOf(546), magayaWS);
/*      */                     
/* 2627 */                     Reader in = new StringReader(xmlTrans);
/* 2628 */                     Document document = builder.build(in);
/* 2629 */                     Element rootNode = document.getRootElement();
/*      */                     
/* 2631 */                     String guid = rootNode.getAttributeValue("GUID");
/*      */                     
/* 2633 */                     //Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR Faltante: " + wrNumberFaltante + " (" + guid + ")" + "  ----  Marcado para crear");
/*      */                     
/*      */                     try {
/* 2636 */                       Object id = sincronizadorWRMagayaToOERP.marcarParaCrearTransaccion(TransactionType.WarehouseReceipt, openerpClient, guid, null, null);
/* 2637 */                       //Logger.getLogger("SincronizadorWRMagayaToOERP").info("WR OERP ID: " + id.toString());
/* 2638 */                     } catch (Exception e) {
/* 2639 */                       Logger.getLogger("SincronizadorWRMagayaToOERP").error("Error al intentar marcar para crear, intentando marcar para actualizar...");
/* 2640 */                       sincronizadorWRMagayaToOERP.marcarParaActualizarTransaccion(TransactionType.WarehouseReceipt, guid, openerpClient, Integer.valueOf(1), (Date)null, (Date)null);
/* 2641 */                       Logger.getLogger("SincronizadorWRMagayaToOERP").info("Transaccion marcada para actualizar");
/*      */                     }
/*      */                   
/* 2644 */                   } catch (Exception e) {
/* 2645 */                     Logger.getLogger("SincronizadorWRMagayaToOERP").error("Error al procesar transaccion faltante, numero: " + wrNumberFaltante, e);
/*      */                   } 
/*      */                 } 
/*      */               } 
/*      */             } 
/* 2650 */             if (accion.equals("Reprocesar")) {
/* 2651 */               Integer de = Integer.valueOf((getArg(args, "de") != null) ? Integer.valueOf(getArg(args, "de")).intValue() : 0);
/* 2652 */               Integer a = Integer.valueOf((getArg(args, "a") != null) ? Integer.valueOf(getArg(args, "a")).intValue() : Integer.MAX_VALUE);
/* 2653 */               String soloEstado = getArg(args, "soloEstado");
/*      */ 
/*      */               
/* 2656 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/*      */ 
/*      */ 
/*      */ 
/*      */               
/* 2661 */               sincronizadorWRMagayaToOERP.reprocesarTransaccionesDesdeUltimosXMLs(TransactionType.WarehouseReceipt, de, a, soloEstado);
/*      */             } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 2692 */             if (accion.equals("PruebaQuerylog")) {
/* 2693 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("QueryLog, modificaciones entre 2013-01-31T08:00:00-0500 y 2013-01-31T15:00:00-0500");
/*      */               
/* 2695 */               magayaWS = new MagayaWS();
/* 2696 */               magayaWS.login("API", "Ecuador153");
/* 2697 */               String respuesta = magayaWS.queryLog("WH", "2013-01-31T12:00:00-0500", "2013-01-31T21:00:00-0500", Integer.valueOf(4), Integer.valueOf(0));
/*      */               
/* 2699 */               Logger.getLogger("SincronizadorWRMagayaToOERP").info("######################################################################");
/* 2700 */               Logger.getLogger("SincronizadorWRMagayaToOERP").info(respuesta);
/* 2701 */               Logger.getLogger("SincronizadorWRMagayaToOERP").info("######################################################################");
/*      */             } 
/*      */ 
/*      */             
/* 2705 */             if (accion.equals("MigrarXMLsDeOpenerpADirectorio")) {
/* 2706 */               SincronizadorWRMagayaToOERP sincronizadorWRMagayaToOERP = new SincronizadorWRMagayaToOERP();
/* 2707 */               sincronizadorWRMagayaToOERP.migrarUltimosXmls(TransactionType.WarehouseReceipt);
/*      */             } 
/*      */             
/* 2710 */             if (accion.equals("MigrarAttachments")) {
/* 2711 */               String userO = "admin";
/* 2712 */               String passO = "arri4ta";
/* 2713 */               String dbO = "siati_at2";
/* 2714 */               String dbD = "siati_v3";
/* 2715 */               String hostO = "http://localhost";
/* 2716 */               String portO = "8069";
/*      */               
/* 2718 */               //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Credenciales cargadas para \"" + dbO + "\"");
/*      */               
/* 2720 */               OpenerpClient openerpClientO = new OpenerpClient();
/* 2721 */               openerpClientO.login(dbO, userO, passO, hostO, portO);
/*      */               
/* 2723 */               OpenerpClient openerpClientD = new OpenerpClient();
/* 2724 */               openerpClientD.login(dbD, userO, passO, hostO, portO);
/*      */ 
/*      */               
/* 2727 */               Vector<Vector<Object>> vectorOfVectorFilterTuplesModelos = new Vector<Vector<Object>>();
/* 2728 */               Object[] ids_modelos = openerpClientO.search("ir.model", vectorOfVectorFilterTuplesModelos);
/*      */               
/* 2730 */               Vector<String> fieldNamesModelos = new Vector();
/* 2731 */               fieldNamesModelos.add("model");
/* 2732 */               Object[] readModelos = openerpClientO.read("ir.model", ids_modelos, fieldNamesModelos);
/*      */ 
/*      */ 
/*      */               
/* 2736 */               for (int m = 0; m < readModelos.length; m++) {
/* 2737 */                 HashMap<Object, Object> hashModelo = (HashMap<Object, Object>)readModelos[m];
/*      */                 
/* 2739 */                 Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/* 2740 */                 Vector<Object> filtro = new Vector();
/* 2741 */                 filtro.add("res_model");
/* 2742 */                 filtro.add("=");
/* 2743 */                 filtro.add(hashModelo.get("model"));
/* 2744 */                 vectorOfVectorFilterTuples.add(filtro);
/*      */                 
/* 2746 */                 Object[] ids_origen = openerpClientO.search("ir.attachment", vectorOfVectorFilterTuples);
/*      */                 
/* 2748 */                 Vector<String> fieldNames = new Vector();
/* 2749 */                 fieldNames.add("datas");
/* 2750 */                 fieldNames.add("res_id");
/* 2751 */                 fieldNames.add("res_model");
/* 2752 */                 fieldNames.add("name");
/* 2753 */                 for (int i = 0; i < ids_origen.length; i++) {
/* 2754 */                   Object readO = openerpClientO.read("ir.attachment", new Object[] { ids_origen[i] }, fieldNames);
/*      */                   
/* 2756 */                   HashMap<Object, Object> hash = (HashMap<Object, Object>)((Object[])readO)[0];
/* 2757 */                   openerpClientD.write("ir.attachment", ids_origen[i], hash);
/*      */                 }
/*      */               
/*      */               } 
/*      */             }  }
/*      */           else
/*      */           
/* 2764 */           { System.out.println("Error: Debe especificar el parametro \"accion\" con valores \"VerificadorActualizaciones\", \"Actualizador\" o \"DescargaMasiva\"\nEjemplos de uso:\n\njava -cp magaya_java_lib/ -jar magaya_openerp.jar accion=VerificadorActualizaciones desde=2012-10-24T08:00:00 hasta=2012-10-24T20:00:00 trans=WarehouseReceipt,Shipment\n\njava -cp magaya_java_lib/ -jar magaya_openerp.jar accion=Actualizador desde=2012-10-24T08:00:00 hasta=2012-10-24T20:00:00 trans=WarehouseReceipt,Shipment\n\njava -cp magaya_java_lib/ -jar magaya_openerp.jar accion=DescargaMasiva desde=2012-10-24 hasta=2012-10-24 trans=WarehouseReceipt,Shipment soloOnHand=true\n\n(Los parametros desde, hasta y trans son opcionales.\nSi no se especifica desde, se hara desde la fecha de la ultima transaccion existente en openerp. Si no existen transacciones, se hara desde la fecha de inicio seteada en la configuracion de magaya en openerp.\nSi no se especifica hasta, se hara hasta el momento en que se ejecute esta aplicacion.\nSi no se especifica trans, se procesaran tanto Shipment como WarehouseReceipt. Si se especifica, se procesaran los tipos de transacciones especificadas, en el orden especificado)\nNOTA: en el caso de DescargaMasiva, desde y hasta son solo fechas, en los otros casos son fechas y horas. Los formatos son como se muestra en los ejemplos"); } 
/* 2765 */         } catch (Exception e) {
/* 2766 */           e.printStackTrace();
/*      */         } 
/* 2768 */         desmarcarCorriendo(pidfile);
/*      */       } else {
/* 2770 */         Logger.getLogger("SincronizadorWRMagayaToOERP").warn("Ya hay una instancia del sincronizador ejecutandose (el archivo \"" + pidfile + "\" existe)");
/*      */       }  }
/* 2772 */     else { Logger.getLogger("SincronizadorWRMagayaToOERP").warn("DEBE especificar el parametro pidfile"); }
/*      */   
/*      */   }
/*      */   
/*      */   private void migrarUltimosXmls(TransactionType transactionType) throws FileNotFoundException, IOException {
/* 2777 */     DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");
/*      */ 
/*      */     
/* 2780 */     String user = null;
/* 2781 */     String pass = null;
/* 2782 */     String db = null;
/* 2783 */     String host = null;
/* 2784 */     String port = null;
/* 2785 */     Properties props = new Properties();
/*      */     
/* 2787 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2788 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 2790 */     db = props.getProperty("db");
/* 2791 */     user = props.getProperty("user");
/* 2792 */     pass = props.getProperty("pass");
/* 2793 */     host = props.getProperty("host");
/* 2794 */     port = props.getProperty("port");
/* 2795 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 2797 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 2800 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/* 2801 */       SAXBuilder builder = new SAXBuilder();
/*      */       
/* 2803 */       Integer totalTransacciones = Integer.valueOf(0);
/* 2804 */       Integer totalProcesadas = Integer.valueOf(0);
/*      */       
/* 2806 */       Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/* 2807 */       Vector<String> filterDeleted = new Vector();
/* 2808 */       filterDeleted.addElement("deleted");
/* 2809 */       filterDeleted.addElement("=");
/* 2810 */       filterDeleted.addElement(Boolean.FALSE);
/* 2811 */       vectorOfVectorFilterTuples.add(filterDeleted);
/* 2812 */       Object[] trans_ids = openerpClient.search(transactionType.getOpenerpModel(), vectorOfVectorFilterTuples, Integer.valueOf(0), Integer.valueOf(100000000), "update_required desc");
/*      */       
/* 2814 */       totalTransacciones = Integer.valueOf(trans_ids.length);
/* 2815 */       //Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR a reprocesar: " + totalTransacciones);
/*      */       
/* 2817 */       Object[] trans_id = new Object[1];
/* 2818 */       for (int i = 0; i < trans_ids.length; i++) {
/* 2819 */         trans_id[0] = trans_ids[i];
/*      */         
/* 2821 */         //Logger.getLogger(getClass().getSimpleName()).info("Procesando " + (i + 1) + " de " + totalTransacciones);
/*      */         
/* 2823 */         Vector<String> fieldNames = new Vector();
/* 2824 */         fieldNames.add("guid");
/* 2825 */         fieldNames.add("state");
/* 2826 */         HashMap<Object, Object> transRead = (HashMap<Object, Object>)openerpClient.read(transactionType.getOpenerpModel(), trans_id, fieldNames)[0];
/*      */         
/* 2828 */         Object wrOpenerpId = transRead.get("id");
/* 2829 */         String transGuid = transRead.get("guid").toString();
/* 2830 */         String transState = transRead.get("state").toString();
/*      */         
/* 2832 */         Vector<Vector<Object>> filtrosXml = new Vector<Vector<Object>>();
/* 2833 */         Vector<Object> filtroWrId = new Vector();
/* 2834 */         filtroWrId.add("wr_id");
/* 2835 */         filtroWrId.add("=");
/* 2836 */         filtroWrId.add(wrOpenerpId);
/* 2837 */         filtrosXml.add(filtroWrId);
/* 2838 */         Object[] xml_ids = openerpClient.search("courier.magaya.wr.xml", filtrosXml, Integer.valueOf(0), Integer.valueOf(1), "name desc");
/*      */         
/* 2840 */         if (xml_ids.length > 0) {
/* 2841 */           totalProcesadas = Integer.valueOf(totalProcesadas.intValue() + 1);
/*      */           
/* 2843 */           Vector<String> fieldNamesXml = new Vector();
/* 2844 */           fieldNamesXml.add("xml_response");
/* 2845 */           HashMap<Object, Object> xmlRead = (HashMap<Object, Object>)openerpClient.read("courier.magaya.wr.xml", xml_ids, fieldNamesXml)[0];
/*      */           
/* 2847 */           String xmlTranss = xmlRead.get("xml_response").toString();
/*      */ 
/*      */ 
/*      */           
/* 2851 */           //Logger.getLogger(getClass().getSimpleName()).info("Tamaño XML: " + MagayaWS.getHumanSize(Integer.valueOf((xmlTranss != null) ? xmlTranss.length() : 0)));
/*      */           
/* 2853 */           if (xmlTranss != null) {
/*      */             
/* 2855 */             Reader in = new StringReader(xmlTranss);
/* 2856 */             Document document = builder.build(in);
/* 2857 */             Element nodoTrans = document.getRootElement();
/*      */             
/* 2859 */             XmlRepository.getInstance().saveUltimoXmlPara(transactionType, transGuid, nodoTrans);
/* 2860 */             //Logger.getLogger(getClass().getSimpleName()).info("Wr XML migrado al filesystem correctamente, GUID: " + transGuid);
/*      */           } else {
/* 2862 */             Logger.getLogger(getClass().getSimpleName()).info("Error al migrar el Wr XML al filesystem, GUID: " + transGuid);
/*      */           } 
/*      */         } 
/*      */       } 
/*      */       
/* 2867 */       Logger.getLogger(getClass().getSimpleName()).info("Total transacciones WR: " + totalTransacciones);
/* 2868 */       Logger.getLogger(getClass().getSimpleName()).info("Total WR procesados: " + totalProcesadas);
/*      */     }
/* 2870 */     catch (XmlRpcException e1) {
/*      */       
/* 2872 */       e1.printStackTrace();
/* 2873 */     } catch (JDOMException e) {
/*      */       
/* 2875 */       e.printStackTrace();
/* 2876 */     } catch (IOException e) {
/*      */       
/* 2878 */       e.printStackTrace();
/*      */     } 
/*      */   }
/*      */   
/*      */   private HashMap<String, String> getWRsNumbersFromCSVs(String csvFileNames) throws IOException {
/* 2883 */     HashMap<String, String> hash = new HashMap<String, String>();
/* 2884 */     String[] fileNames = csvFileNames.split(",");
/* 2885 */     for (int i = 0; i < fileNames.length; i++) {
/* 2886 */       CsvReader wrs = new CsvReader(fileNames[i]);
/* 2887 */       wrs.readHeaders();
/* 2888 */       while (wrs.readRecord()) {
/* 2889 */         String wrNumber = wrs.get("Number");
/* 2890 */         hash.put(wrNumber, wrNumber);
/*      */       } 
/* 2892 */       wrs.close();
/*      */     } 
/* 2894 */     return hash;
/*      */   }
/*      */   
/*      */   private static String getArg(String[] args, String parametro) {
/* 2898 */     for (int i = 0; i < args.length; i++) {
/* 2899 */       if (args[i].contains("=") && args[i].split("=")[0].equals(parametro))
/* 2900 */         return args[i].split("=")[1]; 
/*      */     } 
/* 2902 */     return null;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private static void desmarcarCorriendo(String pidfile) {
/*      */     try {
/* 2916 */       Runtime.getRuntime().exec(new String[] { "/bin/bash", "-c", "rm " + pidfile }).waitFor();
/*      */     }
/* 2918 */     catch (IOException e) {
/*      */       
/* 2920 */       e.printStackTrace();
/* 2921 */     } catch (InterruptedException e) {
/*      */       
/* 2923 */       e.printStackTrace();
/*      */     } 
/* 2925 */     //Logger.getLogger("SincronizadorWRMagayaToOERP").info(String.valueOf(pidfile) + " eliminado");
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private static void marcarCorriendo(String pidfile) {
/* 2938 */     //Logger.getLogger("SincronizadorWRMagayaToOERP").info("Creando archivo \"" + pidfile + "\"...");
/*      */     try {
/* 2940 */       (new File(pidfile)).createNewFile();
/* 2941 */     } catch (IOException e) {
/* 2942 */       e.printStackTrace();
/*      */     } 
/*      */   }
/*      */ 
/*      */   
/*      */   private static boolean corriendo(String pidfile) {
/* 2948 */     return (new File(pidfile)).exists();
/*      */   }
/*      */   
/*      */   public void getTransaccion(TransactionType transactionType, String wrNumber) throws FileNotFoundException, IOException {
/* 2952 */     DecimalFormat formaterMonto = new DecimalFormat("#,##0.00;(#,##0.00)");
/*      */ 
/*      */     
/* 2955 */     String user = null;
/* 2956 */     String pass = null;
/* 2957 */     String db = null;
/* 2958 */     String host = null;
/* 2959 */     String port = null;
/* 2960 */     Properties props = new Properties();
/*      */     
/* 2962 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 2963 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 2965 */     db = props.getProperty("db");
/* 2966 */     user = props.getProperty("user");
/* 2967 */     pass = props.getProperty("pass");
/* 2968 */     host = props.getProperty("host");
/* 2969 */     port = props.getProperty("port");
/* 2970 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 2972 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/*      */     try {
/* 2975 */       HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/*      */ 
/*      */       
/* 2978 */       magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */       
/* 2980 */       SAXBuilder builder = new SAXBuilder();
/*      */       
/* 2982 */       //Logger.getLogger(getClass().getSimpleName()).info("Obteniendo transaccion " + transactionType.getCode() + " : Numero: " + wrNumber + " ...");
/*      */       
/* 2984 */       String xmlTranss = magayaWS.getTransaction(transactionType.getCode(), wrNumber, Integer.valueOf(546));
/* 2985 */       if (xmlTranss != null) {
/* 2986 */         xmlTranss = xmlTranss.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 2987 */         //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/* 2988 */         Reader in = new StringReader(xmlTranss);
/* 2989 */         Document document = builder.build(in);
/* 2990 */         Element nodoTrans = document.getRootElement();
/*      */         
/*      */         try {
/* 2993 */           //Logger.getLogger(getClass().getSimpleName()).info("--------------------------------------");
/* 2994 */           //Logger.getLogger(getClass().getSimpleName()).info(String.valueOf(nodoTrans.getAttributeValue("GUID")) + " ----- " + nodoTrans.getChildText("CreatedOn"));
/*      */           
/* 2996 */           //Logger.getLogger(getClass().getSimpleName()).info("Actualizando transaccion (" + transactionType.getCode() + ")...");
/*      */         
/*      */         }
/* 2999 */         catch (Exception e) {
/* 3000 */           Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTrans), e);
/*      */         } 
/*      */       } else {
/*      */         
/* 3004 */         Logger.getLogger(getClass().getSimpleName()).warn("Se recibio NULL en la respuesta, se elimina fisicamente la transaccion. ");
/*      */       
/*      */       }
/*      */     
/*      */     }
/* 3009 */     catch (XmlRpcException e1) {
/*      */       
/* 3011 */       e1.printStackTrace();
/* 3012 */     } catch (JDOMException e) {
/*      */       
/* 3014 */       e.printStackTrace();
/* 3015 */     } catch (IOException e) {
/*      */       
/* 3017 */       e.printStackTrace();
/*      */     } 
/*      */   }
/*      */   
/*      */   public HashMap<String, String> getWRsGUIDsExistentesEnMagaya() throws JDOMException, IOException, XmlRpcException {
/* 3022 */     HashMap<String, String> hashGuidWrNumber = new HashMap<String, String>();
/* 3023 */     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
/*      */     
/* 3025 */     String user = null;
/* 3026 */     String pass = null;
/* 3027 */     String db = null;
/* 3028 */     String host = null;
/* 3029 */     String port = null;
/* 3030 */     Properties props = new Properties();
/*      */ 
/*      */     
/* 3033 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/* 3034 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 3036 */     db = props.getProperty("db");
/* 3037 */     user = props.getProperty("user");
/* 3038 */     pass = props.getProperty("pass");
/* 3039 */     host = props.getProperty("host");
/* 3040 */     port = props.getProperty("port");
/* 3041 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 3043 */     openerpClient.login(db, user, pass, host, port);
/*      */ 
/*      */     
/* 3046 */     HashMap<String, Object> configMagaya = openerpClient.getConfigMagaya();
/* 3047 */     magayaWS.login(configMagaya.get("user").toString(), configMagaya.get("password").toString());
/*      */     
/* 3049 */     Date desde = null;
/*      */     try {
/* 3051 */       desde = formater.parse("2012-10-01");
/* 3052 */     } catch (ParseException e1) {
/* 3053 */       e1.printStackTrace();
/*      */     } 
/* 3055 */     Date hasta = new Date();
/*      */     
/* 3057 */     SAXBuilder builder = new SAXBuilder();
/*      */     
/* 3059 */     //Logger.getLogger(getClass().getSimpleName()).info("Verificando WRs faltantes entre " + formater.format(desde) + " y " + formater.format(hasta) + " ...");
/*      */     
/* 3061 */     //Logger.getLogger(getClass().getSimpleName()).info("Descargando log de creaciones...");
/* 3062 */     String xmlTransCreadas = magayaWS.queryLog(TransactionType.WarehouseReceipt.getCode(), formater.format(desde), formater.format(hasta), Integer.valueOf(1), Integer.valueOf(0));
/* 3063 */     xmlTransCreadas = xmlTransCreadas.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 3064 */     //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */     
/* 3066 */     Reader in = new StringReader(xmlTransCreadas);
/* 3067 */     Document document = builder.build(in);
/* 3068 */     Element rootNode = document.getRootElement();
/* 3069 */     List<Element> nodosTrans = rootNode.getChildren("GUIDItem");
/* 3070 */     //Logger.getRootLogger().info("Creaciones obtenidas: " + nodosTrans.size());
/* 3071 */     for (Element nodoTransGuid : nodosTrans) {
/*      */       try {
/* 3073 */         hashGuidWrNumber.put(nodoTransGuid.getChildText("GUID"), nodoTransGuid.getChildText("GUID"));
/* 3074 */       } catch (Exception e) {
/* 3075 */         Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
/*      */       } 
/*      */     } 
/*      */     
/* 3079 */     //Logger.getLogger(getClass().getSimpleName()).info("Descargando log de modificaciones...");
/* 3080 */     String xmlTransModificadas = magayaWS.queryLog(TransactionType.WarehouseReceipt.getCode(), formater.format(desde), formater.format(hasta), Integer.valueOf(4), Integer.valueOf(0));
/* 3081 */     xmlTransModificadas = xmlTransModificadas.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 3082 */     //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */     
/* 3084 */     in = new StringReader(xmlTransModificadas);
/* 3085 */     document = builder.build(in);
/* 3086 */     rootNode = document.getRootElement();
/* 3087 */     nodosTrans = rootNode.getChildren("GUIDItem");
/* 3088 */     //Logger.getRootLogger().info("Modificaciones obtenidas: " + nodosTrans.size());
/* 3089 */     for (Element nodoTransGuid : nodosTrans) {
/*      */       try {
/* 3091 */         hashGuidWrNumber.put(nodoTransGuid.getChildText("GUID"), nodoTransGuid.getChildText("GUID"));
/* 3092 */       } catch (Exception e) {
/* 3093 */         Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
/*      */       } 
/*      */     } 
/*      */     
/* 3097 */     //Logger.getLogger(getClass().getSimpleName()).info("Descargando log de eliminaciones...");
/* 3098 */     String xmlTransEliminadas = magayaWS.queryLog(TransactionType.WarehouseReceipt.getCode(), formater.format(desde), formater.format(hasta), Integer.valueOf(2), Integer.valueOf(0));
/* 3099 */     xmlTransEliminadas = xmlTransEliminadas.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 3100 */     //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */     
/* 3102 */     in = new StringReader(xmlTransEliminadas);
/* 3103 */     document = builder.build(in);
/* 3104 */     rootNode = document.getRootElement();
/* 3105 */     nodosTrans = rootNode.getChildren("GUIDItem");
/* 3106 */     //Logger.getRootLogger().info("Creaciones obtenidas: " + nodosTrans.size());
/* 3107 */     for (Element nodoTransGuid : nodosTrans) {
/*      */       try {
/* 3109 */         if (hashGuidWrNumber.containsKey(nodoTransGuid.getChildText("GUID")))
/* 3110 */           hashGuidWrNumber.remove(nodoTransGuid.getChildText("GUID")); 
/* 3111 */       } catch (Exception e) {
/* 3112 */         Logger.getLogger(getClass().getSimpleName()).error("Ocurrio un error al procesar la transaccion:\n" + (new XMLOutputter(Format.getPrettyFormat())).outputString(nodoTransGuid), e);
/*      */       } 
/*      */     } 
/* 3115 */     Logger.getLogger(getClass().getSimpleName()).info("Descarga completa");
/* 3116 */     Logger.getLogger(getClass().getSimpleName()).info("Total de transacciones que deben existir (desde que se utiliza la funcion logging): " + hashGuidWrNumber.keySet().size());
/*      */     
/* 3118 */     return hashGuidWrNumber;
/*      */   }
/*      */   
/*      */   private HashMap<String, String> getWRsGUIDsExistentesEnOERP() throws FileNotFoundException, IOException, XmlRpcException {
/* 3122 */     HashMap<String, String> hashGuidWrNumber = new HashMap<String, String>();
/* 3123 */     OpenerpClient openerpClient = new OpenerpClient();
/* 3124 */     String user = null;
/* 3125 */     String pass = null;
/* 3126 */     String db = null;
/* 3127 */     String host = null;
/* 3128 */     String port = null;
/* 3129 */     Properties props = new Properties();
/*      */     
/* 3131 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/*      */     
/* 3133 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 3135 */     db = props.getProperty("db");
/* 3136 */     user = props.getProperty("user");
/* 3137 */     pass = props.getProperty("pass");
/* 3138 */     host = props.getProperty("host");
/* 3139 */     port = props.getProperty("port");
/* 3140 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 3142 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/* 3144 */     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/* 3145 */     Vector<Object> filtroNoEliminadas = new Vector();
/* 3146 */     filtroNoEliminadas.add("deleted");
/* 3147 */     filtroNoEliminadas.add("=");
/* 3148 */     filtroNoEliminadas.add(Boolean.FALSE);
/* 3149 */     vectorOfVectorFilterTuples.add(filtroNoEliminadas);
/* 3150 */     Object[] idsNoEliminadas = openerpClient.search("courier.magaya.wr", vectorOfVectorFilterTuples);
/*      */     
/* 3152 */     Vector<String> fieldNames = new Vector();
/* 3153 */     fieldNames.add("guid");
/* 3154 */     Object[] guidsVector = openerpClient.read("courier.magaya.wr", idsNoEliminadas, fieldNames);
/* 3155 */     for (int i = 0; i < guidsVector.length; i++) {
/* 3156 */       hashGuidWrNumber.put(((HashMap<?, V>)guidsVector[i]).get("guid").toString(), ((HashMap<?, V>)guidsVector[i]).get("guid").toString());
/*      */     }
/* 3158 */     return hashGuidWrNumber;
/*      */   }
/*      */   
/*      */   private HashMap<String, String> getWRsNumbersExistentesEnOERP() throws FileNotFoundException, IOException, XmlRpcException {
/* 3162 */     HashMap<String, String> hashGuidWrNumber = new HashMap<String, String>();
/* 3163 */     OpenerpClient openerpClient = new OpenerpClient();
/* 3164 */     String user = null;
/* 3165 */     String pass = null;
/* 3166 */     String db = null;
/* 3167 */     String host = null;
/* 3168 */     String port = null;
/* 3169 */     Properties props = new Properties();
/*      */     
/* 3171 */     //Logger.getLogger(getClass().getSimpleName()).info("Buscando configuracion de conexion a openerp en " + (new File("operp_db.conf")).getAbsoluteFile());
/*      */     
/* 3173 */     props.load(new FileInputStream("operp_db.conf"));
/*      */     
/* 3175 */     db = props.getProperty("db");
/* 3176 */     user = props.getProperty("user");
/* 3177 */     pass = props.getProperty("pass");
/* 3178 */     host = props.getProperty("host");
/* 3179 */     port = props.getProperty("port");
/* 3180 */     //Logger.getLogger(getClass().getSimpleName()).info("Credenciales cargadas para \"" + db + "\"");
/*      */     
/* 3182 */     openerpClient.login(db, user, pass, host, port);
/*      */     
/* 3184 */     Vector<Vector<Object>> vectorOfVectorFilterTuples = new Vector<Vector<Object>>();
/* 3185 */     Vector<Object> filtroNoEliminadas = new Vector();
/* 3186 */     filtroNoEliminadas.add("deleted");
/* 3187 */     filtroNoEliminadas.add("=");
/* 3188 */     filtroNoEliminadas.add(Boolean.FALSE);
/* 3189 */     vectorOfVectorFilterTuples.add(filtroNoEliminadas);
/* 3190 */     Object[] idsNoEliminadas = openerpClient.search("courier.magaya.wr", vectorOfVectorFilterTuples);
/*      */     
/* 3192 */     Vector<String> fieldNames = new Vector();
/* 3193 */     fieldNames.add("name");
/* 3194 */     Object[] guidsVector = openerpClient.read("courier.magaya.wr", idsNoEliminadas, fieldNames);
/* 3195 */     for (int i = 0; i < guidsVector.length; i++) {
/* 3196 */       hashGuidWrNumber.put(((HashMap<?, V>)guidsVector[i]).get("name").toString(), ((HashMap<?, V>)guidsVector[i]).get("name").toString());
/*      */     }
/* 3198 */     return hashGuidWrNumber;
/*      */   }
/*      */ 
/*      */   
/*      */   public String getTransaction(TransactionType transType, String numberOrGUID, Integer flags, MagayaWS magayaWS) throws JDOMException, IOException, XmlRpcException {
/* 3203 */     SAXBuilder builder = new SAXBuilder();
/*      */     
/* 3205 */     //Logger.getLogger(getClass().getSimpleName()).info("Descargando: " + numberOrGUID + " ...");
/*      */     
/* 3207 */     String xmlTrans = magayaWS.getTransaction(transType.getCode(), numberOrGUID, flags);
/* 3208 */     xmlTrans = xmlTrans.replace("xmlns=\"http://www.magaya.com/XMLSchema/V1\"", "");
/* 3209 */     //Logger.getLogger(getClass().getSimpleName()).info("Parseando respuesta...");
/*      */     
/* 3211 */     Reader in = new StringReader(xmlTrans);
/* 3212 */     Document document = builder.build(in);
/* 3213 */     Element rootNode = document.getRootElement();
/*      */     
/* 3215 */     xmlTrans = (new XMLOutputter(Format.getPrettyFormat())).outputString(rootNode);
/*      */     
/* 3217 */     return xmlTrans;
/*      */   }
/*      */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\SincronizadorWRMagayaToOERP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */