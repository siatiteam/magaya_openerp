/*     */ package com.siatigroup;
/*     */ 
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.util.Properties;
/*     */ import org.jdom2.Document;
/*     */ import org.jdom2.Element;
/*     */ import org.jdom2.JDOMException;
/*     */ import org.jdom2.input.SAXBuilder;
/*     */ import org.jdom2.output.Format;
/*     */ import org.jdom2.output.XMLOutputter;
/*     */ 
/*     */ public class XmlRepository
/*     */ {
/*  18 */   private static XmlRepository _instance = null;
/*     */   private File dirXml;
/*     */   private File dirXmlWr;
/*     */   private File dirXmlSh;
/*     */   private File dirXmlCr;
/*     */   private String magayaXMLPath;
/*     */   private String magayaXMLPathWr;
/*     */   private String magayaXMLPathSh;
/*     */   private String magayaXMLPathCr;
/*     */   
/*     */   private XmlRepository() throws FileNotFoundException, IOException {
/*  29 */     Properties props = new Properties();
/*  30 */     props.load(new FileInputStream("operp_db.conf"));
/*     */     
/*  32 */     this.magayaXMLPath = (props.getProperty("magayaXMLPath") != null) ? props.getProperty("magayaXMLPath") : props.getProperty("magayaxmlpath");
/*  33 */     this.magayaXMLPathWr = String.valueOf(this.magayaXMLPath) + "/wr";
/*  34 */     this.magayaXMLPathSh = String.valueOf(this.magayaXMLPath) + "/sh";
/*  35 */     this.magayaXMLPathCr = String.valueOf(this.magayaXMLPath) + "/cr";
/*     */     
/*  37 */     this.dirXml = new File(this.magayaXMLPath);
/*  38 */     this.dirXmlWr = new File(this.magayaXMLPathWr);
/*  39 */     this.dirXmlSh = new File(this.magayaXMLPathSh);
/*  40 */     this.dirXmlCr = new File(this.magayaXMLPathCr);
/*     */     
/*  42 */     if (!this.dirXml.exists()) {
/*  43 */       this.dirXml.mkdir();
/*     */     }
/*  45 */     if (!this.dirXmlWr.exists()) {
/*  46 */       this.dirXmlWr.mkdir();
/*     */     }
/*  48 */     if (!this.dirXmlSh.exists()) {
/*  49 */       this.dirXmlSh.mkdir();
/*     */     }
/*  51 */     if (!this.dirXmlCr.exists()) {
/*  52 */       this.dirXmlCr.mkdir();
/*     */     }
/*     */   }
/*     */   
/*     */   public static XmlRepository getInstance() {
/*  57 */     if (_instance == null)
/*     */       try {
/*  59 */         _instance = new XmlRepository();
/*  60 */       } catch (FileNotFoundException e) {
/*  61 */         e.printStackTrace();
/*  62 */       } catch (IOException e) {
/*  63 */         e.printStackTrace();
/*     */       }  
/*  65 */     return _instance;
/*     */   }
/*     */   
/*     */   public String getUltimoXmlPara(TransactionType type, String guid) throws JDOMException, IOException {
/*  69 */     SAXBuilder builder = new SAXBuilder();
/*  70 */     File xmlFile = null;
/*  71 */     switch (type) {
/*     */       case WarehouseReceipt:
/*  73 */         xmlFile = new File(String.valueOf(this.magayaXMLPathWr) + "/" + guid);
/*     */         break;
/*     */       case Shipment:
/*  76 */         xmlFile = new File(String.valueOf(this.magayaXMLPathSh) + "/" + guid);
/*     */         break;
/*     */       case CargoRelease:
/*  79 */         xmlFile = new File(String.valueOf(this.magayaXMLPathCr) + "/" + guid);
/*     */         break;
/*     */     } 
/*  82 */     if (xmlFile.exists()) {
/*  83 */       Document document = builder.build(xmlFile);
/*  84 */       Element rootNode = document.getRootElement();
/*  85 */       return (new XMLOutputter(Format.getPrettyFormat())).outputString(rootNode);
/*     */     } 
/*     */     
/*  88 */     return null;
/*     */   }
/*     */ 
/*     */   
/*     */   public void saveUltimoXmlPara(TransactionType type, String guid, String xml) throws JDOMException, IOException {
/*  93 */     SAXBuilder builder = new SAXBuilder();
/*  94 */     File xmlFile = null;
/*  95 */     switch (type) {
/*     */       case WarehouseReceipt:
/*  97 */         xmlFile = new File(String.valueOf(this.magayaXMLPathWr) + "/" + guid);
/*     */         break;
/*     */       case Shipment:
/* 100 */         xmlFile = new File(String.valueOf(this.magayaXMLPathSh) + "/" + guid);
/*     */         break;
/*     */       case CargoRelease:
/* 103 */         xmlFile = new File(String.valueOf(this.magayaXMLPathCr) + "/" + guid);
/*     */         break;
/*     */     } 
/*     */     
/* 107 */     Document document = builder.build(xml);
/* 108 */     XMLOutputter xmlOutput = new XMLOutputter();
/*     */     
/* 110 */     xmlOutput.setFormat(Format.getPrettyFormat());
/* 111 */     xmlOutput.output(document, new FileWriter(xmlFile));
/*     */   }
/*     */   
/*     */   public void saveUltimoXmlPara(TransactionType type, String guid, Element xml) throws JDOMException, IOException {
/* 115 */     SAXBuilder builder = new SAXBuilder();
/* 116 */     File xmlFile = null;
/* 117 */     switch (type) {
/*     */       case WarehouseReceipt:
/* 119 */         xmlFile = new File(String.valueOf(this.magayaXMLPathWr) + "/" + guid);
/*     */         break;
/*     */       case Shipment:
/* 122 */         xmlFile = new File(String.valueOf(this.magayaXMLPathSh) + "/" + guid);
/*     */         break;
/*     */       case CargoRelease:
/* 125 */         xmlFile = new File(String.valueOf(this.magayaXMLPathCr) + "/" + guid);
/*     */         break;
/*     */     } 
/*     */     
/* 129 */     XMLOutputter xmlOutput = new XMLOutputter();
/*     */     
/* 131 */     xmlOutput.setFormat(Format.getPrettyFormat());
/* 132 */     xmlOutput.output(xml, new FileWriter(xmlFile));
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\XmlRepository.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */