/*     */ package com.siatigroup;
/*     */ 
/*     */ import cssoapservice.CSSoapServiceStub;
/*     */ import cssoapservice.EndSession;
/*     */ import cssoapservice.GetActiveCurrencies;
/*     */ import cssoapservice.GetActiveCurrenciesResponse;
/*     */ import cssoapservice.StartSession;
/*     */ import cssoapservice.StartSessionResponse;
/*     */ import java.rmi.RemoteException;
/*     */ import java.text.SimpleDateFormat;
/*     */ import org.apache.log4j.BasicConfigurator;
/*     */ import org.apache.log4j.Level;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Start
/*     */ {
/*  19 */   private SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sszzz");
/*     */   
/*     */   public void login(String user, String pass) {
/*  22 */     Integer access_key = null;
/*     */     
/*  24 */     BasicConfigurator.configure();
/*  25 */     Logger.getLogger("org.apache").setLevel(Level.INFO);
/*     */     
/*     */     try {
/*  28 */       CSSoapServiceStub stub = new CSSoapServiceStub();
/*     */       
/*  30 */       stub._getServiceClient().getOptions().setProperty("__CHUNKED__", Boolean.FALSE);
/*  31 */       stub._getServiceClient().getOptions().setProperty("CONNECTION_TIMEOUT", new Integer(10000));
/*     */       
/*  33 */       StartSession startSession = new StartSession();
/*  34 */       startSession.setUser(user);
/*  35 */       startSession.setPass(pass);
/*     */       
/*  37 */       StartSessionResponse response = stub.startSession(startSession);
/*  38 */       access_key = Integer.valueOf(response.getAccess_key());
/*  39 */       System.out.println("Logueado! access key: " + access_key);
/*  40 */       System.out.println();
/*     */       
/*  42 */       GetActiveCurrencies getActiveCurr = new GetActiveCurrencies();
/*  43 */       getActiveCurr.setAccess_key(access_key.intValue());
/*  44 */       GetActiveCurrenciesResponse activeCurr = stub.getActiveCurrencies(getActiveCurr);
/*  45 */       System.out.println("Active currencies: " + activeCurr.getCurrency_list_xml());
/*  46 */       System.out.println();
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       
/*  96 */       EndSession endSession = new EndSession();
/*  97 */       endSession.setAccess_key(access_key.intValue());
/*  98 */       stub.endSession(endSession);
/*     */     
/*     */     }
/* 101 */     catch (RemoteException e) {
/*     */       
/* 103 */       e.printStackTrace();
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void main(String[] args) {
/* 113 */     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sszzz");
/*     */     
/* 115 */     MagayaWS magayaWS = new MagayaWS();
/* 116 */     magayaWS.login("API", "Ecuador153");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     
/* 124 */     System.out.println("-----------------------------------------------------------------------");
/* 125 */     System.out.println(magayaWS.getTransRangeByDate("SH", "2012-10-31", "2012-10-31", Integer.valueOf(546)));
/*     */     
/* 127 */     System.out.println("-----------------------------------------------------------------------");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     
/* 136 */     magayaWS.logout();
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\Start.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */