 package com.siatigroup;
 
 import cssoapservice.CSSoapServiceStub;
 import cssoapservice.EndSession;
 import cssoapservice.ExistsTransaction;
 import cssoapservice.ExistsTransactionResponse;
 import cssoapservice.GetActiveCurrencies;
 import cssoapservice.GetActiveCurrenciesResponse;
 import cssoapservice.GetAttachment;
 import cssoapservice.GetAttachmentResponse;
 import cssoapservice.GetEntities;
 import cssoapservice.GetEntitiesResponse;
 import cssoapservice.GetTransRangeByDate;
 import cssoapservice.GetTransRangeByDateResponse;
 import cssoapservice.GetTransaction;
 import cssoapservice.GetTransactionResponse;
 import cssoapservice.QueryLog;
 import cssoapservice.QueryLogResponse;
 import cssoapservice.StartSession;
 import cssoapservice.StartSessionResponse;
 import java.rmi.RemoteException;
 import java.text.DecimalFormat;
 import java.text.SimpleDateFormat;
 import org.apache.log4j.BasicConfigurator;
 import org.apache.log4j.Logger;
 
 
 
 
 public class MagayaWS
 {
   private Integer access_key;
   private SimpleDateFormat formater;
   private CSSoapServiceStub stub;
   private Logger logger;
   
   public MagayaWS() {
/*  38 */     BasicConfigurator.configure();
/*  39 */     this.logger = Logger.getLogger(getClass().getSimpleName());
 
 
     
/*  43 */     this.formater = new SimpleDateFormat("yyyy-MM-dd");
     
     try {
/*  46 */       this.stub = new CSSoapServiceStub();
/*  47 */       this.stub._getServiceClient().getOptions().setProperty("__CHUNKED__", Boolean.FALSE);
 
     
     }
/*  51 */     catch (RemoteException e) {
/*  52 */       this.logger.fatal("", e);
     } 
   }
   
   public boolean login(String user, String pass) {
     try {
/*  58 */       StartSession startSession = new StartSession();
/*  59 */       startSession.setUser(user);
/*  60 */       startSession.setPass(pass);
       
/*  62 */       StartSessionResponse response = this.stub.startSession(startSession);
/*  63 */       this.access_key = Integer.valueOf(response.getAccess_key());
       
/*  65 */       //this.logger.info("Login OK!");
     }
/*  67 */     catch (RemoteException e) {
/*  68 */       this.logger.error("", e);
     } 
/*  70 */     return (this.access_key != null && this.access_key.intValue() != 0);
   }
   
   public void logout() {
     try {
/*  75 */       EndSession endSession = new EndSession();
/*  76 */       endSession.setAccess_key(this.access_key.intValue());
/*  77 */       this.stub.endSession(endSession);
       
/*  79 */       //this.logger.info("Logout OK, bye!");
/*  80 */     } catch (RemoteException e) {
/*  81 */       this.logger.error("", e);
     } 
   }
   
   public String getActiveCurrencies() {
/*  86 */     String xmlCurrencies = null;
     try {
/*  88 */       GetActiveCurrencies getActiveCurr = new GetActiveCurrencies();
/*  89 */       getActiveCurr.setAccess_key(this.access_key.intValue());
/*  90 */       GetActiveCurrenciesResponse activeCurr = this.stub.getActiveCurrencies(getActiveCurr);
/*  91 */       xmlCurrencies = activeCurr.getCurrency_list_xml();
       
/*  93 */       this.logger.debug("Active currencies:\n " + xmlCurrencies);
/*  94 */     } catch (RemoteException e) {
/*  95 */       this.logger.error("", e);
     } 
/*  97 */     return xmlCurrencies;
   }
   
   public String getTransaction(String transTypeCode, String transNum, Integer flags) {
/* 101 */     String xmlTransaction = null;
     try {
/* 103 */       GetTransaction getTransaction = new GetTransaction();
/* 104 */       getTransaction.setAccess_key(this.access_key.intValue());
/* 105 */       getTransaction.setType(transTypeCode);
/* 106 */       getTransaction.setNumber(transNum);
/* 107 */       getTransaction.setFlags(flags.intValue());
/* 108 */       GetTransactionResponse getTransactionResponse = this.stub.getTransaction(getTransaction);
/* 109 */       xmlTransaction = getTransactionResponse.getTrans_xml();
       
/* 111 */       //this.logger.info("Tamaño transaccion: " + ((xmlTransaction != null) ? getHumanSize(Integer.valueOf(xmlTransaction.length())) : "(0 Bytes)"));
/* 112 */     } catch (RemoteException e) {
/* 113 */       this.logger.error("", e);
     } 
/* 115 */     return xmlTransaction;
   }
 
 
   
   public static String getHumanSize(Integer bytes) {
/* 121 */     if (bytes.intValue() / 1024 < 1)
/* 122 */       return String.valueOf(DecimalFormat.getInstance().format(bytes)) + " B"; 
/* 123 */     if (bytes.intValue() / 1048576 < 1)
/* 124 */       return String.valueOf(DecimalFormat.getInstance().format((bytes.intValue() / 1024))) + " KB"; 
/* 125 */     if (bytes.intValue() / 1073741824 < 1) {
/* 126 */       return String.valueOf(DecimalFormat.getInstance().format((bytes.intValue() / 1048576))) + " MB";
     }
/* 128 */     return String.valueOf(DecimalFormat.getInstance().format((bytes.intValue() / 1073741824))) + " GB";
   }
 
 
 
 
 
 
 
 
 
 
   
   public String getTransRangeByDate(String transTypeCode, String startDate, String endDate, Integer flags) {
/* 142 */     String xmlTransaction = null;
     try {
/* 144 */       GetTransRangeByDate getTransRangeByDate = new GetTransRangeByDate();
/* 145 */       getTransRangeByDate.setAccess_key(this.access_key.intValue());
/* 146 */       getTransRangeByDate.setType(transTypeCode);
/* 147 */       getTransRangeByDate.setStart_date(startDate);
/* 148 */       getTransRangeByDate.setEnd_date(endDate);
/* 149 */       getTransRangeByDate.setFlags(flags.intValue());
/* 150 */       GetTransRangeByDateResponse getTransRangeByDateResponse = this.stub.getTransRangeByDate(getTransRangeByDate);
/* 151 */       xmlTransaction = getTransRangeByDateResponse.getTrans_list_xml();
     
     }
/* 154 */     catch (RemoteException e) {
/* 155 */       this.logger.error("", e);
     } 
/* 157 */     return xmlTransaction;
   }
 
 
 
 
 
 
 
 
 
   
   public String queryLog(String transTypeCode, String startDate, String endDate, Integer logEntryType, Integer flags) {
/* 170 */     if (startDate != null && startDate.length() > 3) {
/* 171 */       String nuevaFecha = "";
/* 172 */       for (int i = 0; i < startDate.length(); i++) {
/* 173 */         nuevaFecha = String.valueOf(nuevaFecha) + startDate.charAt(i);
/* 174 */         if (i == 21) {
/* 175 */           nuevaFecha = String.valueOf(nuevaFecha) + ":";
         }
       } 
/* 178 */       startDate = nuevaFecha;
     } 
/* 180 */     if (endDate != null && endDate.length() > 3) {
/* 181 */       String nuevaFecha = "";
/* 182 */       for (int i = 0; i < endDate.length(); i++) {
/* 183 */         nuevaFecha = String.valueOf(nuevaFecha) + endDate.charAt(i);
/* 184 */         if (i == 21) {
/* 185 */           nuevaFecha = String.valueOf(nuevaFecha) + ":";
         }
       } 
/* 188 */       endDate = nuevaFecha;
     } 
 
 
     
/* 193 */     String xmlTransaction = null;
     try {
/* 195 */       QueryLog queryLog = new QueryLog();
/* 196 */       queryLog.setAccess_key(this.access_key.intValue());
/* 197 */       queryLog.setTrans_type(transTypeCode);
/* 198 */       queryLog.setLog_entry_type(logEntryType.intValue());
/* 199 */       queryLog.setStart_date(startDate);
/* 200 */       queryLog.setEnd_date(endDate);
/* 201 */       queryLog.setFlags(flags.intValue());
/* 202 */       QueryLogResponse queryLogResponse = this.stub.queryLog(queryLog);
/* 203 */       xmlTransaction = queryLogResponse.getTrans_list_xml();
     }
/* 205 */     catch (RemoteException e) {
/* 206 */       this.logger.error("", e);
     } 
/* 208 */     return xmlTransaction;
   }
 
   
   public boolean existsTransaction(String transTypeCode, String transNum) {
/* 213 */     boolean exist = false;
     try {
/* 215 */       ExistsTransaction existsTransaction = new ExistsTransaction();
/* 216 */       existsTransaction.setAccess_key(this.access_key.intValue());
/* 217 */       existsTransaction.setType(transTypeCode);
/* 218 */       existsTransaction.setNumber(transNum);
       
/* 220 */       ExistsTransactionResponse existsTransactionResponse = this.stub.existsTransaction(existsTransaction);
/* 221 */       exist = (existsTransactionResponse.getExist_trans() == 1);
     
     }
/* 224 */     catch (RemoteException e) {
/* 225 */       this.logger.error("", e);
     } 
/* 227 */     return exist;
   }
   
   public String getEntities(String startWith) {
/* 231 */     String xmlEntities = null;
     
     try {
/* 234 */       GetEntities getEntities = new GetEntities();
/* 235 */       getEntities.setAccess_key(this.access_key.intValue());
/* 236 */       getEntities.setStart_with(startWith);
/* 237 */       getEntities.setFlags(0);
       
/* 239 */       GetEntitiesResponse getEntitiesResponse = this.stub.getEntities(getEntities);
/* 240 */       xmlEntities = getEntitiesResponse.getEntity_list_xml();
     }
/* 242 */     catch (Exception e) {
/* 243 */       e.printStackTrace();
     } 
     
/* 246 */     return xmlEntities;
   }
 
 
   
   public String getAttachment(String ownerType, String ownerGuid, String attachmentId) {
/* 252 */     String xmlTransaction = null;
     try {
/* 254 */       GetAttachment getAttachment = new GetAttachment();
/* 255 */       getAttachment.setAttach_id(Integer.valueOf(attachmentId).intValue());
/* 256 */       getAttachment.setTrans_uuid(ownerGuid);
/* 257 */       getAttachment.setApp(ownerType);
/* 258 */       GetAttachmentResponse getTransactionResponse = this.stub.getAttachment(getAttachment);
/* 259 */       xmlTransaction = getTransactionResponse.getAttach_xml();
       
/* 261 */       //this.logger.info("Tamaño attachment: " + ((xmlTransaction != null) ? getHumanSize(Integer.valueOf(xmlTransaction.length())) : "(0 Bytes)"));
/* 262 */     } catch (RemoteException e) {
/* 263 */       this.logger.error("", e);
     } 
/* 265 */     return xmlTransaction;
   }
 }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\MagayaWS.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */