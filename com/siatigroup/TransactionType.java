/*    */ package com.siatigroup;
/*    */ 
/*    */ public enum TransactionType
/*    */ {
/*  5 */   WarehouseReceipt("WH", "Warehouse Receipt", "courier.magaya.wr"),
/*  6 */   Shipment("SH", "Shipment", "courier.magaya.sh"),
/*  7 */   CargoRelease("CR", "Cargo Release", "courier.magaya.cr"),
/*  8 */   PickupOrder("PK", "Pickup Order", ""),
/*  9 */   Invoice("IN", "Invoice", ""),
/* 10 */   Inventory("IV", "Inventory", ""),
/* 11 */   Booking("BK", "Booking", ""),
/* 12 */   Quotation("QT", "Quotation", ""),
/* 13 */   Bill("BI", "Bill", ""),
/* 14 */   PurchaseOrder("PO", "Purchase Order", ""),
/* 15 */   SalesOrder("SO", "Sales Order", ""),
/* 16 */   WarehouseItem("WI", "Warehouse Item", "courier.magaya.wr.line"),
/* 17 */   Payment("PM", "Payment", ""),
/* 18 */   Deposit("DP", "Deposit", ""),
/* 19 */   Check("CK", "Check", ""),
/* 20 */   JournalEntry("JE", "Journal entry", "");
/*    */   
/*    */   private String code;
/*    */   private String description;
/*    */   private String openerpModel;
/*    */   
/*    */   TransactionType(String code, String description, String openerpModel) {
/* 27 */     this.code = code;
/* 28 */     this.description = description;
/* 29 */     this.openerpModel = openerpModel;
/*    */   }
/*    */   
/*    */   public String getCode() {
/* 33 */     return this.code;
/*    */   }
/*    */   
/*    */   public String getDescription() {
/* 37 */     return this.description;
/*    */   }
/*    */   
/*    */   public String getOpenerpModel() {
/* 41 */     return this.openerpModel;
/*    */   }
/*    */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\com\siatigroup\TransactionType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */