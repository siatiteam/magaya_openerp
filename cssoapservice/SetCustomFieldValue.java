/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SetCustomFieldValue implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SetCustomFieldValue", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */   
/*     */   protected String localType;
/*     */ 
/*     */   
/*     */   protected String localNumber;
/*     */   
/*     */   protected String localField_internal_name;
/*     */   
/*     */   protected String localField_value;
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/*  71 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/*  82 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNumber() {
/* 101 */     return this.localNumber;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNumber(String param) {
/* 112 */     this.localNumber = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getField_internal_name() {
/* 131 */     return this.localField_internal_name;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setField_internal_name(String param) {
/* 142 */     this.localField_internal_name = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getField_value() {
/* 161 */     return this.localField_value;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setField_value(String param) {
/* 172 */     this.localField_value = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 192 */     ADBDataSource aDBDataSource = 
/* 193 */       new ADBDataSource(this, MY_QNAME);
/* 194 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 201 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 212 */     String prefix = null;
/* 213 */     String namespace = null;
/*     */ 
/*     */     
/* 216 */     prefix = parentQName.getPrefix();
/* 217 */     namespace = parentQName.getNamespaceURI();
/* 218 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 220 */     if (serializeType) {
/*     */ 
/*     */       
/* 223 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 224 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 225 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 226 */             String.valueOf(namespacePrefix) + ":SetCustomFieldValue", 
/* 227 */             xmlWriter);
/*     */       } else {
/* 229 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 230 */             "SetCustomFieldValue", 
/* 231 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 237 */     namespace = "";
/* 238 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 240 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 242 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 245 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 248 */     xmlWriter.writeEndElement();
/*     */     
/* 250 */     namespace = "";
/* 251 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 254 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 257 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 262 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 266 */     xmlWriter.writeEndElement();
/*     */     
/* 268 */     namespace = "";
/* 269 */     writeStartElement(null, namespace, "number", xmlWriter);
/*     */ 
/*     */     
/* 272 */     if (this.localNumber == null) {
/*     */ 
/*     */       
/* 275 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 280 */       xmlWriter.writeCharacters(this.localNumber);
/*     */     } 
/*     */ 
/*     */     
/* 284 */     xmlWriter.writeEndElement();
/*     */     
/* 286 */     namespace = "";
/* 287 */     writeStartElement(null, namespace, "field_internal_name", xmlWriter);
/*     */ 
/*     */     
/* 290 */     if (this.localField_internal_name == null) {
/*     */ 
/*     */       
/* 293 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 298 */       xmlWriter.writeCharacters(this.localField_internal_name);
/*     */     } 
/*     */ 
/*     */     
/* 302 */     xmlWriter.writeEndElement();
/*     */     
/* 304 */     namespace = "";
/* 305 */     writeStartElement(null, namespace, "field_value", xmlWriter);
/*     */ 
/*     */     
/* 308 */     if (this.localField_value == null) {
/*     */ 
/*     */       
/* 311 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 316 */       xmlWriter.writeCharacters(this.localField_value);
/*     */     } 
/*     */ 
/*     */     
/* 320 */     xmlWriter.writeEndElement();
/*     */     
/* 322 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 328 */     if (namespace.equals("urn:CSSoapService")) {
/* 329 */       return "ns1";
/*     */     }
/* 331 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 339 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 340 */     if (writerPrefix != null) {
/* 341 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 343 */       if (namespace.length() == 0) {
/* 344 */         prefix = "";
/* 345 */       } else if (prefix == null) {
/* 346 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 349 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 350 */       xmlWriter.writeNamespace(prefix, namespace);
/* 351 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 360 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 361 */       xmlWriter.writeNamespace(prefix, namespace);
/* 362 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 364 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 372 */     if (namespace.equals("")) {
/* 373 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 375 */       registerPrefix(xmlWriter, namespace);
/* 376 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 387 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 388 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 389 */     if (attributePrefix == null) {
/* 390 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 393 */     if (attributePrefix.trim().length() > 0) {
/* 394 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 396 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 399 */     if (namespace.equals("")) {
/* 400 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 402 */       registerPrefix(xmlWriter, namespace);
/* 403 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 412 */     String namespaceURI = qname.getNamespaceURI();
/* 413 */     if (namespaceURI != null) {
/* 414 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 415 */       if (prefix == null) {
/* 416 */         prefix = generatePrefix(namespaceURI);
/* 417 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 418 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 421 */       if (prefix.trim().length() > 0) {
/* 422 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 425 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 429 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 436 */     if (qnames != null) {
/*     */ 
/*     */       
/* 439 */       StringBuffer stringToWrite = new StringBuffer();
/* 440 */       String namespaceURI = null;
/* 441 */       String prefix = null;
/*     */       
/* 443 */       for (int i = 0; i < qnames.length; i++) {
/* 444 */         if (i > 0) {
/* 445 */           stringToWrite.append(" ");
/*     */         }
/* 447 */         namespaceURI = qnames[i].getNamespaceURI();
/* 448 */         if (namespaceURI != null) {
/* 449 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 450 */           if (prefix == null || prefix.length() == 0) {
/* 451 */             prefix = generatePrefix(namespaceURI);
/* 452 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 453 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 456 */           if (prefix.trim().length() > 0) {
/* 457 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 459 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 462 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 465 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 475 */     String prefix = xmlWriter.getPrefix(namespace);
/* 476 */     if (prefix == null) {
/* 477 */       prefix = generatePrefix(namespace);
/* 478 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 480 */         String uri = nsContext.getNamespaceURI(prefix);
/* 481 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 484 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 486 */       xmlWriter.writeNamespace(prefix, namespace);
/* 487 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 489 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 503 */     ArrayList<QName> elementList = new ArrayList();
/* 504 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 507 */     elementList.add(new QName("", 
/* 508 */           "access_key"));
/*     */     
/* 510 */     elementList.add(
/* 511 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 513 */     elementList.add(new QName("", 
/* 514 */           "type"));
/*     */     
/* 516 */     elementList.add((this.localType == null) ? null : 
/* 517 */         ConverterUtil.convertToString(this.localType));
/*     */     
/* 519 */     elementList.add(new QName("", 
/* 520 */           "number"));
/*     */     
/* 522 */     elementList.add((this.localNumber == null) ? null : 
/* 523 */         ConverterUtil.convertToString(this.localNumber));
/*     */     
/* 525 */     elementList.add(new QName("", 
/* 526 */           "field_internal_name"));
/*     */     
/* 528 */     elementList.add((this.localField_internal_name == null) ? null : 
/* 529 */         ConverterUtil.convertToString(this.localField_internal_name));
/*     */     
/* 531 */     elementList.add(new QName("", 
/* 532 */           "field_value"));
/*     */     
/* 534 */     elementList.add((this.localField_value == null) ? null : 
/* 535 */         ConverterUtil.convertToString(this.localField_value));
/*     */ 
/*     */     
/* 538 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SetCustomFieldValue parse(XMLStreamReader reader) throws Exception {
/* 562 */       SetCustomFieldValue object = 
/* 563 */         new SetCustomFieldValue();
/*     */ 
/*     */       
/* 566 */       String nillableValue = null;
/* 567 */       String prefix = "";
/* 568 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 571 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 572 */           reader.next();
/*     */         }
/*     */         
/* 575 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 576 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 577 */               "type");
/* 578 */           if (fullTypeName != null) {
/* 579 */             String nsPrefix = null;
/* 580 */             if (fullTypeName.indexOf(":") > -1) {
/* 581 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 583 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 585 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 587 */             if (!"SetCustomFieldValue".equals(type)) {
/*     */               
/* 589 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 590 */               return (SetCustomFieldValue)ExtensionMapper.getTypeObject(
/* 591 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 605 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 610 */         reader.next();
/*     */ 
/*     */         
/* 613 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 615 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 617 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 618 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 621 */             String content = reader.getElementText();
/*     */             
/* 623 */             object.setAccess_key(
/* 624 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 629 */             object.setAccess_key(-2147483648);
/*     */             
/* 631 */             reader.getElementText();
/*     */           } 
/*     */           
/* 634 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 640 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 644 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 646 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 648 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 649 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 652 */             String content = reader.getElementText();
/*     */             
/* 654 */             object.setType(
/* 655 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 660 */             reader.getElementText();
/*     */           } 
/*     */           
/* 663 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 669 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 673 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 675 */         if (reader.isStartElement() && (new QName("", "number")).equals(reader.getName())) {
/*     */           
/* 677 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 678 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 681 */             String content = reader.getElementText();
/*     */             
/* 683 */             object.setNumber(
/* 684 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 689 */             reader.getElementText();
/*     */           } 
/*     */           
/* 692 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 698 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 702 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 704 */         if (reader.isStartElement() && (new QName("", "field_internal_name")).equals(reader.getName())) {
/*     */           
/* 706 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 707 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 710 */             String content = reader.getElementText();
/*     */             
/* 712 */             object.setField_internal_name(
/* 713 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 718 */             reader.getElementText();
/*     */           } 
/*     */           
/* 721 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 727 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 731 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 733 */         if (reader.isStartElement() && (new QName("", "field_value")).equals(reader.getName())) {
/*     */           
/* 735 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 736 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 739 */             String content = reader.getElementText();
/*     */             
/* 741 */             object.setField_value(
/* 742 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 747 */             reader.getElementText();
/*     */           } 
/*     */           
/* 750 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 756 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 759 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 760 */           reader.next();
/*     */         }
/* 762 */         if (reader.isStartElement())
/*     */         {
/* 764 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 769 */       catch (XMLStreamException e) {
/* 770 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 773 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SetCustomFieldValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */