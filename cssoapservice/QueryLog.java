/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class QueryLog implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "QueryLog", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */   
/*     */   protected String localStart_date;
/*     */   
/*     */   protected String localEnd_date;
/*     */   
/*     */   protected int localLog_entry_type;
/*     */   
/*     */   protected String localTrans_type;
/*     */   
/*     */   protected int localFlags;
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getStart_date() {
/*  71 */     return this.localStart_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStart_date(String param) {
/*  82 */     this.localStart_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEnd_date() {
/* 101 */     return this.localEnd_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEnd_date(String param) {
/* 112 */     this.localEnd_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getLog_entry_type() {
/* 131 */     return this.localLog_entry_type;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setLog_entry_type(int param) {
/* 142 */     this.localLog_entry_type = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getTrans_type() {
/* 161 */     return this.localTrans_type;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setTrans_type(String param) {
/* 172 */     this.localTrans_type = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/* 191 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/* 202 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 222 */     ADBDataSource aDBDataSource = 
/* 223 */       new ADBDataSource(this, MY_QNAME);
/* 224 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 231 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 242 */     String prefix = null;
/* 243 */     String namespace = null;
/*     */ 
/*     */     
/* 246 */     prefix = parentQName.getPrefix();
/* 247 */     namespace = parentQName.getNamespaceURI();
/* 248 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 250 */     if (serializeType) {
/*     */ 
/*     */       
/* 253 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 254 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 255 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 256 */             String.valueOf(namespacePrefix) + ":QueryLog", 
/* 257 */             xmlWriter);
/*     */       } else {
/* 259 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 260 */             "QueryLog", 
/* 261 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 267 */     namespace = "";
/* 268 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 270 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 272 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 275 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 278 */     xmlWriter.writeEndElement();
/*     */     
/* 280 */     namespace = "";
/* 281 */     writeStartElement(null, namespace, "start_date", xmlWriter);
/*     */ 
/*     */     
/* 284 */     if (this.localStart_date == null) {
/*     */ 
/*     */       
/* 287 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 292 */       xmlWriter.writeCharacters(this.localStart_date);
/*     */     } 
/*     */ 
/*     */     
/* 296 */     xmlWriter.writeEndElement();
/*     */     
/* 298 */     namespace = "";
/* 299 */     writeStartElement(null, namespace, "end_date", xmlWriter);
/*     */ 
/*     */     
/* 302 */     if (this.localEnd_date == null) {
/*     */ 
/*     */       
/* 305 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 310 */       xmlWriter.writeCharacters(this.localEnd_date);
/*     */     } 
/*     */ 
/*     */     
/* 314 */     xmlWriter.writeEndElement();
/*     */     
/* 316 */     namespace = "";
/* 317 */     writeStartElement(null, namespace, "log_entry_type", xmlWriter);
/*     */     
/* 319 */     if (this.localLog_entry_type == Integer.MIN_VALUE) {
/*     */       
/* 321 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 324 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localLog_entry_type));
/*     */     } 
/*     */     
/* 327 */     xmlWriter.writeEndElement();
/*     */     
/* 329 */     namespace = "";
/* 330 */     writeStartElement(null, namespace, "trans_type", xmlWriter);
/*     */ 
/*     */     
/* 333 */     if (this.localTrans_type == null) {
/*     */ 
/*     */       
/* 336 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 341 */       xmlWriter.writeCharacters(this.localTrans_type);
/*     */     } 
/*     */ 
/*     */     
/* 345 */     xmlWriter.writeEndElement();
/*     */     
/* 347 */     namespace = "";
/* 348 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 350 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 352 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 355 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 358 */     xmlWriter.writeEndElement();
/*     */     
/* 360 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 366 */     if (namespace.equals("urn:CSSoapService")) {
/* 367 */       return "ns1";
/*     */     }
/* 369 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 377 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 378 */     if (writerPrefix != null) {
/* 379 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 381 */       if (namespace.length() == 0) {
/* 382 */         prefix = "";
/* 383 */       } else if (prefix == null) {
/* 384 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 387 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 388 */       xmlWriter.writeNamespace(prefix, namespace);
/* 389 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 398 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 399 */       xmlWriter.writeNamespace(prefix, namespace);
/* 400 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 402 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 410 */     if (namespace.equals("")) {
/* 411 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 413 */       registerPrefix(xmlWriter, namespace);
/* 414 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 425 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 426 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 427 */     if (attributePrefix == null) {
/* 428 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 431 */     if (attributePrefix.trim().length() > 0) {
/* 432 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 434 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 437 */     if (namespace.equals("")) {
/* 438 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 440 */       registerPrefix(xmlWriter, namespace);
/* 441 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 450 */     String namespaceURI = qname.getNamespaceURI();
/* 451 */     if (namespaceURI != null) {
/* 452 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 453 */       if (prefix == null) {
/* 454 */         prefix = generatePrefix(namespaceURI);
/* 455 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 456 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 459 */       if (prefix.trim().length() > 0) {
/* 460 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 463 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 467 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 474 */     if (qnames != null) {
/*     */ 
/*     */       
/* 477 */       StringBuffer stringToWrite = new StringBuffer();
/* 478 */       String namespaceURI = null;
/* 479 */       String prefix = null;
/*     */       
/* 481 */       for (int i = 0; i < qnames.length; i++) {
/* 482 */         if (i > 0) {
/* 483 */           stringToWrite.append(" ");
/*     */         }
/* 485 */         namespaceURI = qnames[i].getNamespaceURI();
/* 486 */         if (namespaceURI != null) {
/* 487 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 488 */           if (prefix == null || prefix.length() == 0) {
/* 489 */             prefix = generatePrefix(namespaceURI);
/* 490 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 491 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 494 */           if (prefix.trim().length() > 0) {
/* 495 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 497 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 500 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 503 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 513 */     String prefix = xmlWriter.getPrefix(namespace);
/* 514 */     if (prefix == null) {
/* 515 */       prefix = generatePrefix(namespace);
/* 516 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 518 */         String uri = nsContext.getNamespaceURI(prefix);
/* 519 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 522 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 524 */       xmlWriter.writeNamespace(prefix, namespace);
/* 525 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 527 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 541 */     ArrayList<QName> elementList = new ArrayList();
/* 542 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 545 */     elementList.add(new QName("", 
/* 546 */           "access_key"));
/*     */     
/* 548 */     elementList.add(
/* 549 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 551 */     elementList.add(new QName("", 
/* 552 */           "start_date"));
/*     */     
/* 554 */     elementList.add((this.localStart_date == null) ? null : 
/* 555 */         ConverterUtil.convertToString(this.localStart_date));
/*     */     
/* 557 */     elementList.add(new QName("", 
/* 558 */           "end_date"));
/*     */     
/* 560 */     elementList.add((this.localEnd_date == null) ? null : 
/* 561 */         ConverterUtil.convertToString(this.localEnd_date));
/*     */     
/* 563 */     elementList.add(new QName("", 
/* 564 */           "log_entry_type"));
/*     */     
/* 566 */     elementList.add(
/* 567 */         ConverterUtil.convertToString(this.localLog_entry_type));
/*     */     
/* 569 */     elementList.add(new QName("", 
/* 570 */           "trans_type"));
/*     */     
/* 572 */     elementList.add((this.localTrans_type == null) ? null : 
/* 573 */         ConverterUtil.convertToString(this.localTrans_type));
/*     */     
/* 575 */     elementList.add(new QName("", 
/* 576 */           "flags"));
/*     */     
/* 578 */     elementList.add(
/* 579 */         ConverterUtil.convertToString(this.localFlags));
/*     */ 
/*     */     
/* 582 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static QueryLog parse(XMLStreamReader reader) throws Exception {
/* 606 */       QueryLog object = 
/* 607 */         new QueryLog();
/*     */ 
/*     */       
/* 610 */       String nillableValue = null;
/* 611 */       String prefix = "";
/* 612 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 615 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 616 */           reader.next();
/*     */         }
/*     */         
/* 619 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 620 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 621 */               "type");
/* 622 */           if (fullTypeName != null) {
/* 623 */             String nsPrefix = null;
/* 624 */             if (fullTypeName.indexOf(":") > -1) {
/* 625 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 627 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 629 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 631 */             if (!"QueryLog".equals(type)) {
/*     */               
/* 633 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 634 */               return (QueryLog)ExtensionMapper.getTypeObject(
/* 635 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 649 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 654 */         reader.next();
/*     */ 
/*     */         
/* 657 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 659 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 661 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 662 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 665 */             String content = reader.getElementText();
/*     */             
/* 667 */             object.setAccess_key(
/* 668 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 673 */             object.setAccess_key(-2147483648);
/*     */             
/* 675 */             reader.getElementText();
/*     */           } 
/*     */           
/* 678 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 684 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 688 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 690 */         if (reader.isStartElement() && (new QName("", "start_date")).equals(reader.getName())) {
/*     */           
/* 692 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 693 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 696 */             String content = reader.getElementText();
/*     */             
/* 698 */             object.setStart_date(
/* 699 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 704 */             reader.getElementText();
/*     */           } 
/*     */           
/* 707 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 713 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 717 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 719 */         if (reader.isStartElement() && (new QName("", "end_date")).equals(reader.getName())) {
/*     */           
/* 721 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 722 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 725 */             String content = reader.getElementText();
/*     */             
/* 727 */             object.setEnd_date(
/* 728 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 733 */             reader.getElementText();
/*     */           } 
/*     */           
/* 736 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 742 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 746 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 748 */         if (reader.isStartElement() && (new QName("", "log_entry_type")).equals(reader.getName())) {
/*     */           
/* 750 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 751 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 754 */             String content = reader.getElementText();
/*     */             
/* 756 */             object.setLog_entry_type(
/* 757 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 762 */             object.setLog_entry_type(-2147483648);
/*     */             
/* 764 */             reader.getElementText();
/*     */           } 
/*     */           
/* 767 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 773 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 777 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 779 */         if (reader.isStartElement() && (new QName("", "trans_type")).equals(reader.getName())) {
/*     */           
/* 781 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 782 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 785 */             String content = reader.getElementText();
/*     */             
/* 787 */             object.setTrans_type(
/* 788 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 793 */             reader.getElementText();
/*     */           } 
/*     */           
/* 796 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 802 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 806 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 808 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 810 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 811 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 814 */             String content = reader.getElementText();
/*     */             
/* 816 */             object.setFlags(
/* 817 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 822 */             object.setFlags(-2147483648);
/*     */             
/* 824 */             reader.getElementText();
/*     */           } 
/*     */           
/* 827 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 833 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 836 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 837 */           reader.next();
/*     */         }
/* 839 */         if (reader.isStartElement())
/*     */         {
/* 841 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 846 */       catch (XMLStreamException e) {
/* 847 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 850 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\QueryLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */