/*      */ package cssoapservice;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.Vector;
/*      */ import javax.xml.namespace.NamespaceContext;
/*      */ import javax.xml.namespace.QName;
/*      */ import javax.xml.stream.XMLStreamException;
/*      */ import javax.xml.stream.XMLStreamReader;
/*      */ import javax.xml.stream.XMLStreamWriter;
/*      */ import org.apache.axiom.om.OMDataSource;
/*      */ import org.apache.axiom.om.OMElement;
/*      */ import org.apache.axiom.om.OMFactory;
/*      */ import org.apache.axis2.databinding.ADBBean;
/*      */ import org.apache.axis2.databinding.ADBDataSource;
/*      */ import org.apache.axis2.databinding.ADBException;
/*      */ import org.apache.axis2.databinding.utils.BeanUtil;
/*      */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*      */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*      */ 
/*      */ public class QueryLogJS implements ADBBean {
/*   21 */   public static final QName MY_QNAME = new QName(
/*   22 */       "urn:CSSoapService", 
/*   23 */       "QueryLogJS", 
/*   24 */       "ns1");
/*      */   
/*      */   protected int localAccess_key;
/*      */   
/*      */   protected String localStart_date;
/*      */   
/*      */   protected String localEnd_date;
/*      */   
/*      */   protected int localLog_entry_type;
/*      */   
/*      */   protected String localTrans_type;
/*      */   protected String localFunction;
/*      */   protected String localXml_params;
/*      */   protected int localLog_flags;
/*      */   protected int localXml_flags;
/*      */   
/*      */   public int getAccess_key() {
/*   41 */     return this.localAccess_key;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setAccess_key(int param) {
/*   52 */     this.localAccess_key = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getStart_date() {
/*   71 */     return this.localStart_date;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setStart_date(String param) {
/*   82 */     this.localStart_date = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getEnd_date() {
/*  101 */     return this.localEnd_date;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setEnd_date(String param) {
/*  112 */     this.localEnd_date = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getLog_entry_type() {
/*  131 */     return this.localLog_entry_type;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLog_entry_type(int param) {
/*  142 */     this.localLog_entry_type = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getTrans_type() {
/*  161 */     return this.localTrans_type;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setTrans_type(String param) {
/*  172 */     this.localTrans_type = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getFunction() {
/*  191 */     return this.localFunction;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setFunction(String param) {
/*  202 */     this.localFunction = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getXml_params() {
/*  221 */     return this.localXml_params;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setXml_params(String param) {
/*  232 */     this.localXml_params = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getLog_flags() {
/*  251 */     return this.localLog_flags;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLog_flags(int param) {
/*  262 */     this.localLog_flags = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getXml_flags() {
/*  281 */     return this.localXml_flags;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setXml_flags(int param) {
/*  292 */     this.localXml_flags = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  312 */     ADBDataSource aDBDataSource = 
/*  313 */       new ADBDataSource(this, MY_QNAME);
/*  314 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  321 */     serialize(parentQName, xmlWriter, false);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  332 */     String prefix = null;
/*  333 */     String namespace = null;
/*      */ 
/*      */     
/*  336 */     prefix = parentQName.getPrefix();
/*  337 */     namespace = parentQName.getNamespaceURI();
/*  338 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*      */     
/*  340 */     if (serializeType) {
/*      */ 
/*      */       
/*  343 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/*  344 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/*  345 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  346 */             String.valueOf(namespacePrefix) + ":QueryLogJS", 
/*  347 */             xmlWriter);
/*      */       } else {
/*  349 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  350 */             "QueryLogJS", 
/*  351 */             xmlWriter);
/*      */       } 
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*  357 */     namespace = "";
/*  358 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*      */     
/*  360 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*      */       
/*  362 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  365 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*      */     } 
/*      */     
/*  368 */     xmlWriter.writeEndElement();
/*      */     
/*  370 */     namespace = "";
/*  371 */     writeStartElement(null, namespace, "start_date", xmlWriter);
/*      */ 
/*      */     
/*  374 */     if (this.localStart_date == null) {
/*      */ 
/*      */       
/*  377 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  382 */       xmlWriter.writeCharacters(this.localStart_date);
/*      */     } 
/*      */ 
/*      */     
/*  386 */     xmlWriter.writeEndElement();
/*      */     
/*  388 */     namespace = "";
/*  389 */     writeStartElement(null, namespace, "end_date", xmlWriter);
/*      */ 
/*      */     
/*  392 */     if (this.localEnd_date == null) {
/*      */ 
/*      */       
/*  395 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  400 */       xmlWriter.writeCharacters(this.localEnd_date);
/*      */     } 
/*      */ 
/*      */     
/*  404 */     xmlWriter.writeEndElement();
/*      */     
/*  406 */     namespace = "";
/*  407 */     writeStartElement(null, namespace, "log_entry_type", xmlWriter);
/*      */     
/*  409 */     if (this.localLog_entry_type == Integer.MIN_VALUE) {
/*      */       
/*  411 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  414 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localLog_entry_type));
/*      */     } 
/*      */     
/*  417 */     xmlWriter.writeEndElement();
/*      */     
/*  419 */     namespace = "";
/*  420 */     writeStartElement(null, namespace, "trans_type", xmlWriter);
/*      */ 
/*      */     
/*  423 */     if (this.localTrans_type == null) {
/*      */ 
/*      */       
/*  426 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  431 */       xmlWriter.writeCharacters(this.localTrans_type);
/*      */     } 
/*      */ 
/*      */     
/*  435 */     xmlWriter.writeEndElement();
/*      */     
/*  437 */     namespace = "";
/*  438 */     writeStartElement(null, namespace, "function", xmlWriter);
/*      */ 
/*      */     
/*  441 */     if (this.localFunction == null) {
/*      */ 
/*      */       
/*  444 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  449 */       xmlWriter.writeCharacters(this.localFunction);
/*      */     } 
/*      */ 
/*      */     
/*  453 */     xmlWriter.writeEndElement();
/*      */     
/*  455 */     namespace = "";
/*  456 */     writeStartElement(null, namespace, "xml_params", xmlWriter);
/*      */ 
/*      */     
/*  459 */     if (this.localXml_params == null) {
/*      */ 
/*      */       
/*  462 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     
/*      */     }
/*      */     else {
/*      */       
/*  467 */       xmlWriter.writeCharacters(this.localXml_params);
/*      */     } 
/*      */ 
/*      */     
/*  471 */     xmlWriter.writeEndElement();
/*      */     
/*  473 */     namespace = "";
/*  474 */     writeStartElement(null, namespace, "log_flags", xmlWriter);
/*      */     
/*  476 */     if (this.localLog_flags == Integer.MIN_VALUE) {
/*      */       
/*  478 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  481 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localLog_flags));
/*      */     } 
/*      */     
/*  484 */     xmlWriter.writeEndElement();
/*      */     
/*  486 */     namespace = "";
/*  487 */     writeStartElement(null, namespace, "xml_flags", xmlWriter);
/*      */     
/*  489 */     if (this.localXml_flags == Integer.MIN_VALUE) {
/*      */       
/*  491 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*      */     } else {
/*      */       
/*  494 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localXml_flags));
/*      */     } 
/*      */     
/*  497 */     xmlWriter.writeEndElement();
/*      */     
/*  499 */     xmlWriter.writeEndElement();
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private static String generatePrefix(String namespace) {
/*  505 */     if (namespace.equals("urn:CSSoapService")) {
/*  506 */       return "ns1";
/*      */     }
/*  508 */     return BeanUtil.getUniquePrefix();
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  516 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/*  517 */     if (writerPrefix != null) {
/*  518 */       xmlWriter.writeStartElement(namespace, localPart);
/*      */     } else {
/*  520 */       if (namespace.length() == 0) {
/*  521 */         prefix = "";
/*  522 */       } else if (prefix == null) {
/*  523 */         prefix = generatePrefix(namespace);
/*      */       } 
/*      */       
/*  526 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/*  527 */       xmlWriter.writeNamespace(prefix, namespace);
/*  528 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  537 */     if (xmlWriter.getPrefix(namespace) == null) {
/*  538 */       xmlWriter.writeNamespace(prefix, namespace);
/*  539 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  541 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  549 */     if (namespace.equals("")) {
/*  550 */       xmlWriter.writeAttribute(attName, attValue);
/*      */     } else {
/*  552 */       registerPrefix(xmlWriter, namespace);
/*  553 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  564 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/*  565 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/*  566 */     if (attributePrefix == null) {
/*  567 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*      */     }
/*      */     
/*  570 */     if (attributePrefix.trim().length() > 0) {
/*  571 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*      */     } else {
/*  573 */       attributeValue = qname.getLocalPart();
/*      */     } 
/*      */     
/*  576 */     if (namespace.equals("")) {
/*  577 */       xmlWriter.writeAttribute(attName, attributeValue);
/*      */     } else {
/*  579 */       registerPrefix(xmlWriter, namespace);
/*  580 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  589 */     String namespaceURI = qname.getNamespaceURI();
/*  590 */     if (namespaceURI != null) {
/*  591 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/*  592 */       if (prefix == null) {
/*  593 */         prefix = generatePrefix(namespaceURI);
/*  594 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/*  595 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*      */       } 
/*      */       
/*  598 */       if (prefix.trim().length() > 0) {
/*  599 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*      */       } else {
/*      */         
/*  602 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */       } 
/*      */     } else {
/*      */       
/*  606 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  613 */     if (qnames != null) {
/*      */ 
/*      */       
/*  616 */       StringBuffer stringToWrite = new StringBuffer();
/*  617 */       String namespaceURI = null;
/*  618 */       String prefix = null;
/*      */       
/*  620 */       for (int i = 0; i < qnames.length; i++) {
/*  621 */         if (i > 0) {
/*  622 */           stringToWrite.append(" ");
/*      */         }
/*  624 */         namespaceURI = qnames[i].getNamespaceURI();
/*  625 */         if (namespaceURI != null) {
/*  626 */           prefix = xmlWriter.getPrefix(namespaceURI);
/*  627 */           if (prefix == null || prefix.length() == 0) {
/*  628 */             prefix = generatePrefix(namespaceURI);
/*  629 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/*  630 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*      */           } 
/*      */           
/*  633 */           if (prefix.trim().length() > 0) {
/*  634 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*      */           } else {
/*  636 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */           } 
/*      */         } else {
/*  639 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */         } 
/*      */       } 
/*  642 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/*  652 */     String prefix = xmlWriter.getPrefix(namespace);
/*  653 */     if (prefix == null) {
/*  654 */       prefix = generatePrefix(namespace);
/*  655 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*      */       while (true) {
/*  657 */         String uri = nsContext.getNamespaceURI(prefix);
/*  658 */         if (uri == null || uri.length() == 0) {
/*      */           break;
/*      */         }
/*  661 */         prefix = BeanUtil.getUniquePrefix();
/*      */       } 
/*  663 */       xmlWriter.writeNamespace(prefix, namespace);
/*  664 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  666 */     return prefix;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/*  680 */     ArrayList<QName> elementList = new ArrayList();
/*  681 */     ArrayList attribList = new ArrayList();
/*      */ 
/*      */     
/*  684 */     elementList.add(new QName("", 
/*  685 */           "access_key"));
/*      */     
/*  687 */     elementList.add(
/*  688 */         ConverterUtil.convertToString(this.localAccess_key));
/*      */     
/*  690 */     elementList.add(new QName("", 
/*  691 */           "start_date"));
/*      */     
/*  693 */     elementList.add((this.localStart_date == null) ? null : 
/*  694 */         ConverterUtil.convertToString(this.localStart_date));
/*      */     
/*  696 */     elementList.add(new QName("", 
/*  697 */           "end_date"));
/*      */     
/*  699 */     elementList.add((this.localEnd_date == null) ? null : 
/*  700 */         ConverterUtil.convertToString(this.localEnd_date));
/*      */     
/*  702 */     elementList.add(new QName("", 
/*  703 */           "log_entry_type"));
/*      */     
/*  705 */     elementList.add(
/*  706 */         ConverterUtil.convertToString(this.localLog_entry_type));
/*      */     
/*  708 */     elementList.add(new QName("", 
/*  709 */           "trans_type"));
/*      */     
/*  711 */     elementList.add((this.localTrans_type == null) ? null : 
/*  712 */         ConverterUtil.convertToString(this.localTrans_type));
/*      */     
/*  714 */     elementList.add(new QName("", 
/*  715 */           "function"));
/*      */     
/*  717 */     elementList.add((this.localFunction == null) ? null : 
/*  718 */         ConverterUtil.convertToString(this.localFunction));
/*      */     
/*  720 */     elementList.add(new QName("", 
/*  721 */           "xml_params"));
/*      */     
/*  723 */     elementList.add((this.localXml_params == null) ? null : 
/*  724 */         ConverterUtil.convertToString(this.localXml_params));
/*      */     
/*  726 */     elementList.add(new QName("", 
/*  727 */           "log_flags"));
/*      */     
/*  729 */     elementList.add(
/*  730 */         ConverterUtil.convertToString(this.localLog_flags));
/*      */     
/*  732 */     elementList.add(new QName("", 
/*  733 */           "xml_flags"));
/*      */     
/*  735 */     elementList.add(
/*  736 */         ConverterUtil.convertToString(this.localXml_flags));
/*      */ 
/*      */     
/*  739 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public static class Factory
/*      */   {
/*      */     public static QueryLogJS parse(XMLStreamReader reader) throws Exception {
/*  763 */       QueryLogJS object = 
/*  764 */         new QueryLogJS();
/*      */ 
/*      */       
/*  767 */       String nillableValue = null;
/*  768 */       String prefix = "";
/*  769 */       String namespaceuri = "";
/*      */       
/*      */       try {
/*  772 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/*  773 */           reader.next();
/*      */         }
/*      */         
/*  776 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/*  777 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/*  778 */               "type");
/*  779 */           if (fullTypeName != null) {
/*  780 */             String nsPrefix = null;
/*  781 */             if (fullTypeName.indexOf(":") > -1) {
/*  782 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*      */             }
/*  784 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*      */             
/*  786 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*      */             
/*  788 */             if (!"QueryLogJS".equals(type)) {
/*      */               
/*  790 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/*  791 */               return (QueryLogJS)ExtensionMapper.getTypeObject(
/*  792 */                   nsUri, type, reader);
/*      */             } 
/*      */           } 
/*      */         } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  806 */         Vector handledAttributes = new Vector();
/*      */ 
/*      */ 
/*      */ 
/*      */         
/*  811 */         reader.next();
/*      */ 
/*      */         
/*  814 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  816 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*      */           
/*  818 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  819 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  822 */             String content = reader.getElementText();
/*      */             
/*  824 */             object.setAccess_key(
/*  825 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  830 */             object.setAccess_key(-2147483648);
/*      */             
/*  832 */             reader.getElementText();
/*      */           } 
/*      */           
/*  835 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  841 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  845 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  847 */         if (reader.isStartElement() && (new QName("", "start_date")).equals(reader.getName())) {
/*      */           
/*  849 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  850 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  853 */             String content = reader.getElementText();
/*      */             
/*  855 */             object.setStart_date(
/*  856 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  861 */             reader.getElementText();
/*      */           } 
/*      */           
/*  864 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  870 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  874 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  876 */         if (reader.isStartElement() && (new QName("", "end_date")).equals(reader.getName())) {
/*      */           
/*  878 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  879 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  882 */             String content = reader.getElementText();
/*      */             
/*  884 */             object.setEnd_date(
/*  885 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  890 */             reader.getElementText();
/*      */           } 
/*      */           
/*  893 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  899 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  903 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  905 */         if (reader.isStartElement() && (new QName("", "log_entry_type")).equals(reader.getName())) {
/*      */           
/*  907 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  908 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  911 */             String content = reader.getElementText();
/*      */             
/*  913 */             object.setLog_entry_type(
/*  914 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  919 */             object.setLog_entry_type(-2147483648);
/*      */             
/*  921 */             reader.getElementText();
/*      */           } 
/*      */           
/*  924 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  930 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  934 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  936 */         if (reader.isStartElement() && (new QName("", "trans_type")).equals(reader.getName())) {
/*      */           
/*  938 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  939 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  942 */             String content = reader.getElementText();
/*      */             
/*  944 */             object.setTrans_type(
/*  945 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  950 */             reader.getElementText();
/*      */           } 
/*      */           
/*  953 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  959 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  963 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  965 */         if (reader.isStartElement() && (new QName("", "function")).equals(reader.getName())) {
/*      */           
/*  967 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  968 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/*  971 */             String content = reader.getElementText();
/*      */             
/*  973 */             object.setFunction(
/*  974 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/*  979 */             reader.getElementText();
/*      */           } 
/*      */           
/*  982 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/*  988 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/*  992 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/*  994 */         if (reader.isStartElement() && (new QName("", "xml_params")).equals(reader.getName())) {
/*      */           
/*  996 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/*  997 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/* 1000 */             String content = reader.getElementText();
/*      */             
/* 1002 */             object.setXml_params(
/* 1003 */                 ConverterUtil.convertToString(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/* 1008 */             reader.getElementText();
/*      */           } 
/*      */           
/* 1011 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1017 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1021 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1023 */         if (reader.isStartElement() && (new QName("", "log_flags")).equals(reader.getName())) {
/*      */           
/* 1025 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1026 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/* 1029 */             String content = reader.getElementText();
/*      */             
/* 1031 */             object.setLog_flags(
/* 1032 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/* 1037 */             object.setLog_flags(-2147483648);
/*      */             
/* 1039 */             reader.getElementText();
/*      */           } 
/*      */           
/* 1042 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1048 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1052 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1054 */         if (reader.isStartElement() && (new QName("", "xml_flags")).equals(reader.getName())) {
/*      */           
/* 1056 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1057 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*      */ 
/*      */             
/* 1060 */             String content = reader.getElementText();
/*      */             
/* 1062 */             object.setXml_flags(
/* 1063 */                 ConverterUtil.convertToInt(content));
/*      */           
/*      */           }
/*      */           else {
/*      */             
/* 1068 */             object.setXml_flags(-2147483648);
/*      */             
/* 1070 */             reader.getElementText();
/*      */           } 
/*      */           
/* 1073 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1079 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */         
/* 1082 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1083 */           reader.next();
/*      */         }
/* 1085 */         if (reader.isStartElement())
/*      */         {
/* 1087 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         
/*      */         }
/*      */       
/*      */       }
/* 1092 */       catch (XMLStreamException e) {
/* 1093 */         throw new Exception(e);
/*      */       } 
/*      */       
/* 1096 */       return object;
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\QueryLogJS.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */