/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetPODDataResponse implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetPODDataResponse", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localPod_xml;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getPod_xml() {
/*  41 */     return this.localPod_xml;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPod_xml(String param) {
/*  52 */     this.localPod_xml = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  72 */     ADBDataSource aDBDataSource = 
/*  73 */       new ADBDataSource(this, MY_QNAME);
/*  74 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  81 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  92 */     String prefix = null;
/*  93 */     String namespace = null;
/*     */ 
/*     */     
/*  96 */     prefix = parentQName.getPrefix();
/*  97 */     namespace = parentQName.getNamespaceURI();
/*  98 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 100 */     if (serializeType) {
/*     */ 
/*     */       
/* 103 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 104 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 105 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 106 */             String.valueOf(namespacePrefix) + ":GetPODDataResponse", 
/* 107 */             xmlWriter);
/*     */       } else {
/* 109 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 110 */             "GetPODDataResponse", 
/* 111 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 117 */     namespace = "";
/* 118 */     writeStartElement(null, namespace, "pod_xml", xmlWriter);
/*     */ 
/*     */     
/* 121 */     if (this.localPod_xml == null) {
/*     */ 
/*     */       
/* 124 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 129 */       xmlWriter.writeCharacters(this.localPod_xml);
/*     */     } 
/*     */ 
/*     */     
/* 133 */     xmlWriter.writeEndElement();
/*     */     
/* 135 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 141 */     if (namespace.equals("urn:CSSoapService")) {
/* 142 */       return "ns1";
/*     */     }
/* 144 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 152 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 153 */     if (writerPrefix != null) {
/* 154 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 156 */       if (namespace.length() == 0) {
/* 157 */         prefix = "";
/* 158 */       } else if (prefix == null) {
/* 159 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 162 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 163 */       xmlWriter.writeNamespace(prefix, namespace);
/* 164 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 173 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 174 */       xmlWriter.writeNamespace(prefix, namespace);
/* 175 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 177 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 185 */     if (namespace.equals("")) {
/* 186 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 188 */       registerPrefix(xmlWriter, namespace);
/* 189 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 200 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 201 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 202 */     if (attributePrefix == null) {
/* 203 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 206 */     if (attributePrefix.trim().length() > 0) {
/* 207 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 209 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 212 */     if (namespace.equals("")) {
/* 213 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 215 */       registerPrefix(xmlWriter, namespace);
/* 216 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 225 */     String namespaceURI = qname.getNamespaceURI();
/* 226 */     if (namespaceURI != null) {
/* 227 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 228 */       if (prefix == null) {
/* 229 */         prefix = generatePrefix(namespaceURI);
/* 230 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 231 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 234 */       if (prefix.trim().length() > 0) {
/* 235 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 238 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 242 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 249 */     if (qnames != null) {
/*     */ 
/*     */       
/* 252 */       StringBuffer stringToWrite = new StringBuffer();
/* 253 */       String namespaceURI = null;
/* 254 */       String prefix = null;
/*     */       
/* 256 */       for (int i = 0; i < qnames.length; i++) {
/* 257 */         if (i > 0) {
/* 258 */           stringToWrite.append(" ");
/*     */         }
/* 260 */         namespaceURI = qnames[i].getNamespaceURI();
/* 261 */         if (namespaceURI != null) {
/* 262 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 263 */           if (prefix == null || prefix.length() == 0) {
/* 264 */             prefix = generatePrefix(namespaceURI);
/* 265 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 266 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 269 */           if (prefix.trim().length() > 0) {
/* 270 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 272 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 275 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 278 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 288 */     String prefix = xmlWriter.getPrefix(namespace);
/* 289 */     if (prefix == null) {
/* 290 */       prefix = generatePrefix(namespace);
/* 291 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 293 */         String uri = nsContext.getNamespaceURI(prefix);
/* 294 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 297 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 299 */       xmlWriter.writeNamespace(prefix, namespace);
/* 300 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 302 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 316 */     ArrayList<QName> elementList = new ArrayList();
/* 317 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 320 */     elementList.add(new QName("", 
/* 321 */           "pod_xml"));
/*     */     
/* 323 */     elementList.add((this.localPod_xml == null) ? null : 
/* 324 */         ConverterUtil.convertToString(this.localPod_xml));
/*     */ 
/*     */     
/* 327 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetPODDataResponse parse(XMLStreamReader reader) throws Exception {
/* 351 */       GetPODDataResponse object = 
/* 352 */         new GetPODDataResponse();
/*     */ 
/*     */       
/* 355 */       String nillableValue = null;
/* 356 */       String prefix = "";
/* 357 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 360 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 361 */           reader.next();
/*     */         }
/*     */         
/* 364 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 365 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 366 */               "type");
/* 367 */           if (fullTypeName != null) {
/* 368 */             String nsPrefix = null;
/* 369 */             if (fullTypeName.indexOf(":") > -1) {
/* 370 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 372 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 374 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 376 */             if (!"GetPODDataResponse".equals(type)) {
/*     */               
/* 378 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 379 */               return (GetPODDataResponse)ExtensionMapper.getTypeObject(
/* 380 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 394 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 399 */         reader.next();
/*     */ 
/*     */         
/* 402 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 404 */         if (reader.isStartElement() && (new QName("", "pod_xml")).equals(reader.getName())) {
/*     */           
/* 406 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 407 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 410 */             String content = reader.getElementText();
/*     */             
/* 412 */             object.setPod_xml(
/* 413 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 418 */             reader.getElementText();
/*     */           } 
/*     */           
/* 421 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 427 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 430 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 431 */           reader.next();
/*     */         }
/* 433 */         if (reader.isStartElement())
/*     */         {
/* 435 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 440 */       catch (XMLStreamException e) {
/* 441 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 444 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetPODDataResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */