/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SetShipmentStatus implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SetShipmentStatus", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localStatus;
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localShipment_uuid;
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getStatus() {
/*  71 */     return this.localStatus;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStatus(String param) {
/*  82 */     this.localStatus = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getShipment_uuid() {
/* 101 */     return this.localShipment_uuid;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setShipment_uuid(String param) {
/* 112 */     this.localShipment_uuid = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 132 */     ADBDataSource aDBDataSource = 
/* 133 */       new ADBDataSource(this, MY_QNAME);
/* 134 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 141 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 152 */     String prefix = null;
/* 153 */     String namespace = null;
/*     */ 
/*     */     
/* 156 */     prefix = parentQName.getPrefix();
/* 157 */     namespace = parentQName.getNamespaceURI();
/* 158 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 160 */     if (serializeType) {
/*     */ 
/*     */       
/* 163 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 164 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 165 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 166 */             String.valueOf(namespacePrefix) + ":SetShipmentStatus", 
/* 167 */             xmlWriter);
/*     */       } else {
/* 169 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 170 */             "SetShipmentStatus", 
/* 171 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 177 */     namespace = "";
/* 178 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 180 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 182 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 185 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 188 */     xmlWriter.writeEndElement();
/*     */     
/* 190 */     namespace = "";
/* 191 */     writeStartElement(null, namespace, "status", xmlWriter);
/*     */ 
/*     */     
/* 194 */     if (this.localStatus == null) {
/*     */ 
/*     */       
/* 197 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 202 */       xmlWriter.writeCharacters(this.localStatus);
/*     */     } 
/*     */ 
/*     */     
/* 206 */     xmlWriter.writeEndElement();
/*     */     
/* 208 */     namespace = "";
/* 209 */     writeStartElement(null, namespace, "shipment_uuid", xmlWriter);
/*     */ 
/*     */     
/* 212 */     if (this.localShipment_uuid == null) {
/*     */ 
/*     */       
/* 215 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 220 */       xmlWriter.writeCharacters(this.localShipment_uuid);
/*     */     } 
/*     */ 
/*     */     
/* 224 */     xmlWriter.writeEndElement();
/*     */     
/* 226 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 232 */     if (namespace.equals("urn:CSSoapService")) {
/* 233 */       return "ns1";
/*     */     }
/* 235 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 243 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 244 */     if (writerPrefix != null) {
/* 245 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 247 */       if (namespace.length() == 0) {
/* 248 */         prefix = "";
/* 249 */       } else if (prefix == null) {
/* 250 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 253 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 254 */       xmlWriter.writeNamespace(prefix, namespace);
/* 255 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 264 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 265 */       xmlWriter.writeNamespace(prefix, namespace);
/* 266 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 268 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 276 */     if (namespace.equals("")) {
/* 277 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 279 */       registerPrefix(xmlWriter, namespace);
/* 280 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 291 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 292 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 293 */     if (attributePrefix == null) {
/* 294 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 297 */     if (attributePrefix.trim().length() > 0) {
/* 298 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 300 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 303 */     if (namespace.equals("")) {
/* 304 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 306 */       registerPrefix(xmlWriter, namespace);
/* 307 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 316 */     String namespaceURI = qname.getNamespaceURI();
/* 317 */     if (namespaceURI != null) {
/* 318 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 319 */       if (prefix == null) {
/* 320 */         prefix = generatePrefix(namespaceURI);
/* 321 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 322 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 325 */       if (prefix.trim().length() > 0) {
/* 326 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 329 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 333 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 340 */     if (qnames != null) {
/*     */ 
/*     */       
/* 343 */       StringBuffer stringToWrite = new StringBuffer();
/* 344 */       String namespaceURI = null;
/* 345 */       String prefix = null;
/*     */       
/* 347 */       for (int i = 0; i < qnames.length; i++) {
/* 348 */         if (i > 0) {
/* 349 */           stringToWrite.append(" ");
/*     */         }
/* 351 */         namespaceURI = qnames[i].getNamespaceURI();
/* 352 */         if (namespaceURI != null) {
/* 353 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 354 */           if (prefix == null || prefix.length() == 0) {
/* 355 */             prefix = generatePrefix(namespaceURI);
/* 356 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 357 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 360 */           if (prefix.trim().length() > 0) {
/* 361 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 363 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 366 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 369 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 379 */     String prefix = xmlWriter.getPrefix(namespace);
/* 380 */     if (prefix == null) {
/* 381 */       prefix = generatePrefix(namespace);
/* 382 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 384 */         String uri = nsContext.getNamespaceURI(prefix);
/* 385 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 388 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 390 */       xmlWriter.writeNamespace(prefix, namespace);
/* 391 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 393 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 407 */     ArrayList<QName> elementList = new ArrayList();
/* 408 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 411 */     elementList.add(new QName("", 
/* 412 */           "access_key"));
/*     */     
/* 414 */     elementList.add(
/* 415 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 417 */     elementList.add(new QName("", 
/* 418 */           "status"));
/*     */     
/* 420 */     elementList.add((this.localStatus == null) ? null : 
/* 421 */         ConverterUtil.convertToString(this.localStatus));
/*     */     
/* 423 */     elementList.add(new QName("", 
/* 424 */           "shipment_uuid"));
/*     */     
/* 426 */     elementList.add((this.localShipment_uuid == null) ? null : 
/* 427 */         ConverterUtil.convertToString(this.localShipment_uuid));
/*     */ 
/*     */     
/* 430 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SetShipmentStatus parse(XMLStreamReader reader) throws Exception {
/* 454 */       SetShipmentStatus object = 
/* 455 */         new SetShipmentStatus();
/*     */ 
/*     */       
/* 458 */       String nillableValue = null;
/* 459 */       String prefix = "";
/* 460 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 463 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 464 */           reader.next();
/*     */         }
/*     */         
/* 467 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 468 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 469 */               "type");
/* 470 */           if (fullTypeName != null) {
/* 471 */             String nsPrefix = null;
/* 472 */             if (fullTypeName.indexOf(":") > -1) {
/* 473 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 475 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 477 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 479 */             if (!"SetShipmentStatus".equals(type)) {
/*     */               
/* 481 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 482 */               return (SetShipmentStatus)ExtensionMapper.getTypeObject(
/* 483 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 497 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 502 */         reader.next();
/*     */ 
/*     */         
/* 505 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 507 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 509 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 510 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 513 */             String content = reader.getElementText();
/*     */             
/* 515 */             object.setAccess_key(
/* 516 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 521 */             object.setAccess_key(-2147483648);
/*     */             
/* 523 */             reader.getElementText();
/*     */           } 
/*     */           
/* 526 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 532 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 536 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 538 */         if (reader.isStartElement() && (new QName("", "status")).equals(reader.getName())) {
/*     */           
/* 540 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 541 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 544 */             String content = reader.getElementText();
/*     */             
/* 546 */             object.setStatus(
/* 547 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 552 */             reader.getElementText();
/*     */           } 
/*     */           
/* 555 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 561 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 565 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 567 */         if (reader.isStartElement() && (new QName("", "shipment_uuid")).equals(reader.getName())) {
/*     */           
/* 569 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 570 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 573 */             String content = reader.getElementText();
/*     */             
/* 575 */             object.setShipment_uuid(
/* 576 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 581 */             reader.getElementText();
/*     */           } 
/*     */           
/* 584 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 590 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 593 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 594 */           reader.next();
/*     */         }
/* 596 */         if (reader.isStartElement())
/*     */         {
/* 598 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 603 */       catch (XMLStreamException e) {
/* 604 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 607 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SetShipmentStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */