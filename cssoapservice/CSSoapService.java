package cssoapservice;

import java.rmi.RemoteException;

public interface CSSoapService {
  ExistsTransactionResponse existsTransaction(ExistsTransaction paramExistsTransaction) throws RemoteException;
  
  GetTrackingTransactionResponse getTrackingTransaction(GetTrackingTransaction paramGetTrackingTransaction) throws RemoteException;
  
  GetFirstTransbyDateResponse getFirstTransbyDate(GetFirstTransbyDate paramGetFirstTransbyDate) throws RemoteException;
  
  SetRateResponse setRate(SetRate paramSetRate) throws RemoteException;
  
  QueryLogResponse queryLog(QueryLog paramQueryLog) throws RemoteException;
  
  GetMagayaDocumentResponse getMagayaDocument(GetMagayaDocument paramGetMagayaDocument) throws RemoteException;
  
  GetWebDocumentResponse getWebDocument(GetWebDocument paramGetWebDocument) throws RemoteException;
  
  GetFirstTransbyDateJSResponse getFirstTransbyDateJS(GetFirstTransbyDateJS paramGetFirstTransbyDateJS) throws RemoteException;
  
  GetPackageTypesResponse getPackageTypes(GetPackageTypes paramGetPackageTypes) throws RemoteException;
  
  GetSecureTrackingTransactionResponse getSecureTrackingTransaction(GetSecureTrackingTransaction paramGetSecureTrackingTransaction) throws RemoteException;
  
  GetChargeDefinitionsResponse getChargeDefinitions(GetChargeDefinitions paramGetChargeDefinitions) throws RemoteException;
  
  GetAttachmentResponse getAttachment(GetAttachment paramGetAttachment) throws RemoteException;
  
  InvitationFromPeerResponse invitationFromPeer(InvitationFromPeer paramInvitationFromPeer) throws RemoteException;
  
  GetInventoryItemsByItemDefinitionResponse getInventoryItemsByItemDefinition(GetInventoryItemsByItemDefinition paramGetInventoryItemsByItemDefinition) throws RemoteException;
  
  SetTransactionEventsResponse setTransactionEvents(SetTransactionEvents paramSetTransactionEvents) throws RemoteException;
  
  GetItemFromVINResponse getItemFromVIN(GetItemFromVIN paramGetItemFromVIN) throws RemoteException;
  
  BookingRequestResponse bookingRequest(BookingRequest paramBookingRequest) throws RemoteException;
  
  GetWorkingPortsResponse getWorkingPorts(GetWorkingPorts paramGetWorkingPorts) throws RemoteException;
  
  EndSessionResponse endSession(EndSession paramEndSession) throws RemoteException;
  
  UpdateOrderResponse updateOrder(UpdateOrder paramUpdateOrder) throws RemoteException;
  
  StartSessionResponse startSession(StartSession paramStartSession) throws RemoteException;
  
  SetShipmentStatusResponse setShipmentStatus(SetShipmentStatus paramSetShipmentStatus) throws RemoteException;
  
  GetNextTransbyDateResponse getNextTransbyDate(GetNextTransbyDate paramGetNextTransbyDate) throws RemoteException;
  
  GetActiveCurrenciesResponse getActiveCurrencies(GetActiveCurrencies paramGetActiveCurrencies) throws RemoteException;
  
  StartTrackingResponse startTracking(StartTracking paramStartTracking) throws RemoteException;
  
  GetStandardRatesResponse getStandardRates(GetStandardRates paramGetStandardRates) throws RemoteException;
  
  GetTransactionStatusResponse getTransactionStatus(GetTransactionStatus paramGetTransactionStatus) throws RemoteException;
  
  SubmitSalesOrderResponse submitSalesOrder(SubmitSalesOrder paramSubmitSalesOrder) throws RemoteException;
  
  RenameTransactionResponse renameTransaction(RenameTransaction paramRenameTransaction) throws RemoteException;
  
  QueryLogJSResponse queryLogJS(QueryLogJS paramQueryLogJS) throws RemoteException;
  
  GetEntitiesOfTypeResponse getEntitiesOfType(GetEntitiesOfType paramGetEntitiesOfType) throws RemoteException;
  
  GetItemDefinitionsByCustomerResponse getItemDefinitionsByCustomer(GetItemDefinitionsByCustomer paramGetItemDefinitionsByCustomer) throws RemoteException;
  
  StartTracking2Response startTracking2(StartTracking2 paramStartTracking2) throws RemoteException;
  
  DeleteTransactionResponse deleteTransaction(DeleteTransaction paramDeleteTransaction) throws RemoteException;
  
  GetTransactionResponse getTransaction(GetTransaction paramGetTransaction) throws RemoteException;
  
  GetEntityTransactionsResponse getEntityTransactions(GetEntityTransactions paramGetEntityTransactions) throws RemoteException;
  
  SubmitShipmentResponse submitShipment(SubmitShipment paramSubmitShipment) throws RemoteException;
  
  GetEntityContactsResponse getEntityContacts(GetEntityContacts paramGetEntityContacts) throws RemoteException;
  
  CancelBookingResponse cancelBooking(CancelBooking paramCancelBooking) throws RemoteException;
  
  SetTransactionResponse setTransaction(SetTransaction paramSetTransaction) throws RemoteException;
  
  SetEntityResponse setEntity(SetEntity paramSetEntity) throws RemoteException;
  
  GetClientChargeDefinitionsResponse getClientChargeDefinitions(GetClientChargeDefinitions paramGetClientChargeDefinitions) throws RemoteException;
  
  GetAccountingTransactionsResponse getAccountingTransactions(GetAccountingTransactions paramGetAccountingTransactions) throws RemoteException;
  
  GetTripScheduleResponse getTripSchedule(GetTripSchedule paramGetTripSchedule) throws RemoteException;
  
  GetEventDefinitionsResponse getEventDefinitions(GetEventDefinitions paramGetEventDefinitions) throws RemoteException;
  
  GetCarrierRatesResponse getCarrierRates(GetCarrierRates paramGetCarrierRates) throws RemoteException;
  
  GetPODDataResponse getPODData(GetPODData paramGetPODData) throws RemoteException;
  
  GetCustomFieldDefinitionsResponse getCustomFieldDefinitions(GetCustomFieldDefinitions paramGetCustomFieldDefinitions) throws RemoteException;
  
  SubmitCargoReleaseResponse submitCargoRelease(SubmitCargoRelease paramSubmitCargoRelease) throws RemoteException;
  
  GetClientRatesResponse getClientRates(GetClientRates paramGetClientRates) throws RemoteException;
  
  GetTransRangeByDateJSResponse getTransRangeByDateJS(GetTransRangeByDateJS paramGetTransRangeByDateJS) throws RemoteException;
  
  GetAccountDefinitionsResponse getAccountDefinitions(GetAccountDefinitions paramGetAccountDefinitions) throws RemoteException;
  
  SetApprovalStatusResponse setApprovalStatus(SetApprovalStatus paramSetApprovalStatus) throws RemoteException;
  
  GetRelatedTransactionsResponse getRelatedTransactions(GetRelatedTransactions paramGetRelatedTransactions) throws RemoteException;
  
  SetCustomFieldValueResponse setCustomFieldValue(SetCustomFieldValue paramSetCustomFieldValue) throws RemoteException;
  
  GetEntitiesResponse getEntities(GetEntities paramGetEntities) throws RemoteException;
  
  AnswerInvitationResponse answerInvitation(AnswerInvitation paramAnswerInvitation) throws RemoteException;
  
  TestConnectionResponse testConnection(TestConnection paramTestConnection) throws RemoteException;
  
  GetTransRangeByDateResponse getTransRangeByDate(GetTransRangeByDate paramGetTransRangeByDate) throws RemoteException;
  
  GetTransactionsByBillingClientResponse getTransactionsByBillingClient(GetTransactionsByBillingClient paramGetTransactionsByBillingClient) throws RemoteException;
  
  SetTrackingUserResponse setTrackingUser(SetTrackingUser paramSetTrackingUser) throws RemoteException;
  
  AnswerInvitation2Response answerInvitation2(AnswerInvitation2 paramAnswerInvitation2) throws RemoteException;
  
  GetDocumentResponse getDocument(GetDocument paramGetDocument) throws RemoteException;
}


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\CSSoapService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */