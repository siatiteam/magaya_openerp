/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class AnswerInvitation2Response implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "AnswerInvitation2Response", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected int local_return;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected Contact_info2 localPInfo;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int get_return() {
/*  41 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(int param) {
/*  52 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public Contact_info2 getPInfo() {
/*  71 */     return this.localPInfo;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPInfo(Contact_info2 param) {
/*  82 */     this.localPInfo = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 102 */     ADBDataSource aDBDataSource = 
/* 103 */       new ADBDataSource(this, MY_QNAME);
/* 104 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 111 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 122 */     String prefix = null;
/* 123 */     String namespace = null;
/*     */ 
/*     */     
/* 126 */     prefix = parentQName.getPrefix();
/* 127 */     namespace = parentQName.getNamespaceURI();
/* 128 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 130 */     if (serializeType) {
/*     */ 
/*     */       
/* 133 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 134 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 135 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 136 */             String.valueOf(namespacePrefix) + ":AnswerInvitation2Response", 
/* 137 */             xmlWriter);
/*     */       } else {
/* 139 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 140 */             "AnswerInvitation2Response", 
/* 141 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 147 */     namespace = "";
/* 148 */     writeStartElement(null, namespace, "return", xmlWriter);
/*     */     
/* 150 */     if (this.local_return == Integer.MIN_VALUE) {
/*     */       
/* 152 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 155 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.local_return));
/*     */     } 
/*     */     
/* 158 */     xmlWriter.writeEndElement();
/*     */     
/* 160 */     if (this.localPInfo == null) {
/*     */       
/* 162 */       writeStartElement(null, "", "pInfo", xmlWriter);
/*     */ 
/*     */       
/* 165 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/* 166 */       xmlWriter.writeEndElement();
/*     */     } else {
/* 168 */       this.localPInfo.serialize(new QName("", "pInfo"), 
/* 169 */           xmlWriter);
/*     */     } 
/*     */     
/* 172 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 178 */     if (namespace.equals("urn:CSSoapService")) {
/* 179 */       return "ns1";
/*     */     }
/* 181 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 189 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 190 */     if (writerPrefix != null) {
/* 191 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 193 */       if (namespace.length() == 0) {
/* 194 */         prefix = "";
/* 195 */       } else if (prefix == null) {
/* 196 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 199 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 200 */       xmlWriter.writeNamespace(prefix, namespace);
/* 201 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 210 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 211 */       xmlWriter.writeNamespace(prefix, namespace);
/* 212 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 214 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 222 */     if (namespace.equals("")) {
/* 223 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 225 */       registerPrefix(xmlWriter, namespace);
/* 226 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 237 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 238 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 239 */     if (attributePrefix == null) {
/* 240 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 243 */     if (attributePrefix.trim().length() > 0) {
/* 244 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 246 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 249 */     if (namespace.equals("")) {
/* 250 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 252 */       registerPrefix(xmlWriter, namespace);
/* 253 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 262 */     String namespaceURI = qname.getNamespaceURI();
/* 263 */     if (namespaceURI != null) {
/* 264 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 265 */       if (prefix == null) {
/* 266 */         prefix = generatePrefix(namespaceURI);
/* 267 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 268 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 271 */       if (prefix.trim().length() > 0) {
/* 272 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 275 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 279 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 286 */     if (qnames != null) {
/*     */ 
/*     */       
/* 289 */       StringBuffer stringToWrite = new StringBuffer();
/* 290 */       String namespaceURI = null;
/* 291 */       String prefix = null;
/*     */       
/* 293 */       for (int i = 0; i < qnames.length; i++) {
/* 294 */         if (i > 0) {
/* 295 */           stringToWrite.append(" ");
/*     */         }
/* 297 */         namespaceURI = qnames[i].getNamespaceURI();
/* 298 */         if (namespaceURI != null) {
/* 299 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 300 */           if (prefix == null || prefix.length() == 0) {
/* 301 */             prefix = generatePrefix(namespaceURI);
/* 302 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 303 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 306 */           if (prefix.trim().length() > 0) {
/* 307 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 309 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 312 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 315 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 325 */     String prefix = xmlWriter.getPrefix(namespace);
/* 326 */     if (prefix == null) {
/* 327 */       prefix = generatePrefix(namespace);
/* 328 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 330 */         String uri = nsContext.getNamespaceURI(prefix);
/* 331 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 334 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 336 */       xmlWriter.writeNamespace(prefix, namespace);
/* 337 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 339 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 353 */     ArrayList<QName> elementList = new ArrayList();
/* 354 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 357 */     elementList.add(new QName("", 
/* 358 */           "return"));
/*     */     
/* 360 */     elementList.add(
/* 361 */         ConverterUtil.convertToString(this.local_return));
/*     */     
/* 363 */     elementList.add(new QName("", 
/* 364 */           "pInfo"));
/*     */ 
/*     */     
/* 367 */     elementList.add((this.localPInfo == null) ? null : 
/* 368 */         this.localPInfo);
/*     */ 
/*     */     
/* 371 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static AnswerInvitation2Response parse(XMLStreamReader reader) throws Exception {
/* 395 */       AnswerInvitation2Response object = 
/* 396 */         new AnswerInvitation2Response();
/*     */ 
/*     */       
/* 399 */       String nillableValue = null;
/* 400 */       String prefix = "";
/* 401 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 404 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 405 */           reader.next();
/*     */         }
/*     */         
/* 408 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 409 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 410 */               "type");
/* 411 */           if (fullTypeName != null) {
/* 412 */             String nsPrefix = null;
/* 413 */             if (fullTypeName.indexOf(":") > -1) {
/* 414 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 416 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 418 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 420 */             if (!"AnswerInvitation2Response".equals(type)) {
/*     */               
/* 422 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 423 */               return (AnswerInvitation2Response)ExtensionMapper.getTypeObject(
/* 424 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 438 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 443 */         reader.next();
/*     */ 
/*     */         
/* 446 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 448 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
/*     */           
/* 450 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 451 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 454 */             String content = reader.getElementText();
/*     */             
/* 456 */             object.set_return(
/* 457 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 462 */             object.set_return(-2147483648);
/*     */             
/* 464 */             reader.getElementText();
/*     */           } 
/*     */           
/* 467 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 473 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 477 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 479 */         if (reader.isStartElement() && (new QName("", "pInfo")).equals(reader.getName())) {
/*     */           
/* 481 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 482 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 483 */             object.setPInfo(null);
/* 484 */             reader.next();
/*     */             
/* 486 */             reader.next();
/*     */           }
/*     */           else {
/*     */             
/* 490 */             object.setPInfo(Contact_info2.Factory.parse(reader));
/*     */             
/* 492 */             reader.next();
/*     */           }
/*     */         
/*     */         }
/*     */         else {
/*     */           
/* 498 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 501 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 502 */           reader.next();
/*     */         }
/* 504 */         if (reader.isStartElement())
/*     */         {
/* 506 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 511 */       catch (XMLStreamException e) {
/* 512 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 515 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\AnswerInvitation2Response.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */