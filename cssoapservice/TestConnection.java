/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class TestConnection implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "TestConnection", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localIn_cookie;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localId;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getIn_cookie() {
/*  41 */     return this.localIn_cookie;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setIn_cookie(int param) {
/*  52 */     this.localIn_cookie = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getId() {
/*  71 */     return this.localId;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setId(int param) {
/*  82 */     this.localId = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 102 */     ADBDataSource aDBDataSource = 
/* 103 */       new ADBDataSource(this, MY_QNAME);
/* 104 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 111 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 122 */     String prefix = null;
/* 123 */     String namespace = null;
/*     */ 
/*     */     
/* 126 */     prefix = parentQName.getPrefix();
/* 127 */     namespace = parentQName.getNamespaceURI();
/* 128 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 130 */     if (serializeType) {
/*     */ 
/*     */       
/* 133 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 134 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 135 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 136 */             String.valueOf(namespacePrefix) + ":TestConnection", 
/* 137 */             xmlWriter);
/*     */       } else {
/* 139 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 140 */             "TestConnection", 
/* 141 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 147 */     namespace = "";
/* 148 */     writeStartElement(null, namespace, "in_cookie", xmlWriter);
/*     */     
/* 150 */     if (this.localIn_cookie == Integer.MIN_VALUE) {
/*     */       
/* 152 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 155 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localIn_cookie));
/*     */     } 
/*     */     
/* 158 */     xmlWriter.writeEndElement();
/*     */     
/* 160 */     namespace = "";
/* 161 */     writeStartElement(null, namespace, "id", xmlWriter);
/*     */     
/* 163 */     if (this.localId == Integer.MIN_VALUE) {
/*     */       
/* 165 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 168 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localId));
/*     */     } 
/*     */     
/* 171 */     xmlWriter.writeEndElement();
/*     */     
/* 173 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 179 */     if (namespace.equals("urn:CSSoapService")) {
/* 180 */       return "ns1";
/*     */     }
/* 182 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 190 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 191 */     if (writerPrefix != null) {
/* 192 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 194 */       if (namespace.length() == 0) {
/* 195 */         prefix = "";
/* 196 */       } else if (prefix == null) {
/* 197 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 200 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 201 */       xmlWriter.writeNamespace(prefix, namespace);
/* 202 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 211 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 212 */       xmlWriter.writeNamespace(prefix, namespace);
/* 213 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 215 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 223 */     if (namespace.equals("")) {
/* 224 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 226 */       registerPrefix(xmlWriter, namespace);
/* 227 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 238 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 239 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 240 */     if (attributePrefix == null) {
/* 241 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 244 */     if (attributePrefix.trim().length() > 0) {
/* 245 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 247 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 250 */     if (namespace.equals("")) {
/* 251 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 253 */       registerPrefix(xmlWriter, namespace);
/* 254 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 263 */     String namespaceURI = qname.getNamespaceURI();
/* 264 */     if (namespaceURI != null) {
/* 265 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 266 */       if (prefix == null) {
/* 267 */         prefix = generatePrefix(namespaceURI);
/* 268 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 269 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 272 */       if (prefix.trim().length() > 0) {
/* 273 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 276 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 280 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 287 */     if (qnames != null) {
/*     */ 
/*     */       
/* 290 */       StringBuffer stringToWrite = new StringBuffer();
/* 291 */       String namespaceURI = null;
/* 292 */       String prefix = null;
/*     */       
/* 294 */       for (int i = 0; i < qnames.length; i++) {
/* 295 */         if (i > 0) {
/* 296 */           stringToWrite.append(" ");
/*     */         }
/* 298 */         namespaceURI = qnames[i].getNamespaceURI();
/* 299 */         if (namespaceURI != null) {
/* 300 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 301 */           if (prefix == null || prefix.length() == 0) {
/* 302 */             prefix = generatePrefix(namespaceURI);
/* 303 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 304 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 307 */           if (prefix.trim().length() > 0) {
/* 308 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 310 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 313 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 316 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 326 */     String prefix = xmlWriter.getPrefix(namespace);
/* 327 */     if (prefix == null) {
/* 328 */       prefix = generatePrefix(namespace);
/* 329 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 331 */         String uri = nsContext.getNamespaceURI(prefix);
/* 332 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 335 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 337 */       xmlWriter.writeNamespace(prefix, namespace);
/* 338 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 340 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 354 */     ArrayList<QName> elementList = new ArrayList();
/* 355 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 358 */     elementList.add(new QName("", 
/* 359 */           "in_cookie"));
/*     */     
/* 361 */     elementList.add(
/* 362 */         ConverterUtil.convertToString(this.localIn_cookie));
/*     */     
/* 364 */     elementList.add(new QName("", 
/* 365 */           "id"));
/*     */     
/* 367 */     elementList.add(
/* 368 */         ConverterUtil.convertToString(this.localId));
/*     */ 
/*     */     
/* 371 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static TestConnection parse(XMLStreamReader reader) throws Exception {
/* 395 */       TestConnection object = 
/* 396 */         new TestConnection();
/*     */ 
/*     */       
/* 399 */       String nillableValue = null;
/* 400 */       String prefix = "";
/* 401 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 404 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 405 */           reader.next();
/*     */         }
/*     */         
/* 408 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 409 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 410 */               "type");
/* 411 */           if (fullTypeName != null) {
/* 412 */             String nsPrefix = null;
/* 413 */             if (fullTypeName.indexOf(":") > -1) {
/* 414 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 416 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 418 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 420 */             if (!"TestConnection".equals(type)) {
/*     */               
/* 422 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 423 */               return (TestConnection)ExtensionMapper.getTypeObject(
/* 424 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 438 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 443 */         reader.next();
/*     */ 
/*     */         
/* 446 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 448 */         if (reader.isStartElement() && (new QName("", "in_cookie")).equals(reader.getName())) {
/*     */           
/* 450 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 451 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 454 */             String content = reader.getElementText();
/*     */             
/* 456 */             object.setIn_cookie(
/* 457 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 462 */             object.setIn_cookie(-2147483648);
/*     */             
/* 464 */             reader.getElementText();
/*     */           } 
/*     */           
/* 467 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 473 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 477 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 479 */         if (reader.isStartElement() && (new QName("", "id")).equals(reader.getName())) {
/*     */           
/* 481 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 482 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 485 */             String content = reader.getElementText();
/*     */             
/* 487 */             object.setId(
/* 488 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 493 */             object.setId(-2147483648);
/*     */             
/* 495 */             reader.getElementText();
/*     */           } 
/*     */           
/* 498 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 504 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 507 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 508 */           reader.next();
/*     */         }
/* 510 */         if (reader.isStartElement())
/*     */         {
/* 512 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 517 */       catch (XMLStreamException e) {
/* 518 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 521 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\TestConnection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */