/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SetTransactionEvents implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SetTransactionEvents", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */   
/*     */   protected int localFlags;
/*     */ 
/*     */   
/*     */   protected String localType;
/*     */   
/*     */   protected String localNumber;
/*     */   
/*     */   protected String localEvent_list_xml;
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/*  71 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/*  82 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/* 101 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/* 112 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNumber() {
/* 131 */     return this.localNumber;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNumber(String param) {
/* 142 */     this.localNumber = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEvent_list_xml() {
/* 161 */     return this.localEvent_list_xml;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEvent_list_xml(String param) {
/* 172 */     this.localEvent_list_xml = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 192 */     ADBDataSource aDBDataSource = 
/* 193 */       new ADBDataSource(this, MY_QNAME);
/* 194 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 201 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 212 */     String prefix = null;
/* 213 */     String namespace = null;
/*     */ 
/*     */     
/* 216 */     prefix = parentQName.getPrefix();
/* 217 */     namespace = parentQName.getNamespaceURI();
/* 218 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 220 */     if (serializeType) {
/*     */ 
/*     */       
/* 223 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 224 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 225 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 226 */             String.valueOf(namespacePrefix) + ":SetTransactionEvents", 
/* 227 */             xmlWriter);
/*     */       } else {
/* 229 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 230 */             "SetTransactionEvents", 
/* 231 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 237 */     namespace = "";
/* 238 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 240 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 242 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 245 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 248 */     xmlWriter.writeEndElement();
/*     */     
/* 250 */     namespace = "";
/* 251 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 253 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 255 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 258 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 261 */     xmlWriter.writeEndElement();
/*     */     
/* 263 */     namespace = "";
/* 264 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 267 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 270 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 275 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 279 */     xmlWriter.writeEndElement();
/*     */     
/* 281 */     namespace = "";
/* 282 */     writeStartElement(null, namespace, "number", xmlWriter);
/*     */ 
/*     */     
/* 285 */     if (this.localNumber == null) {
/*     */ 
/*     */       
/* 288 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 293 */       xmlWriter.writeCharacters(this.localNumber);
/*     */     } 
/*     */ 
/*     */     
/* 297 */     xmlWriter.writeEndElement();
/*     */     
/* 299 */     namespace = "";
/* 300 */     writeStartElement(null, namespace, "event_list_xml", xmlWriter);
/*     */ 
/*     */     
/* 303 */     if (this.localEvent_list_xml == null) {
/*     */ 
/*     */       
/* 306 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 311 */       xmlWriter.writeCharacters(this.localEvent_list_xml);
/*     */     } 
/*     */ 
/*     */     
/* 315 */     xmlWriter.writeEndElement();
/*     */     
/* 317 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 323 */     if (namespace.equals("urn:CSSoapService")) {
/* 324 */       return "ns1";
/*     */     }
/* 326 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 334 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 335 */     if (writerPrefix != null) {
/* 336 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 338 */       if (namespace.length() == 0) {
/* 339 */         prefix = "";
/* 340 */       } else if (prefix == null) {
/* 341 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 344 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 345 */       xmlWriter.writeNamespace(prefix, namespace);
/* 346 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 355 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 356 */       xmlWriter.writeNamespace(prefix, namespace);
/* 357 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 359 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 367 */     if (namespace.equals("")) {
/* 368 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 370 */       registerPrefix(xmlWriter, namespace);
/* 371 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 382 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 383 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 384 */     if (attributePrefix == null) {
/* 385 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 388 */     if (attributePrefix.trim().length() > 0) {
/* 389 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 391 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 394 */     if (namespace.equals("")) {
/* 395 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 397 */       registerPrefix(xmlWriter, namespace);
/* 398 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 407 */     String namespaceURI = qname.getNamespaceURI();
/* 408 */     if (namespaceURI != null) {
/* 409 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 410 */       if (prefix == null) {
/* 411 */         prefix = generatePrefix(namespaceURI);
/* 412 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 413 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 416 */       if (prefix.trim().length() > 0) {
/* 417 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 420 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 424 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 431 */     if (qnames != null) {
/*     */ 
/*     */       
/* 434 */       StringBuffer stringToWrite = new StringBuffer();
/* 435 */       String namespaceURI = null;
/* 436 */       String prefix = null;
/*     */       
/* 438 */       for (int i = 0; i < qnames.length; i++) {
/* 439 */         if (i > 0) {
/* 440 */           stringToWrite.append(" ");
/*     */         }
/* 442 */         namespaceURI = qnames[i].getNamespaceURI();
/* 443 */         if (namespaceURI != null) {
/* 444 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 445 */           if (prefix == null || prefix.length() == 0) {
/* 446 */             prefix = generatePrefix(namespaceURI);
/* 447 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 448 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 451 */           if (prefix.trim().length() > 0) {
/* 452 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 454 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 457 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 460 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 470 */     String prefix = xmlWriter.getPrefix(namespace);
/* 471 */     if (prefix == null) {
/* 472 */       prefix = generatePrefix(namespace);
/* 473 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 475 */         String uri = nsContext.getNamespaceURI(prefix);
/* 476 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 479 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 481 */       xmlWriter.writeNamespace(prefix, namespace);
/* 482 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 484 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 498 */     ArrayList<QName> elementList = new ArrayList();
/* 499 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 502 */     elementList.add(new QName("", 
/* 503 */           "access_key"));
/*     */     
/* 505 */     elementList.add(
/* 506 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 508 */     elementList.add(new QName("", 
/* 509 */           "flags"));
/*     */     
/* 511 */     elementList.add(
/* 512 */         ConverterUtil.convertToString(this.localFlags));
/*     */     
/* 514 */     elementList.add(new QName("", 
/* 515 */           "type"));
/*     */     
/* 517 */     elementList.add((this.localType == null) ? null : 
/* 518 */         ConverterUtil.convertToString(this.localType));
/*     */     
/* 520 */     elementList.add(new QName("", 
/* 521 */           "number"));
/*     */     
/* 523 */     elementList.add((this.localNumber == null) ? null : 
/* 524 */         ConverterUtil.convertToString(this.localNumber));
/*     */     
/* 526 */     elementList.add(new QName("", 
/* 527 */           "event_list_xml"));
/*     */     
/* 529 */     elementList.add((this.localEvent_list_xml == null) ? null : 
/* 530 */         ConverterUtil.convertToString(this.localEvent_list_xml));
/*     */ 
/*     */     
/* 533 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SetTransactionEvents parse(XMLStreamReader reader) throws Exception {
/* 557 */       SetTransactionEvents object = 
/* 558 */         new SetTransactionEvents();
/*     */ 
/*     */       
/* 561 */       String nillableValue = null;
/* 562 */       String prefix = "";
/* 563 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 566 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 567 */           reader.next();
/*     */         }
/*     */         
/* 570 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 571 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 572 */               "type");
/* 573 */           if (fullTypeName != null) {
/* 574 */             String nsPrefix = null;
/* 575 */             if (fullTypeName.indexOf(":") > -1) {
/* 576 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 578 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 580 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 582 */             if (!"SetTransactionEvents".equals(type)) {
/*     */               
/* 584 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 585 */               return (SetTransactionEvents)ExtensionMapper.getTypeObject(
/* 586 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 600 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 605 */         reader.next();
/*     */ 
/*     */         
/* 608 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 610 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 612 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 613 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 616 */             String content = reader.getElementText();
/*     */             
/* 618 */             object.setAccess_key(
/* 619 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 624 */             object.setAccess_key(-2147483648);
/*     */             
/* 626 */             reader.getElementText();
/*     */           } 
/*     */           
/* 629 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 635 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 639 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 641 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 643 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 644 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 647 */             String content = reader.getElementText();
/*     */             
/* 649 */             object.setFlags(
/* 650 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 655 */             object.setFlags(-2147483648);
/*     */             
/* 657 */             reader.getElementText();
/*     */           } 
/*     */           
/* 660 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 666 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 670 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 672 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 674 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 675 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 678 */             String content = reader.getElementText();
/*     */             
/* 680 */             object.setType(
/* 681 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 686 */             reader.getElementText();
/*     */           } 
/*     */           
/* 689 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 695 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 699 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 701 */         if (reader.isStartElement() && (new QName("", "number")).equals(reader.getName())) {
/*     */           
/* 703 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 704 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 707 */             String content = reader.getElementText();
/*     */             
/* 709 */             object.setNumber(
/* 710 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 715 */             reader.getElementText();
/*     */           } 
/*     */           
/* 718 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 724 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 728 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 730 */         if (reader.isStartElement() && (new QName("", "event_list_xml")).equals(reader.getName())) {
/*     */           
/* 732 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 733 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 736 */             String content = reader.getElementText();
/*     */             
/* 738 */             object.setEvent_list_xml(
/* 739 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 744 */             reader.getElementText();
/*     */           } 
/*     */           
/* 747 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 753 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 756 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 757 */           reader.next();
/*     */         }
/* 759 */         if (reader.isStartElement())
/*     */         {
/* 761 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 766 */       catch (XMLStreamException e) {
/* 767 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 770 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SetTransactionEvents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */