/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class EndSession implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "EndSession", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  72 */     ADBDataSource aDBDataSource = 
/*  73 */       new ADBDataSource(this, MY_QNAME);
/*  74 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  81 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  92 */     String prefix = null;
/*  93 */     String namespace = null;
/*     */ 
/*     */     
/*  96 */     prefix = parentQName.getPrefix();
/*  97 */     namespace = parentQName.getNamespaceURI();
/*  98 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 100 */     if (serializeType) {
/*     */ 
/*     */       
/* 103 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 104 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 105 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 106 */             String.valueOf(namespacePrefix) + ":EndSession", 
/* 107 */             xmlWriter);
/*     */       } else {
/* 109 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 110 */             "EndSession", 
/* 111 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 117 */     namespace = "";
/* 118 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 120 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 122 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 125 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 128 */     xmlWriter.writeEndElement();
/*     */     
/* 130 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 136 */     if (namespace.equals("urn:CSSoapService")) {
/* 137 */       return "ns1";
/*     */     }
/* 139 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 147 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 148 */     if (writerPrefix != null) {
/* 149 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 151 */       if (namespace.length() == 0) {
/* 152 */         prefix = "";
/* 153 */       } else if (prefix == null) {
/* 154 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 157 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 158 */       xmlWriter.writeNamespace(prefix, namespace);
/* 159 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 168 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 169 */       xmlWriter.writeNamespace(prefix, namespace);
/* 170 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 172 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 180 */     if (namespace.equals("")) {
/* 181 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 183 */       registerPrefix(xmlWriter, namespace);
/* 184 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 195 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 196 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 197 */     if (attributePrefix == null) {
/* 198 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 201 */     if (attributePrefix.trim().length() > 0) {
/* 202 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 204 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 207 */     if (namespace.equals("")) {
/* 208 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 210 */       registerPrefix(xmlWriter, namespace);
/* 211 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 220 */     String namespaceURI = qname.getNamespaceURI();
/* 221 */     if (namespaceURI != null) {
/* 222 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 223 */       if (prefix == null) {
/* 224 */         prefix = generatePrefix(namespaceURI);
/* 225 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 226 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 229 */       if (prefix.trim().length() > 0) {
/* 230 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 233 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 237 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 244 */     if (qnames != null) {
/*     */ 
/*     */       
/* 247 */       StringBuffer stringToWrite = new StringBuffer();
/* 248 */       String namespaceURI = null;
/* 249 */       String prefix = null;
/*     */       
/* 251 */       for (int i = 0; i < qnames.length; i++) {
/* 252 */         if (i > 0) {
/* 253 */           stringToWrite.append(" ");
/*     */         }
/* 255 */         namespaceURI = qnames[i].getNamespaceURI();
/* 256 */         if (namespaceURI != null) {
/* 257 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 258 */           if (prefix == null || prefix.length() == 0) {
/* 259 */             prefix = generatePrefix(namespaceURI);
/* 260 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 261 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 264 */           if (prefix.trim().length() > 0) {
/* 265 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 267 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 270 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 273 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 283 */     String prefix = xmlWriter.getPrefix(namespace);
/* 284 */     if (prefix == null) {
/* 285 */       prefix = generatePrefix(namespace);
/* 286 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 288 */         String uri = nsContext.getNamespaceURI(prefix);
/* 289 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 292 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 294 */       xmlWriter.writeNamespace(prefix, namespace);
/* 295 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 297 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 311 */     ArrayList<QName> elementList = new ArrayList();
/* 312 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 315 */     elementList.add(new QName("", 
/* 316 */           "access_key"));
/*     */     
/* 318 */     elementList.add(
/* 319 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */ 
/*     */     
/* 322 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static EndSession parse(XMLStreamReader reader) throws Exception {
/* 346 */       EndSession object = 
/* 347 */         new EndSession();
/*     */ 
/*     */       
/* 350 */       String nillableValue = null;
/* 351 */       String prefix = "";
/* 352 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 355 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 356 */           reader.next();
/*     */         }
/*     */         
/* 359 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 360 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 361 */               "type");
/* 362 */           if (fullTypeName != null) {
/* 363 */             String nsPrefix = null;
/* 364 */             if (fullTypeName.indexOf(":") > -1) {
/* 365 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 367 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 369 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 371 */             if (!"EndSession".equals(type)) {
/*     */               
/* 373 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 374 */               return (EndSession)ExtensionMapper.getTypeObject(
/* 375 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 389 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 394 */         reader.next();
/*     */ 
/*     */         
/* 397 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 399 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 401 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 402 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 405 */             String content = reader.getElementText();
/*     */             
/* 407 */             object.setAccess_key(
/* 408 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 413 */             object.setAccess_key(-2147483648);
/*     */             
/* 415 */             reader.getElementText();
/*     */           } 
/*     */           
/* 418 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 424 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 427 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 428 */           reader.next();
/*     */         }
/* 430 */         if (reader.isStartElement())
/*     */         {
/* 432 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 437 */       catch (XMLStreamException e) {
/* 438 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 441 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\EndSession.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */