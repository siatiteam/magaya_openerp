/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetFirstTransbyDate implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetFirstTransbyDate", 
/*  24 */       "ns1");
/*     */   
/*     */   protected int localAccess_key;
/*     */   
/*     */   protected String localType;
/*     */   
/*     */   protected String localStart_date;
/*     */   
/*     */   protected String localEnd_date;
/*     */   
/*     */   protected int localFlags;
/*     */   
/*     */   protected int localRecord_quantity;
/*     */   
/*     */   protected int localBackwards_order;
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/*  71 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/*  82 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getStart_date() {
/* 101 */     return this.localStart_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStart_date(String param) {
/* 112 */     this.localStart_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEnd_date() {
/* 131 */     return this.localEnd_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEnd_date(String param) {
/* 142 */     this.localEnd_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/* 161 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/* 172 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getRecord_quantity() {
/* 191 */     return this.localRecord_quantity;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setRecord_quantity(int param) {
/* 202 */     this.localRecord_quantity = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getBackwards_order() {
/* 221 */     return this.localBackwards_order;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setBackwards_order(int param) {
/* 232 */     this.localBackwards_order = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 252 */     ADBDataSource aDBDataSource = 
/* 253 */       new ADBDataSource(this, MY_QNAME);
/* 254 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 261 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 272 */     String prefix = null;
/* 273 */     String namespace = null;
/*     */ 
/*     */     
/* 276 */     prefix = parentQName.getPrefix();
/* 277 */     namespace = parentQName.getNamespaceURI();
/* 278 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 280 */     if (serializeType) {
/*     */ 
/*     */       
/* 283 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 284 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 285 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 286 */             String.valueOf(namespacePrefix) + ":GetFirstTransbyDate", 
/* 287 */             xmlWriter);
/*     */       } else {
/* 289 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 290 */             "GetFirstTransbyDate", 
/* 291 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 297 */     namespace = "";
/* 298 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 300 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 302 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 305 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 308 */     xmlWriter.writeEndElement();
/*     */     
/* 310 */     namespace = "";
/* 311 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 314 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 317 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 322 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 326 */     xmlWriter.writeEndElement();
/*     */     
/* 328 */     namespace = "";
/* 329 */     writeStartElement(null, namespace, "start_date", xmlWriter);
/*     */ 
/*     */     
/* 332 */     if (this.localStart_date == null) {
/*     */ 
/*     */       
/* 335 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 340 */       xmlWriter.writeCharacters(this.localStart_date);
/*     */     } 
/*     */ 
/*     */     
/* 344 */     xmlWriter.writeEndElement();
/*     */     
/* 346 */     namespace = "";
/* 347 */     writeStartElement(null, namespace, "end_date", xmlWriter);
/*     */ 
/*     */     
/* 350 */     if (this.localEnd_date == null) {
/*     */ 
/*     */       
/* 353 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 358 */       xmlWriter.writeCharacters(this.localEnd_date);
/*     */     } 
/*     */ 
/*     */     
/* 362 */     xmlWriter.writeEndElement();
/*     */     
/* 364 */     namespace = "";
/* 365 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 367 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 369 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 372 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 375 */     xmlWriter.writeEndElement();
/*     */     
/* 377 */     namespace = "";
/* 378 */     writeStartElement(null, namespace, "record_quantity", xmlWriter);
/*     */     
/* 380 */     if (this.localRecord_quantity == Integer.MIN_VALUE) {
/*     */       
/* 382 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 385 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localRecord_quantity));
/*     */     } 
/*     */     
/* 388 */     xmlWriter.writeEndElement();
/*     */     
/* 390 */     namespace = "";
/* 391 */     writeStartElement(null, namespace, "backwards_order", xmlWriter);
/*     */     
/* 393 */     if (this.localBackwards_order == Integer.MIN_VALUE) {
/*     */       
/* 395 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 398 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localBackwards_order));
/*     */     } 
/*     */     
/* 401 */     xmlWriter.writeEndElement();
/*     */     
/* 403 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 409 */     if (namespace.equals("urn:CSSoapService")) {
/* 410 */       return "ns1";
/*     */     }
/* 412 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 420 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 421 */     if (writerPrefix != null) {
/* 422 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 424 */       if (namespace.length() == 0) {
/* 425 */         prefix = "";
/* 426 */       } else if (prefix == null) {
/* 427 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 430 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 431 */       xmlWriter.writeNamespace(prefix, namespace);
/* 432 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 441 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 442 */       xmlWriter.writeNamespace(prefix, namespace);
/* 443 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 445 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 453 */     if (namespace.equals("")) {
/* 454 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 456 */       registerPrefix(xmlWriter, namespace);
/* 457 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 468 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 469 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 470 */     if (attributePrefix == null) {
/* 471 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 474 */     if (attributePrefix.trim().length() > 0) {
/* 475 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 477 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 480 */     if (namespace.equals("")) {
/* 481 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 483 */       registerPrefix(xmlWriter, namespace);
/* 484 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 493 */     String namespaceURI = qname.getNamespaceURI();
/* 494 */     if (namespaceURI != null) {
/* 495 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 496 */       if (prefix == null) {
/* 497 */         prefix = generatePrefix(namespaceURI);
/* 498 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 499 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 502 */       if (prefix.trim().length() > 0) {
/* 503 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 506 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 510 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 517 */     if (qnames != null) {
/*     */ 
/*     */       
/* 520 */       StringBuffer stringToWrite = new StringBuffer();
/* 521 */       String namespaceURI = null;
/* 522 */       String prefix = null;
/*     */       
/* 524 */       for (int i = 0; i < qnames.length; i++) {
/* 525 */         if (i > 0) {
/* 526 */           stringToWrite.append(" ");
/*     */         }
/* 528 */         namespaceURI = qnames[i].getNamespaceURI();
/* 529 */         if (namespaceURI != null) {
/* 530 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 531 */           if (prefix == null || prefix.length() == 0) {
/* 532 */             prefix = generatePrefix(namespaceURI);
/* 533 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 534 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 537 */           if (prefix.trim().length() > 0) {
/* 538 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 540 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 543 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 546 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 556 */     String prefix = xmlWriter.getPrefix(namespace);
/* 557 */     if (prefix == null) {
/* 558 */       prefix = generatePrefix(namespace);
/* 559 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 561 */         String uri = nsContext.getNamespaceURI(prefix);
/* 562 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 565 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 567 */       xmlWriter.writeNamespace(prefix, namespace);
/* 568 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 570 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 584 */     ArrayList<QName> elementList = new ArrayList();
/* 585 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 588 */     elementList.add(new QName("", 
/* 589 */           "access_key"));
/*     */     
/* 591 */     elementList.add(
/* 592 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 594 */     elementList.add(new QName("", 
/* 595 */           "type"));
/*     */     
/* 597 */     elementList.add((this.localType == null) ? null : 
/* 598 */         ConverterUtil.convertToString(this.localType));
/*     */     
/* 600 */     elementList.add(new QName("", 
/* 601 */           "start_date"));
/*     */     
/* 603 */     elementList.add((this.localStart_date == null) ? null : 
/* 604 */         ConverterUtil.convertToString(this.localStart_date));
/*     */     
/* 606 */     elementList.add(new QName("", 
/* 607 */           "end_date"));
/*     */     
/* 609 */     elementList.add((this.localEnd_date == null) ? null : 
/* 610 */         ConverterUtil.convertToString(this.localEnd_date));
/*     */     
/* 612 */     elementList.add(new QName("", 
/* 613 */           "flags"));
/*     */     
/* 615 */     elementList.add(
/* 616 */         ConverterUtil.convertToString(this.localFlags));
/*     */     
/* 618 */     elementList.add(new QName("", 
/* 619 */           "record_quantity"));
/*     */     
/* 621 */     elementList.add(
/* 622 */         ConverterUtil.convertToString(this.localRecord_quantity));
/*     */     
/* 624 */     elementList.add(new QName("", 
/* 625 */           "backwards_order"));
/*     */     
/* 627 */     elementList.add(
/* 628 */         ConverterUtil.convertToString(this.localBackwards_order));
/*     */ 
/*     */     
/* 631 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetFirstTransbyDate parse(XMLStreamReader reader) throws Exception {
/* 655 */       GetFirstTransbyDate object = 
/* 656 */         new GetFirstTransbyDate();
/*     */ 
/*     */       
/* 659 */       String nillableValue = null;
/* 660 */       String prefix = "";
/* 661 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 664 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 665 */           reader.next();
/*     */         }
/*     */         
/* 668 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 669 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 670 */               "type");
/* 671 */           if (fullTypeName != null) {
/* 672 */             String nsPrefix = null;
/* 673 */             if (fullTypeName.indexOf(":") > -1) {
/* 674 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 676 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 678 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 680 */             if (!"GetFirstTransbyDate".equals(type)) {
/*     */               
/* 682 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 683 */               return (GetFirstTransbyDate)ExtensionMapper.getTypeObject(
/* 684 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 698 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 703 */         reader.next();
/*     */ 
/*     */         
/* 706 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 708 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 710 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 711 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 714 */             String content = reader.getElementText();
/*     */             
/* 716 */             object.setAccess_key(
/* 717 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 722 */             object.setAccess_key(-2147483648);
/*     */             
/* 724 */             reader.getElementText();
/*     */           } 
/*     */           
/* 727 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 733 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 737 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 739 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 741 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 742 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 745 */             String content = reader.getElementText();
/*     */             
/* 747 */             object.setType(
/* 748 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 753 */             reader.getElementText();
/*     */           } 
/*     */           
/* 756 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 762 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 766 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 768 */         if (reader.isStartElement() && (new QName("", "start_date")).equals(reader.getName())) {
/*     */           
/* 770 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 771 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 774 */             String content = reader.getElementText();
/*     */             
/* 776 */             object.setStart_date(
/* 777 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 782 */             reader.getElementText();
/*     */           } 
/*     */           
/* 785 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 791 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 795 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 797 */         if (reader.isStartElement() && (new QName("", "end_date")).equals(reader.getName())) {
/*     */           
/* 799 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 800 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 803 */             String content = reader.getElementText();
/*     */             
/* 805 */             object.setEnd_date(
/* 806 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 811 */             reader.getElementText();
/*     */           } 
/*     */           
/* 814 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 820 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 824 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 826 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 828 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 829 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 832 */             String content = reader.getElementText();
/*     */             
/* 834 */             object.setFlags(
/* 835 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 840 */             object.setFlags(-2147483648);
/*     */             
/* 842 */             reader.getElementText();
/*     */           } 
/*     */           
/* 845 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 851 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 855 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 857 */         if (reader.isStartElement() && (new QName("", "record_quantity")).equals(reader.getName())) {
/*     */           
/* 859 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 860 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 863 */             String content = reader.getElementText();
/*     */             
/* 865 */             object.setRecord_quantity(
/* 866 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 871 */             object.setRecord_quantity(-2147483648);
/*     */             
/* 873 */             reader.getElementText();
/*     */           } 
/*     */           
/* 876 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 882 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 886 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 888 */         if (reader.isStartElement() && (new QName("", "backwards_order")).equals(reader.getName())) {
/*     */           
/* 890 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 891 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 894 */             String content = reader.getElementText();
/*     */             
/* 896 */             object.setBackwards_order(
/* 897 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 902 */             object.setBackwards_order(-2147483648);
/*     */             
/* 904 */             reader.getElementText();
/*     */           } 
/*     */           
/* 907 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 913 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 916 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 917 */           reader.next();
/*     */         }
/* 919 */         if (reader.isStartElement())
/*     */         {
/* 921 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 926 */       catch (XMLStreamException e) {
/* 927 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 930 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetFirstTransbyDate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */