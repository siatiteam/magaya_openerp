/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetDocument implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetDocument", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localTrans_uuid;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected int localDoc_id;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getTrans_uuid() {
/*  41 */     return this.localTrans_uuid;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setTrans_uuid(String param) {
/*  52 */     this.localTrans_uuid = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getDoc_id() {
/*  71 */     return this.localDoc_id;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setDoc_id(int param) {
/*  82 */     this.localDoc_id = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 102 */     ADBDataSource aDBDataSource = 
/* 103 */       new ADBDataSource(this, MY_QNAME);
/* 104 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 111 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 122 */     String prefix = null;
/* 123 */     String namespace = null;
/*     */ 
/*     */     
/* 126 */     prefix = parentQName.getPrefix();
/* 127 */     namespace = parentQName.getNamespaceURI();
/* 128 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 130 */     if (serializeType) {
/*     */ 
/*     */       
/* 133 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 134 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 135 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 136 */             String.valueOf(namespacePrefix) + ":GetDocument", 
/* 137 */             xmlWriter);
/*     */       } else {
/* 139 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 140 */             "GetDocument", 
/* 141 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 147 */     namespace = "";
/* 148 */     writeStartElement(null, namespace, "trans_uuid", xmlWriter);
/*     */ 
/*     */     
/* 151 */     if (this.localTrans_uuid == null) {
/*     */ 
/*     */       
/* 154 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 159 */       xmlWriter.writeCharacters(this.localTrans_uuid);
/*     */     } 
/*     */ 
/*     */     
/* 163 */     xmlWriter.writeEndElement();
/*     */     
/* 165 */     namespace = "";
/* 166 */     writeStartElement(null, namespace, "doc_id", xmlWriter);
/*     */     
/* 168 */     if (this.localDoc_id == Integer.MIN_VALUE) {
/*     */       
/* 170 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 173 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localDoc_id));
/*     */     } 
/*     */     
/* 176 */     xmlWriter.writeEndElement();
/*     */     
/* 178 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 184 */     if (namespace.equals("urn:CSSoapService")) {
/* 185 */       return "ns1";
/*     */     }
/* 187 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 195 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 196 */     if (writerPrefix != null) {
/* 197 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 199 */       if (namespace.length() == 0) {
/* 200 */         prefix = "";
/* 201 */       } else if (prefix == null) {
/* 202 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 205 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 206 */       xmlWriter.writeNamespace(prefix, namespace);
/* 207 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 216 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 217 */       xmlWriter.writeNamespace(prefix, namespace);
/* 218 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 220 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 228 */     if (namespace.equals("")) {
/* 229 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 231 */       registerPrefix(xmlWriter, namespace);
/* 232 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 243 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 244 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 245 */     if (attributePrefix == null) {
/* 246 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 249 */     if (attributePrefix.trim().length() > 0) {
/* 250 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 252 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 255 */     if (namespace.equals("")) {
/* 256 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 258 */       registerPrefix(xmlWriter, namespace);
/* 259 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 268 */     String namespaceURI = qname.getNamespaceURI();
/* 269 */     if (namespaceURI != null) {
/* 270 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 271 */       if (prefix == null) {
/* 272 */         prefix = generatePrefix(namespaceURI);
/* 273 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 274 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 277 */       if (prefix.trim().length() > 0) {
/* 278 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 281 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 285 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 292 */     if (qnames != null) {
/*     */ 
/*     */       
/* 295 */       StringBuffer stringToWrite = new StringBuffer();
/* 296 */       String namespaceURI = null;
/* 297 */       String prefix = null;
/*     */       
/* 299 */       for (int i = 0; i < qnames.length; i++) {
/* 300 */         if (i > 0) {
/* 301 */           stringToWrite.append(" ");
/*     */         }
/* 303 */         namespaceURI = qnames[i].getNamespaceURI();
/* 304 */         if (namespaceURI != null) {
/* 305 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 306 */           if (prefix == null || prefix.length() == 0) {
/* 307 */             prefix = generatePrefix(namespaceURI);
/* 308 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 309 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 312 */           if (prefix.trim().length() > 0) {
/* 313 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 315 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 318 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 321 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 331 */     String prefix = xmlWriter.getPrefix(namespace);
/* 332 */     if (prefix == null) {
/* 333 */       prefix = generatePrefix(namespace);
/* 334 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 336 */         String uri = nsContext.getNamespaceURI(prefix);
/* 337 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 340 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 342 */       xmlWriter.writeNamespace(prefix, namespace);
/* 343 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 345 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 359 */     ArrayList<QName> elementList = new ArrayList();
/* 360 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 363 */     elementList.add(new QName("", 
/* 364 */           "trans_uuid"));
/*     */     
/* 366 */     elementList.add((this.localTrans_uuid == null) ? null : 
/* 367 */         ConverterUtil.convertToString(this.localTrans_uuid));
/*     */     
/* 369 */     elementList.add(new QName("", 
/* 370 */           "doc_id"));
/*     */     
/* 372 */     elementList.add(
/* 373 */         ConverterUtil.convertToString(this.localDoc_id));
/*     */ 
/*     */     
/* 376 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetDocument parse(XMLStreamReader reader) throws Exception {
/* 400 */       GetDocument object = 
/* 401 */         new GetDocument();
/*     */ 
/*     */       
/* 404 */       String nillableValue = null;
/* 405 */       String prefix = "";
/* 406 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 409 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 410 */           reader.next();
/*     */         }
/*     */         
/* 413 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 414 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 415 */               "type");
/* 416 */           if (fullTypeName != null) {
/* 417 */             String nsPrefix = null;
/* 418 */             if (fullTypeName.indexOf(":") > -1) {
/* 419 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 421 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 423 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 425 */             if (!"GetDocument".equals(type)) {
/*     */               
/* 427 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 428 */               return (GetDocument)ExtensionMapper.getTypeObject(
/* 429 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 443 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 448 */         reader.next();
/*     */ 
/*     */         
/* 451 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 453 */         if (reader.isStartElement() && (new QName("", "trans_uuid")).equals(reader.getName())) {
/*     */           
/* 455 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 456 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 459 */             String content = reader.getElementText();
/*     */             
/* 461 */             object.setTrans_uuid(
/* 462 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 467 */             reader.getElementText();
/*     */           } 
/*     */           
/* 470 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 476 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 480 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 482 */         if (reader.isStartElement() && (new QName("", "doc_id")).equals(reader.getName())) {
/*     */           
/* 484 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 485 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 488 */             String content = reader.getElementText();
/*     */             
/* 490 */             object.setDoc_id(
/* 491 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 496 */             object.setDoc_id(-2147483648);
/*     */             
/* 498 */             reader.getElementText();
/*     */           } 
/*     */           
/* 501 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 507 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 510 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 511 */           reader.next();
/*     */         }
/* 513 */         if (reader.isStartElement())
/*     */         {
/* 515 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 520 */       catch (XMLStreamException e) {
/* 521 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 524 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetDocument.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */