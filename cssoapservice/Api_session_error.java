/*     */ package cssoapservice;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class Api_session_error implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "api_session_error", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localApi_session_error;
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*  35 */   private static HashMap _table_ = new HashMap<Object, Object>();
/*     */ 
/*     */ 
/*     */   
/*     */   protected Api_session_error(String value, boolean isRegisterValue) {
/*  40 */     this.localApi_session_error = value;
/*  41 */     if (isRegisterValue)
/*     */     {
/*  43 */       _table_.put(this.localApi_session_error, this);
/*     */     }
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*  50 */   public static final String _license_in_use = ConverterUtil.convertToString("license_in_use");
/*     */ 
/*     */   
/*  53 */   public static final String _over_credit_limit = ConverterUtil.convertToString("over_credit_limit");
/*     */ 
/*     */   
/*  56 */   public static final String _no_capacity_in_trip = ConverterUtil.convertToString("no_capacity_in_trip");
/*     */ 
/*     */   
/*  59 */   public static final String _service_unreachable = ConverterUtil.convertToString("service_unreachable");
/*     */ 
/*     */   
/*  62 */   public static final String _invalid_operation = ConverterUtil.convertToString("invalid_operation");
/*     */ 
/*     */   
/*  65 */   public static final String _error_saving_to_database = ConverterUtil.convertToString("error_saving_to_database");
/*     */ 
/*     */   
/*  68 */   public static final String _unknown_object = ConverterUtil.convertToString("unknown_object");
/*     */ 
/*     */   
/*  71 */   public static final String _xml_validation_error = ConverterUtil.convertToString("xml_validation_error");
/*     */ 
/*     */   
/*  74 */   public static final String _invalid_xml = ConverterUtil.convertToString("invalid_xml");
/*     */ 
/*     */   
/*  77 */   public static final String _transaction_not_found = ConverterUtil.convertToString("transaction_not_found");
/*     */ 
/*     */   
/*  80 */   public static final String _access_denied = ConverterUtil.convertToString("access_denied");
/*     */ 
/*     */   
/*  83 */   public static final String _too_many_open_sessions = ConverterUtil.convertToString("too_many_open_sessions");
/*     */ 
/*     */   
/*  86 */   public static final String _no_error = ConverterUtil.convertToString("no_error");
/*     */ 
/*     */   
/*  89 */   public static final Api_session_error license_in_use = new Api_session_error(_license_in_use, true);
/*     */ 
/*     */   
/*  92 */   public static final Api_session_error over_credit_limit = new Api_session_error(_over_credit_limit, true);
/*     */ 
/*     */   
/*  95 */   public static final Api_session_error no_capacity_in_trip = new Api_session_error(_no_capacity_in_trip, true);
/*     */ 
/*     */   
/*  98 */   public static final Api_session_error service_unreachable = new Api_session_error(_service_unreachable, true);
/*     */ 
/*     */   
/* 101 */   public static final Api_session_error invalid_operation = new Api_session_error(_invalid_operation, true);
/*     */ 
/*     */   
/* 104 */   public static final Api_session_error error_saving_to_database = new Api_session_error(_error_saving_to_database, true);
/*     */ 
/*     */   
/* 107 */   public static final Api_session_error unknown_object = new Api_session_error(_unknown_object, true);
/*     */ 
/*     */   
/* 110 */   public static final Api_session_error xml_validation_error = new Api_session_error(_xml_validation_error, true);
/*     */ 
/*     */   
/* 113 */   public static final Api_session_error invalid_xml = new Api_session_error(_invalid_xml, true);
/*     */ 
/*     */   
/* 116 */   public static final Api_session_error transaction_not_found = new Api_session_error(_transaction_not_found, true);
/*     */ 
/*     */   
/* 119 */   public static final Api_session_error access_denied = new Api_session_error(_access_denied, true);
/*     */ 
/*     */   
/* 122 */   public static final Api_session_error too_many_open_sessions = new Api_session_error(_too_many_open_sessions, true);
/*     */ 
/*     */   
/* 125 */   public static final Api_session_error no_error = new Api_session_error(_no_error, true);
/*     */   
/*     */   public String getValue() {
/* 128 */     return this.localApi_session_error;
/*     */   }
/* 130 */   public boolean equals(Object obj) { return (obj == this); } public int hashCode() {
/* 131 */     return toString().hashCode();
/*     */   }
/*     */   public String toString() {
/* 134 */     return this.localApi_session_error.toString();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 155 */     ADBDataSource aDBDataSource = 
/* 156 */       new ADBDataSource(this, MY_QNAME);
/* 157 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 164 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 175 */     String namespace = parentQName.getNamespaceURI();
/* 176 */     String _localName = parentQName.getLocalPart();
/*     */     
/* 178 */     writeStartElement(null, namespace, _localName, xmlWriter);
/*     */ 
/*     */     
/* 181 */     if (serializeType) {
/* 182 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 183 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 184 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 185 */             String.valueOf(namespacePrefix) + ":api_session_error", 
/* 186 */             xmlWriter);
/*     */       } else {
/* 188 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 189 */             "api_session_error", 
/* 190 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */     
/* 194 */     if (this.localApi_session_error == null)
/*     */     {
/* 196 */       throw new ADBException("api_session_error cannot be null !!");
/*     */     }
/*     */ 
/*     */     
/* 200 */     xmlWriter.writeCharacters(this.localApi_session_error);
/*     */ 
/*     */ 
/*     */     
/* 204 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 210 */     if (namespace.equals("urn:CSSoapService")) {
/* 211 */       return "ns1";
/*     */     }
/* 213 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 221 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 222 */     if (writerPrefix != null) {
/* 223 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 225 */       if (namespace.length() == 0) {
/* 226 */         prefix = "";
/* 227 */       } else if (prefix == null) {
/* 228 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 231 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 232 */       xmlWriter.writeNamespace(prefix, namespace);
/* 233 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 242 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 243 */       xmlWriter.writeNamespace(prefix, namespace);
/* 244 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 246 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 254 */     if (namespace.equals("")) {
/* 255 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 257 */       registerPrefix(xmlWriter, namespace);
/* 258 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 269 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 270 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 271 */     if (attributePrefix == null) {
/* 272 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 275 */     if (attributePrefix.trim().length() > 0) {
/* 276 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 278 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 281 */     if (namespace.equals("")) {
/* 282 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 284 */       registerPrefix(xmlWriter, namespace);
/* 285 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 294 */     String namespaceURI = qname.getNamespaceURI();
/* 295 */     if (namespaceURI != null) {
/* 296 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 297 */       if (prefix == null) {
/* 298 */         prefix = generatePrefix(namespaceURI);
/* 299 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 300 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 303 */       if (prefix.trim().length() > 0) {
/* 304 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 307 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 311 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 318 */     if (qnames != null) {
/*     */ 
/*     */       
/* 321 */       StringBuffer stringToWrite = new StringBuffer();
/* 322 */       String namespaceURI = null;
/* 323 */       String prefix = null;
/*     */       
/* 325 */       for (int i = 0; i < qnames.length; i++) {
/* 326 */         if (i > 0) {
/* 327 */           stringToWrite.append(" ");
/*     */         }
/* 329 */         namespaceURI = qnames[i].getNamespaceURI();
/* 330 */         if (namespaceURI != null) {
/* 331 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 332 */           if (prefix == null || prefix.length() == 0) {
/* 333 */             prefix = generatePrefix(namespaceURI);
/* 334 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 335 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 338 */           if (prefix.trim().length() > 0) {
/* 339 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 341 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 344 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 347 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 357 */     String prefix = xmlWriter.getPrefix(namespace);
/* 358 */     if (prefix == null) {
/* 359 */       prefix = generatePrefix(namespace);
/* 360 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 362 */         String uri = nsContext.getNamespaceURI(prefix);
/* 363 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 366 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 368 */       xmlWriter.writeNamespace(prefix, namespace);
/* 369 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 371 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 387 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(MY_QNAME, 
/* 388 */         new Object[] {
/* 389 */           "Element Text", 
/* 390 */           ConverterUtil.convertToString(this.localApi_session_error)
/*     */         },
/* 392 */         null);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static Api_session_error fromValue(String value) throws IllegalArgumentException {
/* 407 */       Api_session_error enumeration = 
/*     */         
/* 409 */         (Api_session_error)Api_session_error._table_.get(value);
/*     */ 
/*     */       
/* 412 */       if (enumeration == null && value != null && !value.equals("")) {
/* 413 */         throw new IllegalArgumentException();
/*     */       }
/* 415 */       return enumeration;
/*     */     }
/*     */ 
/*     */     
/*     */     public static Api_session_error fromString(String value, String namespaceURI) throws IllegalArgumentException {
/*     */       try {
/* 421 */         return fromValue(ConverterUtil.convertToString(value));
/*     */       
/*     */       }
/* 424 */       catch (Exception e) {
/* 425 */         throw new IllegalArgumentException();
/*     */       } 
/*     */     }
/*     */ 
/*     */     
/*     */     public static Api_session_error fromString(XMLStreamReader xmlStreamReader, String content) {
/* 431 */       if (content.indexOf(":") > -1) {
/* 432 */         String prefix = content.substring(0, content.indexOf(":"));
/* 433 */         String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
/* 434 */         return fromString(content, namespaceUri);
/*     */       } 
/* 436 */       return fromString(content, "");
/*     */     }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     
/*     */     public static Api_session_error parse(XMLStreamReader reader) throws Exception {
/* 449 */       Api_session_error object = null;
/*     */       
/* 451 */       Map<Object, Object> attributeMap = new HashMap<Object, Object>();
/* 452 */       List extraAttributeList = new ArrayList();
/*     */ 
/*     */ 
/*     */       
/* 456 */       String nillableValue = null;
/* 457 */       String prefix = "";
/* 458 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 461 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 462 */           reader.next();
/*     */         }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 469 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */         
/* 473 */         while (!reader.isEndElement()) {
/* 474 */           if (reader.isStartElement() || reader.hasText()) {
/*     */             
/* 476 */             nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 477 */             if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 478 */               throw new ADBException("The element: api_session_error  cannot be null");
/*     */             }
/*     */ 
/*     */             
/* 482 */             String content = reader.getElementText();
/*     */             
/* 484 */             if (content.indexOf(":") > 0) {
/*     */               
/* 486 */               prefix = content.substring(0, content.indexOf(":"));
/* 487 */               namespaceuri = reader.getNamespaceURI(prefix);
/* 488 */               object = fromString(content, namespaceuri);
/*     */               
/*     */               continue;
/*     */             } 
/* 492 */             object = fromString(content, "");
/*     */             
/*     */             continue;
/*     */           } 
/*     */           
/* 497 */           reader.next();
/*     */ 
/*     */         
/*     */         }
/*     */ 
/*     */       
/*     */       }
/* 504 */       catch (XMLStreamException e) {
/* 505 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 508 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\Api_session_error.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */