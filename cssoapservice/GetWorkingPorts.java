/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetWorkingPorts implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetWorkingPorts", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  42 */     ADBDataSource aDBDataSource = 
/*  43 */       new ADBDataSource(this, MY_QNAME);
/*  44 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  51 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  62 */     String prefix = null;
/*  63 */     String namespace = null;
/*     */ 
/*     */     
/*  66 */     prefix = parentQName.getPrefix();
/*  67 */     namespace = parentQName.getNamespaceURI();
/*  68 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/*  70 */     if (serializeType) {
/*     */ 
/*     */       
/*  73 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/*  74 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/*  75 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  76 */             String.valueOf(namespacePrefix) + ":GetWorkingPorts", 
/*  77 */             xmlWriter);
/*     */       } else {
/*  79 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  80 */             "GetWorkingPorts", 
/*  81 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/*  87 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/*  93 */     if (namespace.equals("urn:CSSoapService")) {
/*  94 */       return "ns1";
/*     */     }
/*  96 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 104 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 105 */     if (writerPrefix != null) {
/* 106 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 108 */       if (namespace.length() == 0) {
/* 109 */         prefix = "";
/* 110 */       } else if (prefix == null) {
/* 111 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 114 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 115 */       xmlWriter.writeNamespace(prefix, namespace);
/* 116 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 125 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 126 */       xmlWriter.writeNamespace(prefix, namespace);
/* 127 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 129 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 137 */     if (namespace.equals("")) {
/* 138 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 140 */       registerPrefix(xmlWriter, namespace);
/* 141 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 152 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 153 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 154 */     if (attributePrefix == null) {
/* 155 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 158 */     if (attributePrefix.trim().length() > 0) {
/* 159 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 161 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 164 */     if (namespace.equals("")) {
/* 165 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 167 */       registerPrefix(xmlWriter, namespace);
/* 168 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 177 */     String namespaceURI = qname.getNamespaceURI();
/* 178 */     if (namespaceURI != null) {
/* 179 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 180 */       if (prefix == null) {
/* 181 */         prefix = generatePrefix(namespaceURI);
/* 182 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 183 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 186 */       if (prefix.trim().length() > 0) {
/* 187 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 190 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 194 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 201 */     if (qnames != null) {
/*     */ 
/*     */       
/* 204 */       StringBuffer stringToWrite = new StringBuffer();
/* 205 */       String namespaceURI = null;
/* 206 */       String prefix = null;
/*     */       
/* 208 */       for (int i = 0; i < qnames.length; i++) {
/* 209 */         if (i > 0) {
/* 210 */           stringToWrite.append(" ");
/*     */         }
/* 212 */         namespaceURI = qnames[i].getNamespaceURI();
/* 213 */         if (namespaceURI != null) {
/* 214 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 215 */           if (prefix == null || prefix.length() == 0) {
/* 216 */             prefix = generatePrefix(namespaceURI);
/* 217 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 218 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 221 */           if (prefix.trim().length() > 0) {
/* 222 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 224 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 227 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 230 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 240 */     String prefix = xmlWriter.getPrefix(namespace);
/* 241 */     if (prefix == null) {
/* 242 */       prefix = generatePrefix(namespace);
/* 243 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 245 */         String uri = nsContext.getNamespaceURI(prefix);
/* 246 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 249 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 251 */       xmlWriter.writeNamespace(prefix, namespace);
/* 252 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 254 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 268 */     ArrayList elementList = new ArrayList();
/* 269 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */ 
/*     */     
/* 273 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetWorkingPorts parse(XMLStreamReader reader) throws Exception {
/* 297 */       GetWorkingPorts object = 
/* 298 */         new GetWorkingPorts();
/*     */ 
/*     */       
/* 301 */       String nillableValue = null;
/* 302 */       String prefix = "";
/* 303 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 306 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 307 */           reader.next();
/*     */         }
/*     */         
/* 310 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 311 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 312 */               "type");
/* 313 */           if (fullTypeName != null) {
/* 314 */             String nsPrefix = null;
/* 315 */             if (fullTypeName.indexOf(":") > -1) {
/* 316 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 318 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 320 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 322 */             if (!"GetWorkingPorts".equals(type)) {
/*     */               
/* 324 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 325 */               return (GetWorkingPorts)ExtensionMapper.getTypeObject(
/* 326 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 340 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 345 */         reader.next();
/*     */         
/* 347 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 348 */           reader.next();
/*     */         }
/* 350 */         if (reader.isStartElement())
/*     */         {
/* 352 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 357 */       catch (XMLStreamException e) {
/* 358 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 361 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetWorkingPorts.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */