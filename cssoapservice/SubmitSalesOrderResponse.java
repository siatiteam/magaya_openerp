/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SubmitSalesOrderResponse implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SubmitSalesOrderResponse", 
/*  24 */       "ns1");
/*     */ 
/*     */ 
/*     */   
/*     */   protected Api_session_error local_return;
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localSales_order_number;
/*     */ 
/*     */ 
/*     */   
/*     */   protected String localResult_string;
/*     */ 
/*     */ 
/*     */   
/*     */   public Api_session_error get_return() {
/*  41 */     return this.local_return;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void set_return(Api_session_error param) {
/*  52 */     this.local_return = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getSales_order_number() {
/*  71 */     return this.localSales_order_number;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setSales_order_number(String param) {
/*  82 */     this.localSales_order_number = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getResult_string() {
/* 101 */     return this.localResult_string;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setResult_string(String param) {
/* 112 */     this.localResult_string = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 132 */     ADBDataSource aDBDataSource = 
/* 133 */       new ADBDataSource(this, MY_QNAME);
/* 134 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 141 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 152 */     String prefix = null;
/* 153 */     String namespace = null;
/*     */ 
/*     */     
/* 156 */     prefix = parentQName.getPrefix();
/* 157 */     namespace = parentQName.getNamespaceURI();
/* 158 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 160 */     if (serializeType) {
/*     */ 
/*     */       
/* 163 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 164 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 165 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 166 */             String.valueOf(namespacePrefix) + ":SubmitSalesOrderResponse", 
/* 167 */             xmlWriter);
/*     */       } else {
/* 169 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 170 */             "SubmitSalesOrderResponse", 
/* 171 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 177 */     if (this.local_return == null) {
/*     */       
/* 179 */       writeStartElement(null, "", "return", xmlWriter);
/*     */ 
/*     */       
/* 182 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/* 183 */       xmlWriter.writeEndElement();
/*     */     } else {
/* 185 */       this.local_return.serialize(new QName("", "return"), 
/* 186 */           xmlWriter);
/*     */     } 
/*     */     
/* 189 */     namespace = "";
/* 190 */     writeStartElement(null, namespace, "sales_order_number", xmlWriter);
/*     */ 
/*     */     
/* 193 */     if (this.localSales_order_number == null) {
/*     */ 
/*     */       
/* 196 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 201 */       xmlWriter.writeCharacters(this.localSales_order_number);
/*     */     } 
/*     */ 
/*     */     
/* 205 */     xmlWriter.writeEndElement();
/*     */     
/* 207 */     namespace = "";
/* 208 */     writeStartElement(null, namespace, "result_string", xmlWriter);
/*     */ 
/*     */     
/* 211 */     if (this.localResult_string == null) {
/*     */ 
/*     */       
/* 214 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 219 */       xmlWriter.writeCharacters(this.localResult_string);
/*     */     } 
/*     */ 
/*     */     
/* 223 */     xmlWriter.writeEndElement();
/*     */     
/* 225 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 231 */     if (namespace.equals("urn:CSSoapService")) {
/* 232 */       return "ns1";
/*     */     }
/* 234 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 242 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 243 */     if (writerPrefix != null) {
/* 244 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 246 */       if (namespace.length() == 0) {
/* 247 */         prefix = "";
/* 248 */       } else if (prefix == null) {
/* 249 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 252 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 253 */       xmlWriter.writeNamespace(prefix, namespace);
/* 254 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 263 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 264 */       xmlWriter.writeNamespace(prefix, namespace);
/* 265 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 267 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 275 */     if (namespace.equals("")) {
/* 276 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 278 */       registerPrefix(xmlWriter, namespace);
/* 279 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 290 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 291 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 292 */     if (attributePrefix == null) {
/* 293 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 296 */     if (attributePrefix.trim().length() > 0) {
/* 297 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 299 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 302 */     if (namespace.equals("")) {
/* 303 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 305 */       registerPrefix(xmlWriter, namespace);
/* 306 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 315 */     String namespaceURI = qname.getNamespaceURI();
/* 316 */     if (namespaceURI != null) {
/* 317 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 318 */       if (prefix == null) {
/* 319 */         prefix = generatePrefix(namespaceURI);
/* 320 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 321 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 324 */       if (prefix.trim().length() > 0) {
/* 325 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 328 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 332 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 339 */     if (qnames != null) {
/*     */ 
/*     */       
/* 342 */       StringBuffer stringToWrite = new StringBuffer();
/* 343 */       String namespaceURI = null;
/* 344 */       String prefix = null;
/*     */       
/* 346 */       for (int i = 0; i < qnames.length; i++) {
/* 347 */         if (i > 0) {
/* 348 */           stringToWrite.append(" ");
/*     */         }
/* 350 */         namespaceURI = qnames[i].getNamespaceURI();
/* 351 */         if (namespaceURI != null) {
/* 352 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 353 */           if (prefix == null || prefix.length() == 0) {
/* 354 */             prefix = generatePrefix(namespaceURI);
/* 355 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 356 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 359 */           if (prefix.trim().length() > 0) {
/* 360 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 362 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 365 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 368 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 378 */     String prefix = xmlWriter.getPrefix(namespace);
/* 379 */     if (prefix == null) {
/* 380 */       prefix = generatePrefix(namespace);
/* 381 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 383 */         String uri = nsContext.getNamespaceURI(prefix);
/* 384 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 387 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 389 */       xmlWriter.writeNamespace(prefix, namespace);
/* 390 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 392 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 406 */     ArrayList<QName> elementList = new ArrayList();
/* 407 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 410 */     elementList.add(new QName("", 
/* 411 */           "return"));
/*     */ 
/*     */     
/* 414 */     elementList.add((this.local_return == null) ? null : 
/* 415 */         this.local_return);
/*     */     
/* 417 */     elementList.add(new QName("", 
/* 418 */           "sales_order_number"));
/*     */     
/* 420 */     elementList.add((this.localSales_order_number == null) ? null : 
/* 421 */         ConverterUtil.convertToString(this.localSales_order_number));
/*     */     
/* 423 */     elementList.add(new QName("", 
/* 424 */           "result_string"));
/*     */     
/* 426 */     elementList.add((this.localResult_string == null) ? null : 
/* 427 */         ConverterUtil.convertToString(this.localResult_string));
/*     */ 
/*     */     
/* 430 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SubmitSalesOrderResponse parse(XMLStreamReader reader) throws Exception {
/* 454 */       SubmitSalesOrderResponse object = 
/* 455 */         new SubmitSalesOrderResponse();
/*     */ 
/*     */       
/* 458 */       String nillableValue = null;
/* 459 */       String prefix = "";
/* 460 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 463 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 464 */           reader.next();
/*     */         }
/*     */         
/* 467 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 468 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 469 */               "type");
/* 470 */           if (fullTypeName != null) {
/* 471 */             String nsPrefix = null;
/* 472 */             if (fullTypeName.indexOf(":") > -1) {
/* 473 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 475 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 477 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 479 */             if (!"SubmitSalesOrderResponse".equals(type)) {
/*     */               
/* 481 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 482 */               return (SubmitSalesOrderResponse)ExtensionMapper.getTypeObject(
/* 483 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 497 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 502 */         reader.next();
/*     */ 
/*     */         
/* 505 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 507 */         if (reader.isStartElement() && (new QName("", "return")).equals(reader.getName())) {
/*     */           
/* 509 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 510 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 511 */             object.set_return(null);
/* 512 */             reader.next();
/*     */             
/* 514 */             reader.next();
/*     */           }
/*     */           else {
/*     */             
/* 518 */             object.set_return(Api_session_error.Factory.parse(reader));
/*     */             
/* 520 */             reader.next();
/*     */           }
/*     */         
/*     */         }
/*     */         else {
/*     */           
/* 526 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 530 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 532 */         if (reader.isStartElement() && (new QName("", "sales_order_number")).equals(reader.getName())) {
/*     */           
/* 534 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 535 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 538 */             String content = reader.getElementText();
/*     */             
/* 540 */             object.setSales_order_number(
/* 541 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 546 */             reader.getElementText();
/*     */           } 
/*     */           
/* 549 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 555 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 559 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 561 */         if (reader.isStartElement() && (new QName("", "result_string")).equals(reader.getName())) {
/*     */           
/* 563 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 564 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 567 */             String content = reader.getElementText();
/*     */             
/* 569 */             object.setResult_string(
/* 570 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 575 */             reader.getElementText();
/*     */           } 
/*     */           
/* 578 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 584 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 587 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 588 */           reader.next();
/*     */         }
/* 590 */         if (reader.isStartElement())
/*     */         {
/* 592 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 597 */       catch (XMLStreamException e) {
/* 598 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 601 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SubmitSalesOrderResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */