/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class SetApprovalStatus implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "SetApprovalStatus", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */   
/*     */   protected String localType;
/*     */ 
/*     */   
/*     */   protected String localNumber;
/*     */ 
/*     */   
/*     */   protected int localStatus;
/*     */ 
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/*  71 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/*  82 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getNumber() {
/* 101 */     return this.localNumber;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setNumber(String param) {
/* 112 */     this.localNumber = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getStatus() {
/* 131 */     return this.localStatus;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStatus(int param) {
/* 142 */     this.localStatus = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 162 */     ADBDataSource aDBDataSource = 
/* 163 */       new ADBDataSource(this, MY_QNAME);
/* 164 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 171 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 182 */     String prefix = null;
/* 183 */     String namespace = null;
/*     */ 
/*     */     
/* 186 */     prefix = parentQName.getPrefix();
/* 187 */     namespace = parentQName.getNamespaceURI();
/* 188 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 190 */     if (serializeType) {
/*     */ 
/*     */       
/* 193 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 194 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 195 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 196 */             String.valueOf(namespacePrefix) + ":SetApprovalStatus", 
/* 197 */             xmlWriter);
/*     */       } else {
/* 199 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 200 */             "SetApprovalStatus", 
/* 201 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 207 */     namespace = "";
/* 208 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 210 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 212 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 215 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 218 */     xmlWriter.writeEndElement();
/*     */     
/* 220 */     namespace = "";
/* 221 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 224 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 227 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 232 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 236 */     xmlWriter.writeEndElement();
/*     */     
/* 238 */     namespace = "";
/* 239 */     writeStartElement(null, namespace, "number", xmlWriter);
/*     */ 
/*     */     
/* 242 */     if (this.localNumber == null) {
/*     */ 
/*     */       
/* 245 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 250 */       xmlWriter.writeCharacters(this.localNumber);
/*     */     } 
/*     */ 
/*     */     
/* 254 */     xmlWriter.writeEndElement();
/*     */     
/* 256 */     namespace = "";
/* 257 */     writeStartElement(null, namespace, "status", xmlWriter);
/*     */     
/* 259 */     if (this.localStatus == Integer.MIN_VALUE) {
/*     */       
/* 261 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 264 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localStatus));
/*     */     } 
/*     */     
/* 267 */     xmlWriter.writeEndElement();
/*     */     
/* 269 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 275 */     if (namespace.equals("urn:CSSoapService")) {
/* 276 */       return "ns1";
/*     */     }
/* 278 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 286 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 287 */     if (writerPrefix != null) {
/* 288 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 290 */       if (namespace.length() == 0) {
/* 291 */         prefix = "";
/* 292 */       } else if (prefix == null) {
/* 293 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 296 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 297 */       xmlWriter.writeNamespace(prefix, namespace);
/* 298 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 307 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 308 */       xmlWriter.writeNamespace(prefix, namespace);
/* 309 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 311 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 319 */     if (namespace.equals("")) {
/* 320 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 322 */       registerPrefix(xmlWriter, namespace);
/* 323 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 334 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 335 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 336 */     if (attributePrefix == null) {
/* 337 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 340 */     if (attributePrefix.trim().length() > 0) {
/* 341 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 343 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 346 */     if (namespace.equals("")) {
/* 347 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 349 */       registerPrefix(xmlWriter, namespace);
/* 350 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 359 */     String namespaceURI = qname.getNamespaceURI();
/* 360 */     if (namespaceURI != null) {
/* 361 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 362 */       if (prefix == null) {
/* 363 */         prefix = generatePrefix(namespaceURI);
/* 364 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 365 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 368 */       if (prefix.trim().length() > 0) {
/* 369 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 372 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 376 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 383 */     if (qnames != null) {
/*     */ 
/*     */       
/* 386 */       StringBuffer stringToWrite = new StringBuffer();
/* 387 */       String namespaceURI = null;
/* 388 */       String prefix = null;
/*     */       
/* 390 */       for (int i = 0; i < qnames.length; i++) {
/* 391 */         if (i > 0) {
/* 392 */           stringToWrite.append(" ");
/*     */         }
/* 394 */         namespaceURI = qnames[i].getNamespaceURI();
/* 395 */         if (namespaceURI != null) {
/* 396 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 397 */           if (prefix == null || prefix.length() == 0) {
/* 398 */             prefix = generatePrefix(namespaceURI);
/* 399 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 400 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 403 */           if (prefix.trim().length() > 0) {
/* 404 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 406 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 409 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 412 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 422 */     String prefix = xmlWriter.getPrefix(namespace);
/* 423 */     if (prefix == null) {
/* 424 */       prefix = generatePrefix(namespace);
/* 425 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 427 */         String uri = nsContext.getNamespaceURI(prefix);
/* 428 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 431 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 433 */       xmlWriter.writeNamespace(prefix, namespace);
/* 434 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 436 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 450 */     ArrayList<QName> elementList = new ArrayList();
/* 451 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 454 */     elementList.add(new QName("", 
/* 455 */           "access_key"));
/*     */     
/* 457 */     elementList.add(
/* 458 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 460 */     elementList.add(new QName("", 
/* 461 */           "type"));
/*     */     
/* 463 */     elementList.add((this.localType == null) ? null : 
/* 464 */         ConverterUtil.convertToString(this.localType));
/*     */     
/* 466 */     elementList.add(new QName("", 
/* 467 */           "number"));
/*     */     
/* 469 */     elementList.add((this.localNumber == null) ? null : 
/* 470 */         ConverterUtil.convertToString(this.localNumber));
/*     */     
/* 472 */     elementList.add(new QName("", 
/* 473 */           "status"));
/*     */     
/* 475 */     elementList.add(
/* 476 */         ConverterUtil.convertToString(this.localStatus));
/*     */ 
/*     */     
/* 479 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static SetApprovalStatus parse(XMLStreamReader reader) throws Exception {
/* 503 */       SetApprovalStatus object = 
/* 504 */         new SetApprovalStatus();
/*     */ 
/*     */       
/* 507 */       String nillableValue = null;
/* 508 */       String prefix = "";
/* 509 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 512 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 513 */           reader.next();
/*     */         }
/*     */         
/* 516 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 517 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 518 */               "type");
/* 519 */           if (fullTypeName != null) {
/* 520 */             String nsPrefix = null;
/* 521 */             if (fullTypeName.indexOf(":") > -1) {
/* 522 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 524 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 526 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 528 */             if (!"SetApprovalStatus".equals(type)) {
/*     */               
/* 530 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 531 */               return (SetApprovalStatus)ExtensionMapper.getTypeObject(
/* 532 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 546 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 551 */         reader.next();
/*     */ 
/*     */         
/* 554 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 556 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 558 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 559 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 562 */             String content = reader.getElementText();
/*     */             
/* 564 */             object.setAccess_key(
/* 565 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 570 */             object.setAccess_key(-2147483648);
/*     */             
/* 572 */             reader.getElementText();
/*     */           } 
/*     */           
/* 575 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 581 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 585 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 587 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 589 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 590 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 593 */             String content = reader.getElementText();
/*     */             
/* 595 */             object.setType(
/* 596 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 601 */             reader.getElementText();
/*     */           } 
/*     */           
/* 604 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 610 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 614 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 616 */         if (reader.isStartElement() && (new QName("", "number")).equals(reader.getName())) {
/*     */           
/* 618 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 619 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 622 */             String content = reader.getElementText();
/*     */             
/* 624 */             object.setNumber(
/* 625 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 630 */             reader.getElementText();
/*     */           } 
/*     */           
/* 633 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 639 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 643 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 645 */         if (reader.isStartElement() && (new QName("", "status")).equals(reader.getName())) {
/*     */           
/* 647 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 648 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 651 */             String content = reader.getElementText();
/*     */             
/* 653 */             object.setStatus(
/* 654 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 659 */             object.setStatus(-2147483648);
/*     */             
/* 661 */             reader.getElementText();
/*     */           } 
/*     */           
/* 664 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 670 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 673 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 674 */           reader.next();
/*     */         }
/* 676 */         if (reader.isStartElement())
/*     */         {
/* 678 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 683 */       catch (XMLStreamException e) {
/* 684 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 687 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\SetApprovalStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */