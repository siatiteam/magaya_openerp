/*     */ package cssoapservice;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Vector;
/*     */ import javax.xml.namespace.NamespaceContext;
/*     */ import javax.xml.namespace.QName;
/*     */ import javax.xml.stream.XMLStreamException;
/*     */ import javax.xml.stream.XMLStreamReader;
/*     */ import javax.xml.stream.XMLStreamWriter;
/*     */ import org.apache.axiom.om.OMDataSource;
/*     */ import org.apache.axiom.om.OMElement;
/*     */ import org.apache.axiom.om.OMFactory;
/*     */ import org.apache.axis2.databinding.ADBBean;
/*     */ import org.apache.axis2.databinding.ADBDataSource;
/*     */ import org.apache.axis2.databinding.ADBException;
/*     */ import org.apache.axis2.databinding.utils.BeanUtil;
/*     */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*     */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*     */ 
/*     */ public class GetTransRangeByDate implements ADBBean {
/*  21 */   public static final QName MY_QNAME = new QName(
/*  22 */       "urn:CSSoapService", 
/*  23 */       "GetTransRangeByDate", 
/*  24 */       "ns1");
/*     */ 
/*     */   
/*     */   protected int localAccess_key;
/*     */ 
/*     */   
/*     */   protected String localType;
/*     */ 
/*     */   
/*     */   protected String localStart_date;
/*     */   
/*     */   protected String localEnd_date;
/*     */   
/*     */   protected int localFlags;
/*     */ 
/*     */   
/*     */   public int getAccess_key() {
/*  41 */     return this.localAccess_key;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setAccess_key(int param) {
/*  52 */     this.localAccess_key = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getType() {
/*  71 */     return this.localType;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setType(String param) {
/*  82 */     this.localType = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getStart_date() {
/* 101 */     return this.localStart_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setStart_date(String param) {
/* 112 */     this.localStart_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getEnd_date() {
/* 131 */     return this.localEnd_date;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setEnd_date(String param) {
/* 142 */     this.localEnd_date = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getFlags() {
/* 161 */     return this.localFlags;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setFlags(int param) {
/* 172 */     this.localFlags = param;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/* 192 */     ADBDataSource aDBDataSource = 
/* 193 */       new ADBDataSource(this, MY_QNAME);
/* 194 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, MY_QNAME);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/* 201 */     serialize(parentQName, xmlWriter, false);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/* 212 */     String prefix = null;
/* 213 */     String namespace = null;
/*     */ 
/*     */     
/* 216 */     prefix = parentQName.getPrefix();
/* 217 */     namespace = parentQName.getNamespaceURI();
/* 218 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*     */     
/* 220 */     if (serializeType) {
/*     */ 
/*     */       
/* 223 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/* 224 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/* 225 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 226 */             String.valueOf(namespacePrefix) + ":GetTransRangeByDate", 
/* 227 */             xmlWriter);
/*     */       } else {
/* 229 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/* 230 */             "GetTransRangeByDate", 
/* 231 */             xmlWriter);
/*     */       } 
/*     */     } 
/*     */ 
/*     */ 
/*     */     
/* 237 */     namespace = "";
/* 238 */     writeStartElement(null, namespace, "access_key", xmlWriter);
/*     */     
/* 240 */     if (this.localAccess_key == Integer.MIN_VALUE) {
/*     */       
/* 242 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 245 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAccess_key));
/*     */     } 
/*     */     
/* 248 */     xmlWriter.writeEndElement();
/*     */     
/* 250 */     namespace = "";
/* 251 */     writeStartElement(null, namespace, "type", xmlWriter);
/*     */ 
/*     */     
/* 254 */     if (this.localType == null) {
/*     */ 
/*     */       
/* 257 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 262 */       xmlWriter.writeCharacters(this.localType);
/*     */     } 
/*     */ 
/*     */     
/* 266 */     xmlWriter.writeEndElement();
/*     */     
/* 268 */     namespace = "";
/* 269 */     writeStartElement(null, namespace, "start_date", xmlWriter);
/*     */ 
/*     */     
/* 272 */     if (this.localStart_date == null) {
/*     */ 
/*     */       
/* 275 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 280 */       xmlWriter.writeCharacters(this.localStart_date);
/*     */     } 
/*     */ 
/*     */     
/* 284 */     xmlWriter.writeEndElement();
/*     */     
/* 286 */     namespace = "";
/* 287 */     writeStartElement(null, namespace, "end_date", xmlWriter);
/*     */ 
/*     */     
/* 290 */     if (this.localEnd_date == null) {
/*     */ 
/*     */       
/* 293 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     
/*     */     }
/*     */     else {
/*     */       
/* 298 */       xmlWriter.writeCharacters(this.localEnd_date);
/*     */     } 
/*     */ 
/*     */     
/* 302 */     xmlWriter.writeEndElement();
/*     */     
/* 304 */     namespace = "";
/* 305 */     writeStartElement(null, namespace, "flags", xmlWriter);
/*     */     
/* 307 */     if (this.localFlags == Integer.MIN_VALUE) {
/*     */       
/* 309 */       writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
/*     */     } else {
/*     */       
/* 312 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localFlags));
/*     */     } 
/*     */     
/* 315 */     xmlWriter.writeEndElement();
/*     */     
/* 317 */     xmlWriter.writeEndElement();
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private static String generatePrefix(String namespace) {
/* 323 */     if (namespace.equals("urn:CSSoapService")) {
/* 324 */       return "ns1";
/*     */     }
/* 326 */     return BeanUtil.getUniquePrefix();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 334 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/* 335 */     if (writerPrefix != null) {
/* 336 */       xmlWriter.writeStartElement(namespace, localPart);
/*     */     } else {
/* 338 */       if (namespace.length() == 0) {
/* 339 */         prefix = "";
/* 340 */       } else if (prefix == null) {
/* 341 */         prefix = generatePrefix(namespace);
/*     */       } 
/*     */       
/* 344 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/* 345 */       xmlWriter.writeNamespace(prefix, namespace);
/* 346 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 355 */     if (xmlWriter.getPrefix(namespace) == null) {
/* 356 */       xmlWriter.writeNamespace(prefix, namespace);
/* 357 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 359 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 367 */     if (namespace.equals("")) {
/* 368 */       xmlWriter.writeAttribute(attName, attValue);
/*     */     } else {
/* 370 */       registerPrefix(xmlWriter, namespace);
/* 371 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 382 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/* 383 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/* 384 */     if (attributePrefix == null) {
/* 385 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*     */     }
/*     */     
/* 388 */     if (attributePrefix.trim().length() > 0) {
/* 389 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*     */     } else {
/* 391 */       attributeValue = qname.getLocalPart();
/*     */     } 
/*     */     
/* 394 */     if (namespace.equals("")) {
/* 395 */       xmlWriter.writeAttribute(attName, attributeValue);
/*     */     } else {
/* 397 */       registerPrefix(xmlWriter, namespace);
/* 398 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 407 */     String namespaceURI = qname.getNamespaceURI();
/* 408 */     if (namespaceURI != null) {
/* 409 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/* 410 */       if (prefix == null) {
/* 411 */         prefix = generatePrefix(namespaceURI);
/* 412 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/* 413 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*     */       } 
/*     */       
/* 416 */       if (prefix.trim().length() > 0) {
/* 417 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*     */       } else {
/*     */         
/* 420 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */       } 
/*     */     } else {
/*     */       
/* 424 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/* 431 */     if (qnames != null) {
/*     */ 
/*     */       
/* 434 */       StringBuffer stringToWrite = new StringBuffer();
/* 435 */       String namespaceURI = null;
/* 436 */       String prefix = null;
/*     */       
/* 438 */       for (int i = 0; i < qnames.length; i++) {
/* 439 */         if (i > 0) {
/* 440 */           stringToWrite.append(" ");
/*     */         }
/* 442 */         namespaceURI = qnames[i].getNamespaceURI();
/* 443 */         if (namespaceURI != null) {
/* 444 */           prefix = xmlWriter.getPrefix(namespaceURI);
/* 445 */           if (prefix == null || prefix.length() == 0) {
/* 446 */             prefix = generatePrefix(namespaceURI);
/* 447 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/* 448 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*     */           } 
/*     */           
/* 451 */           if (prefix.trim().length() > 0) {
/* 452 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*     */           } else {
/* 454 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */           } 
/*     */         } else {
/* 457 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*     */         } 
/*     */       } 
/* 460 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/* 470 */     String prefix = xmlWriter.getPrefix(namespace);
/* 471 */     if (prefix == null) {
/* 472 */       prefix = generatePrefix(namespace);
/* 473 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*     */       while (true) {
/* 475 */         String uri = nsContext.getNamespaceURI(prefix);
/* 476 */         if (uri == null || uri.length() == 0) {
/*     */           break;
/*     */         }
/* 479 */         prefix = BeanUtil.getUniquePrefix();
/*     */       } 
/* 481 */       xmlWriter.writeNamespace(prefix, namespace);
/* 482 */       xmlWriter.setPrefix(prefix, namespace);
/*     */     } 
/* 484 */     return prefix;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/* 498 */     ArrayList<QName> elementList = new ArrayList();
/* 499 */     ArrayList attribList = new ArrayList();
/*     */ 
/*     */     
/* 502 */     elementList.add(new QName("", 
/* 503 */           "access_key"));
/*     */     
/* 505 */     elementList.add(
/* 506 */         ConverterUtil.convertToString(this.localAccess_key));
/*     */     
/* 508 */     elementList.add(new QName("", 
/* 509 */           "type"));
/*     */     
/* 511 */     elementList.add((this.localType == null) ? null : 
/* 512 */         ConverterUtil.convertToString(this.localType));
/*     */     
/* 514 */     elementList.add(new QName("", 
/* 515 */           "start_date"));
/*     */     
/* 517 */     elementList.add((this.localStart_date == null) ? null : 
/* 518 */         ConverterUtil.convertToString(this.localStart_date));
/*     */     
/* 520 */     elementList.add(new QName("", 
/* 521 */           "end_date"));
/*     */     
/* 523 */     elementList.add((this.localEnd_date == null) ? null : 
/* 524 */         ConverterUtil.convertToString(this.localEnd_date));
/*     */     
/* 526 */     elementList.add(new QName("", 
/* 527 */           "flags"));
/*     */     
/* 529 */     elementList.add(
/* 530 */         ConverterUtil.convertToString(this.localFlags));
/*     */ 
/*     */     
/* 533 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static class Factory
/*     */   {
/*     */     public static GetTransRangeByDate parse(XMLStreamReader reader) throws Exception {
/* 557 */       GetTransRangeByDate object = 
/* 558 */         new GetTransRangeByDate();
/*     */ 
/*     */       
/* 561 */       String nillableValue = null;
/* 562 */       String prefix = "";
/* 563 */       String namespaceuri = "";
/*     */       
/*     */       try {
/* 566 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 567 */           reader.next();
/*     */         }
/*     */         
/* 570 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 571 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 572 */               "type");
/* 573 */           if (fullTypeName != null) {
/* 574 */             String nsPrefix = null;
/* 575 */             if (fullTypeName.indexOf(":") > -1) {
/* 576 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*     */             }
/* 578 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*     */             
/* 580 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*     */             
/* 582 */             if (!"GetTransRangeByDate".equals(type)) {
/*     */               
/* 584 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 585 */               return (GetTransRangeByDate)ExtensionMapper.getTypeObject(
/* 586 */                   nsUri, type, reader);
/*     */             } 
/*     */           } 
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 600 */         Vector handledAttributes = new Vector();
/*     */ 
/*     */ 
/*     */ 
/*     */         
/* 605 */         reader.next();
/*     */ 
/*     */         
/* 608 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 610 */         if (reader.isStartElement() && (new QName("", "access_key")).equals(reader.getName())) {
/*     */           
/* 612 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 613 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 616 */             String content = reader.getElementText();
/*     */             
/* 618 */             object.setAccess_key(
/* 619 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 624 */             object.setAccess_key(-2147483648);
/*     */             
/* 626 */             reader.getElementText();
/*     */           } 
/*     */           
/* 629 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 635 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 639 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 641 */         if (reader.isStartElement() && (new QName("", "type")).equals(reader.getName())) {
/*     */           
/* 643 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 644 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 647 */             String content = reader.getElementText();
/*     */             
/* 649 */             object.setType(
/* 650 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 655 */             reader.getElementText();
/*     */           } 
/*     */           
/* 658 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 664 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 668 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 670 */         if (reader.isStartElement() && (new QName("", "start_date")).equals(reader.getName())) {
/*     */           
/* 672 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 673 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 676 */             String content = reader.getElementText();
/*     */             
/* 678 */             object.setStart_date(
/* 679 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 684 */             reader.getElementText();
/*     */           } 
/*     */           
/* 687 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 693 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 697 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 699 */         if (reader.isStartElement() && (new QName("", "end_date")).equals(reader.getName())) {
/*     */           
/* 701 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 702 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 705 */             String content = reader.getElementText();
/*     */             
/* 707 */             object.setEnd_date(
/* 708 */                 ConverterUtil.convertToString(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 713 */             reader.getElementText();
/*     */           } 
/*     */           
/* 716 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 722 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */ 
/*     */         
/* 726 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*     */         
/* 728 */         if (reader.isStartElement() && (new QName("", "flags")).equals(reader.getName())) {
/*     */           
/* 730 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 731 */           if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
/*     */ 
/*     */             
/* 734 */             String content = reader.getElementText();
/*     */             
/* 736 */             object.setFlags(
/* 737 */                 ConverterUtil.convertToInt(content));
/*     */           
/*     */           }
/*     */           else {
/*     */             
/* 742 */             object.setFlags(-2147483648);
/*     */             
/* 744 */             reader.getElementText();
/*     */           } 
/*     */           
/* 747 */           reader.next();
/*     */         
/*     */         }
/*     */         else {
/*     */ 
/*     */           
/* 753 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         } 
/*     */         
/* 756 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 757 */           reader.next();
/*     */         }
/* 759 */         if (reader.isStartElement())
/*     */         {
/* 761 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*     */         
/*     */         }
/*     */       
/*     */       }
/* 766 */       catch (XMLStreamException e) {
/* 767 */         throw new Exception(e);
/*     */       } 
/*     */       
/* 770 */       return object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\GetTransRangeByDate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */