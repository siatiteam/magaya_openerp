/*      */ package cssoapservice;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.Vector;
/*      */ import javax.xml.namespace.NamespaceContext;
/*      */ import javax.xml.namespace.QName;
/*      */ import javax.xml.stream.XMLStreamException;
/*      */ import javax.xml.stream.XMLStreamReader;
/*      */ import javax.xml.stream.XMLStreamWriter;
/*      */ import org.apache.axiom.om.OMDataSource;
/*      */ import org.apache.axiom.om.OMElement;
/*      */ import org.apache.axiom.om.OMFactory;
/*      */ import org.apache.axis2.databinding.ADBBean;
/*      */ import org.apache.axis2.databinding.ADBDataSource;
/*      */ import org.apache.axis2.databinding.ADBException;
/*      */ import org.apache.axis2.databinding.types.UnsignedByte;
/*      */ import org.apache.axis2.databinding.utils.BeanUtil;
/*      */ import org.apache.axis2.databinding.utils.ConverterUtil;
/*      */ import org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl;
/*      */ 
/*      */ public class Contact_info2 implements ADBBean {
/*      */   protected int localId;
/*      */   protected String localName;
/*      */   protected String localEmail;
/*      */   protected String localWeb_site;
/*      */   protected String localWan_ip;
/*      */   protected String localLan_ip;
/*      */   protected short localPort;
/*      */   protected UnsignedByte localConnect_type;
/*      */   protected UnsignedByte localNet_version;
/*      */   protected int localAvailability;
/*      */   protected int localVersion;
/*      */   protected short localLoc_lic;
/*      */   protected short localRem_lic;
/*      */   protected int localServices;
/*      */   protected short localLan_port;
/*      */   protected short localWms_lic;
/*      */   
/*      */   public int getId() {
/*   40 */     return this.localId;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setId(int param) {
/*   51 */     this.localId = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getName() {
/*   70 */     return this.localName;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setName(String param) {
/*   81 */     this.localName = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getEmail() {
/*  100 */     return this.localEmail;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setEmail(String param) {
/*  111 */     this.localEmail = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getWeb_site() {
/*  130 */     return this.localWeb_site;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setWeb_site(String param) {
/*  141 */     this.localWeb_site = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getWan_ip() {
/*  160 */     return this.localWan_ip;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setWan_ip(String param) {
/*  171 */     this.localWan_ip = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public String getLan_ip() {
/*  190 */     return this.localLan_ip;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLan_ip(String param) {
/*  201 */     this.localLan_ip = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getPort() {
/*  220 */     return this.localPort;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setPort(short param) {
/*  231 */     this.localPort = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public UnsignedByte getConnect_type() {
/*  250 */     return this.localConnect_type;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setConnect_type(UnsignedByte param) {
/*  261 */     this.localConnect_type = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public UnsignedByte getNet_version() {
/*  280 */     return this.localNet_version;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setNet_version(UnsignedByte param) {
/*  291 */     this.localNet_version = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getAvailability() {
/*  310 */     return this.localAvailability;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setAvailability(int param) {
/*  321 */     this.localAvailability = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getVersion() {
/*  340 */     return this.localVersion;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setVersion(int param) {
/*  351 */     this.localVersion = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getLoc_lic() {
/*  370 */     return this.localLoc_lic;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLoc_lic(short param) {
/*  381 */     this.localLoc_lic = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getRem_lic() {
/*  400 */     return this.localRem_lic;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setRem_lic(short param) {
/*  411 */     this.localRem_lic = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public int getServices() {
/*  430 */     return this.localServices;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setServices(int param) {
/*  441 */     this.localServices = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getLan_port() {
/*  460 */     return this.localLan_port;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setLan_port(short param) {
/*  471 */     this.localLan_port = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public short getWms_lic() {
/*  490 */     return this.localWms_lic;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void setWms_lic(short param) {
/*  501 */     this.localWms_lic = param;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public OMElement getOMElement(QName parentQName, OMFactory factory) throws ADBException {
/*  521 */     ADBDataSource aDBDataSource = 
/*  522 */       new ADBDataSource(this, parentQName);
/*  523 */     return (OMElement)factory.createOMElement((OMDataSource)aDBDataSource, parentQName);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter) throws XMLStreamException, ADBException {
/*  530 */     serialize(parentQName, xmlWriter, false);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public void serialize(QName parentQName, XMLStreamWriter xmlWriter, boolean serializeType) throws XMLStreamException, ADBException {
/*  541 */     String prefix = null;
/*  542 */     String namespace = null;
/*      */ 
/*      */     
/*  545 */     prefix = parentQName.getPrefix();
/*  546 */     namespace = parentQName.getNamespaceURI();
/*  547 */     writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
/*      */     
/*  549 */     if (serializeType) {
/*      */ 
/*      */       
/*  552 */       String namespacePrefix = registerPrefix(xmlWriter, "urn:CSSoapService");
/*  553 */       if (namespacePrefix != null && namespacePrefix.trim().length() > 0) {
/*  554 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  555 */             String.valueOf(namespacePrefix) + ":contact_info2", 
/*  556 */             xmlWriter);
/*      */       } else {
/*  558 */         writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", 
/*  559 */             "contact_info2", 
/*  560 */             xmlWriter);
/*      */       } 
/*      */     } 
/*      */ 
/*      */ 
/*      */     
/*  566 */     namespace = "urn:CSSoapService";
/*  567 */     writeStartElement(null, namespace, "id", xmlWriter);
/*      */     
/*  569 */     if (this.localId == Integer.MIN_VALUE)
/*      */     {
/*  571 */       throw new ADBException("id cannot be null!!");
/*      */     }
/*      */     
/*  574 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localId));
/*      */ 
/*      */     
/*  577 */     xmlWriter.writeEndElement();
/*      */     
/*  579 */     namespace = "urn:CSSoapService";
/*  580 */     writeStartElement(null, namespace, "name", xmlWriter);
/*      */ 
/*      */     
/*  583 */     if (this.localName == null)
/*      */     {
/*      */       
/*  586 */       throw new ADBException("name cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  591 */     xmlWriter.writeCharacters(this.localName);
/*      */ 
/*      */ 
/*      */     
/*  595 */     xmlWriter.writeEndElement();
/*      */     
/*  597 */     namespace = "urn:CSSoapService";
/*  598 */     writeStartElement(null, namespace, "email", xmlWriter);
/*      */ 
/*      */     
/*  601 */     if (this.localEmail == null)
/*      */     {
/*      */       
/*  604 */       throw new ADBException("email cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  609 */     xmlWriter.writeCharacters(this.localEmail);
/*      */ 
/*      */ 
/*      */     
/*  613 */     xmlWriter.writeEndElement();
/*      */     
/*  615 */     namespace = "urn:CSSoapService";
/*  616 */     writeStartElement(null, namespace, "web_site", xmlWriter);
/*      */ 
/*      */     
/*  619 */     if (this.localWeb_site == null)
/*      */     {
/*      */       
/*  622 */       throw new ADBException("web_site cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  627 */     xmlWriter.writeCharacters(this.localWeb_site);
/*      */ 
/*      */ 
/*      */     
/*  631 */     xmlWriter.writeEndElement();
/*      */     
/*  633 */     namespace = "urn:CSSoapService";
/*  634 */     writeStartElement(null, namespace, "wan_ip", xmlWriter);
/*      */ 
/*      */     
/*  637 */     if (this.localWan_ip == null)
/*      */     {
/*      */       
/*  640 */       throw new ADBException("wan_ip cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  645 */     xmlWriter.writeCharacters(this.localWan_ip);
/*      */ 
/*      */ 
/*      */     
/*  649 */     xmlWriter.writeEndElement();
/*      */     
/*  651 */     namespace = "urn:CSSoapService";
/*  652 */     writeStartElement(null, namespace, "lan_ip", xmlWriter);
/*      */ 
/*      */     
/*  655 */     if (this.localLan_ip == null)
/*      */     {
/*      */       
/*  658 */       throw new ADBException("lan_ip cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  663 */     xmlWriter.writeCharacters(this.localLan_ip);
/*      */ 
/*      */ 
/*      */     
/*  667 */     xmlWriter.writeEndElement();
/*      */     
/*  669 */     namespace = "urn:CSSoapService";
/*  670 */     writeStartElement(null, namespace, "port", xmlWriter);
/*      */     
/*  672 */     if (this.localPort == Short.MIN_VALUE)
/*      */     {
/*  674 */       throw new ADBException("port cannot be null!!");
/*      */     }
/*      */     
/*  677 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localPort));
/*      */ 
/*      */     
/*  680 */     xmlWriter.writeEndElement();
/*      */     
/*  682 */     namespace = "urn:CSSoapService";
/*  683 */     writeStartElement(null, namespace, "connect_type", xmlWriter);
/*      */ 
/*      */     
/*  686 */     if (this.localConnect_type == null)
/*      */     {
/*      */       
/*  689 */       throw new ADBException("connect_type cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  694 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localConnect_type));
/*      */ 
/*      */ 
/*      */     
/*  698 */     xmlWriter.writeEndElement();
/*      */     
/*  700 */     namespace = "urn:CSSoapService";
/*  701 */     writeStartElement(null, namespace, "net_version", xmlWriter);
/*      */ 
/*      */     
/*  704 */     if (this.localNet_version == null)
/*      */     {
/*      */       
/*  707 */       throw new ADBException("net_version cannot be null!!");
/*      */     }
/*      */ 
/*      */ 
/*      */     
/*  712 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localNet_version));
/*      */ 
/*      */ 
/*      */     
/*  716 */     xmlWriter.writeEndElement();
/*      */     
/*  718 */     namespace = "urn:CSSoapService";
/*  719 */     writeStartElement(null, namespace, "availability", xmlWriter);
/*      */     
/*  721 */     if (this.localAvailability == Integer.MIN_VALUE)
/*      */     {
/*  723 */       throw new ADBException("availability cannot be null!!");
/*      */     }
/*      */     
/*  726 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localAvailability));
/*      */ 
/*      */     
/*  729 */     xmlWriter.writeEndElement();
/*      */     
/*  731 */     namespace = "urn:CSSoapService";
/*  732 */     writeStartElement(null, namespace, "version", xmlWriter);
/*      */     
/*  734 */     if (this.localVersion == Integer.MIN_VALUE)
/*      */     {
/*  736 */       throw new ADBException("version cannot be null!!");
/*      */     }
/*      */     
/*  739 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localVersion));
/*      */ 
/*      */     
/*  742 */     xmlWriter.writeEndElement();
/*      */     
/*  744 */     namespace = "urn:CSSoapService";
/*  745 */     writeStartElement(null, namespace, "loc_lic", xmlWriter);
/*      */     
/*  747 */     if (this.localLoc_lic == Short.MIN_VALUE)
/*      */     {
/*  749 */       throw new ADBException("loc_lic cannot be null!!");
/*      */     }
/*      */     
/*  752 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localLoc_lic));
/*      */ 
/*      */     
/*  755 */     xmlWriter.writeEndElement();
/*      */     
/*  757 */     namespace = "urn:CSSoapService";
/*  758 */     writeStartElement(null, namespace, "rem_lic", xmlWriter);
/*      */     
/*  760 */     if (this.localRem_lic == Short.MIN_VALUE)
/*      */     {
/*  762 */       throw new ADBException("rem_lic cannot be null!!");
/*      */     }
/*      */     
/*  765 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localRem_lic));
/*      */ 
/*      */     
/*  768 */     xmlWriter.writeEndElement();
/*      */     
/*  770 */     namespace = "urn:CSSoapService";
/*  771 */     writeStartElement(null, namespace, "services", xmlWriter);
/*      */     
/*  773 */     if (this.localServices == Integer.MIN_VALUE)
/*      */     {
/*  775 */       throw new ADBException("services cannot be null!!");
/*      */     }
/*      */     
/*  778 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localServices));
/*      */ 
/*      */     
/*  781 */     xmlWriter.writeEndElement();
/*      */     
/*  783 */     namespace = "urn:CSSoapService";
/*  784 */     writeStartElement(null, namespace, "lan_port", xmlWriter);
/*      */     
/*  786 */     if (this.localLan_port == Short.MIN_VALUE)
/*      */     {
/*  788 */       throw new ADBException("lan_port cannot be null!!");
/*      */     }
/*      */     
/*  791 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localLan_port));
/*      */ 
/*      */     
/*  794 */     xmlWriter.writeEndElement();
/*      */     
/*  796 */     namespace = "urn:CSSoapService";
/*  797 */     writeStartElement(null, namespace, "wms_lic", xmlWriter);
/*      */     
/*  799 */     if (this.localWms_lic == Short.MIN_VALUE)
/*      */     {
/*  801 */       throw new ADBException("wms_lic cannot be null!!");
/*      */     }
/*      */     
/*  804 */     xmlWriter.writeCharacters(ConverterUtil.convertToString(this.localWms_lic));
/*      */ 
/*      */     
/*  807 */     xmlWriter.writeEndElement();
/*      */     
/*  809 */     xmlWriter.writeEndElement();
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private static String generatePrefix(String namespace) {
/*  815 */     if (namespace.equals("urn:CSSoapService")) {
/*  816 */       return "ns1";
/*      */     }
/*  818 */     return BeanUtil.getUniquePrefix();
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeStartElement(String prefix, String namespace, String localPart, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  826 */     String writerPrefix = xmlWriter.getPrefix(namespace);
/*  827 */     if (writerPrefix != null) {
/*  828 */       xmlWriter.writeStartElement(namespace, localPart);
/*      */     } else {
/*  830 */       if (namespace.length() == 0) {
/*  831 */         prefix = "";
/*  832 */       } else if (prefix == null) {
/*  833 */         prefix = generatePrefix(namespace);
/*      */       } 
/*      */       
/*  836 */       xmlWriter.writeStartElement(prefix, localPart, namespace);
/*  837 */       xmlWriter.writeNamespace(prefix, namespace);
/*  838 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String prefix, String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  847 */     if (xmlWriter.getPrefix(namespace) == null) {
/*  848 */       xmlWriter.writeNamespace(prefix, namespace);
/*  849 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  851 */     xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeAttribute(String namespace, String attName, String attValue, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  859 */     if (namespace.equals("")) {
/*  860 */       xmlWriter.writeAttribute(attName, attValue);
/*      */     } else {
/*  862 */       registerPrefix(xmlWriter, namespace);
/*  863 */       xmlWriter.writeAttribute(namespace, attName, attValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNameAttribute(String namespace, String attName, QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  874 */     String attributeValue, attributeNamespace = qname.getNamespaceURI();
/*  875 */     String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
/*  876 */     if (attributePrefix == null) {
/*  877 */       attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
/*      */     }
/*      */     
/*  880 */     if (attributePrefix.trim().length() > 0) {
/*  881 */       attributeValue = String.valueOf(attributePrefix) + ":" + qname.getLocalPart();
/*      */     } else {
/*  883 */       attributeValue = qname.getLocalPart();
/*      */     } 
/*      */     
/*  886 */     if (namespace.equals("")) {
/*  887 */       xmlWriter.writeAttribute(attName, attributeValue);
/*      */     } else {
/*  889 */       registerPrefix(xmlWriter, namespace);
/*  890 */       xmlWriter.writeAttribute(namespace, attName, attributeValue);
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQName(QName qname, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  899 */     String namespaceURI = qname.getNamespaceURI();
/*  900 */     if (namespaceURI != null) {
/*  901 */       String prefix = xmlWriter.getPrefix(namespaceURI);
/*  902 */       if (prefix == null) {
/*  903 */         prefix = generatePrefix(namespaceURI);
/*  904 */         xmlWriter.writeNamespace(prefix, namespaceURI);
/*  905 */         xmlWriter.setPrefix(prefix, namespaceURI);
/*      */       } 
/*      */       
/*  908 */       if (prefix.trim().length() > 0) {
/*  909 */         xmlWriter.writeCharacters(String.valueOf(prefix) + ":" + ConverterUtil.convertToString(qname));
/*      */       } else {
/*      */         
/*  912 */         xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */       } 
/*      */     } else {
/*      */       
/*  916 */       xmlWriter.writeCharacters(ConverterUtil.convertToString(qname));
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */   
/*      */   private void writeQNames(QName[] qnames, XMLStreamWriter xmlWriter) throws XMLStreamException {
/*  923 */     if (qnames != null) {
/*      */ 
/*      */       
/*  926 */       StringBuffer stringToWrite = new StringBuffer();
/*  927 */       String namespaceURI = null;
/*  928 */       String prefix = null;
/*      */       
/*  930 */       for (int i = 0; i < qnames.length; i++) {
/*  931 */         if (i > 0) {
/*  932 */           stringToWrite.append(" ");
/*      */         }
/*  934 */         namespaceURI = qnames[i].getNamespaceURI();
/*  935 */         if (namespaceURI != null) {
/*  936 */           prefix = xmlWriter.getPrefix(namespaceURI);
/*  937 */           if (prefix == null || prefix.length() == 0) {
/*  938 */             prefix = generatePrefix(namespaceURI);
/*  939 */             xmlWriter.writeNamespace(prefix, namespaceURI);
/*  940 */             xmlWriter.setPrefix(prefix, namespaceURI);
/*      */           } 
/*      */           
/*  943 */           if (prefix.trim().length() > 0) {
/*  944 */             stringToWrite.append(prefix).append(":").append(ConverterUtil.convertToString(qnames[i]));
/*      */           } else {
/*  946 */             stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */           } 
/*      */         } else {
/*  949 */           stringToWrite.append(ConverterUtil.convertToString(qnames[i]));
/*      */         } 
/*      */       } 
/*  952 */       xmlWriter.writeCharacters(stringToWrite.toString());
/*      */     } 
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   private String registerPrefix(XMLStreamWriter xmlWriter, String namespace) throws XMLStreamException {
/*  962 */     String prefix = xmlWriter.getPrefix(namespace);
/*  963 */     if (prefix == null) {
/*  964 */       prefix = generatePrefix(namespace);
/*  965 */       NamespaceContext nsContext = xmlWriter.getNamespaceContext();
/*      */       while (true) {
/*  967 */         String uri = nsContext.getNamespaceURI(prefix);
/*  968 */         if (uri == null || uri.length() == 0) {
/*      */           break;
/*      */         }
/*  971 */         prefix = BeanUtil.getUniquePrefix();
/*      */       } 
/*  973 */       xmlWriter.writeNamespace(prefix, namespace);
/*  974 */       xmlWriter.setPrefix(prefix, namespace);
/*      */     } 
/*  976 */     return prefix;
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public XMLStreamReader getPullParser(QName qName) throws ADBException {
/*  990 */     ArrayList<QName> elementList = new ArrayList();
/*  991 */     ArrayList attribList = new ArrayList();
/*      */ 
/*      */     
/*  994 */     elementList.add(new QName("urn:CSSoapService", 
/*  995 */           "id"));
/*      */     
/*  997 */     elementList.add(
/*  998 */         ConverterUtil.convertToString(this.localId));
/*      */     
/* 1000 */     elementList.add(new QName("urn:CSSoapService", 
/* 1001 */           "name"));
/*      */     
/* 1003 */     if (this.localName != null) {
/* 1004 */       elementList.add(ConverterUtil.convertToString(this.localName));
/*      */     } else {
/* 1006 */       throw new ADBException("name cannot be null!!");
/*      */     } 
/*      */     
/* 1009 */     elementList.add(new QName("urn:CSSoapService", 
/* 1010 */           "email"));
/*      */     
/* 1012 */     if (this.localEmail != null) {
/* 1013 */       elementList.add(ConverterUtil.convertToString(this.localEmail));
/*      */     } else {
/* 1015 */       throw new ADBException("email cannot be null!!");
/*      */     } 
/*      */     
/* 1018 */     elementList.add(new QName("urn:CSSoapService", 
/* 1019 */           "web_site"));
/*      */     
/* 1021 */     if (this.localWeb_site != null) {
/* 1022 */       elementList.add(ConverterUtil.convertToString(this.localWeb_site));
/*      */     } else {
/* 1024 */       throw new ADBException("web_site cannot be null!!");
/*      */     } 
/*      */     
/* 1027 */     elementList.add(new QName("urn:CSSoapService", 
/* 1028 */           "wan_ip"));
/*      */     
/* 1030 */     if (this.localWan_ip != null) {
/* 1031 */       elementList.add(ConverterUtil.convertToString(this.localWan_ip));
/*      */     } else {
/* 1033 */       throw new ADBException("wan_ip cannot be null!!");
/*      */     } 
/*      */     
/* 1036 */     elementList.add(new QName("urn:CSSoapService", 
/* 1037 */           "lan_ip"));
/*      */     
/* 1039 */     if (this.localLan_ip != null) {
/* 1040 */       elementList.add(ConverterUtil.convertToString(this.localLan_ip));
/*      */     } else {
/* 1042 */       throw new ADBException("lan_ip cannot be null!!");
/*      */     } 
/*      */     
/* 1045 */     elementList.add(new QName("urn:CSSoapService", 
/* 1046 */           "port"));
/*      */     
/* 1048 */     elementList.add(
/* 1049 */         ConverterUtil.convertToString(this.localPort));
/*      */     
/* 1051 */     elementList.add(new QName("urn:CSSoapService", 
/* 1052 */           "connect_type"));
/*      */     
/* 1054 */     if (this.localConnect_type != null) {
/* 1055 */       elementList.add(ConverterUtil.convertToString(this.localConnect_type));
/*      */     } else {
/* 1057 */       throw new ADBException("connect_type cannot be null!!");
/*      */     } 
/*      */     
/* 1060 */     elementList.add(new QName("urn:CSSoapService", 
/* 1061 */           "net_version"));
/*      */     
/* 1063 */     if (this.localNet_version != null) {
/* 1064 */       elementList.add(ConverterUtil.convertToString(this.localNet_version));
/*      */     } else {
/* 1066 */       throw new ADBException("net_version cannot be null!!");
/*      */     } 
/*      */     
/* 1069 */     elementList.add(new QName("urn:CSSoapService", 
/* 1070 */           "availability"));
/*      */     
/* 1072 */     elementList.add(
/* 1073 */         ConverterUtil.convertToString(this.localAvailability));
/*      */     
/* 1075 */     elementList.add(new QName("urn:CSSoapService", 
/* 1076 */           "version"));
/*      */     
/* 1078 */     elementList.add(
/* 1079 */         ConverterUtil.convertToString(this.localVersion));
/*      */     
/* 1081 */     elementList.add(new QName("urn:CSSoapService", 
/* 1082 */           "loc_lic"));
/*      */     
/* 1084 */     elementList.add(
/* 1085 */         ConverterUtil.convertToString(this.localLoc_lic));
/*      */     
/* 1087 */     elementList.add(new QName("urn:CSSoapService", 
/* 1088 */           "rem_lic"));
/*      */     
/* 1090 */     elementList.add(
/* 1091 */         ConverterUtil.convertToString(this.localRem_lic));
/*      */     
/* 1093 */     elementList.add(new QName("urn:CSSoapService", 
/* 1094 */           "services"));
/*      */     
/* 1096 */     elementList.add(
/* 1097 */         ConverterUtil.convertToString(this.localServices));
/*      */     
/* 1099 */     elementList.add(new QName("urn:CSSoapService", 
/* 1100 */           "lan_port"));
/*      */     
/* 1102 */     elementList.add(
/* 1103 */         ConverterUtil.convertToString(this.localLan_port));
/*      */     
/* 1105 */     elementList.add(new QName("urn:CSSoapService", 
/* 1106 */           "wms_lic"));
/*      */     
/* 1108 */     elementList.add(
/* 1109 */         ConverterUtil.convertToString(this.localWms_lic));
/*      */ 
/*      */     
/* 1112 */     return (XMLStreamReader)new ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
/*      */   }
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   
/*      */   public static class Factory
/*      */   {
/*      */     public static Contact_info2 parse(XMLStreamReader reader) throws Exception {
/* 1136 */       Contact_info2 object = 
/* 1137 */         new Contact_info2();
/*      */ 
/*      */       
/* 1140 */       String nillableValue = null;
/* 1141 */       String prefix = "";
/* 1142 */       String namespaceuri = "";
/*      */       
/*      */       try {
/* 1145 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1146 */           reader.next();
/*      */         }
/*      */         
/* 1149 */         if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
/* 1150 */           String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", 
/* 1151 */               "type");
/* 1152 */           if (fullTypeName != null) {
/* 1153 */             String nsPrefix = null;
/* 1154 */             if (fullTypeName.indexOf(":") > -1) {
/* 1155 */               nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
/*      */             }
/* 1157 */             nsPrefix = (nsPrefix == null) ? "" : nsPrefix;
/*      */             
/* 1159 */             String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
/*      */             
/* 1161 */             if (!"contact_info2".equals(type)) {
/*      */               
/* 1163 */               String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
/* 1164 */               return (Contact_info2)ExtensionMapper.getTypeObject(
/* 1165 */                   nsUri, type, reader);
/*      */             } 
/*      */           } 
/*      */         } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         
/* 1179 */         Vector handledAttributes = new Vector();
/*      */ 
/*      */ 
/*      */ 
/*      */         
/* 1184 */         reader.next();
/*      */ 
/*      */         
/* 1187 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1189 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "id")).equals(reader.getName())) {
/*      */           
/* 1191 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1192 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1193 */             throw new ADBException("The element: id  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1197 */           String content = reader.getElementText();
/*      */           
/* 1199 */           object.setId(
/* 1200 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1202 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1208 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1212 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1214 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "name")).equals(reader.getName())) {
/*      */           
/* 1216 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1217 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1218 */             throw new ADBException("The element: name  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1222 */           String content = reader.getElementText();
/*      */           
/* 1224 */           object.setName(
/* 1225 */               ConverterUtil.convertToString(content));
/*      */           
/* 1227 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1233 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1237 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1239 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "email")).equals(reader.getName())) {
/*      */           
/* 1241 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1242 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1243 */             throw new ADBException("The element: email  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1247 */           String content = reader.getElementText();
/*      */           
/* 1249 */           object.setEmail(
/* 1250 */               ConverterUtil.convertToString(content));
/*      */           
/* 1252 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1258 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1262 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1264 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "web_site")).equals(reader.getName())) {
/*      */           
/* 1266 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1267 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1268 */             throw new ADBException("The element: web_site  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1272 */           String content = reader.getElementText();
/*      */           
/* 1274 */           object.setWeb_site(
/* 1275 */               ConverterUtil.convertToString(content));
/*      */           
/* 1277 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1283 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1287 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1289 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "wan_ip")).equals(reader.getName())) {
/*      */           
/* 1291 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1292 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1293 */             throw new ADBException("The element: wan_ip  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1297 */           String content = reader.getElementText();
/*      */           
/* 1299 */           object.setWan_ip(
/* 1300 */               ConverterUtil.convertToString(content));
/*      */           
/* 1302 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1308 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1312 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1314 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "lan_ip")).equals(reader.getName())) {
/*      */           
/* 1316 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1317 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1318 */             throw new ADBException("The element: lan_ip  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1322 */           String content = reader.getElementText();
/*      */           
/* 1324 */           object.setLan_ip(
/* 1325 */               ConverterUtil.convertToString(content));
/*      */           
/* 1327 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1333 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1337 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1339 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "port")).equals(reader.getName())) {
/*      */           
/* 1341 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1342 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1343 */             throw new ADBException("The element: port  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1347 */           String content = reader.getElementText();
/*      */           
/* 1349 */           object.setPort(
/* 1350 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1352 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1358 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1362 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1364 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "connect_type")).equals(reader.getName())) {
/*      */           
/* 1366 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1367 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1368 */             throw new ADBException("The element: connect_type  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1372 */           String content = reader.getElementText();
/*      */           
/* 1374 */           object.setConnect_type(
/* 1375 */               ConverterUtil.convertToUnsignedByte(content));
/*      */           
/* 1377 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1383 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1387 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1389 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "net_version")).equals(reader.getName())) {
/*      */           
/* 1391 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1392 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1393 */             throw new ADBException("The element: net_version  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1397 */           String content = reader.getElementText();
/*      */           
/* 1399 */           object.setNet_version(
/* 1400 */               ConverterUtil.convertToUnsignedByte(content));
/*      */           
/* 1402 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1408 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1412 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1414 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "availability")).equals(reader.getName())) {
/*      */           
/* 1416 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1417 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1418 */             throw new ADBException("The element: availability  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1422 */           String content = reader.getElementText();
/*      */           
/* 1424 */           object.setAvailability(
/* 1425 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1427 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1433 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1437 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1439 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "version")).equals(reader.getName())) {
/*      */           
/* 1441 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1442 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1443 */             throw new ADBException("The element: version  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1447 */           String content = reader.getElementText();
/*      */           
/* 1449 */           object.setVersion(
/* 1450 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1452 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1458 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1462 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1464 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "loc_lic")).equals(reader.getName())) {
/*      */           
/* 1466 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1467 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1468 */             throw new ADBException("The element: loc_lic  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1472 */           String content = reader.getElementText();
/*      */           
/* 1474 */           object.setLoc_lic(
/* 1475 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1477 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1483 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1487 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1489 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "rem_lic")).equals(reader.getName())) {
/*      */           
/* 1491 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1492 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1493 */             throw new ADBException("The element: rem_lic  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1497 */           String content = reader.getElementText();
/*      */           
/* 1499 */           object.setRem_lic(
/* 1500 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1502 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1508 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1512 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1514 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "services")).equals(reader.getName())) {
/*      */           
/* 1516 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1517 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1518 */             throw new ADBException("The element: services  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1522 */           String content = reader.getElementText();
/*      */           
/* 1524 */           object.setServices(
/* 1525 */               ConverterUtil.convertToInt(content));
/*      */           
/* 1527 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1533 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1537 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1539 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "lan_port")).equals(reader.getName())) {
/*      */           
/* 1541 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1542 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1543 */             throw new ADBException("The element: lan_port  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1547 */           String content = reader.getElementText();
/*      */           
/* 1549 */           object.setLan_port(
/* 1550 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1552 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1558 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */ 
/*      */         
/* 1562 */         for (; !reader.isStartElement() && !reader.isEndElement(); reader.next());
/*      */         
/* 1564 */         if (reader.isStartElement() && (new QName("urn:CSSoapService", "wms_lic")).equals(reader.getName())) {
/*      */           
/* 1566 */           nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
/* 1567 */           if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
/* 1568 */             throw new ADBException("The element: wms_lic  cannot be null");
/*      */           }
/*      */ 
/*      */           
/* 1572 */           String content = reader.getElementText();
/*      */           
/* 1574 */           object.setWms_lic(
/* 1575 */               ConverterUtil.convertToShort(content));
/*      */           
/* 1577 */           reader.next();
/*      */         
/*      */         }
/*      */         else {
/*      */ 
/*      */           
/* 1583 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         } 
/*      */         
/* 1586 */         while (!reader.isStartElement() && !reader.isEndElement()) {
/* 1587 */           reader.next();
/*      */         }
/* 1589 */         if (reader.isStartElement())
/*      */         {
/* 1591 */           throw new ADBException("Unexpected subelement " + reader.getName());
/*      */         
/*      */         }
/*      */       
/*      */       }
/* 1596 */       catch (XMLStreamException e) {
/* 1597 */         throw new Exception(e);
/*      */       } 
/*      */       
/* 1600 */       return object;
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\epacheco\Documents\PROYECTOS\magaya_java\magaya_openerp.jar!\cssoapservice\Contact_info2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */